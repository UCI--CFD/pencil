/*
 * Discretized 2D heat equation stencil with non periodic boundary conditions
 * Adapted from Pochoir test bench
 *
 * Irshad Pananilath: irshad@csa.iisc.ernet.in
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#define NI 480L
#define NJ 480L
#define NK 480L
#define T   60L

const double PI = 3.14159265359;
const int    nh = 2;

// arrays
double A[2][NI+4][NJ+4][NK+4];
double B[NI+4][NJ+4][NK+4];

// timer
double mytime()
{
  struct timespec t;
  clock_gettime(CLOCK_REALTIME, &t);
  return (double)(t.tv_sec) + (double)(t.tv_nsec)*1.0e-9;
}


int main(int argc, char *argv[]) {
  long int i, j, k, t;
  double h  = 1.0 / NI;
  double w0 = 2.0/3.0, w1 = 1.0/3.0;
  double c1 =16.0* w0/90.0, c2 = -w0/90.0, cRhs = -12.0*h*h*w0/90.0;
  double time;

  // init
  #pragma omp parallel for
  for (i = 0; i < NI + 2*nh; i++) {
    for (j = 0; j < NJ + 2*nh; j++) {
      for (k = 0; k < NK + 2*nh; k++) {
        A[0][i][j][k] = 0.0;
        A[1][i][j][k] = 0.0;
        B[i][j][k]    = -12.0*PI*PI*sin(2*PI*(i-nh+0.5)*h)
                      * sin(2*PI*(j-nh+0.5)*h) * sin(2*PI*(k-nh+0.5)*h);
      }
    }
  }

#ifdef TIME
  time = mytime();
#endif

#pragma scop
  for (t = 0; t < T; t++) {
    for (i = 2; i < NI + 2; i++) {
      for (j = 2; j < NJ + 2; j++) {
        for (k = 2; k < NK + 2; k++) {
          A[(t+1)%2][i][j][k]
            = c1 * ( A[t%2][i+1][j][k] + A[t%2][i-1][j][k] + A[t%2][i][j+1][k]
                   + A[t%2][i][j-1][k] + A[t%2][i][j][k-1] + A[t%2][i][j][k+1]) 
            + c2 * ( A[t%2][i+2][j][k] + A[t%2][i-2][j][k] + A[t%2][i][j+2][k]
                   + A[t%2][i][j-2][k] + A[t%2][i][j][k-2] + A[t%2][i][j][k+2]) 
            + cRhs * B[i][j][k] + w1 * A[t%2][i][j][k];
        }
      }
    }
  }
#pragma endscop

#ifdef TIME
  time = mytime() - time;
  printf("Time: %10.8lf\n", time);
  // avg
  double avg = 0.0;
  for (i=nh; i<NI+nh; ++i)
    for (j=nh; j<NJ+nh; ++j)
      for (k=nh; k<NK+nh; ++k)
        avg += fabs(A[0][i][j][k]);
  avg = avg/NI/NJ/NK;
  printf("avg: %10.8lf\n", avg);
#endif

  return 0;
}

// icc -O3 -fp-model precise heat_1d_np.c -o op-heat-1d-np -lm
// /* @ begin PrimeTile (num_tiling_levels=1; first_depth=1; last_depth=-1;
// boundary_tiling_level=-1;) @*/
// /* @ begin PrimeRegTile (scalar_replacement=0; T1t3=8; T1t4=8; ) @*/
// /* @ end @*/
