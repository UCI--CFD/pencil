#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#define NI 480
#define NJ 480
#define NK 480
#define is 234256
#define js 484
#define ijk i*234256+j*484+k
#define T   60

const double PI = 3.14159265359;
const int    nh = 2;

double A[2][(NI+4)*(NJ+4)*(NK+4)];
double U[(NI+4)*(NJ+4)*(NK+4)], V[(NI+4)*(NJ+4)*(NK+4)], W[(NI+4)*(NJ+4)*(NK+4)];

// timer
double mytime()
{
  struct timespec t;
  clock_gettime(CLOCK_REALTIME, &t);
  return (double)(t.tv_sec) + (double)(t.tv_nsec)*1.0e-9;
}

int main(int argc, char *argv[]) {
  int i, j, k, t;
  double h  = 1.0 / NI, q2h = 0.5/h, dt = 0.0001;
  double time;

  //ALLOC

  // init
  #pragma omp parallel for
  for (i = 0; i < NI + 2*nh; i++) {
    for (j = 0; j < NJ + 2*nh; j++) {
      for (k = 0; k < NK + 2*nh; k++) {
        A[1][ijk] = 0.0;
        A[0][ijk] = -12.0*PI*PI*sin(2*PI*(i-nh+0.5)*h) * sin(2*PI*(j-nh+0.5)*h) * sin(2*PI*(k-nh+0.5)*h);
        U[ijk] = 0.01;
        V[ijk] = 0.01;
        W[ijk] = 0.01;
      }
    }
  }

#ifdef TIME
  time = mytime();
#endif

#pragma scop
  for (t = 0; t < T; t++) {
    for (i = 2; i < NI + 2; i++) {
      for (j = 2; j < NJ + 2; j++) {
        for (k = 2; k < NK + 2; k++) {
          A[(t+1)%2][ijk] = A[t%2][ijk]
            - dt * ( U[ijk] * (U[ijk] > 0 ? 3*A[t%2][ijk] - 4*A[t%2][ijk-is] + A[t%2][ijk-2*is] : -3*A[t%2][ijk] + 4*A[t%2][ijk+is] - A[t%2][ijk+2*is]) * q2h
                   + V[ijk] * (V[ijk] > 0 ? 3*A[t%2][ijk] - 4*A[t%2][ijk-js] + A[t%2][ijk-2*js] : -3*A[t%2][ijk] + 4*A[t%2][ijk+js] - A[t%2][ijk+2*js]) * q2h
                   + W[ijk] * (W[ijk] > 0 ? 3*A[t%2][ijk] - 4*A[t%2][ijk-1] + A[t%2][ijk-2] : -3*A[t%2][ijk] + 4*A[t%2][ijk+1] - A[t%2][ijk+2]) * q2h);
        }
      }
    }
  }
#pragma endscop

#ifdef TIME
  time = mytime() - time;
  printf("Time: %10.8lf\n", time);
  // avg
  double avg = 0.0;
  for (i=nh; i<NI+nh; ++i)
    for (j=nh; j<NJ+nh; ++j)
      for (k=nh; k<NK+nh; ++k)
        avg += fabs(A[0][ijk]);
  avg = avg/NI/NJ/NK;
  printf("avg: %10.8lf\n", avg);
#endif

  //FREE

  return 0;
}
