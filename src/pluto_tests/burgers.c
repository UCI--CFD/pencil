#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#define NI 480
#define NJ 480
#define NK 480
#define is 232324
#define js 482
#define ijk i*232324+j*482+k
#define T   60

const double PI = 3.14159265359;
const int    nh = 1;

double u[2][(NI+2)*(NJ+2)*(NK+2)], v[2][(NI+2)*(NJ+2)*(NK+2)], w[2][(NI+2)*(NJ+2)*(NK+2)];

// timer
double mytime()
{
  struct timespec t;
  clock_gettime(CLOCK_REALTIME, &t);
  return (double)(t.tv_sec) + (double)(t.tv_nsec)*1.0e-9;
}

int main(int argc, char *argv[]) {
  int i, j, k, t;
  double h  = 1.0 / NI, q4h = 0.5/h, dt = 0.0001, nuqh2 = 0.1;
  double time;

  //ALLOC

  // init
  #pragma omp parallel for
  for (i = 0; i < NI + 2*nh; i++) {
    for (j = 0; j < NJ + 2*nh; j++) {
      for (k = 0; k < NK + 2*nh; k++) {
        u[0][ijk] = 0.01*sin(2*PI*(i-nh+0.5)*h);
        v[0][ijk] = 0.02*cos(2*PI*(j-nh+0.5)*h);
        w[0][ijk] = 0.03*sin(2*PI*(k-nh+0.5)*h);
        u[1][ijk] = 0.0;
        v[1][ijk] = 0.0;
        w[1][ijk] = 0.0;
      }
    }
  }

#ifdef TIME
  time = mytime();
#endif

#pragma scop
  for (t = 0; t < T; t++) {
    for (i = 1; i < NI + 1; i++) {
      for (j = 1; j < NJ + 1; j++) {
        for (k = 1; k < NK + 1; k++) {
          //
          u[(t+1)%2][ijk] 
            = ( ( 2*(u[t%2][ijk+is] + u[t%2][ijk-is]) + u[t%2][ijk+js] + u[t%2][ijk-js] + u[t%2][ijk+1] + u[t%2][ijk-1] - 8*u[t%2][ijk]
                + v[t%2][ijk+is] - v[t%2][ijk+is-js] - v[t%2][ijk] + v[t%2][ijk-js]
                + w[t%2][ijk+is] - w[t%2][ijk+is-1] - w[t%2][ijk] + w[t%2][ijk-1]) *nuqh2
              - ( (u[t%2][ijk] + u[t%2][ijk+is]) * (u[t%2][ijk] + u[t%2][ijk+is]) 
                - (u[t%2][ijk] + u[t%2][ijk-is]) * (u[t%2][ijk] + u[t%2][ijk-is])
                + (u[t%2][ijk] + u[t%2][ijk+js]) * (v[t%2][ijk] + v[t%2][ijk+is])
                - (u[t%2][ijk-js] + u[t%2][ijk]) * (v[t%2][ijk-js] + v[t%2][ijk+is-js])
                + (w[t%2][ijk] + w[t%2][ijk+is]) * (u[t%2][ijk] + u[t%2][ijk+1])
                - (w[t%2][ijk-1]  + w[t%2][ijk+is-1] ) * (u[t%2][ijk-1] + u[t%2][ijk])) * q4h
              ) * dt + u[t%2][ijk];
          //
          v[(t+1)%2][ijk] 
            = ( ( v[t%2][ijk+is] + v[t%2][ijk-is] + 2*(v[t%2][ijk+js] + v[t%2][ijk-js]) + v[t%2][ijk+1] + v[t%2][ijk-1] - 8*v[t%2][ijk]
                + u[t%2][ijk+js] - u[t%2][ijk-is+js] - u[t%2][ijk] + u[t%2][ijk-is]
                + w[t%2][ijk+js] - w[t%2][ijk+js-1] - w[t%2][ijk] + w[t%2][ijk-1]) * nuqh2
              - ( (u[t%2][ijk] + u[t%2][ijk+js]) * (v[t%2][ijk] + v[t%2][ijk+is])
                - (u[t%2][ijk-is] + u[t%2][ijk-is+js]) * (v[t%2][ijk-is] + v[t%2][ijk])
                + (v[t%2][ijk] + v[t%2][ijk+js]) * (v[t%2][ijk] + v[t%2][ijk+js]) 
                - (v[t%2][ijk] + v[t%2][ijk-js]) * (v[t%2][ijk] + v[t%2][ijk-js])
                + (v[t%2][ijk] + v[t%2][ijk+1] ) * (w[t%2][ijk] + w[t%2][ijk+js])
                - (v[t%2][ijk-1] + v[t%2][ijk]) * (w[t%2][ijk-1] + w[t%2][ijk+js-1])) * q4h
              ) * dt + v[t%2][ijk];
          //
          w[(t+1)%2][ijk] 
            = ( ( w[t%2][ijk+is] + w[t%2][ijk-is] + w[t%2][ijk+js] + w[t%2][ijk-js] + 2*(w[t%2][ijk+1] + w[t%2][ijk-1]) - 8.0*w[t%2][ijk]
                + u[t%2][ijk+1] - u[t%2][ijk] - u[t%2][ijk-is+1] + u[t%2][ijk-is]
                + v[t%2][ijk+1] - v[t%2][ijk] - v[t%2][ijk-js+1] + v[t%2][ijk-js]) * nuqh2
              - ( (w[t%2][ijk] + w[t%2][ijk+is]) * (u[t%2][ijk] + u[t%2][ijk+1])
                - (w[t%2][ijk-is] + w[t%2][ijk]) * (u[t%2][ijk-is] + u[t%2][ijk-is+1])
                + (v[t%2][ijk] + v[t%2][ijk+1] ) * (w[t%2][ijk] + w[t%2][ijk+js])
                - (v[t%2][ijk-js] + v[t%2][ijk-js+1]) * (w[t%2][ijk-js] + w[t%2][ijk])
                + (w[t%2][ijk] + w[t%2][ijk+1]) * (w[t%2][ijk] + w[t%2][ijk+1]) 
                - (w[t%2][ijk] + w[t%2][ijk-1]) * (w[t%2][ijk] + w[t%2][ijk-1])) * q4h
              ) * dt + w[t%2][ijk];
        }
      }
    }
  }
#pragma endscop

#ifdef TIME
  time = mytime() - time;
  printf("Time: %10.8lf\n", time);
  // avg
  double avg = 0.0;
  for (i=nh; i<NI+nh; ++i)
    for (j=nh; j<NJ+nh; ++j)
      for (k=nh; k<NK+nh; ++k)
        avg += fabs(u[0][ijk]);
  avg = avg/NI/NJ/NK;
  printf("avg: %10.8lf\n", avg);
#endif

  //FREE

  return 0;
}
