#include <math.h> 
#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#define NI 480L
#define NJ 480L
#define NK 480L
#define is 234256L
#define js 484L
#define ijk i*234256+j*484+k
#define T   60L

// macros to simplify weno's formulas
#define IA (eps + (A[t%2][i*234256+j*484+k] - 2*A[t%2][(i-1)*234256+j*484+k] + A[t%2][(i-2)*234256+j*484+k]) * (A[t%2][i*234256+j*484+k] - 2*A[t%2][(i-1)*234256+j*484+k] + A[t%2][(i-2)*234256+j*484+k]))
#define IB (eps + (A[t%2][(i+1)*234256+j*484+k] - 2*A[t%2][i*234256+j*484+k] + A[t%2][(i-1)*234256+j*484+k]) * (A[t%2][(i+1)*234256+j*484+k] - 2*A[t%2][i*234256+j*484+k] + A[t%2][(i-1)*234256+j*484+k]))
#define IC (eps + (A[t%2][(i+2)*234256+j*484+k] - 2*A[t%2][(i+1)*234256+j*484+k] + A[t%2][i*234256+j*484+k]) * (A[t%2][(i+2)*234256+j*484+k] - 2*A[t%2][(i+1)*234256+j*484+k] + A[t%2][i*234256+j*484+k]))
#define JA (eps + (A[t%2][i*234256+j*484+k] - 2*A[t%2][i*234256+(j-1)*484+k] + A[t%2][i*234256+(j-2)*484+k]) * (A[t%2][i*234256+j*484+k] - 2*A[t%2][i*234256+(j-1)*484+k] + A[t%2][i*234256+(j-2)*484+k]))
#define JB (eps + (A[t%2][i*234256+(j+1)*484+k] - 2*A[t%2][i*234256+j*484+k] + A[t%2][i*234256+(j-1)*484+k]) * (A[t%2][i*234256+(j+1)*484+k] - 2*A[t%2][i*234256+j*484+k] + A[t%2][i*234256+(j-1)*484+k]))
#define JC (eps + (A[t%2][i*234256+(j+2)*484+k] - 2*A[t%2][i*234256+(j+1)*484+k] + A[t%2][i*234256+j*484+k]) * (A[t%2][i*234256+(j+2)*484+k] - 2*A[t%2][i*234256+(j+1)*484+k] + A[t%2][i*234256+j*484+k]))
#define KA (eps + (A[t%2][i*234256+j*484+k] - 2*A[t%2][i*234256+j*484+k-1] + A[t%2][i*234256+j*484+k-2]) * (A[t%2][i*234256+j*484+k] - 2*A[t%2][i*234256+j*484+k-1] + A[t%2][i*234256+j*484+k-2]))
#define KB (eps + (A[t%2][i*234256+j*484+k+1] - 2*A[t%2][i*234256+j*484+k] + A[t%2][i*234256+j*484+k-1]) * (A[t%2][i*234256+j*484+k+1] - 2*A[t%2][i*234256+j*484+k] + A[t%2][i*234256+j*484+k-1]))
#define KC (eps + (A[t%2][i*234256+j*484+k+2] - 2*A[t%2][i*234256+j*484+k+1] + A[t%2][i*234256+j*484+k]) * (A[t%2][i*234256+j*484+k+2] - 2*A[t%2][i*234256+j*484+k+1] + A[t%2][i*234256+j*484+k]))

const double PI = 3.14159265359;
const int    nh = 2;

double A[2][(NI+4)*(NJ+4)*(NK+4)];
double U[(NI+4)*(NJ+4)*(NK+4)], V[(NI+4)*(NJ+4)*(NK+4)], W[(NI+4)*(NJ+4)*(NK+4)];

// timer
double mytime()
{
  struct timespec t;
  clock_gettime(CLOCK_REALTIME, &t);
  return (double)(t.tv_sec) + (double)(t.tv_nsec)*1.0e-9;
}

int main(int argc, char *argv[]) {
  long int i, j, k, t;
  double h  = 1.0 / NI, q2h = 0.5/h, dt = 0.0001, eps = 1.0e-8;
  double time;

  //ALLOC

  // init
  #pragma omp parallel for
  for (i = 0; i < NI + 2*nh; i++) {
    for (j = 0; j < NJ + 2*nh; j++) {
      for (k = 0; k < NK + 2*nh; k++) {
        A[1][ijk] = 0.0;
        A[0][ijk] = -12.0*PI*PI*sin(2*PI*(i-nh+0.5)*h) * sin(2*PI*(j-nh+0.5)*h) * sin(2*PI*(k-nh+0.5)*h);
        U[ijk] = 0.01;
        V[ijk] = 0.01;
        W[ijk] = 0.01;
      }
    }
  }

#ifdef TIME
  time = mytime();
#endif

#pragma scop
  for (t = 0; t < T; t++) {
    for (i = 2; i < NI + 2; i++) {
      for (j = 2; j < NJ + 2; j++) {
        for (k = 2; k < NK + 2; k++) {
          A[(t+1)%2][ijk] = A[t%2][ijk] - dt * q2h * ( 
              U[ijk] * (U[ijk] > 0 ? (A[t%2][ijk+is] - A[t%2][ijk-is]) + (-A[t%2][ijk+is] + 3*A[t%2][ijk] - 3*A[t%2][ijk-is] + A[t%2][ijk-2*is]) * IB*IB/(IB*IB+2*IA*IA)
                                   : (A[t%2][ijk+is] - A[t%2][ijk-is]) + (-A[t%2][ijk+2*is] + 3*A[t%2][ijk+is] - 3*A[t%2][ijk] + A[t%2][ijk-is]) * IB*IB/(IB*IB+2*IC*IC))
            + V[ijk] * (V[ijk] > 0 ? (A[t%2][ijk+js] - A[t%2][ijk-js]) + (-A[t%2][ijk+js] + 3*A[t%2][ijk] - 3*A[t%2][ijk-js] + A[t%2][ijk-2*js]) * JB*JB/(JB*JB+2*JA*JA)
                                   : (A[t%2][ijk+js] - A[t%2][ijk-js]) + (-A[t%2][ijk+2*js] + 3*A[t%2][ijk+js] - 3*A[t%2][ijk] + A[t%2][ijk-js]) * JB*JB/(JB*JB+2*JC*JC))
            + W[ijk] * (W[ijk] > 0 ? (A[t%2][ijk+1] - A[t%2][ijk-1]) + (-A[t%2][ijk+1] + 3*A[t%2][ijk] - 3*A[t%2][ijk-1] + A[t%2][ijk-1]) * KB*KB/(KB*KB+2*KA*KA)
                                   : (A[t%2][ijk+1] - A[t%2][ijk-1]) + (-A[t%2][ijk+1] + 3*A[t%2][ijk+1] - 3*A[t%2][ijk] + A[t%2][ijk-1]) * KB*KB/(KB*KB+2*KC*KC)));
            //- dt * U[ijk] * (U[ijk] > 0 ? ( (A[t%2][ijk+is] - A[t%2][ijk-is]) + (-A[t%2][ijk+is] + 3*A[t%2][ijk] - 3*A[t%2][ijk-is] + A[t%2][ijk-2*is]) 
            //* (eps + (A[t%2][ijk+is] - 2*A[t%2][ijk] + A[t%2][ijk-is]) * (A[t%2][ijk+is] - 2*A[t%2][ijk] + A[t%2][ijk-is])) 
            //* (eps + (A[t%2][ijk+is] - 2*A[t%2][ijk] + A[t%2][ijk-is]) * (A[t%2][ijk+is] - 2*A[t%2][ijk] + A[t%2][ijk-is]))
            /// ((eps + (A[t%2][ijk+is] - 2*A[t%2][ijk] + A[t%2][ijk-is]) * (A[t%2][ijk+is] - 2*A[t%2][ijk] + A[t%2][ijk-is]))*(eps + (A[t%2][ijk+is] - 2*A[t%2][ijk] + A[t%2][ijk-is]) * (A[t%2][ijk+is] - 2*A[t%2][ijk] + A[t%2][ijk-is]))+2*(eps + (A[t%2][ijk] - 2*A[t%2][ijk-is] + A[t%2][ijk-2*is]) * (A[t%2][ijk] - 2*A[t%2][ijk-is] + A[t%2][ijk-2*is])) * (eps + (A[t%2][ijk] - 2*A[t%2][ijk-is] + A[t%2][ijk-2*is]) * (A[t%2][ijk] - 2*A[t%2][ijk-is] + A[t%2][ijk-2*is])))) : )*q2h;
          //a = (eps + (A[t%2][ijk] - 2*A[t%2][ijk-is] + A[t%2][ijk-2*is]) * (A[t%2][ijk] - 2*A[t%2][ijk-is] + A[t%2][ijk-2*is])) * (eps + (A[t%2][ijk] - 2*A[t%2][ijk-is] + A[t%2][ijk-2*is]) * (A[t%2][ijk] - 2*A[t%2][ijk-is] + A[t%2][ijk-2*is]));
          //b = (eps + (A[t%2][ijk+is] - 2*A[t%2][ijk] + A[t%2][ijk-is]) * (A[t%2][ijk+is] - 2*A[t%2][ijk] + A[t%2][ijk-is])) * (eps + (A[t%2][ijk+is] - 2*A[t%2][ijk] + A[t%2][ijk-is]) * (A[t%2][ijk+is] - 2*A[t%2][ijk] + A[t%2][ijk-is]));
        }
      }
    }
  }
#pragma endscop

#ifdef TIME
  time = mytime() - time;
  printf("Time: %10.8lf\n", time);
  // avg
  double avg = 0.0;
  for (i=nh; i<NI+nh; ++i)
    for (j=nh; j<NJ+nh; ++j)
      for (k=nh; k<NK+nh; ++k)
        avg += fabs(A[0][ijk]);
  avg = avg/NI/NJ/NK;
  printf("avg: %10.8lf\n", avg);
#endif

  //FREE

  return 0;
}
