#include "WenoSolver.h"
#include "constants.h"
#include "Timer.h"
#include <algorithm>

using std::max;
using std::min;


WenoSolver::WenoSolver(DomainDecomp& dd, UniMesh& mesh, int nh):
  Solver(dd, mesh, nh),
  _dt(0.0001)
{}


WenoSolver::~WenoSolver() 
{
  for (int i=0; i<_nVar; ++i) delete [] _d[i];
  delete [] _d;
}


void WenoSolver::set_from_option()
{
  Solver::set_from_option();
}


int WenoSolver::setup()
{
  // set halo store, transfer, etc, nVar, pipe are set from option
  Solver::setup();

  _sr = 2; // stencil radius

  // allocate data
  _nVar = 5;
  int szs[3];
  _ptDd->get_local_size(szs);
  _d = new double* [_nVar];
  for (int i=0; i<_nVar; ++i) 
    _d[i] = new double [(szs[0]+2*_nHalo) *(szs[1]+2*_nHalo)*(szs[2]+2*_nHalo)];

  // halo variable
  _haloVar.push_back(vector<int>({1}));
  _haloVar.push_back(vector<int>({0}));

  _setup_diamonds();

  return 0;
}


int WenoSolver::init()
{
  int szs[3], starts[3];
  _ptDd->get_local_size(szs);
  _ptDd->get_local_start(starts);
  int jStrd = szs[2] + 2*_nHalo;
  int iStrd = jStrd * (szs[1] + 2*_nHalo);
  int totSz = (szs[0]+2*_nHalo) * iStrd; 

  // velocities
  //std::fill(_d[0], _d[0]+totSz, 0.0);
  std::fill(_d[1], _d[1]+totSz, 0.0);
  std::fill(_d[2], _d[2]+totSz, 0.01);
  std::fill(_d[3], _d[3]+totSz, 0.01);
  std::fill(_d[4], _d[4]+totSz, 0.01);

  double R = 0.3, w = 4*_h;
  for (int i=0; i<szs[0]+2*_nHalo; ++i) {
    for (int j=0; j<szs[1]+2*_nHalo; ++j) {
      for (int k=0; k<szs[2]+2*_nHalo; ++k) {
        int  ijk = i*iStrd + j*jStrd + k;
        double x = (i-_nHalo+starts[0]+0.5)*_h;
        double y = (j-_nHalo+starts[1]+0.5)*_h;
        double z = (k-_nHalo+starts[2]+0.5)*_h;
        double r = sqrt((x-0.5)*(x-0.5) + (y-0.5)*(y-0.5) + (z-0.5)*(z-0.5));
        _d[0][ijk] = (r>=R-w && r<=R+w) ? sin(0.5*PI*(r-R)/w) : (r>R+w ? 0 : 1);
      }
    }
  }

  for (int i=0; i<_ptDd->num_thread(); ++i)
    for (int j=0; j<TIMER_MAX_ITEM; ++j)
      std::fill(_t[i][j], _t[i][j]+TIMER_MAX_SIZE, 0.0);

  return 0;
}


int WenoSolver::init_numa()
{
  int tid = omp_get_thread_num();
  int rngs[6], tRngs[6], starts[3];
  _ptDd->get_local_size(rngs);
  _ptDd->get_local_start(starts);
  int jStrd = rngs[2] + 2*_nHalo, iStrd = jStrd * (rngs[1] + 2*_nHalo);
  for (int i=0; i<3; ++i) {
    rngs[i+3] = rngs[i] + _nHalo;
    rngs[i]   = _nHalo;
  }

  // pipeline (+ovlp) only init with nThrd-1 threads, numa
  if ((_mode == PipelineTile && !_isDiamond)) {
    int nThrd = _ptDd->num_thread();
    if (tid < nThrd-1) {
      // thread range include halo
      int nThrds[3] = {1, nThrd-1, 1};
      compute_thread_range(rngs, nThrds, tid, tRngs);
      for (int i=0; i<3; ++i) {
        if (tRngs[i] == rngs[i]) tRngs[i] = 0;
        if (tRngs[i+3] == rngs[i+3]) tRngs[i+3] += _nHalo;
      }
      // numa init
      double R = 0.3, w = 4*_h;
      for (int i=tRngs[0]; i<tRngs[3]; ++i) {
        for (int j=tRngs[1]; j<tRngs[4]; ++j) {
          for (int k=tRngs[2]; k<tRngs[5]; ++k) {
            int ijk = i*iStrd + j*jStrd + k;
            _d[1][ijk] = 0.0;
            _d[2][ijk] = 0.01;
            _d[3][ijk] = 0.01;
            _d[4][ijk] = 0.01;
            double x = (i-_nHalo+starts[0]+0.5)*_h;
            double y = (j-_nHalo+starts[1]+0.5)*_h;
            double z = (k-_nHalo+starts[2]+0.5)*_h;
            double r = sqrt((x-0.5)*(x-0.5) + (y-0.5)*(y-0.5) + (z-0.5)*(z-0.5));
            _d[0][ijk] = (r>=R-w && r<=R+w) ? sin(0.5*PI*(r-R)/w) : (r>R+w ? 0 : 1);
          }
        }
      }// end for i
    }// end if tid
  }// end if pipeline
  // all threads init, numa
  else {
    // thread range include halo
    _ptDd->get_thread_range(tid, tRngs);
    for (int i=0; i<6; ++i)  tRngs[i] += _nHalo;
    for (int i=0; i<3; ++i) {
      if (tRngs[i] == rngs[i]) tRngs[i]  = 0;
      if (tRngs[i+3] == rngs[i+3]) tRngs[i+3] += _nHalo;
    }
    // numa init
    double R = 0.3, w = 4*_h;
    for (int i=tRngs[0]; i<tRngs[3]; ++i) {
      for (int j=tRngs[1]; j<tRngs[4]; ++j) {
        for (int k=tRngs[2]; k<tRngs[5]; ++k) {
          int ijk = i*iStrd + j*jStrd + k;
          _d[1][ijk] = 0.0;
          _d[2][ijk] = 0.01;
          _d[3][ijk] = 0.01;
          _d[4][ijk] = 0.01;
          double x = (i-_nHalo+starts[0]+0.5)*_h;
          double y = (j-_nHalo+starts[1]+0.5)*_h;
          double z = (k-_nHalo+starts[2]+0.5)*_h;
          double r = sqrt((x-0.5)*(x-0.5) + (y-0.5)*(y-0.5) + (z-0.5)*(z-0.5));
          _d[0][ijk] = (r>=R-w && r<=R+w) ? sin(0.5*PI*(r-R)/w) : (r>R+w ? 0 : 1);
        }
      }
    }
  }// end else

  // init time
  for (int i=0; i<TIMER_MAX_ITEM; ++i)
    for (int j=0; j<TIMER_MAX_SIZE; ++j)
      _t[tid][i][j] = 0.0;

  #pragma omp barrier
  // init buffer
  if (_isBufNuma) {
    int nPipe = _hs.num_pipe();
    if (nPipe == 1) {
      _hts[0].init_buffer_numa(_hs);
    }
    else {
      for (int i=0; i<nPipe; ++i) _hts[i].init_buffer_numa(_hs);
      for (int i=0; i<nPipe+2; ++i) _hps[i].init_buffer_numa(_hs);
    }
  }

  return 0;
}


void WenoSolver::_compute_range(int rngs[6], bool isEven)
{
  int    szs[3];  _ptDd->get_local_size(szs);
  int    js = szs[2] + 2*_nHalo, is = js * (szs[1] + 2*_nHalo), base=rngs[0]*is + rngs[1]*js;
  int    jsb = _nHalo, isb = _nHalo*szs[1];
  int    kmbs = _hs.kmBufStart[0], kpbs = _hs.kpBufStart[0];
  int    tid = omp_get_thread_num();
  double *p, *a, *u = _d[2], *v = _d[3], *w = _d[4], qh = 1.0/_h, q2h = 0.5*qh;
  bool   mergePackkm = _ptDd->need_comm(2) && _bypassKHalo;
  bool   mergePackkp = _ptDd->need_comm(5) && _bypassKHalo;
  bool   mergeUnpackkm = mergePackkm && _thrdIter[tid] > 0;
  bool   mergeUnpackkp = mergePackkp && _thrdIter[tid] > 0;

  if (isEven) {
    p = _d[0]; a = _d[1];
  }
  else {
    p = _d[1]; a = _d[0];
  }

  if (_scheme == 0) { // weno3
    for (int i=rngs[0]; i<rngs[3]; ++i) {
      int ij  = base;
      int ijb = (i - _nHalo)*isb + (rngs[1]-_nHalo) * jsb;
      for (int j=rngs[1]; j<rngs[4]; ++j) {
        // unpack
        if (mergeUnpackkm)
          for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kmbs+ijb+k];
        if (mergeUnpackkp)
          for (int k=0; k<_nHalo; ++k)  p[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+ijb+k];
        // compute
        #pragma vector nontemporal
        #pragma omp simd
        for (int k=rngs[2]; k<rngs[5]; ++k) {
          int ijk = ij + k;
          double convi, convj, convk;
          convi = u[ijk] > 0 ?  u[ijk] * weno3(p[ijk-2*is], p[ijk-is], p[ijk], p[ijk+is], q2h)
                             : -u[ijk] * weno3(p[ijk+2*is], p[ijk+is], p[ijk], p[ijk-is], q2h);
          convj = v[ijk] > 0 ?  v[ijk] * weno3(p[ijk-2*js], p[ijk-js], p[ijk], p[ijk+js], q2h)
                             : -v[ijk] * weno3(p[ijk+2*js], p[ijk+js], p[ijk], p[ijk-js], q2h);
          convk = w[ijk] > 0 ?  w[ijk] * weno3(p[ijk-2], p[ijk-1], p[ijk], p[ijk+1], q2h)
                             : -w[ijk] * weno3(p[ijk+2], p[ijk+1], p[ijk], p[ijk-1], q2h);
          a[ijk] = p[ijk] - _dt * (convi + convj + convk);
        }// end for k
        // pack
        if (mergePackkm)
          for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
        if (mergePackkm)
          for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
        //
        ij  += js;
        ijb += jsb;
      }// end for j
      base += is;
    }// end for i
  }// end if weno3
  else if (_scheme == 1) { // weno5
    for (int i=rngs[0]; i<rngs[3]; ++i) {
      int ij  = base;
      int ijb   = (i - _nHalo)*isb + (rngs[1] - _nHalo) * jsb;
      for (int j=rngs[1]; j<rngs[4]; ++j) {
        // unpack
        if (mergeUnpackkm)
          for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kmbs+ijb+k];
        if (mergeUnpackkp)
          for (int k=0; k<_nHalo; ++k)  p[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+ijb+k];
        // compute
        #pragma vector nontemporal
        #pragma omp simd
        for (int k=rngs[2]; k<rngs[5]; ++k) {
          int ijk = ij + k;
          double convi, convj, convk;
          convi = u[ijk] > 0 ? u[ijk] * ( 3*p[ijk] - 4*p[ijk-is] + p[ijk-2*is]) * q2h
                             : u[ijk] * (-3*p[ijk] + 4*p[ijk+is] - p[ijk+2*is]) * q2h;
          convj = v[ijk] > 0 ? v[ijk] * ( 3*p[ijk] - 4*p[ijk-js] + p[ijk-2*js]) * q2h
                             : v[ijk] * (-3*p[ijk] + 4*p[ijk+js] - p[ijk+2*js]) * q2h;
          convk = w[ijk] > 0 ? w[ijk] * ( 3*p[ijk] - 4*p[ijk-1] + p[ijk-2]) * q2h
                             : w[ijk] * (-3*p[ijk] + 4*p[ijk+1] - p[ijk+2]) * q2h;
          a[ijk] = p[ijk] - _dt * (convi + convj + convk);
        }// end for k
        // pack
        if (mergePackkm)
          for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
        if (mergePackkp)
          for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
        //
        ij  += js;
        ijb += jsb;
      }// end for j
      base += is;
    }// end for i
  }// end else weno5
}


void WenoSolver::_compute_range_rectangle(int rngs[6], int dir, double *c, 
                                          int isl, int jsl, int pipe, HaloTransfer* ptHt)
{
  int tid    = omp_get_thread_num();
  int szs[3];
  _ptDd->get_local_size(szs);
  int js  = szs[2] + 2*_nHalo, is  = js * (szs[1] + 2*_nHalo);
  int sw = 2*_sr + 1;
  int jsb = _nHalo, isb = _nHalo*szs[1], pipeLen = szs[0]/_hs.num_pipe();
  int kmbs = _hs.kmBufStart[pipe], kpbs = _hs.kpBufStart[pipe];
  bool mergePackkm = _ptDd->need_comm(2) && _bypassKHalo;
  bool mergePackkp = _ptDd->need_comm(5) && _bypassKHalo;
  bool mergeUnpackkm = mergePackkm && _thrdIter[tid] > 0;
  bool mergeUnpackkp = mergePackkp && _thrdIter[tid] > 0;

  double *p, *a, *u = _d[2], *v = _d[3], *w = _d[4], qh = 1.0/_h, q2h = 0.5*qh;
  if (dir == 0) {
    p = _d[0]; a = _d[1];
  }
  else {
    p = _d[1]; a = _d[0];
  }

  int iBegin = max(rngs[0]-_nHalo+_sr, ((_dhOff && !_ptDd->need_comm(0))? _nHalo : -100));
  int iEnd   = min(rngs[3]+_nHalo-_sr, ((_dhOff && !_ptDd->need_comm(3))? szs[0]+_nHalo : 10000));
  int jBegin = max(rngs[1]-_nHalo+_sr, ((_dhOff && !_ptDd->need_comm(1))? _nHalo : -100));
  int jEnd   = min(rngs[4]+_nHalo-_sr, ((_dhOff && !_ptDd->need_comm(4))? szs[1]+_nHalo : 10000));
  int kBegin = max(rngs[2]-_nHalo+_sr, ((_dhOff && !_ptDd->need_comm(2))? _nHalo : -100));
  int kEnd   = min(rngs[5]+_nHalo-_sr, ((_dhOff && !_ptDd->need_comm(5))? szs[2]+_nHalo : 10000));
  int jlDHLow  = (_dhOff && !_ptDd->need_comm(1) && rngs[1] <= _nHalo) ? _nHalo : -100;
  int jlDHHgih = (_dhOff && !_ptDd->need_comm(4) && rngs[4] >= szs[1]+_nHalo) ? rngs[4]-rngs[1]+_nHalo : 10000;
  int klDHLow  = (_dhOff && !_ptDd->need_comm(2) && rngs[2] <= _nHalo) ? _nHalo : -100;
  int klDHHgih = (_dhOff && !_ptDd->need_comm(5) && rngs[5] >= szs[2]+_nHalo) ? rngs[5]-rngs[2]+_nHalo : 10000;

  // unpack i=nHalo to nHalo+_sr if deep halo is off
  // HARDCODE: assume no communication in i
  if (_dhOff) {
    if (mergeUnpackkm) {
      for (int i=rngs[0]; i<rngs[0]+_sr; ++i) {
        int ij  =  i*is + rngs[1]*js;
        int ijb = (i - _nHalo) * isb + (rngs[1] - _nHalo) * jsb;
        for (int j=rngs[1]; j<rngs[4]; ++j) {
          for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kmbs+ijb+k];
          ij += js; ijb += jsb;
        }
      }
    }
    if (mergeUnpackkp) {
      for (int i=rngs[0]; i<rngs[0]+_sr; ++i) {
        int ij  =  i*is + rngs[1]*js + szs[2] + _nHalo;
        int ijb = (i - _nHalo)*isb + (rngs[1] - _nHalo) * jsb;
        for (int j=rngs[1]; j<rngs[4]; ++j) {
          for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kpbs+ijb+k];
          ij += js; ijb += jsb;
        }
      }
    }
  }

  if (_scheme == 0) {// weno3
    for (int i=rngs[0]-_nHalo+_sr; i<rngs[3]+_nHalo-_sr; ++i) {
      // p -> c
      int ij    = i * is + jBegin * js;
      int basel = (i-1)%sw*isl + (jBegin-rngs[1]+_nHalo)*jsl - (rngs[2]-_nHalo);
      bool isIReadInside = (i+_sr >= _nHalo) && (i+_sr < szs[0]+_nHalo);
      if (_dhOff && (i < _nHalo || i >= szs[0]+_nHalo)) {
        _nullify_layer(c, basel, jBegin, jEnd, jsl, kBegin, kEnd);
      }
      else {
        for (int j=jBegin; j<jEnd; ++j) {
          // unpack
          int ijb   = (i - _nHalo - pipe*pipeLen)*isb + (j - _nHalo) * jsb;
          if (mergeUnpackkm && isIReadInside && j >= _nHalo && j < szs[1]+_nHalo)
            for (int k=0; k<_nHalo; ++k)  p[ij+_sr*is+k] = _hs.rBuf[kmbs+ijb+_sr*isb+k];
          if (mergeUnpackkp && isIReadInside && j >= _nHalo && j < szs[1]+_nHalo)
            for (int k=0; k<_nHalo; ++k)  p[ij+_sr*is+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+ijb+_sr*isb+k];
          // compute
          #pragma omp simd
          for (int k=kBegin; k<kEnd; ++k) {
            int ijk = ij + k;
            double convi, convj, convk;
            convi = (u[ijk] > 0) ?  u[ijk] * weno3(p[ijk-2*is], p[ijk-is], p[ijk], p[ijk+is], q2h)
                                 : -u[ijk] * weno3(p[ijk+2*is], p[ijk+is], p[ijk], p[ijk-is], q2h);
            convj = (v[ijk] > 0) ?  v[ijk] * weno3(p[ijk-2*js], p[ijk-js], p[ijk], p[ijk+js], q2h)
                                 : -v[ijk] * weno3(p[ijk+2*js], p[ijk+js], p[ijk], p[ijk-js], q2h);
            convk = (w[ijk] > 0) ?  w[ijk] * weno3(p[ijk-2], p[ijk-1], p[ijk], p[ijk+1], q2h)
                                 : -w[ijk] * weno3(p[ijk+2], p[ijk+1], p[ijk], p[ijk-1], q2h);
            c[basel+k] = p[ijk] - _dt * (convi + convj + convk);
          }
          ij    += js;
          basel += jsl;
        }
      }
      // c -> c
      int nf     = _nHalo / _sr; // # fuesd iterations
      int iLayer = i - _sr;                      // current iteration layer in domain
      int iLow   = rngs[0] - _nHalo + 2*_sr;     // i lower bound for current iter
      int iHigh  = rngs[3] + _nHalo - 1 - 2*_sr; // i higher bound 
      int margin = 2 * _sr;
      for (int m=1; m<nf-1; ++m) {
        int jlBegin = max(margin, jlDHLow);
        int jlEnd   = min(rngs[4]-rngs[1]+2*_nHalo-margin,jlDHHgih);
        if (iLayer >= iLow && iLayer <= iHigh) {
          int ijl   = ((iLayer-1+sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
          int im1jl = ((iLayer-2+sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
          int im2jl = ((iLayer-3+sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
          int ip1jl = ((iLayer  +sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
          int ip2jl = ((iLayer+1+sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
          int ijlTo = ijl + sw*isl;
          int ij    = iLayer*is + (rngs[1]-_nHalo+jlBegin)*js;
          if (_dhOff && (iLayer < _nHalo || iLayer >= szs[0]+_nHalo)) {
            for (int j=jlBegin; j<jlEnd; ++j) {
              int klBegin = max(margin,klDHLow), klEnd = min(rngs[5]-rngs[2]+2*_nHalo-margin,klDHHgih);
              #pragma omp simd
              for (int k=klBegin; k<klEnd; ++k) c[ijlTo+k] = 0.0;
              ijlTo += jsl;
            }// end for j
          }// end if dhOff
          else {
            for (int j=jlBegin; j<jlEnd; ++j) {
              int klBegin = max(margin,klDHLow), klEnd = min(rngs[5]-rngs[2]+2*_nHalo-margin,klDHHgih);
              #pragma omp simd
              for (int k=klBegin; k<klEnd; ++k) {
                int ijkl = ijl + k, ijk = ij + k + rngs[2] - _nHalo;
                double convi, convj, convk;
                convi = u[ijk] > 0 ?  u[ijk] * weno3(c[im2jl+k], c[im1jl+k], c[ijkl], c[ip1jl+k], q2h)
                                   : -u[ijk] * weno3(c[ip2jl+k], c[ip1jl+k], c[ijkl], c[im1jl+k], q2h); 
                convj = v[ijk] > 0 ?  v[ijk] * weno3(c[ijkl-2*jsl], c[ijkl-jsl], c[ijkl], c[ijkl+jsl], q2h)
                                   : -v[ijk] * weno3(c[ijkl+2*jsl], c[ijkl+jsl], c[ijkl], c[ijkl-jsl], q2h); 
                convk = w[ijk] > 0 ?  w[ijk] * weno3(c[ijkl-2], c[ijkl-1], c[ijkl], c[ijkl+1], q2h)
                                   : -w[ijk] * weno3(c[ijkl+2], c[ijkl+1], c[ijkl], c[ijkl-1], q2h);
                c[ijlTo+k] = c[ijkl] - _dt * (convi + convj + convk);
              }// end for k
              ij  += js;
              ijl += jsl; im1jl += jsl; im2jl += jsl; ip1jl += jsl; ip2jl += jsl; ijlTo += jsl;
            }// end for j
          }// end else
          margin += _sr;
          iLayer -= _sr;
          iLow   += _sr;
          iHigh  -= _sr;
        }// end if
      }// end for m
      // c -> a
      // use iLayer, iLow, iHigh, margin at previous iteration
      if (iLayer >= iLow && iLayer <= iHigh) {
        int ijl   = ((iLayer-1+sw)%sw + (nf-2)*sw) * isl + margin*jsl;
        int im1jl = ((iLayer-2+sw)%sw + (nf-2)*sw) * isl + margin*jsl;
        int im2jl = ((iLayer-3+sw)%sw + (nf-2)*sw) * isl + margin*jsl;
        int ip1jl = ((iLayer  +sw)%sw + (nf-2)*sw) * isl + margin*jsl;
        int ip2jl = ((iLayer+1+sw)%sw + (nf-2)*sw) * isl + margin*jsl;
        int ij    = iLayer*is + rngs[1]*js;
        int ijb   = (iLayer - _nHalo - pipe*pipeLen)*isb + (rngs[1] - _nHalo) * jsb;
        for (int j=_nHalo; j<rngs[4]-rngs[1]+_nHalo; ++j) {
          #pragma omp simd
          for (int k=_nHalo; k<rngs[5]-rngs[2]+_nHalo; ++k) {
            int ijkl = ijl + k, ijk = ij + k + rngs[2] - _nHalo;
            double convi, convj, convk;
            convi = u[ijk] > 0 ?  u[ijk] * weno3(c[im2jl+k], c[im1jl+k], c[ijkl], c[ip1jl+k], q2h)
                               : -u[ijk] * weno3(c[ip2jl+k], c[ip1jl+k], c[ijkl], c[im1jl+k], q2h);
            convj = v[ijk] > 0 ?  v[ijk] * weno3(c[ijkl-2*jsl], c[ijkl-jsl], c[ijkl], c[ijkl+jsl], q2h)
                               : -v[ijk] * weno3(c[ijkl+2*jsl], c[ijkl+jsl], c[ijkl], c[ijkl-jsl], q2h);
            convk = w[ijk] > 0 ?  w[ijk] * weno3(c[ijkl-2], c[ijkl-1], c[ijkl], c[ijkl+1], q2h)
                               : -w[ijk] * weno3(c[ijkl+2], c[ijkl+1], c[ijkl], c[ijkl-1], q2h);
            a[ijk] = c[ijkl] - _dt * (convi + convj + convk);
          }// end for k
          // pack
          if (mergePackkm)
            for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
          if (mergePackkp)
            for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
          //
          ij  += js;
          ijl += jsl; im1jl += jsl; im2jl += jsl; ip1jl += jsl; ip2jl += jsl;
          ijb += jsb;
        }// end for j
      }// end if
      // poll network if enabled
      if (tid == 0 && ptHt)  ptHt->poll(); 
    }// end for i
  }// end if weno3
  else if (_scheme == 1) {// upwind
    for (int i=iBegin; i<rngs[3]+_nHalo-_sr; ++i) {
      // p -> c
      int ij    = i * is + (rngs[1]-_nHalo+_sr) * js;
      int basel = (i-1)%sw*isl + (jBegin-rngs[1]+_nHalo)*jsl - (rngs[2]-_nHalo);
      bool isIReadInside = (i+_sr >= _nHalo) && (i+_sr < szs[0]+_nHalo);
      if (_dhOff && (i < _nHalo || i >= szs[0]+_nHalo)) {
        _nullify_layer(c, basel, jBegin, jEnd, jsl, kBegin, kEnd);
      }
      else {
        for (int j=jBegin; j<jEnd; ++j) {
          // unpack
          int ijb   = (i - _nHalo - pipe*pipeLen)*isb + (j - _nHalo) * jsb;
          if (mergeUnpackkm && isIReadInside && j >= _nHalo && j < szs[1]+_nHalo)
            for (int k=0; k<_nHalo; ++k)  p[ij+_sr*is+k] = _hs.rBuf[kmbs+ijb+_sr*isb+k];
          if (mergeUnpackkp && isIReadInside && j >= _nHalo && j < szs[1]+_nHalo)
            for (int k=0; k<_nHalo; ++k)  p[ij+_sr*is+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+ijb+_sr*isb+k];
          #pragma omp simd
          for (int k=kBegin; k<kEnd; ++k) {
            int ijk = ij + k;
            double convi, convj, convk;
            convi = u[ijk] > 0 ? u[ijk] * ( 3*p[ijk] - 4*p[ijk-is] + p[ijk-2*is]) * q2h
                               : u[ijk] * (-3*p[ijk] + 4*p[ijk+is] - p[ijk+2*is]) * q2h;
            convj = v[ijk] > 0 ? v[ijk] * ( 3*p[ijk] - 4*p[ijk-js] + p[ijk-2*js]) * q2h
                               : v[ijk] * (-3*p[ijk] + 4*p[ijk+js] - p[ijk+2*js]) * q2h;
            convk = w[ijk] > 0 ? w[ijk] * ( 3*p[ijk] - 4*p[ijk-1] + p[ijk-2]) * q2h
                               : w[ijk] * (-3*p[ijk] + 4*p[ijk+1] - p[ijk+2]) * q2h;
            c[basel+k] = p[ijk] - _dt * (convi + convj + convk);
          }
          ij    += js;
          basel += jsl;
        }
      }
      // c -> c
      int nf     = _nHalo / _sr; // # fuesd iterations
      int iLayer = i - _sr;                      // current iteration layer in domain
      int iLow   = rngs[0] - _nHalo + 2*_sr;     // i lower bound for current iter
      int iHigh  = rngs[3] + _nHalo - 1 - 2*_sr; // i higher bound 
      int margin = 2 * _sr;
      for (int m=1; m<nf-1; ++m) {
        int jlBegin = max(margin, jlDHLow);
        int jlEnd   = min(rngs[4]-rngs[1]+2*_nHalo-margin,jlDHHgih);
        if (iLayer >= iLow && iLayer <= iHigh) {
          int ijl   = ((iLayer-1+sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
          int im1jl = ((iLayer-2+sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
          int im2jl = ((iLayer-3+sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
          int ip1jl = ((iLayer  +sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
          int ip2jl = ((iLayer+1+sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
          int ijlTo = ijl + sw*isl;
          int ij    = iLayer*is + (rngs[1]-_nHalo+jlBegin)*js;
          if (_dhOff && (iLayer < _nHalo || iLayer >= szs[0]+_nHalo)) {
            for (int j=jlBegin; j<jlEnd; ++j) {
              int klBegin = max(margin,klDHLow), klEnd = min(rngs[5]-rngs[2]+2*_nHalo-margin,klDHHgih);
              #pragma omp simd
              for (int k=klBegin; k<klEnd; ++k) c[ijlTo+k] = 0.0;
              ijlTo += jsl;
            }// end for j
          }
          else {
            for (int j=jlBegin; j<jlEnd; ++j) {
              int klBegin = max(margin,klDHLow), klEnd = min(rngs[5]-rngs[2]+2*_nHalo-margin,klDHHgih);
              #pragma omp simd
              for (int k=klBegin; k<klEnd; ++k) {
                int ijkl = ijl + k, ijk = ij + k + rngs[2] - _nHalo;
                double convi, convj, convk;
                convi = u[ijk] > 0 ? u[ijk] * ( 3*c[ijkl] - 4*c[im1jl+k] + c[im2jl+k]) * q2h
                                   : u[ijk] * (-3*c[ijkl] + 4*c[ip1jl+k] - c[ip2jl+k]) * q2h;
                convj = v[ijk] > 0 ? v[ijk] * ( 3*c[ijkl] - 4*c[ijkl-jsl] + c[ijkl-2*jsl]) * q2h
                                   : v[ijk] * (-3*c[ijkl] + 4*c[ijkl+jsl] - c[ijkl+2*jsl]) * q2h;
                convk = w[ijk] > 0 ? w[ijk] * ( 3*c[ijkl] - 4*c[ijkl-1] + c[ijkl-2]) * q2h
                                   : w[ijk] * (-3*c[ijkl] + 4*c[ijkl+1] - c[ijkl+2]) * q2h;
                c[ijlTo+k] = c[ijkl] - _dt * (convi + convj + convk);
              }
              ij  += js;
              ijl += jsl; ijlTo += jsl; im1jl += jsl; im2jl += jsl; ip1jl += jsl; ip2jl += jsl;
            }
          }
          margin += _sr;
          iLayer -= _sr;
          iLow   += _sr;
          iHigh  -= _sr;
        }
      }
      // c -> a
      // use iLayer, iLow, iHigh, margin at previous iteration
      if (iLayer >= iLow && iLayer <= iHigh) {
        if (iLayer < iBegin || iLayer >= iEnd)  continue;
        int ijl   = ((iLayer-1+sw)%sw + (nf-2)*sw) * isl + margin*jsl;
        int im1jl = ((iLayer-2+sw)%sw + (nf-2)*sw) * isl + margin*jsl;
        int im2jl = ((iLayer-3+sw)%sw + (nf-2)*sw) * isl + margin*jsl;
        int ip1jl = ((iLayer  +sw)%sw + (nf-2)*sw) * isl + margin*jsl;
        int ip2jl = ((iLayer+1+sw)%sw + (nf-2)*sw) * isl + margin*jsl;
        int ij    = iLayer*is + rngs[1]*js;
        int ijb   = (iLayer-_nHalo-pipe*pipeLen)*isb + (rngs[1]-_nHalo)*jsb;
        for (int j=_nHalo; j<rngs[4]-rngs[1]+_nHalo; ++j) {
          #pragma omp simd
          for (int k=_nHalo; k<rngs[5]-rngs[2]+_nHalo; ++k) {
            int ijkl = ijl + k, ijk = ij + k + rngs[2] - _nHalo;
            double convi, convj, convk;
            convi = u[ijk] > 0 ? u[ijk] * ( 3*c[ijkl] - 4*c[im1jl+k] + c[im2jl+k]) * q2h
                               : u[ijk] * (-3*c[ijkl] + 4*c[ip1jl+k] - c[ip2jl+k]) * q2h;
            convj = v[ijk] > 0 ? v[ijk] * ( 3*c[ijkl] - 4*c[ijkl-jsl] + c[ijkl-2*jsl]) * q2h
                               : v[ijk] * (-3*c[ijkl] + 4*c[ijkl+jsl] - c[ijkl+2*jsl]) * q2h;
            convk = w[ijk] > 0 ? w[ijk] * ( 3*c[ijkl] - 4*c[ijkl-1] + c[ijkl-2]) * q2h
                               : w[ijk] * (-3*c[ijkl] + 4*c[ijkl+1] - c[ijkl+2]) * q2h;
            a[ijk] = c[ijkl] - _dt * (convi + convj + convk);
          }// end for k
          // pack
          if (mergePackkm)
            for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
          if (mergePackkp)
            for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
          //
          ij  += js;
          ijl += jsl; im1jl += jsl; im2jl += jsl; ip1jl += jsl; ip2jl += jsl;
          ijb += jsb;
        }// end for j
      }// end if
      // poll network if enabled
      if (tid == 0 && ptHt)  ptHt->poll(); 
    }// end for i
  }// end else weno5

}


void WenoSolver::_compute_pipe_diamond(int di, int dj, int pipe, int dir, HaloTransfer* ptHt)
{
  int szs[3]; _ptDd->get_local_size(szs);
  int js  = szs[2] + 2*_nHalo, is  = js * (szs[1] + 2*_nHalo);
  int kBegin = _sr, kEnd = szs[2] + 2*_nHalo - _sr, jBegin, jEnd;
  int tid = omp_get_thread_num();
  int nf = _nHalo / _sr;
  int nPipe = _hs.num_pipe(), pipeLen = szs[0] / nPipe;
  int jsb = _nHalo, isb = _nHalo*szs[1];
  int kmbs = _hs.kmBufStart[pipe], kpbs = _hs.kpBufStart[pipe];
  bool mergePackkm = _ptDd->need_comm(2) && _bypassKHalo;
  bool mergePackkp = _ptDd->need_comm(5) && _bypassKHalo;
  bool mergeUnpackkm = mergePackkm && _thrdIter[tid] > 0;
  bool mergeUnpackkp = mergePackkp && _thrdIter[tid] > 0;

  double *p = _d[0], *a = _d[1], *u = _d[2], *v = _d[3], *w = _d[4], qh = 1.0/_h, q2h = 0.5*qh;

  // adjust diamond's range based on current pipe
  _diamonds[di][dj].iBegin += pipe * pipeLen;
  _diamonds[di][dj].iEnd   += pipe * pipeLen;
 
  // adjust the triangleup/down range if it locates on pipe boundary
  int iBegin = _diamonds[di][dj].iBegin, iEnd  = _diamonds[di][dj].iEnd;
  int lBound = _diamonds[di][dj].iBegin, lSign =  1;
  int hBound = _diamonds[di][dj].iEnd,   hSign = -1; 
  if (_hs.num_pipe() > 1) {
    if (pipe == 0 && di == _nDiamond[0]-1) {// pipe 0, top is done
      hBound = _diamonds[di][dj].iEnd - 2*(_nHalo - _sr);
      hSign  = 1;
    }
    if (pipe > 1 && di == 0) {// pipe > 1, bottom is done
      iBegin = _diamonds[di][dj].iBegin + 2*(_nHalo - _sr);
      lBound = -1;
      lSign  =  0;
    }
  }

  int iDHLow  = max(iBegin, (_dhOff && !_ptDd->need_comm(0)) ? _nHalo : -100);
  int iDHHigh = min(iEnd,   (_dhOff && !_ptDd->need_comm(3)) ? szs[0]+_nHalo : 10000);
  int jDHLow  = (_dhOff && !_ptDd->need_comm(1)) ? _nHalo : -100;
  int jDHHigh = (_dhOff && !_ptDd->need_comm(4)) ? szs[1]+_nHalo : 10000;
  int kDHLow  = (_dhOff && !_ptDd->need_comm(2)) ? _nHalo : -100;
  int kDHHigh = (_dhOff && !_ptDd->need_comm(5)) ? szs[2]+_nHalo : 10000;

  if (_scheme == 0) {
    switch (_diamonds[di][dj].type) {
      case TriangleUp:
        for (int i=iBegin; i<iEnd; ++i) {
          bool isIReadInside = (i+_sr >= _nHalo) && (i+_sr < szs[0]+_nHalo);
          p = _d[dir%2];
          a = _d[(1-dir)%2];
          // unpack [-_sr,0) of the 1st j
          if (mergeUnpackkm && isIReadInside) {
            if (dj > 0) {
              for (int jj=-_sr; jj<0; ++jj) {
                int ij  = i*is + (_diamonds[di][dj].jBegin+jj)*js;
                int ijb = (i-_nHalo)*isb + (_diamonds[di][dj].jBegin+jj-_nHalo)*jsb;
                for (int k=0; k<_nHalo; ++k) p[ij+_sr*is+k] = _hs.rBuf[kmbs+_sr*isb+ijb+k];
              }
            }
            if (dj < _nDiamond[1]-1) {
              for (int jj=0; jj<_sr; ++jj) {
                int ij  = i*is + (_diamonds[di][dj].jEnd+jj)*js;
                int ijb = (i-_nHalo)*isb + (_diamonds[di][dj].jEnd+jj-_nHalo)*jsb;
                for (int k=0; k<_nHalo; ++k) p[ij+_sr*is+k] = _hs.rBuf[kmbs+_sr*isb+ijb+k];
              }
            }
          }
          if (mergeUnpackkp && isIReadInside) {
            if (dj > 0) {
              for (int jj=-_sr; jj<0; ++jj) {
                int ij  = i*is + (_diamonds[di][dj].jBegin+jj)*js;
                int ijb = (i-_nHalo)*isb + (_diamonds[di][dj].jBegin+jj-_nHalo)*jsb;
                for (int k=0; k<_nHalo; ++k) p[ij+_sr*is+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+_sr*isb+ijb+k];
              }
            }
            if (dj < _nDiamond[1]-1) {
              for (int jj=0; jj<_sr; ++jj) {
                int ij  = i*is + (_diamonds[di][dj].jEnd+jj)*js;
                int ijb = (i-_nHalo)*isb + (_diamonds[di][dj].jEnd+jj-_nHalo)*jsb;
                for (int k=0; k<_nHalo; ++k) p[ij+_sr*is+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+_sr*isb+ijb+k];
              }
            }
          }

          for (int m=0; m<nf; ++m) {
            p = _d[(dir+m)%2]; 
            a = _d[(1-dir+m)%2];
            int margin = m * _sr;
            int iLayer = i - m * _sr;
            if ((iLayer >= lBound + lSign*margin) && (iLayer <  hBound + hSign*margin)) {
              if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
              int ij = iLayer * is + max(_diamonds[di][dj].jBegin+margin,jDHLow) * js;
              for (int j=max(_diamonds[di][dj].jBegin+margin,jDHLow); j<min(_diamonds[di][dj].jEnd-margin,jDHHigh); ++j) {
                // unpack
                int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (j - _nHalo) * jsb;
                bool isIJReadInside = isIReadInside && j >= _nHalo && j < szs[1]+_nHalo;
                if (m == 0 && mergeUnpackkm && isIJReadInside)
                  for (int k=0; k<_nHalo; ++k)  p[ij+_sr*is+k] = _hs.rBuf[kmbs+ijb+_sr*isb+k];
                if (m == 0 && mergeUnpackkp && isIJReadInside)
                  for (int k=0; k<_nHalo; ++k)  p[ij+_sr*is+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+ijb+_sr*isb+k];
                // compute
                #pragma omp simd
                for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                  int ijk = ij + k;
                  double convi, convj, convk;
                  convi = u[ijk] > 0 ?  u[ijk] * weno3(p[ijk-2*is], p[ijk-is], p[ijk], p[ijk+is], q2h)
                                     : -u[ijk] * weno3(p[ijk+2*is], p[ijk+is], p[ijk], p[ijk-is], q2h);
                  convj = v[ijk] > 0 ?  v[ijk] * weno3(p[ijk-2*js], p[ijk-js], p[ijk], p[ijk+js], q2h)
                                     : -v[ijk] * weno3(p[ijk+2*js], p[ijk+js], p[ijk], p[ijk-js], q2h); 
                  convk = w[ijk] > 0 ?  w[ijk] * weno3(p[ijk-2], p[ijk-1], p[ijk], p[ijk+1], q2h)
                                     : -w[ijk] * weno3(p[ijk+2], p[ijk+1], p[ijk], p[ijk-1], q2h);
                  a[ijk] = p[ijk] - _dt * (convi + convj + convk);
                }// end for k
                // pack
                if (m == nf-1 && mergePackkm)
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
                if (m == nf-1 && mergePackkp)
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
                //
                ij += js;
              }// end for j
            }// end if
          }// end for m
          if (tid == 0 && ptHt)  ptHt->poll(); 
        }// end for i
      break;

      case TriangleDown:
        jBegin = _diamonds[di][dj].jBegin + _nHalo - _sr;
        jEnd   = _diamonds[di][dj].jEnd   - _nHalo + _sr;
        for (int i=iBegin; i<iEnd; ++i) {
          bool isIReadInside = (i+_sr >= _nHalo) && (i+_sr < szs[0]+_nHalo);
          for (int m=0; m<nf; ++m) {
            p = _d[(dir+m)%2]; 
            a = _d[(1-dir+m)%2];
            int margin = m * _sr;
            int iLayer = i - m * _sr;
            if ((iLayer >= lBound + lSign*margin) && (iLayer <  hBound + hSign*margin)) {
              if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
              int ij = iLayer * is + max(jBegin - margin, jDHLow) * js;
              for (int j=max(jBegin-margin,jDHLow); j<min(jEnd+margin,jDHHigh); ++j) {
                // unpack
                int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (j - _nHalo) * jsb;
                bool isIJReadInside = isIReadInside && j >= _nHalo && j < szs[1]+_nHalo;
                if (m == 0 && mergeUnpackkm && isIJReadInside)
                  for (int k=0; k<_nHalo; ++k)  p[ij+_sr*is+k] = _hs.rBuf[kmbs+ijb+_sr*isb+k];
                if (m == 0 && mergeUnpackkp && isIJReadInside)
                  for (int k=0; k<_nHalo; ++k)  p[ij+szs[2]+_nHalo+_sr*is+k] = _hs.rBuf[kpbs+ijb+_sr*isb+k];
                #pragma omp simd
                for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                  int ijk = ij + k;
                  double convi, convj, convk;
                  convi = u[ijk] > 0 ?  u[ijk] * weno3(p[ijk-2*is], p[ijk-is], p[ijk], p[ijk+is], q2h)
                                     : -u[ijk] * weno3(p[ijk+2*is], p[ijk+is], p[ijk], p[ijk-is], q2h);
                  convj = v[ijk] > 0 ?  v[ijk] * weno3(p[ijk-2*js], p[ijk-js], p[ijk], p[ijk+js], q2h)
                                     : -v[ijk] * weno3(p[ijk+2*js], p[ijk+js], p[ijk], p[ijk-js], q2h); 
                  convk = w[ijk] > 0 ?  w[ijk] * weno3(p[ijk-2], p[ijk-1], p[ijk], p[ijk+1], q2h)
                                     : -w[ijk] * weno3(p[ijk+2], p[ijk+1], p[ijk], p[ijk-1], q2h);
                  a[ijk] = p[ijk] - _dt * (convi + convj + convk);
                }// end for k
                // pack
                if (m == nf-1 && mergePackkm)
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
                if (m == nf-1 && mergePackkp)
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
                //
                ij += js;
              }// end for j
            }// end if
          }// end for m
          // poll network if enabled
          if (tid == 0 && ptHt)  ptHt->poll(); 
        }// end for i
      break;

      case TriangleLeft:
        for (int i=_diamonds[di][dj].iBegin+_nHalo-_sr; i<_diamonds[di][dj].iEnd+_nHalo-_sr; ++i) {
          for (int m=0; m<nf; ++m) {
            p = _d[(dir+m)%2]; 
            a = _d[(1-dir+m)%2];
            int margin = m * _sr;
            int iLayer = i - m * _sr;
            if (iLayer < _diamonds[di][dj].iEnd - _nHalo + _sr + margin) {
              if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
              int ij = iLayer * is + (_diamonds[di][dj].jBegin + margin) * js;
              int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (_diamonds[di][dj].jBegin+margin-_nHalo) * jsb;
              for (int j=max(_diamonds[di][dj].jBegin+margin, jDHLow); j<min(_diamonds[di][dj].jEnd-margin, jDHHigh); ++j) {
#ifndef SINGLE_NODE
                // unpack
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(2)) 
                  for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kmbs+ijb+k];
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(5)) 
                  for (int k=0; k<_nHalo; ++k)  p[ij+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+ijb+k];
#endif
                #pragma omp simd
                for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                  int ijk = ij + k;
                  double convi, convj, convk;
                  convi = u[ijk] > 0 ?  u[ijk] * weno3(p[ijk-2*is], p[ijk-is], p[ijk], p[ijk+is], q2h)
                                     : -u[ijk] * weno3(p[ijk+2*is], p[ijk+is], p[ijk], p[ijk-is], q2h);
                  convj = v[ijk] > 0 ?  v[ijk] * weno3(p[ijk-2*js], p[ijk-js], p[ijk], p[ijk+js], q2h)
                                     : -v[ijk] * weno3(p[ijk+2*js], p[ijk+js], p[ijk], p[ijk-js], q2h); 
                  convk = w[ijk] > 0 ?  w[ijk] * weno3(p[ijk-2], p[ijk-1], p[ijk], p[ijk+1], q2h)
                                     : -w[ijk] * weno3(p[ijk+2], p[ijk+1], p[ijk], p[ijk-1], q2h);
                  a[ijk] = p[ijk] - _dt * (convi + convj + convk);
                }// end for k
#ifndef SINGLE_NODE
                // pack
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
#endif
                //
                ij += js;
                ijb += jsb;
              }// end for j
            }// end if
          }// end for m
#ifndef SINGLE_NODE
          if (tid == 0 && ptHt)  ptHt->poll(); 
#endif
        }// end for i
      break;

      case Center:
        jBegin = _diamonds[di][dj].jBegin + _nHalo - _sr;
        jEnd   = _diamonds[di][dj].jEnd   - _nHalo + _sr;
        for (int i=_diamonds[di][dj].iBegin+_nHalo-_sr; i<_diamonds[di][dj].iEnd+_nHalo-_sr; ++i) {
          for (int m=0; m<nf; ++m) {
            p = _d[(dir+m)%2]; 
            a = _d[(1-dir+m)%2];
            int margin = m * _sr;
            int iLayer = i - m * _sr;
            if (iLayer < _diamonds[di][dj].iEnd - _nHalo + _sr + margin) {
              if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
              int ij = iLayer * is + (jBegin - margin) * js;
              int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (jBegin-margin-_nHalo) * jsb;
              for (int j=max(jBegin-margin,jDHLow); j<min(jEnd+margin,jDHHigh); ++j) {
#ifndef SINGLE_NODE
                // unpack
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kmbs+ijb+k];
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  p[ij+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+ijb+k];
#endif
                #pragma omp simd
                for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                  int ijk = ij + k;
                  double convi, convj, convk;
                  convi = u[ijk] > 0 ?  u[ijk] * weno3(p[ijk-2*is], p[ijk-is], p[ijk], p[ijk+is], q2h)
                                     : -u[ijk] * weno3(p[ijk+2*is], p[ijk+is], p[ijk], p[ijk-is], q2h);
                  convj = v[ijk] > 0 ?  v[ijk] * weno3(p[ijk-2*js], p[ijk-js], p[ijk], p[ijk+js], q2h)
                                     : -v[ijk] * weno3(p[ijk+2*js], p[ijk+js], p[ijk], p[ijk-js], q2h); 
                  convk = w[ijk] > 0 ?  w[ijk] * weno3(p[ijk-2], p[ijk-1], p[ijk], p[ijk+1], q2h)
                                     : -w[ijk] * weno3(p[ijk+2], p[ijk+1], p[ijk], p[ijk-1], q2h);
                  a[ijk] = p[ijk] - _dt * (convi + convj + convk);
                }// end for k
#ifndef SINGLE_NODE
                // pack
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
#endif
                //
                ij += js;
                ijb += jsb;
              }// end for j
            }// end if
          }// end for m
#ifndef SINGLE_NODE
          if (tid == 0 && ptHt)  ptHt->poll(); 
#endif
        }// end for i
      break;
    }
  }
  else if (_scheme == 1) {
    switch (_diamonds[di][dj].type) {
      case TriangleUp:
        for (int i=iBegin; i<iEnd; ++i) {
          bool isIReadInside = (i+_sr >= _nHalo) && (i+_sr < szs[0]+_nHalo);
          p = _d[dir%2];
          a = _d[(1-dir)%2];
          // unpack [-_sr,0) of the 1st j
          if (mergeUnpackkm && isIReadInside) {
            if (dj > 0) {
              for (int jj=-_sr; jj<0; ++jj) {
                int ij  = i*is + (_diamonds[di][dj].jBegin+jj)*js;
                int ijb = (i-_nHalo)*isb + (_diamonds[di][dj].jBegin+jj-_nHalo)*jsb;
                for (int k=0; k<_nHalo; ++k) p[ij+_sr*is+k] = _hs.rBuf[kmbs+_sr*isb+ijb+k];
              }
            }
            if (dj < _nDiamond[1]-1) {
              for (int jj=0; jj<_sr; ++jj) {
                int ij  = i*is + (_diamonds[di][dj].jEnd+jj)*js;
                int ijb = (i-_nHalo)*isb + (_diamonds[di][dj].jEnd+jj-_nHalo)*jsb;
                for (int k=0; k<_nHalo; ++k) p[ij+_sr*is+k] = _hs.rBuf[kmbs+_sr*isb+ijb+k];
              }
            }
          }
          if (mergeUnpackkp && isIReadInside) {
            if (dj > 0) {
              for (int jj=-_sr; jj<0; ++jj) {
                int ij  = i*is + (_diamonds[di][dj].jBegin+jj)*js;
                int ijb = (i-_nHalo)*isb + (_diamonds[di][dj].jBegin+jj-_nHalo)*jsb;
                for (int k=0; k<_nHalo; ++k) p[ij+_sr*is+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+_sr*isb+ijb+k];
              }
            }
            if (dj < _nDiamond[1]-1) {
              for (int jj=0; jj<_sr; ++jj) {
                int ij  = i*is + (_diamonds[di][dj].jEnd+jj)*js;
                int ijb = (i-_nHalo)*isb + (_diamonds[di][dj].jEnd+jj-_nHalo)*jsb;
                for (int k=0; k<_nHalo; ++k) p[ij+_sr*is+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+_sr*isb+ijb+k];
              }
            }
          }
                
          for (int m=0; m<nf; ++m) {
            p = _d[(dir+m)%2]; 
            a = _d[(1-dir+m)%2];
            int margin = m * _sr;
            int iLayer = i - m * _sr;
            if ((iLayer >= lBound + lSign*margin) && (iLayer <  hBound + hSign*margin)) {
              if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
              int ij = iLayer * is + (_diamonds[di][dj].jBegin + margin) * js;
              for (int j=max(_diamonds[di][dj].jBegin+margin,jDHLow); j<min(_diamonds[di][dj].jEnd-margin,jDHHigh); ++j) {
                // unpack
                int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (j - _nHalo) * jsb;
                if (m == 0 && mergeUnpackkm && isIReadInside && j >= _nHalo && j < szs[1]+_nHalo)
                  for (int k=0; k<_nHalo; ++k)  p[ij+_sr*is+k] = _hs.rBuf[kmbs+ijb+_sr*isb+k];
                if (m == 0 && mergeUnpackkp && isIReadInside && j >= _nHalo && j < szs[1]+_nHalo)
                  for (int k=0; k<_nHalo; ++k)  p[ij+_sr*is+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+ijb+_sr*isb+k];
                // compute
                #pragma omp simd
                for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                  int ijk = ij + k;
                  double convi, convj, convk;
                  convi = u[ijk] > 0 ? u[ijk] * ( 3*p[ijk] - 4*p[ijk-is] + p[ijk-2*is]) * q2h
                                     : u[ijk] * (-3*p[ijk] + 4*p[ijk+is] - p[ijk+2*is]) * q2h;
                  convj = v[ijk] > 0 ? v[ijk] * ( 3*p[ijk] - 4*p[ijk-js] + p[ijk-2*js]) * q2h
                                     : v[ijk] * (-3*p[ijk] + 4*p[ijk+js] - p[ijk+2*js]) * q2h;
                  convk = w[ijk] > 0 ? w[ijk] * ( 3*p[ijk] - 4*p[ijk-1] + p[ijk-2]) * q2h
                                     : w[ijk] * (-3*p[ijk] + 4*p[ijk+1] - p[ijk+2]) * q2h;
                  a[ijk] = p[ijk] - _dt * (convi + convj + convk);
                }// end for k
                // pack
                if (m == nf-1 && mergePackkm)
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
                if (m == nf-1 && mergePackkp)
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
                //
                ij += js;
              }// end for j
            }// end if
          }// end for m
          // poll network if enabled
          if (tid == 0 && ptHt)  ptHt->poll(); 
        }// end for i
      break;

      case TriangleDown:
        jBegin = _diamonds[di][dj].jBegin + _nHalo - _sr;
        jEnd   = _diamonds[di][dj].jEnd   - _nHalo + _sr;
        for (int i=iBegin; i<iEnd; ++i) {
          bool isIReadInside = (i+_sr >= _nHalo) && (i+_sr < szs[0]+_nHalo);
          for (int m=0; m<nf; ++m) {
            p = _d[(dir+m)%2]; 
            a = _d[(1-dir+m)%2];
            int margin = m * _sr;
            int iLayer = i - m * _sr;
            if ((iLayer >= lBound + lSign*margin) && (iLayer <  hBound + hSign*margin)) {
              if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
              int ij = iLayer * is + (jBegin - margin) * js;
              for (int j=max(jBegin-margin,jDHLow); j<min(jEnd+margin,jDHHigh); ++j) {
                // unpack
                int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (j - _nHalo) * jsb;
                if (m == 0 && mergeUnpackkm && isIReadInside && j >= jBegin+_sr && j < jEnd-_sr)
                  for (int k=0; k<_nHalo; ++k)  p[ij+_sr*is+k] = _hs.rBuf[kmbs+ijb+_sr*isb+k];
                if (m == 0 && mergeUnpackkp && isIReadInside && j >= jBegin+_sr && j < jEnd-_sr)
                  for (int k=0; k<_nHalo; ++k)  p[ij+szs[2]+_nHalo+_sr*is+k] = _hs.rBuf[kpbs+ijb+_sr*isb+k];
                // compute
                #pragma omp simd
                for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                  int ijk = ij + k;
                  double convi, convj, convk;
                  convi = u[ijk] > 0 ? u[ijk] * ( 3*p[ijk] - 4*p[ijk-is] + p[ijk-2*is]) * q2h
                                     : u[ijk] * (-3*p[ijk] + 4*p[ijk+is] - p[ijk+2*is]) * q2h;
                  convj = v[ijk] > 0 ? v[ijk] * ( 3*p[ijk] - 4*p[ijk-js] + p[ijk-2*js]) * q2h
                                     : v[ijk] * (-3*p[ijk] + 4*p[ijk+js] - p[ijk+2*js]) * q2h;
                  convk = w[ijk] > 0 ? w[ijk] * ( 3*p[ijk] - 4*p[ijk-1] + p[ijk-2]) * q2h
                                     : w[ijk] * (-3*p[ijk] + 4*p[ijk+1] - p[ijk+2]) * q2h;
                  a[ijk] = p[ijk] - _dt * (convi + convj + convk);
                }// end for k
                // pack
                if (m == nf-1 && mergePackkm)
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
                if (m == nf-1 && mergePackkp)
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
                //
                ij += js;
              }// end for j
            }// end if
          }// end for m
          // poll network if enabled
          if (tid == 0 && ptHt)  ptHt->poll(); 
        }// end for i
      break;

      case TriangleLeft:
        for (int i=_diamonds[di][dj].iBegin+_nHalo-_sr; i<_diamonds[di][dj].iEnd+_nHalo-_sr; ++i) {
          for (int m=0; m<nf; ++m) {
            p = _d[(dir+m)%2]; 
            a = _d[(1-dir+m)%2];
            int margin = m * _sr;
            int iLayer = i - m * _sr;
            if (iLayer < _diamonds[di][dj].iEnd - _nHalo + _sr + margin) {
              if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
              int ij = iLayer * is + (_diamonds[di][dj].jBegin + margin) * js;
              int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (_diamonds[di][dj].jBegin+margin-_nHalo) * jsb;
              for (int j=max(_diamonds[di][dj].jBegin+margin, jDHLow); j<min(_diamonds[di][dj].jEnd-margin, jDHHigh); ++j) {
#ifndef SINGLE_NODE
                // unpack
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kmbs+ijb+k];
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  p[ij+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+ijb+k];
#endif
                #pragma omp simd
                for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                  int ijk = ij + k;
                  double convi, convj, convk;
                  convi = u[ijk] > 0 ? u[ijk] * ( 3*p[ijk] - 4*p[ijk-is] + p[ijk-2*is]) * q2h
                                     : u[ijk] * (-3*p[ijk] + 4*p[ijk+is] - p[ijk+2*is]) * q2h;
                  convj = v[ijk] > 0 ? v[ijk] * ( 3*p[ijk] - 4*p[ijk-js] + p[ijk-2*js]) * q2h
                                     : v[ijk] * (-3*p[ijk] + 4*p[ijk+js] - p[ijk+2*js]) * q2h;
                  convk = w[ijk] > 0 ? w[ijk] * ( 3*p[ijk] - 4*p[ijk-1] + p[ijk-2]) * q2h
                                     : w[ijk] * (-3*p[ijk] + 4*p[ijk+1] - p[ijk+2]) * q2h;
                  a[ijk] = p[ijk] - _dt * (convi + convj + convk);
                }// end for k
#ifndef SINGLE_NODE
                // pack
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
#endif
                //
                ij += js;
                ijb += jsb;
              }// end for j
            }// end if
          }// end for m
#ifndef SINGLE_NODE
          if (tid == 0 && ptHt)  ptHt->poll(); 
#endif
        }// end for i
      break;

      case Center:
        jBegin = _diamonds[di][dj].jBegin + _nHalo - _sr;
        jEnd   = _diamonds[di][dj].jEnd   - _nHalo + _sr;
        for (int i=_diamonds[di][dj].iBegin+_nHalo-_sr; i<_diamonds[di][dj].iEnd+_nHalo-_sr; ++i) {
          for (int m=0; m<nf; ++m) {
            p = _d[(dir+m)%2]; 
            a = _d[(1-dir+m)%2];
            int margin = m * _sr;
            int iLayer = i - m * _sr;
            if (iLayer < _diamonds[di][dj].iEnd - _nHalo + _sr + margin) {
              if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
              int ij = iLayer * is + (jBegin - margin) * js;
              int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (jBegin-margin-_nHalo) * jsb;
              for (int j=max(jBegin-margin,jDHLow); j<min(jEnd+margin,jDHHigh); ++j) {
#ifndef SINGLE_NODE
                // unpack
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kmbs+ijb+k];
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  p[ij+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+ijb+k];
#endif
                #pragma omp simd
                for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                  int ijk = ij + k;
                  double convi, convj, convk;
                  convi = u[ijk] > 0 ? u[ijk] * ( 3*p[ijk] - 4*p[ijk-is] + p[ijk-2*is]) * q2h
                                     : u[ijk] * (-3*p[ijk] + 4*p[ijk+is] - p[ijk+2*is]) * q2h;
                  convj = v[ijk] > 0 ? v[ijk] * ( 3*p[ijk] - 4*p[ijk-js] + p[ijk-2*js]) * q2h
                                     : v[ijk] * (-3*p[ijk] + 4*p[ijk+js] - p[ijk+2*js]) * q2h;
                  convk = w[ijk] > 0 ? w[ijk] * ( 3*p[ijk] - 4*p[ijk-1] + p[ijk-2]) * q2h
                                     : w[ijk] * (-3*p[ijk] + 4*p[ijk+1] - p[ijk+2]) * q2h;
                  a[ijk] = p[ijk] - _dt * (convi + convj + convk);
                }// end for k
#ifndef SINGLE_NODE
                // pack
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
#endif
                //
                ij += js;
                ijb += jsb;
              }// end for j
            }// end if
          }// end for m
#ifndef SINGLE_NODE
          if (tid == 0 && ptHt)  ptHt->poll(); 
#endif
        }// end for i
      break;
    }
  }

  // reset pipelen's range
  _diamonds[di][dj].iBegin -= pipe * pipeLen;
  _diamonds[di][dj].iEnd   -= pipe * pipeLen;
}


double weno5(double vm3, double vm2, double vm1, double v, double v1, double v2, double qh)
{
  double r13q12 = 13.0/12.0, q4 = 0.25, q6 = 1.0/6.0, eps = 1.0E-8;
  double dv[5]  = {(vm2 - vm3)*qh, (vm1 - vm2)*qh, (v - vm1)*qh, (v1 - v)*qh, (v2 - v1)*qh};
  double eno[3], beta[3], w[3], qSum;

  eno[0] =  ( 2.0*dv[0] - 7.0*dv[1] + 11.0*dv[2]) * q6;
  eno[1] =  (-    dv[1] + 5.0*dv[2] +  2.0*dv[3]) * q6;
  eno[2] =  ( 2.0*dv[2] + 5.0*dv[3] -      dv[4]) * q6;

  beta[0] = r13q12 * (dv[0] - 2.0*dv[1] + dv[2]) * (dv[0] - 2.0*dv[1] + dv[2]) 
          + q4 * (dv[0] - 4.0*dv[1] + 3.0*dv[2]) * (dv[0] - 4.0*dv[1] + 3.0*dv[2]);
  //
  beta[1] = r13q12 * (dv[1] - 2.0*dv[2] + dv[3]) * (dv[1] - 2.0*dv[2] + dv[3])
          + q4 * (dv[1] - dv[3]) * (dv[1] - dv[3]);
  //
  beta[2] = r13q12 * (dv[2] - 2.0*dv[3] + dv[4]) * (dv[2] - 2.0*dv[3] + dv[4])
          + q4 * (3.0*dv[2] - 4.0*dv[3] + dv[4]) * (3.0*dv[2] - 4.0*dv[3] + dv[4]);

  beta[0] = (beta[0] + eps) * (beta[0] + eps);
  beta[1] = (beta[1] + eps) * (beta[1] + eps);
  beta[2] = (beta[2] + eps) * (beta[2] + eps);
  w[0]    = 0.1 * beta[1] * beta[2];
  w[1]    = 0.6 * beta[2] * beta[0];
  w[2]    = 0.4 * beta[0] * beta[1];
  qSum    = 1.0 / (w[0] + w[1] + w[2]);
  w[0]   *= qSum;
  w[1]   *= qSum;
  w[2]   *= qSum;

  return w[0]*eno[0] + w[1]*eno[1] + w[2]*eno[2];
} 


double weno3(double vm2, double vm1, double v, double v1, double q2h)
{
  double eps = 1.0e-8;
  double a   = (eps + (v - 2.0*vm1 + vm2) * (v - 2.0*vm1 + vm2));
  double b   = (eps + (v1 - 2.0*v + vm1) * (v1 - 2.0*v + vm1));

  b = b * b;
  a = a * a;

  double w = b / (b + 2.0*a); 
  return ((1.0-w) * (v1 - vm1) + w * (3.0*v - 4.0*vm1 + vm2)) * q2h;
}


void WenoSolver::_nullify_layer(double *c, int basel, int jb, int je, int jsl, int kb, int ke)
{
  for (int j=jb; j<je; ++j) {
    #pragma omp simd
    for (int k=kb; k<ke; ++k) c[basel+k] = 0.0;
    basel += jsl;
  }
}
