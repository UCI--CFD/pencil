#include "WJSolver.h"
#include "constants.h"
#include "Timer.h"
#include <cassert>

using std::max;
using std::min;

WJSolver::WJSolver(DomainDecomp& dd, UniMesh& mesh, int nh): 
  Solver(dd, mesh, nh)
{};


WJSolver::~WJSolver()
{
  for (int i=0; i<3; ++i) delete [] _d[i];
  delete [] _d;
}


int WJSolver::setup()
{
  // set halo store, transfer, etc, nVar, pipe are set from option
  Solver::setup();
  
  _sr = _scheme%2 == 0 ? 1 : 2; // stencil

  // allocate data
  int szs[3]; _ptDd->get_local_size(szs);
  _d = new double* [3];
  for (int i=0; i<3; ++i) 
    _d[i] = new double [(szs[0]+2*_nHalo) *(szs[1]+2*_nHalo)*(szs[2]+2*_nHalo)];

  // halo variable
  _haloVar.push_back(vector<int>({1}));
  _haloVar.push_back(vector<int>({0}));

  _setup_diamonds();

  return 0;
}


void WJSolver::set_from_option()
{
  Solver::set_from_option();
}


int WJSolver::init()
{
  int szs[3], starts[3];
  _ptDd->get_local_size(szs);
  _ptDd->get_local_start(starts);
  int jStrd = szs[2] + 2*_nHalo;
  int iStrd = jStrd * (szs[1] + 2*_nHalo);
  int totSz = (szs[0]+2*_nHalo) * iStrd; 
  std::fill(_d[0], _d[0]+totSz, 0.0);
  std::fill(_d[1], _d[1]+totSz, 0.0);
  //std::fill(_d[2], _d[2]+totSz, 3.0);

  for (int i=0; i<szs[0]+2*_nHalo; ++i) {
    for (int j=0; j<szs[1]+2*_nHalo; ++j) {
      for (int k=0; k<szs[2]+2*_nHalo; ++k) {
        int ijk = i*iStrd + j*jStrd + k;
        _d[2][ijk] = -12.0*PI*PI*sin(2*PI*(i-_nHalo+starts[0]+0.5)*_h)
          * sin(2*PI*(j-_nHalo+starts[1]+0.5)*_h) 
          * sin(2*PI*(k-_nHalo+starts[2]+0.5)*_h);
      }
    }
  }

  for (int i=0; i<_ptDd->num_thread(); ++i)
    for (int j=0; j<TIMER_MAX_ITEM; ++j)
      std::fill(_t[i][j], _t[i][j]+TIMER_MAX_SIZE, 0.0);

  // init buffer
  std::fill(_hs.sBuf, _hs.sBuf+_hs.bufSize, 0.0);
  std::fill(_hs.rBuf, _hs.rBuf+_hs.bufSize, 0.0);

  return 0;
}


int WJSolver::init_numa()
{
  int tid = omp_get_thread_num();
  int rngs[6], tRngs[6], starts[3];
  _ptDd->get_local_size(rngs);
  _ptDd->get_local_start(starts);
  int jStrd = rngs[2] + 2*_nHalo, iStrd = jStrd * (rngs[1] + 2*_nHalo);
  for (int i=0; i<3; ++i) {
    rngs[i+3] = rngs[i]+_nHalo;
    rngs[i]   = _nHalo;
  }

  // pipeline (+ovlp) only init with nThrd-1 threads, numa
  if ((_mode == PipelineTile && !_isDiamond)) {
    int nThrd = _ptDd->num_thread();
    if (tid < nThrd-1) {
      // thread range include halo
      int nThrds[3] = {1, nThrd-1, 1};
      compute_thread_range(rngs, nThrds, tid, tRngs);
      for (int i=0; i<3; ++i) {
        if (tRngs[i] == rngs[i]) tRngs[i] = 0;
        if (tRngs[i+3] == rngs[i+3]) tRngs[i+3] += _nHalo;
      }
      // numa init
      for (int i=tRngs[0]; i<tRngs[3]; ++i) {
        for (int j=tRngs[1]; j<tRngs[4]; ++j) {
          for (int k=tRngs[2]; k<tRngs[5]; ++k) {
            int ijk = i*iStrd + j*jStrd + k;
            _d[0][ijk] = 0.0;
            _d[1][ijk] = 0.0;
            _d[2][ijk] = -12.0*PI*PI*sin(2*PI*(i-_nHalo+starts[0]+0.5)*_h)
                       * sin(2*PI*(j-_nHalo+starts[1]+0.5)*_h) 
                       * sin(2*PI*(k-_nHalo+starts[2]+0.5)*_h);
          }
        }
      }// end for i
    }// end if tid
  }// end if pipeline
  // all threads init, numa
  else {
    // thread range include halo
    _ptDd->get_thread_range(tid, tRngs);
    for (int i=0; i<6; ++i)  tRngs[i] += _nHalo;
    for (int i=0; i<3; ++i) {
      if (tRngs[i] == rngs[i]) tRngs[i]  = 0;
      if (tRngs[i+3] == rngs[i+3]) tRngs[i+3] += _nHalo;
    }
    // numa init
    for (int i=tRngs[0]; i<tRngs[3]; ++i) {
      for (int j=tRngs[1]; j<tRngs[4]; ++j) {
        for (int k=tRngs[2]; k<tRngs[5]; ++k) {
          int ijk = i*iStrd + j*jStrd + k;
          _d[0][ijk] = 0.0;
          _d[1][ijk] = 0.0;
          _d[2][ijk] = -12.0*PI*PI*sin(2*PI*(i-_nHalo+starts[0]+0.5)*_h)
                     * sin(2*PI*(j-_nHalo+starts[1]+0.5)*_h) 
                     * sin(2*PI*(k-_nHalo+starts[2]+0.5)*_h);
        }
      }
    }
  }// end else

  // init time
  for (int i=0; i<TIMER_MAX_ITEM; ++i)
    for (int j=0; j<TIMER_MAX_SIZE; ++j)
      _t[tid][i][j] = 0.0;

  // init buffer
  if (_isBufNuma) {
    int nPipe = _hs.num_pipe();
    if (nPipe == 1) {
      _hts[0].init_buffer_numa(_hs);
    }
    else {
      for (int i=0; i<nPipe; ++i)
        _hts[i].init_buffer_numa(_hs);
      for (int i=0; i<nPipe+2; ++i)
        _hps[i].init_buffer_numa(_hs);
    }
  }

  return 0;
}


void WJSolver::_compute_range_rectangle(int rngs[6], int dir, double *c, int isl, int jsl,
                                        int pipe, HaloTransfer* ptHt)
{
  int tid    = omp_get_thread_num();
  int szs[3];
  _ptDd->get_local_size(szs);
  int js  = szs[2] + 2*_nHalo, is  = js * (szs[1] + 2*_nHalo);
  int sw = 2*_sr + 1;
  int jsb = _nHalo, isb = _nHalo*szs[1], pipeLen = szs[0]/_hs.num_pipe();
  int kmbs = _hs.kmBufStart[pipe], kpbs = _hs.kpBufStart[pipe], offset, offsetb;
  bool mergePackkm = _ptDd->need_comm(2) && _bypassKHalo;
  bool mergePackkp = _ptDd->need_comm(5) && _bypassKHalo;
  bool mergeUnpackkm = mergePackkm && _hs.num_pipe() == 1;
  bool mergeUnpackkp = mergePackkp && _hs.num_pipe() == 1;

  double h2 = _h * _h, w0 = 2.0/3.0, w1 = 1.0/3.0;
  double c1, c2, c11, c111, cRhs, q90=1.0/90.0, q128=1.0/128.0;
  double *p = _d[0], *a = _d[1], *b = _d[2];

  // swap pointers after each iteration, dir denotes even or odd iteration
  if (dir == 0) {
    p = _d[0];
    a = _d[1];
  }
  else {
    p = _d[1];
    a = _d[0];
  }

  int iBegin = max(rngs[0]-_nHalo+_sr, ((_dhOff && !_ptDd->need_comm(0))? _nHalo : -100));
  int iEnd   = min(rngs[3]+_nHalo-_sr, ((_dhOff && !_ptDd->need_comm(3))? szs[0]+_nHalo : 10000));
  int jBegin = max(rngs[1]-_nHalo+_sr, ((_dhOff && !_ptDd->need_comm(1))? _nHalo : -100));
  int jEnd   = min(rngs[4]+_nHalo-_sr, ((_dhOff && !_ptDd->need_comm(4))? szs[1]+_nHalo : 10000));
  int kBegin = max(rngs[2]-_nHalo+_sr, ((_dhOff && !_ptDd->need_comm(2))? _nHalo : -100));
  int kEnd   = min(rngs[5]+_nHalo-_sr, ((_dhOff && !_ptDd->need_comm(5))? szs[2]+_nHalo : 10000));
  int jlDHLow  = (_dhOff && !_ptDd->need_comm(1) && rngs[1] <= _nHalo) ? _nHalo : -100;
  int jlDHHgih = (_dhOff && !_ptDd->need_comm(4) && rngs[4] >= szs[1]+_nHalo) ? rngs[4]-rngs[1]+_nHalo : 10000;
  int klDHLow  = (_dhOff && !_ptDd->need_comm(2) && rngs[2] <= _nHalo) ? _nHalo : -100;
  int klDHHgih = (_dhOff && !_ptDd->need_comm(5) && rngs[5] >= szs[2]+_nHalo) ? rngs[5]-rngs[2]+_nHalo : 10000;

  // unpack i=nHalo to nHalo+_sr if deep halo is off
  // HARDCODE: assume no communication in i
  if (_dhOff) {
    if (mergeUnpackkm) {
      for (int i=rngs[0]; i<rngs[0]+_sr; ++i) {
        for (int j=max(jBegin-_sr,_nHalo); j<min(jEnd+_sr,szs[1]+_nHalo); ++j) {
          int ij  =  i*is + j*js;
          int ijb = (i - _nHalo)*isb + (j - _nHalo) * jsb;
          for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kmbs+ijb+k];
        }
      }
    }
    if (mergeUnpackkp) {
      for (int i=rngs[0]; i<rngs[0]+_sr; ++i) {
        for (int j=max(jBegin-_sr,_nHalo); j<min(jEnd+_sr,szs[1]+_nHalo); ++j) {
          int ij  =  i*is + j*js + szs[2] + _nHalo;
          int ijb = (i - _nHalo)*isb + (j - _nHalo) * jsb;
          for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kpbs+ijb+k];
        }
      }
    }
  }

  switch (_scheme) {
    case 0:
      c1    = w0/6.0;
      cRhs  = -h2*w0/6.0;
      for (int i=rngs[0]-_nHalo+_sr; i<rngs[3]+_nHalo-_sr; ++i) {
        // p -> c
        int ij    = i * is + jBegin * js;
        int basel = (i-1)%sw*isl + (jBegin-rngs[1]+_nHalo)*jsl - (rngs[2]-_nHalo);
        bool isIReadInside = (i+_sr >= _nHalo) && (i+_sr < szs[0]+_nHalo);
        if (_dhOff && (i < _nHalo || i >= szs[0]+_nHalo)) {
          _nullify_layer(c, basel, jBegin, jEnd, jsl, kBegin, kEnd);
        }
        else {
          for (int j=jBegin; j<jEnd; ++j) {
            // unpack
            int ijb   = (i - _nHalo - pipe*pipeLen)*isb + (j - _nHalo) * jsb;
            if (mergeUnpackkm && isIReadInside && j >= _nHalo && j < szs[1]+_nHalo)
              for (int k=0; k<_nHalo; ++k)  p[ij+_sr*is+k] = _hs.rBuf[kmbs+ijb+_sr*isb+k];
            if (mergeUnpackkp && isIReadInside && j >= _nHalo && j < szs[1]+_nHalo)
              for (int k=0; k<_nHalo; ++k)  p[ij+_sr*is+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+ijb+_sr*isb+k];
            #pragma omp simd
            for (int k=kBegin; k<kEnd; ++k) {
              int ijk = ij + k;
              c[basel+k]
                = c1 * (p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] + p[ijk-is] + p[ijk+is])
                + cRhs * b[ijk] + w1 * p[ijk];
            }
            ij    += js;
            basel += jsl;
          }
        }// end else
        // c -> c
        int nf     = _nHalo / _sr; // # fuesd iterations
        int iLayer = i - _sr;                      // current iteration layer in domain
        int iLow   = rngs[0] - _nHalo + 2*_sr;     // i lower bound for current iter
        int iHigh  = rngs[3] + _nHalo - 1 - 2*_sr; // i higher bound 
        int margin = 2 * _sr;
        for (int m=1; m<nf-1; ++m) {
          int jlBegin = max(margin, jlDHLow);
          int jlEnd   = min(rngs[4]-rngs[1]+2*_nHalo-margin,jlDHHgih);
          if (iLayer >= iLow && iLayer <= iHigh) {
            int ijl   = ((iLayer-1+sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
            int im1jl = ((iLayer-2+sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
            int ip1jl = ((iLayer  +sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
            int ijlTo = ijl + sw*isl;
            int ij    = iLayer*is + (rngs[1]-_nHalo+jlBegin)*js;
            if (_dhOff && (iLayer < _nHalo || iLayer >= szs[0]+_nHalo)) {
              for (int j=jlBegin; j<jlEnd; ++j) {
                int klBegin = max(margin,klDHLow), klEnd = min(rngs[5]-rngs[2]+2*_nHalo-margin,klDHHgih);
                #pragma omp simd
                for (int k=klBegin; k<klEnd; ++k) c[ijlTo+k] = 0.0;
                ijlTo += jsl;
              }// end for j
            }
            else {
              for (int j=jlBegin; j<jlEnd; ++j) {
                int klBegin = max(margin,klDHLow), klEnd = min(rngs[5]-rngs[2]+2*_nHalo-margin,klDHHgih);
                #pragma omp simd
                for (int k=klBegin; k<klEnd; ++k) {
                  int ijkl = ijl + k;
                  c[ijlTo+k] = c1 * (c[ijkl-1] + c[ijkl+1] + c[ijkl-jsl] + c[ijkl+jsl] + c[im1jl+k] + c[ip1jl+k])
                             + cRhs * b[ij+k+rngs[2]-_nHalo] + w1 * c[ijkl];
                }// end for k
                ij    += js;
                ijl   += jsl; im1jl += jsl; ip1jl += jsl; ijlTo += jsl;
              }// end for j
            }
            margin += _sr;
            iLayer -= _sr;
            iLow   += _sr;
            iHigh  -= _sr;
          }// end if           
        }// end for m
        // c -> a
        if (iLayer >= iLow && iLayer <= iHigh) {
          int ijl   = ((iLayer-1+sw)%sw + (nf-2)*sw) * isl + margin*jsl;
          int im1jl = ((iLayer-2+sw)%sw + (nf-2)*sw) * isl + margin*jsl;
          int ip1jl = ((iLayer  +sw)%sw + (nf-2)*sw) * isl + margin*jsl;
          int ij    = iLayer*is + rngs[1]*js;
          int ijb   = (iLayer - _nHalo - pipe*pipeLen)*isb + (rngs[1] - _nHalo) * jsb;
          for (int j=_nHalo; j<rngs[4]-rngs[1]+_nHalo; ++j) {
            #pragma omp simd
            for (int k=_nHalo; k<rngs[5]-rngs[2]+_nHalo; ++k) {
              int ijkl = ijl + k, ijk  = ij  + k + rngs[2] - _nHalo;
              a[ijk] = c1 * (c[ijkl-1] + c[ijkl+1] + c[ijkl-jsl] + c[ijkl+jsl] + c[im1jl+k] + c[ip1jl+k])
                     + cRhs * b[ijk] + w1 * c[ijkl];
            }// end for k
            // pack
            if (mergePackkm)
              for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
            if (mergePackkp)
              for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
            //
            ij  += js;
            ijl += jsl; im1jl += jsl; ip1jl += jsl;
            ijb += jsb;
          }// end for j
        }// end if      
        // poll network if enabled
        if (tid == 0 && ptHt)  ptHt->poll(); 
      }// end for i
    break;
    case 1:
      c1 = 16.0*w0/90.0;
      c2 = -w0/90.0;
      cRhs = -12.0*h2*w0/90.0;
      offset = _sr * is;
      offsetb = _sr * isb;
      for (int i=rngs[0]-_nHalo+_sr; i<rngs[3]+_nHalo-_sr; ++i) {
        // p -> c
        int ij    = i * is + jBegin * js;
        int basel = (i-1)%sw*isl + (jBegin-rngs[1]+_nHalo)*jsl - (rngs[2]-_nHalo);
        bool isIReadInside = (i+_sr >= _nHalo) && (i+_sr < szs[0]+_nHalo);
        if (_dhOff && (i < _nHalo || i >= szs[0]+_nHalo)) {
          _nullify_layer(c, basel, jBegin, jEnd, jsl, kBegin, kEnd);
        }
        else {
          for (int j=jBegin; j<jEnd; ++j) {
            // unpack
            int ijb   = (i - _nHalo - pipe*pipeLen)*isb + (j - _nHalo) * jsb;
            if (mergeUnpackkm && isIReadInside && j >= _nHalo && j < szs[1]+_nHalo)
              for (int k=0; k<_nHalo; ++k)  p[ij+offset+k] = _hs.rBuf[kmbs+ijb+offsetb+k];
            if (mergeUnpackkp && isIReadInside && j >= _nHalo && j < szs[1]+_nHalo)
              for (int k=0; k<_nHalo; ++k)  p[ij+offset+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+ijb+offsetb+k];
            #pragma omp simd
            for (int k=kBegin; k<kEnd; ++k) {
              int ijk = ij + k;
              c[basel+k]
                = c1 * (p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] + p[ijk-is] + p[ijk+is]) \
                + c2 * (p[ijk-2] + p[ijk+2] + p[ijk-2*js] + p[ijk+2*js] + p[ijk-2*is] + p[ijk+2*is]) \
                + cRhs * b[ijk] + w1 * p[ijk];
            }
            ij    += js;
            basel += jsl;
          }
        }// end else
        // c -> c
        int nf     = _nHalo / _sr; // # fuesd iterations
        int iLayer = i - _sr;                      // current iteration layer in domain
        int iLow   = rngs[0] - _nHalo + 2*_sr;     // i lower bound for current iter
        int iHigh  = rngs[3] + _nHalo - 1 - 2*_sr; // i higher bound 
        int margin = 2 * _sr;
        for (int m=1; m<nf-1; ++m) {
          int jlBegin = max(margin, jlDHLow);
          int jlEnd   = min(rngs[4]-rngs[1]+2*_nHalo-margin,jlDHHgih);
          if (iLayer >= iLow && iLayer <= iHigh) {
            int ijl   = ((iLayer-1+sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
            int im1jl = ((iLayer-2+sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
            int im2jl = ((iLayer-3+sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
            int ip1jl = ((iLayer  +sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
            int ip2jl = ((iLayer+1+sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
            int ijlTo = ijl + sw*isl;
            int ij    = iLayer*is + (rngs[1]-_nHalo+jlBegin)*js;
            if (_dhOff && (iLayer < _nHalo || iLayer >= szs[0]+_nHalo)) {
              for (int j=jlBegin; j<jlEnd; ++j) {
                int klBegin = max(margin,klDHLow), klEnd = min(rngs[5]-rngs[2]+2*_nHalo-margin,klDHHgih);
                #pragma omp simd
                for (int k=klBegin; k<klEnd; ++k) c[ijlTo+k] = 0.0;
                ijlTo += jsl;
              }// end for j
            }
            else {
              for (int j=jlBegin; j<jlEnd; ++j) {
                int klBegin = max(margin,klDHLow), klEnd = min(rngs[5]-rngs[2]+2*_nHalo-margin,klDHHgih);
                #pragma omp simd
                for (int k=klBegin; k<klEnd; ++k) {
                  int ijkl = ijl + k;
                  c[ijlTo+k] = c1 * (c[ijkl-1] + c[ijkl+1] + c[ijkl-  jsl] + c[ijkl+  jsl] + c[im1jl+k] + c[ip1jl+k])
                             + c2 * (c[ijkl-2] + c[ijkl+2] + c[ijkl-2*jsl] + c[ijkl+2*jsl] + c[im2jl+k] + c[ip2jl+k])
                             + cRhs * b[ij+k+rngs[2]-_nHalo] + w1 * c[ijkl];
                }// end for k
                ij  += js;
                ijl += jsl; im1jl += jsl; im2jl += jsl; ip1jl += jsl; ip2jl += jsl; ijlTo += jsl;
              }// end for j
            }
            margin += _sr;
            iLayer -= _sr;
            iLow   += _sr;
            iHigh  -= _sr;
          }// end if           
        }// end for m
        // c -> a
        if (iLayer >= iLow && iLayer <= iHigh) {
          int ijl   = ((iLayer-1+sw)%sw + (nf-2)*sw) * isl + margin*jsl;
          int im1jl = ((iLayer-2+sw)%sw + (nf-2)*sw) * isl + margin*jsl;
          int im2jl = ((iLayer-3+sw)%sw + (nf-2)*sw) * isl + margin*jsl;
          int ip1jl = ((iLayer  +sw)%sw + (nf-2)*sw) * isl + margin*jsl;
          int ip2jl = ((iLayer+1+sw)%sw + (nf-2)*sw) * isl + margin*jsl;
          int ij    = iLayer*is + rngs[1]*js;
          int ijb   = (iLayer - _nHalo - pipe*pipeLen)*isb + (rngs[1] - _nHalo) * jsb;
          for (int j=_nHalo; j<rngs[4]-rngs[1]+_nHalo; ++j) {
            #pragma omp simd
            for (int k=_nHalo; k<rngs[5]-rngs[2]+_nHalo; ++k) {
              int ijkl = ijl + k, ijk  = ij  + k + rngs[2] - _nHalo;
              a[ijk] = c1 * (c[ijkl-1] + c[ijkl+1] + c[ijkl-  jsl] + c[ijkl+  jsl] + c[im1jl+k] + c[ip1jl+k])
                     + c2 * (c[ijkl-2] + c[ijkl+2] + c[ijkl-2*jsl] + c[ijkl+2*jsl] + c[im2jl+k] + c[ip2jl+k])
                     + cRhs * b[ijk] + w1 * c[ijkl];
            }// end for k
            // pack
            if (mergePackkm)
              for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
            if (mergePackkp)
              for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
            //
            ij  += js;
            ijl += jsl; im1jl += jsl; im2jl += jsl; ip1jl += jsl; ip2jl += jsl;
            ijb += jsb;
          }// end for j
        }// end if      
        // poll network if enabled
        if (tid == 0 && ptHt)  ptHt->poll(); 
      }// end for i
    break;
    case 2:
      c1   = 14.0*w0*q128;
      c11  = 3.0*w0*q128;
      c111 = w0*q128;
      cRhs = -30.0*w0*h2*q128;
      offset = _sr * (is + js); 
      offsetb = _sr * (isb + jsb);
      for (int i=rngs[0]-_nHalo+_sr; i<rngs[3]+_nHalo-_sr; ++i) {
        // p -> c
        int ij    = i * is + jBegin * js;
        int basel = (i-1)%sw*isl + (jBegin-rngs[1]+_nHalo)*jsl - (rngs[2]-_nHalo);
        bool isIReadInside =  (i+_sr >= _nHalo) && (i+_sr < szs[0]+_nHalo);
        if (_dhOff && (i < _nHalo || i >= szs[0]+_nHalo)) {
          _nullify_layer(c, basel, jBegin, jEnd, jsl, kBegin, kEnd);
        }
        else {
          // upack (i+1, [jBegin-_sr, jBegin+_sr-1]) if needed
          int ijb = (i - _nHalo - pipe*pipeLen)*isb + (jBegin - _nHalo) * jsb;
          int nInitUpack = jBegin > _nHalo ? 2*_sr : (_dhOff ? _sr : 0);
          if (mergeUnpackkm && isIReadInside) {
            for (int j=1; j<=nInitUpack; ++j) 
              for (int k=0; k<_nHalo; ++k)  
                p[ij+offset-j*js+k] = _hs.rBuf[kmbs+ijb+offsetb-j*jsb+k];

          }
          if (mergeUnpackkp && isIReadInside) {
            for (int j=1; j<=nInitUpack; ++j) 
              for (int k=0; k<_nHalo; ++k)  
                p[ij+_nHalo+szs[2]+offset-j*js+k] = _hs.rBuf[kpbs+ijb+offsetb-j*jsb+k];

          }
          //
          for (int j=jBegin; j<jEnd; ++j) {
            // unpack
            int ijb   = (i - _nHalo - pipe*pipeLen)*isb + (j - _nHalo) * jsb;
            if (mergeUnpackkm && isIReadInside && j+_sr >= _nHalo && j+_sr < szs[1]+_nHalo)
              for (int k=0; k<_nHalo; ++k)  p[ij+offset+k] = _hs.rBuf[kmbs+ijb+offsetb+k];
            if (mergeUnpackkp && isIReadInside && j+_sr >= _nHalo && j+_sr < szs[1]+_nHalo)
              for (int k=0; k<_nHalo; ++k)  p[ij+offset+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+ijb+offsetb+k];
            // compute
            #pragma omp simd
            for (int k=kBegin; k<kEnd; ++k) {
              int ijk = ij + k;
              c[basel+k]
                = c1  * ( p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] + p[ijk-is] + p[ijk+is])
                + c11 * ( p[ijk-is-js] + p[ijk-is+js] + p[ijk+is-js] + p[ijk+is+js] \
                        + p[ijk-js-1]  + p[ijk-js+1]  + p[ijk+js-1]  + p[ijk+js+1]  \
                        + p[ijk-is-1]  + p[ijk-is+1]  + p[ijk+is-1]  + p[ijk+is+1]) \
                + c111* ( p[ijk-is-js-1] + p[ijk-is-js+1] + p[ijk-is+js-1] + p[ijk-is+js+1] \
                        + p[ijk+is-js-1] + p[ijk+is-js+1] + p[ijk+is+js-1] + p[ijk+is+js+1])
                + cRhs * b[ijk] + w1 * p[ijk];
            }
            ij    += js;
            basel += jsl;
          }
        }
        // c -> c
        int nf     = _nHalo / _sr; // # fuesd iterations
        int iLayer = i - _sr;                      // current iteration layer in domain
        int iLow   = rngs[0] - _nHalo + 2*_sr;     // i lower bound for current iter
        int iHigh  = rngs[3] + _nHalo - 1 - 2*_sr; // i higher bound 
        int margin = 2 * _sr;
        for (int m=1; m<nf-1; ++m) {
          int jlBegin = max(margin, jlDHLow);
          int jlEnd   = min(rngs[4]-rngs[1]+2*_nHalo-margin,jlDHHgih);
          if (iLayer >= iLow && iLayer <= iHigh) {
            int ijl   = ((iLayer-1+sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
            int im1jl = ((iLayer-2+sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
            int ip1jl = ((iLayer  +sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
            int ijlTo = ijl + sw*isl;
            int ij    = iLayer*is + (rngs[1]-_nHalo+jlBegin)*js;
            if (_dhOff && (iLayer < _nHalo || iLayer >= szs[0]+_nHalo)) {
              for (int j=jlBegin; j<jlEnd; ++j) {
                int klBegin = max(margin,klDHLow), klEnd = min(rngs[5]-rngs[2]+2*_nHalo-margin,klDHHgih);
                #pragma omp simd
                for (int k=klBegin; k<klEnd; ++k) c[ijlTo+k] = 0.0;
                ijlTo += jsl;
              }// end for j
            }
            else {
              for (int j=jlBegin; j<jlEnd; ++j) {
                int klBegin = max(margin,klDHLow), klEnd = min(rngs[5]-rngs[2]+2*_nHalo-margin,klDHHgih);
                #pragma omp simd
                for (int k=klBegin; k<klEnd; ++k) {
                  int ijkl = ijl + k;
                  c[ijlTo+k] = c1  * ( c[ijkl-1] + c[ijkl+1] + c[ijkl-jsl] + c[ijkl+jsl] + c[im1jl+k] + c[ip1jl+k])
                             + c11 * ( c[im1jl+k-jsl] + c[im1jl+k+jsl] + c[ip1jl+k-jsl] + c[ip1jl+k+jsl]
                                     + c[ijkl-jsl-1]  + c[ijkl-jsl+1]  + c[ijkl+jsl-1]  + c[ijkl+jsl+1]
                                     + c[im1jl+k-1]   + c[im1jl+k+1]   + c[ip1jl+k-1]   + c[ip1jl+k+1])
                             + c111* ( c[im1jl+k-jsl-1] + c[im1jl+k-jsl+1] + c[im1jl+k+jsl-1] + c[im1jl+k+jsl+1]
                                     + c[ip1jl+k-jsl-1] + c[ip1jl+k-jsl+1] + c[ip1jl+k+jsl-1] + c[ip1jl+k+jsl+1])
                             + cRhs * b[ij+k+rngs[2]-_nHalo] + w1 * c[ijkl];
              //if (ij+k+rngs[2]-_nHalo==3*is+3*js+5&&_ptDd->is_root())
                //std::cout << c[ijlTo+k] << " " << c[ijkl] << " " 
                  //<< c[im1jl-jsl+k-1] << " " << c[im1jl-jsl+k] << " " << c[im1jl-jsl+k+1] << " "
                  //<< c[im1jl+k-1] << " " << c[im1jl+k] << " " << c[im1jl+k+1] << " "
                  //<< c[im1jl+jsl+k-1] << " " << c[im1jl+jsl+k] << " " << c[im1jl+jsl+k+1] << " "
                  //<< c[ijkl-jsl-1] << " " << c[ijkl-jsl] << " " << c[ijkl-jsl+1] << " "
                  //<< c[ijkl-1] << " " << c[ijkl+1] << " "
                  //<< c[ijkl+jsl-1] << " " << c[ijkl+jsl] << " " << c[ijkl+jsl+1] << " "
                  //<< c[ip1jl-jsl+k-1] << " " << c[ip1jl-jsl+k] << " " << c[ip1jl-jsl+k+1] << " "
                  //<< c[ip1jl+k-1] << " " << c[ip1jl+k] << " " << c[ip1jl+k+1] << " "
                  //<< c[ip1jl+jsl+k-1] << " " << c[ip1jl+jsl+k] << " " << c[ip1jl+jsl+k+1] << std::endl;
                }// end for k
                ij    += js;
                ijl   += jsl; im1jl += jsl; ip1jl += jsl; ijlTo += jsl;
              }// end for j
            }
            margin += _sr;
            iLayer -= _sr;
            iLow   += _sr;
            iHigh  -= _sr;
          }// end if           
        }// end for m
        // c -> a
        if (iLayer >= iLow && iLayer <= iHigh) {
          if (iLayer < iBegin || iLayer >= iEnd)  continue;
          int ijl   = ((iLayer-1+sw)%sw + (nf-2)*sw) * isl + margin*jsl;
          int im1jl = ((iLayer-2+sw)%sw + (nf-2)*sw) * isl + margin*jsl;
          int ip1jl = ((iLayer  +sw)%sw + (nf-2)*sw) * isl + margin*jsl;
          int ij    = iLayer*is + rngs[1]*js;
          int ijb   = (iLayer - _nHalo - pipe*pipeLen)*isb + (rngs[1] - _nHalo) * jsb;
          for (int j=_nHalo; j<rngs[4]-rngs[1]+_nHalo; ++j) {
            #pragma omp simd
            for (int k=_nHalo; k<rngs[5]-rngs[2]+_nHalo; ++k) {
              int ijkl = ijl + k, ijk  = ij  + k + rngs[2] - _nHalo;
              a[ijk] = c1  * ( c[ijkl-1] + c[ijkl+1] + c[ijkl-jsl] + c[ijkl+jsl] + c[im1jl+k] + c[ip1jl+k])
                     + c11 * ( c[im1jl+k-jsl] + c[im1jl+k+jsl] + c[ip1jl+k-jsl] + c[ip1jl+k+jsl]
                             + c[ijkl-jsl-1]  + c[ijkl-jsl+1]  + c[ijkl+jsl-1]  + c[ijkl+jsl+1]
                             + c[im1jl+k-1]   + c[im1jl+k+1]   + c[ip1jl+k-1]   + c[ip1jl+k+1])
                     + c111* ( c[im1jl+k-jsl-1] + c[im1jl+k-jsl+1] + c[im1jl+k+jsl-1] + c[im1jl+k+jsl+1]
                             + c[ip1jl+k-jsl-1] + c[ip1jl+k-jsl+1] + c[ip1jl+k+jsl-1] + c[ip1jl+k+jsl+1])
                     + cRhs * b[ijk] + w1 * c[ijkl];
            }// end for k
            // pack
            if (mergePackkm)
              for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
            if (mergePackkp)
              for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
            //
            ij  += js;
            ijl += jsl; im1jl += jsl; ip1jl += jsl;
            ijb += jsb;
          }// end for j
        }// end if      
        // poll network if enabled
        if (tid == 0 && ptHt)  ptHt->poll(); 
      }// end for i
    break;
  }// end switch
}


void WJSolver::_compute_pipe_diamond(int di, int dj, int pipe, int dir, HaloTransfer* ptHt)
{
  int szs[3]; _ptDd->get_local_size(szs);
  int js  = szs[2] + 2*_nHalo, is  = js * (szs[1] + 2*_nHalo);
  int kBegin = _sr, kEnd = szs[2] + 2*_nHalo - _sr, jBegin, jEnd;
  int tid = omp_get_thread_num();
  int nf = _nHalo / _sr;
  int nPipe = _hs.num_pipe(), pipeLen = szs[0] / nPipe;
  int jsb = _nHalo, isb = _nHalo*szs[1];
  int kmbs = _hs.kmBufStart[pipe], kpbs = _hs.kpBufStart[pipe], offset, offsetb;
  bool mergePackkm = _ptDd->need_comm(2) && _bypassKHalo;
  bool mergePackkp = _ptDd->need_comm(5) && _bypassKHalo;
  bool mergeUnpackkm = mergePackkm && _hs.num_pipe() == 1;
  bool mergeUnpackkp = mergePackkp && _hs.num_pipe() == 1;

  double h2 = _h*_h, w0 = 2.0/3.0, w1 = 1.0/3.0, *p = _d[dir%2], *a = _d[(1-dir)%2], *b = _d[2];
  double c1, c2, c11, c111, cRhs, q90=1.0/90.0, q128=1.0/128.0;

  // adjust diamond's range based on current pipe
  _diamonds[di][dj].iBegin += pipe * pipeLen;
  _diamonds[di][dj].iEnd   += pipe * pipeLen;
 
  // adjust the triangleup/down range if it locates on pipe boundary
  int iBegin = _diamonds[di][dj].iBegin, iEnd  = _diamonds[di][dj].iEnd;
  int lBound = _diamonds[di][dj].iBegin, lSign =  1;
  int hBound = _diamonds[di][dj].iEnd,   hSign = -1; 
  if (_hs.num_pipe() > 1) {
    if (pipe == 0 && di == _nDiamond[0]-1) {// pipe 0, top is done
      hBound = _diamonds[di][dj].iEnd - 2*(_nHalo - _sr);
      hSign  = 1;
    }
    if (pipe > 1 && di == 0) {// pipe > 1, bottom is done
      iBegin = _diamonds[di][dj].iBegin + 2*(_nHalo - _sr);
      lBound = -1;
      lSign  =  0;
    }
  }

  int iDHLow  = max(iBegin, (_dhOff && !_ptDd->need_comm(0)) ? _nHalo : -100);
  int iDHHigh = min(iEnd,   (_dhOff && !_ptDd->need_comm(3)) ? szs[0]+_nHalo : 10000);
  int jDHLow  = (_dhOff && !_ptDd->need_comm(1)) ? _nHalo : -100;
  int jDHHigh = (_dhOff && !_ptDd->need_comm(4)) ? szs[1]+_nHalo : 10000;
  int kDHLow  = (_dhOff && !_ptDd->need_comm(2)) ? _nHalo : -100;
  int kDHHigh = (_dhOff && !_ptDd->need_comm(5)) ? szs[2]+_nHalo : 10000;

  // unpack i=nHalo to nHalo+_sr if deep halo is off
  // HARDCODE: assume no communication in i
  if (_dhOff) {
    int jUnpackBegin, jUnpackEnd;
    if (_diamonds[di][dj].type == TriangleUp) {
      jUnpackBegin = (dj > 0) ?  _diamonds[di][dj].jBegin - _sr : _nHalo;
      jUnpackEnd   = (dj < _nDiamond[1]-1) ? _diamonds[di][dj].jEnd + _sr : szs[1] + _nHalo;
    }
    else if (_diamonds[di][dj].type == TriangleDown) {
      jUnpackBegin = _diamonds[di][dj].jBegin + _nHalo;
      jUnpackEnd   = _diamonds[di][dj].jEnd   - _nHalo;
    }
    if (mergeUnpackkm) {
      for (int i=_nHalo; i<_nHalo+_sr; ++i) {
        int ij  =  i*is + jUnpackBegin*js;
        int ijb = (i - _nHalo) * isb + (jUnpackBegin - _nHalo) * jsb;
        for (int j=jUnpackBegin; j<jUnpackEnd; ++j) {
          for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kmbs+ijb+k];
          ij += js; ijb += jsb;
        }
      }
    }
    if (mergeUnpackkp) {
      for (int i=_nHalo; i<_nHalo+_sr; ++i) {
        int ij  =  i*is + jUnpackBegin*js + szs[2] + _nHalo;
        int ijb = (i - _nHalo)*isb + (jUnpackBegin - _nHalo) * jsb;
        for (int j=jUnpackBegin; j<jUnpackEnd; ++j) {
          for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kpbs+ijb+k];
          ij += js; ijb += jsb;
        }
      }
    }
  }

  switch (_scheme) {
    case 0:
    c1    = w0/6.0;
    cRhs  = -h2*w0/6.0;
    switch (_diamonds[di][dj].type) {
      case TriangleUp:
      for (int i=iBegin; i<iEnd; ++i) {
        bool isIReadInside = (i+_sr >= _nHalo) && (i+_sr < szs[0]+_nHalo);
        p = _d[dir%2];
        a = _d[(1-dir)%2];
        // unpack [-_sr,0) of the 1st j
        if (mergeUnpackkm && isIReadInside) {
          if (dj > 0) {
            for (int jj=-_sr; jj<0; ++jj) {
              int ij  = i*is + (_diamonds[di][dj].jBegin+jj)*js;
              int ijb = (i-_nHalo)*isb + (_diamonds[di][dj].jBegin+jj-_nHalo)*jsb;
              for (int k=0; k<_nHalo; ++k) p[ij+_sr*is+k] = _hs.rBuf[kmbs+_sr*isb+ijb+k];
            }
          }
          if (dj < _nDiamond[1]-1) {
            for (int jj=0; jj<_sr; ++jj) {
              int ij  = i*is + (_diamonds[di][dj].jEnd+jj)*js;
              int ijb = (i-_nHalo)*isb + (_diamonds[di][dj].jEnd+jj-_nHalo)*jsb;
              for (int k=0; k<_nHalo; ++k) p[ij+_sr*is+k] = _hs.rBuf[kmbs+_sr*isb+ijb+k];
            }
          }
        }
        if (mergeUnpackkp && isIReadInside) {
          if (dj > 0) {
            for (int jj=-_sr; jj<0; ++jj) {
              int ij  = i*is + (_diamonds[di][dj].jBegin+jj)*js;
              int ijb = (i-_nHalo)*isb + (_diamonds[di][dj].jBegin+jj-_nHalo)*jsb;
              for (int k=0; k<_nHalo; ++k) p[ij+_sr*is+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+_sr*isb+ijb+k];
            }
          }
          if (dj < _nDiamond[1]-1) {
            for (int jj=0; jj<_sr; ++jj) {
              int ij  = i*is + (_diamonds[di][dj].jEnd+jj)*js;
              int ijb = (i-_nHalo)*isb + (_diamonds[di][dj].jEnd+jj-_nHalo)*jsb;
              for (int k=0; k<_nHalo; ++k) p[ij+_sr*is+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+_sr*isb+ijb+k];
            }
          }
        }
              
        for (int m=0; m<nf; ++m) {
          p = _d[(dir+m)%2]; 
          a = _d[(1-dir+m)%2];
          int margin = m * _sr;
          int iLayer = i - m * _sr;
          if ((iLayer >= lBound + lSign*margin) && (iLayer <  hBound + hSign*margin)) {
            if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
            int ij = iLayer * is + max(_diamonds[di][dj].jBegin+margin,jDHLow) * js;
            for (int j=max(_diamonds[di][dj].jBegin+margin,jDHLow); j<min(_diamonds[di][dj].jEnd-margin,jDHHigh); ++j) {
              // unpack
              int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (j - _nHalo) * jsb;
              if (m == 0 && mergeUnpackkm && isIReadInside && j >= _nHalo && j < szs[1]+_nHalo)
                for (int k=0; k<_nHalo; ++k)  p[ij+_sr*is+k] = _hs.rBuf[kmbs+ijb+_sr*isb+k];
              if (m == 0 && mergeUnpackkp && isIReadInside && j >= _nHalo && j < szs[1]+_nHalo)
                for (int k=0; k<_nHalo; ++k)  p[ij+_sr*is+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+ijb+_sr*isb+k];
              #pragma omp simd
              for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                int ijk = ij + k;
                a[ijk] = c1 * ( p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] + p[ijk-is] + p[ijk+is]) \
                       + cRhs * b[ijk] + w1 * p[ijk];
              }// end for k
              // pack
              if (m == nf-1 && mergePackkm)
                for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
              if (m == nf-1 && mergePackkp)
                for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
              //
              ij += js;
            }// end for j
          }// end if
        }// end for m
        // poll network if enabled
        if (tid == 0 && ptHt)  ptHt->poll(); 
      }// end for i
      break;

      case TriangleDown:
        jBegin = _diamonds[di][dj].jBegin + _nHalo - _sr;
        jEnd   = _diamonds[di][dj].jEnd   - _nHalo + _sr;
        for (int i=iBegin; i<iEnd; ++i) {
          bool isIReadInside = (i+_sr >= _nHalo) && (i+_sr < szs[0]+_nHalo);
          for (int m=0; m<nf; ++m) {
            p = _d[(dir+m)%2]; 
            a = _d[(1-dir+m)%2];
            int margin = m * _sr;
            int iLayer = i - m * _sr;
            if ((iLayer >= lBound + lSign*margin) && (iLayer <  hBound + hSign*margin)) {
              if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
              int ij = iLayer * is + max(jBegin - margin, jDHLow) * js;
              for (int j=max(jBegin-margin,jDHLow); j<min(jEnd+margin,jDHHigh); ++j) {
                // unpack
                int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (j - _nHalo) * jsb;
                if (m == 0 && mergeUnpackkm && isIReadInside && j >= jBegin+_sr && j < jEnd-_sr)
                  for (int k=0; k<_nHalo; ++k)  p[ij+_sr*is+k] = _hs.rBuf[kmbs+ijb+_sr*isb+k];
                if (m == 0 && mergeUnpackkp && isIReadInside && j >= jBegin+_sr && j < jEnd-_sr)
                  for (int k=0; k<_nHalo; ++k)  p[ij+szs[2]+_nHalo+_sr*is+k] = _hs.rBuf[kpbs+ijb+_sr*isb+k];
                // compute
                #pragma omp simd
                for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                  int ijk = ij + k;
                  a[ijk] = c1 * ( p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] + p[ijk-is] + p[ijk+is]) \
                         + cRhs * b[ijk] + w1 * p[ijk];
                }// end for k
                // pack
                if (m == nf-1 && mergePackkm)
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
                if (m == nf-1 && mergePackkp)
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
                //
                ij += js;
              }// end for j
            }// end if
          }// end for m
        // poll network if enabled
          if (tid == 0 && ptHt)  ptHt->poll(); 
        }// end for i
      break;

      case TriangleLeft:
        for (int i=_diamonds[di][dj].iBegin+_nHalo-_sr; i<_diamonds[di][dj].iEnd+_nHalo-_sr; ++i) {
          for (int m=0; m<nf; ++m) {
            p = _d[(dir+m)%2]; 
            a = _d[(1-dir+m)%2];
            int margin = m * _sr;
            int iLayer = i - m * _sr;
            if (iLayer < _diamonds[di][dj].iEnd - _nHalo + _sr + margin) {
              if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
              int ij = iLayer * is + max(_diamonds[di][dj].jBegin + margin, jDHLow) * js;
              int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (_diamonds[di][dj].jBegin+margin-_nHalo) * jsb;
              for (int j=max(_diamonds[di][dj].jBegin+margin, jDHLow); j<min(_diamonds[di][dj].jEnd-margin, jDHHigh); ++j) {
                // unpack
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kmbs+ijb+k];
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  p[ij+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+ijb+k];
                #pragma omp simd
                for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                  int ijk = ij + k;
                  a[ijk] = c1 * ( p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] + p[ijk-is] + p[ijk+is]) \
                         + cRhs * b[ijk] + w1 * p[ijk];
                }// end for k
                // pack
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
                //
                ij += js;
                ijb += jsb;
              }// end for j
            }// end if
          }// end for m
          // poll network if enabled
          if (tid == 0 && ptHt)  ptHt->poll(); 
        }// end for i
      break;

      case Center:
        jBegin = _diamonds[di][dj].jBegin + _nHalo - _sr;
        jEnd   = _diamonds[di][dj].jEnd   - _nHalo + _sr;
        for (int i=_diamonds[di][dj].iBegin+_nHalo-_sr; i<_diamonds[di][dj].iEnd+_nHalo-_sr; ++i) {
          for (int m=0; m<nf; ++m) {
            p = _d[(dir+m)%2]; 
            a = _d[(1-dir+m)%2];
            int margin = m * _sr;
            int iLayer = i - m * _sr;
            if (iLayer < _diamonds[di][dj].iEnd - _nHalo + _sr + margin) {
              if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
              int ij = iLayer * is + max(jBegin - margin, jDHLow) * js;
              int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (jBegin-margin-_nHalo) * jsb;
              for (int j=max(jBegin-margin,jDHLow); j<min(jEnd+margin,jDHHigh); ++j) {
                // unpack
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kmbs+ijb+k];
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  p[ij+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+ijb+k];
                #pragma omp simd
                for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                  int ijk = ij + k;
                  a[ijk] = c1 * ( p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] + p[ijk-is] + p[ijk+is]) \
                         + cRhs * b[ijk] + w1 * p[ijk];
                }// end for k
                // pack
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
                //
                ij += js;
                ijb += jsb;
              }// end for j
            }// end if
          }// end for m
          // poll network if enabled
          if (tid == 0 && ptHt)  ptHt->poll(); 
        }// end for i
      break;
    }
    break;// break case 0
    case 1:
    c1 = 16.0*w0/90.0;
    c2 = -w0/90.0;
    cRhs = -12.0*h2*w0/90.0;
    switch (_diamonds[di][dj].type) {
      case TriangleUp:
      for (int i=iBegin; i<iEnd; ++i) {
        bool isIReadInside = (i+_sr >= _nHalo) && (i+_sr < szs[0]+_nHalo);
        p = _d[dir%2];
        a = _d[(1-dir)%2];
        // unpack [-_sr,0) of the 1st j
        if (mergeUnpackkm && isIReadInside) {
          if (dj > 0) {
            for (int jj=-_sr; jj<0; ++jj) {
              int ij  = i*is + (_diamonds[di][dj].jBegin+jj)*js;
              int ijb = (i-_nHalo)*isb + (_diamonds[di][dj].jBegin+jj-_nHalo)*jsb;
              for (int k=0; k<_nHalo; ++k) p[ij+_sr*is+k] = _hs.rBuf[kmbs+_sr*isb+ijb+k];
            }
          }
          if (dj < _nDiamond[1]-1) {
            for (int jj=0; jj<_sr; ++jj) {
              int ij  = i*is + (_diamonds[di][dj].jEnd+jj)*js;
              int ijb = (i-_nHalo)*isb + (_diamonds[di][dj].jEnd+jj-_nHalo)*jsb;
              for (int k=0; k<_nHalo; ++k) p[ij+_sr*is+k] = _hs.rBuf[kmbs+_sr*isb+ijb+k];
            }
          }
        }
        if (mergeUnpackkp && isIReadInside) {
          if (dj > 0) {
            for (int jj=-_sr; jj<0; ++jj) {
              int ij  = i*is + (_diamonds[di][dj].jBegin+jj)*js;
              int ijb = (i-_nHalo)*isb + (_diamonds[di][dj].jBegin+jj-_nHalo)*jsb;
              for (int k=0; k<_nHalo; ++k) p[ij+_sr*is+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+_sr*isb+ijb+k];
            }
          }
          if (dj < _nDiamond[1]-1) {
            for (int jj=0; jj<_sr; ++jj) {
              int ij  = i*is + (_diamonds[di][dj].jEnd+jj)*js;
              int ijb = (i-_nHalo)*isb + (_diamonds[di][dj].jEnd+jj-_nHalo)*jsb;
              for (int k=0; k<_nHalo; ++k) p[ij+_sr*is+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+_sr*isb+ijb+k];
            }
          }
        }

        for (int m=0; m<nf; ++m) {
          p = _d[(dir+m)%2]; 
          a = _d[(1-dir+m)%2];
          int margin = m * _sr;
          int iLayer = i - m * _sr;
          if ((iLayer >= lBound + lSign*margin) && (iLayer <  hBound + hSign*margin)) {
            if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
            int ij = iLayer * is + max(_diamonds[di][dj].jBegin+margin,jDHLow) * js;
            for (int j=max(_diamonds[di][dj].jBegin+margin,jDHLow); j<min(_diamonds[di][dj].jEnd-margin,jDHHigh); ++j) {
              // unpack
              int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (j - _nHalo) * jsb;
              if (m == 0 && mergeUnpackkm && isIReadInside && j >= _nHalo && j < szs[1]+_nHalo)
                for (int k=0; k<_nHalo; ++k)  p[ij+_sr*is+k] = _hs.rBuf[kmbs+ijb+_sr*isb+k];
              if (m == 0 && mergeUnpackkp && isIReadInside && j >= _nHalo && j < szs[1]+_nHalo)
                for (int k=0; k<_nHalo; ++k)  p[ij+_sr*is+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+ijb+_sr*isb+k];
              #pragma omp simd
              for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                int ijk = ij + k;
                a[ijk] = c1 * (p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] + p[ijk-is] + p[ijk+is]) \
                       + c2 * (p[ijk-2] + p[ijk+2] + p[ijk-2*js] + p[ijk+2*js] + p[ijk-2*is] + p[ijk+2*is]) \
                       + cRhs * b[ijk] + w1 * p[ijk];
              }// end for k
              // pack
              if (m == nf-1 && mergePackkm)
                for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
              if (m == nf-1 && mergePackkp)
                for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
              //
              ij += js;
            }// end for j
          }// end if
        }// end for m
        // poll network if enabled
        if (tid == 0 && ptHt)  ptHt->poll(); 
      }// end for i
      break;

      case TriangleDown:
        jBegin = _diamonds[di][dj].jBegin + _nHalo - _sr;
        jEnd   = _diamonds[di][dj].jEnd   - _nHalo + _sr;
        for (int i=iBegin; i<iEnd; ++i) {
          bool isIReadInside = (i+_sr >= _nHalo) && (i+_sr < szs[0]+_nHalo);
          for (int m=0; m<nf; ++m) {
            p = _d[(dir+m)%2]; 
            a = _d[(1-dir+m)%2];
            int margin = m * _sr;
            int iLayer = i - m * _sr;
            if ((iLayer >= lBound + lSign*margin) && (iLayer <  hBound + hSign*margin)) {
              if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
              int ij = iLayer * is + max(jBegin - margin, jDHLow) * js;
              for (int j=max(jBegin-margin,jDHLow); j<min(jEnd+margin,jDHHigh); ++j) {
                // unpack
                int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (j - _nHalo) * jsb;
                if (m == 0 && mergeUnpackkm && isIReadInside && j >= jBegin+_sr && j < jEnd-_sr)
                  for (int k=0; k<_nHalo; ++k)  p[ij+_sr*is+k] = _hs.rBuf[kmbs+ijb+_sr*isb+k];
                if (m == 0 && mergeUnpackkp && isIReadInside && j >= jBegin+_sr && j < jEnd-_sr)
                  for (int k=0; k<_nHalo; ++k)  p[ij+szs[2]+_nHalo+_sr*is+k] = _hs.rBuf[kpbs+ijb+_sr*isb+k];
                // compute
                #pragma omp simd
                for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                  int ijk = ij + k;
                  a[ijk] = c1 * (p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] + p[ijk-is] + p[ijk+is]) \
                         + c2 * (p[ijk-2] + p[ijk+2] + p[ijk-2*js] + p[ijk+2*js] + p[ijk-2*is] + p[ijk+2*is]) \
                         + cRhs * b[ijk] + w1 * p[ijk];
                }// end for k
                // pack
                if (m == nf-1 && mergePackkm)
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
                if (m == nf-1 && mergePackkp)
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
                //
                ij += js;
              }// end for j
            }// end if
          }// end for m
          // poll network if enabled
          if (tid == 0 && ptHt)  ptHt->poll(); 
        }// end for i
      break;

      case TriangleLeft:
        for (int i=_diamonds[di][dj].iBegin+_nHalo-_sr; i<_diamonds[di][dj].iEnd+_nHalo-_sr; ++i) {
          for (int m=0; m<nf; ++m) {
            p = _d[(dir+m)%2]; 
            a = _d[(1-dir+m)%2];
            int margin = m * _sr;
            int iLayer = i - m * _sr;
            if (iLayer < _diamonds[di][dj].iEnd - _nHalo + _sr + margin) {
              if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
              int ij = iLayer * is + (_diamonds[di][dj].jBegin + margin) * js;
              int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (_diamonds[di][dj].jBegin+margin-_nHalo) * jsb;
              for (int j=max(_diamonds[di][dj].jBegin+margin, jDHLow); j<min(_diamonds[di][dj].jEnd-margin, jDHHigh); ++j) {
                // unpack
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kmbs+ijb+k];
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  p[ij+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+ijb+k];
                #pragma omp simd
                for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                  int ijk = ij + k;
                  a[ijk] = c1 * (p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] + p[ijk-is] + p[ijk+is]) \
                         + c2 * (p[ijk-2] + p[ijk+2] + p[ijk-2*js] + p[ijk+2*js] + p[ijk-2*is] + p[ijk+2*is]) \
                         + cRhs * b[ijk] + w1 * p[ijk];
                }// end for k
                // pack
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
                //
                ij += js;
                ijb += jsb;
              }// end for j
            }// end if
          }// end for m
          // poll network if enabled
          if (tid == 0 && ptHt)  ptHt->poll(); 
        }// end for i
      break;

      case Center:
        jBegin = _diamonds[di][dj].jBegin + _nHalo - _sr;
        jEnd   = _diamonds[di][dj].jEnd   - _nHalo + _sr;
        for (int i=_diamonds[di][dj].iBegin+_nHalo-_sr; i<_diamonds[di][dj].iEnd+_nHalo-_sr; ++i) {
          for (int m=0; m<nf; ++m) {
            p = _d[(dir+m)%2]; 
            a = _d[(1-dir+m)%2];
            int margin = m * _sr;
            int iLayer = i - m * _sr;
            if (iLayer < _diamonds[di][dj].iEnd - _nHalo + _sr + margin) {
              if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
              int ij = iLayer * is + (jBegin - margin) * js;
              int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (jBegin-margin-_nHalo) * jsb;
              for (int j=max(jBegin-margin,jDHLow); j<min(jEnd+margin,jDHHigh); ++j) {
                // unpack
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kmbs+ijb+k];
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  p[ij+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+ijb+k];
                #pragma omp simd
                for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                  int ijk = ij + k;
                  a[ijk] = c1 * (p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] + p[ijk-is] + p[ijk+is]) \
                         + c2 * (p[ijk-2] + p[ijk+2] + p[ijk-2*js] + p[ijk+2*js] + p[ijk-2*is] + p[ijk+2*is]) \
                         + cRhs * b[ijk] + w1 * p[ijk];
                }// end for k
                // pack
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
                //
                ij += js;
                ijb += jsb;
              }// end for j
            }// end if
          }// end for m
          // poll network if enabled
          if (tid == 0 && ptHt)  ptHt->poll(); 
        }// end for i
      break;
    }
    break;// break case 1
    case 2:
    c1   = 14.0*w0*q128;
    c11  = 3.0*w0*q128;
    c111 = w0*q128;
    cRhs = -30.0*w0*h2*q128;
    offset = _sr * (is + js);
    offsetb = _sr * (isb + jsb);
    switch (_diamonds[di][dj].type) {
      case TriangleUp:
      for (int i=iBegin; i<iEnd; ++i) {
        bool isIReadInside = (i+_sr >= _nHalo) && (i+_sr < szs[0]+_nHalo);
        p = _d[dir%2];
        a = _d[(1-dir)%2];
        // unpack [-_sr,0) of the 1st j
        if (mergeUnpackkm && isIReadInside) {
          int jUnpackBegin = dj > 0 ? _diamonds[di][dj].jBegin-_sr : (_dhOff ? _nHalo : -1);
          int jUnpackEnd   = dj > 0 ? _diamonds[di][dj].jBegin+_sr : (_dhOff ? _nHalo+_sr : -2);
          for (int j=jUnpackBegin; j<jUnpackEnd; ++j) {
            int ij  = (i+_sr)*is + j*js;
            int ijb = (i+_sr-_nHalo) * isb + (j-_nHalo) * jsb;
            for (int k=0; k<_nHalo; ++k) p[ij+k] = _hs.rBuf[kmbs+ijb+k];
          }
        }
        if (mergeUnpackkp && isIReadInside) {
          int jUnpackBegin = dj > 0 ? _diamonds[di][dj].jBegin-_sr : (_dhOff ? _nHalo : -1);
          int jUnpackEnd   = dj > 0 ? _diamonds[di][dj].jBegin+_sr : (_dhOff ? _nHalo+_sr : -2);
          for (int j=jUnpackBegin; j<jUnpackEnd; ++j) {
            int ij  = (i+_sr)*is + j*js;
            int ijb = (i+_sr-_nHalo) * isb + (j-_nHalo) * jsb;
            for (int k=0; k<_nHalo; ++k) p[ij+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+ijb+k];
          }
        }
        for (int m=0; m<nf; ++m) {
          p = _d[(dir+m)%2]; 
          a = _d[(1-dir+m)%2];
          int margin = m * _sr;
          int iLayer = i - m * _sr;
          if ((iLayer >= lBound + lSign*margin) && (iLayer <  hBound + hSign*margin)) {
            if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
            int ij = iLayer * is + max(_diamonds[di][dj].jBegin+margin,jDHLow) * js;
            for (int j=max(_diamonds[di][dj].jBegin+margin,jDHLow); j<min(_diamonds[di][dj].jEnd-margin,jDHHigh); ++j) {
              // unpack
              int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (j - _nHalo) * jsb;
              bool isReadInside = isIReadInside && j+_sr >= _nHalo && j+_sr < szs[1]+_nHalo; 
              if (m == 0 && mergeUnpackkm && isReadInside) 
                for (int k=0; k<_nHalo; ++k)  p[ij+offset+k] = _hs.rBuf[kmbs+ijb+offsetb+k];
              if (m == 0 && mergeUnpackkp && isReadInside)
                for (int k=0; k<_nHalo; ++k)  p[ij+offset+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+ijb+offsetb+k];
              #pragma omp simd
              for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                int ijk = ij + k;
                a[ijk] = c1  * ( p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] + p[ijk-is] + p[ijk+is]) \
                       + c11 * ( p[ijk-is-js] + p[ijk-is+js] + p[ijk+is-js] + p[ijk+is+js] \
                               + p[ijk-js-1]  + p[ijk-js+1]  + p[ijk+js-1]  + p[ijk+js+1]  \
                               + p[ijk-is-1]  + p[ijk-is+1]  + p[ijk+is-1]  + p[ijk+is+1]) \
                       + c111* ( p[ijk-is-js-1] + p[ijk-is-js+1] + p[ijk-is+js-1] + p[ijk-is+js+1] \
                               + p[ijk+is-js-1] + p[ijk+is-js+1] + p[ijk+is+js-1] + p[ijk+is+js+1])
                       + cRhs * b[ijk] + w1 * p[ijk];
              }// end for k
              // pack
              if (m == nf-1 && mergePackkm)
                for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
              if (m == nf-1 && mergePackkp)
                for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
              //
              ij += js;
            }// end for j
          }// end if
        }// end for m
        // poll network if enabled
        if (tid == 0 && ptHt)  ptHt->poll(); 
      }// end for i
      break;

      case TriangleDown:
        jBegin = _diamonds[di][dj].jBegin + _nHalo - _sr;
        jEnd   = _diamonds[di][dj].jEnd   - _nHalo + _sr;
        for (int i=iBegin; i<iEnd; ++i) {
          bool isIReadInside = (i+_sr >= _nHalo) && (i+_sr < szs[0]+_nHalo);
          for (int m=0; m<nf; ++m) {
            p = _d[(dir+m)%2]; 
            a = _d[(1-dir+m)%2];
            int margin = m * _sr;
            int iLayer = i - m * _sr;
            if ((iLayer >= lBound + lSign*margin) && (iLayer <  hBound + hSign*margin)) {
              if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
              int ij = iLayer * is + max(jBegin - margin, jDHLow) * js;
              for (int j=max(jBegin-margin,jDHLow); j<min(jEnd+margin,jDHHigh); ++j) {
                // unpack
                int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (j - _nHalo) * jsb;
                bool isReadInside = isIReadInside && j+_sr>=jBegin+_sr && j+_sr<jEnd-_sr;
                if (m == 0 && mergeUnpackkm && isReadInside)
                  for (int k=0; k<_nHalo; ++k)  p[ij+offset+k] = _hs.rBuf[kmbs+ijb+offsetb+k];
                if (m == 0 && mergeUnpackkp && isReadInside)
                  for (int k=0; k<_nHalo; ++k)  p[ij+offset+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+offsetb+ijb+k];
                #pragma omp simd
                for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                  int ijk = ij + k;
                  a[ijk] = c1  * ( p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] + p[ijk-is] + p[ijk+is]) \
                         + c11 * ( p[ijk-is-js] + p[ijk-is+js] + p[ijk+is-js] + p[ijk+is+js] \
                                 + p[ijk-js-1]  + p[ijk-js+1]  + p[ijk+js-1]  + p[ijk+js+1]  \
                                 + p[ijk-is-1]  + p[ijk-is+1]  + p[ijk+is-1]  + p[ijk+is+1]) \
                         + c111* ( p[ijk-is-js-1] + p[ijk-is-js+1] + p[ijk-is+js-1] + p[ijk-is+js+1] \
                                 + p[ijk+is-js-1] + p[ijk+is-js+1] + p[ijk+is+js-1] + p[ijk+is+js+1])
                         + cRhs * b[ijk] + w1 * p[ijk];
                }// end for k
                // pack
                if (m == nf-1 && mergePackkm)
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
                if (m == nf-1 && mergePackkp)
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
                //
                ij += js;
              }// end for j
            }// end if
          }// end for m
          // poll network if enabled
          if (tid == 0 && ptHt)  ptHt->poll(); 
        }// end for i
      break;

      case TriangleLeft:
        for (int i=_diamonds[di][dj].iBegin+_nHalo-_sr; i<_diamonds[di][dj].iEnd+_nHalo-_sr; ++i) {
          for (int m=0; m<nf; ++m) {
            p = _d[(dir+m)%2]; 
            a = _d[(1-dir+m)%2];
            int margin = m * _sr;
            int iLayer = i - m * _sr;
            if (iLayer < _diamonds[di][dj].iEnd - _nHalo + _sr + margin) {
              if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
              int ij = iLayer * is + (_diamonds[di][dj].jBegin + margin) * js;
              int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (_diamonds[di][dj].jBegin+margin-_nHalo) * jsb;
              for (int j=max(_diamonds[di][dj].jBegin+margin, jDHLow); j<min(_diamonds[di][dj].jEnd-margin, jDHHigh); ++j) {
                // unpack
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kmbs+ijb+k];
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  p[ij+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+ijb+k];
                #pragma omp simd
                for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                  int ijk = ij + k;
                  a[ijk] = c1  * ( p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] + p[ijk-is] + p[ijk+is]) \
                         + c11 * ( p[ijk-is-js] + p[ijk-is+js] + p[ijk+is-js] + p[ijk+is+js] \
                                 + p[ijk-js-1]  + p[ijk-js+1]  + p[ijk+js-1]  + p[ijk+js+1]  \
                                 + p[ijk-is-1]  + p[ijk-is+1]  + p[ijk+is-1]  + p[ijk+is+1]) \
                         + c111* ( p[ijk-is-js-1] + p[ijk-is-js+1] + p[ijk-is+js-1] + p[ijk-is+js+1] \
                                 + p[ijk+is-js-1] + p[ijk+is-js+1] + p[ijk+is+js-1] + p[ijk+is+js+1])
                         + cRhs * b[ijk] + w1 * p[ijk];
                }// end for k
                // pack
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
                //
                ij += js;
                ijb += jsb;
              }// end for j
            }// end if
          }// end for m
          // poll network if enabled
          if (tid == 0 && ptHt)  ptHt->poll(); 
        }// end for i
      break;

      case Center:
        jBegin = _diamonds[di][dj].jBegin + _nHalo - _sr;
        jEnd   = _diamonds[di][dj].jEnd   - _nHalo + _sr;
        for (int i=_diamonds[di][dj].iBegin+_nHalo-_sr; i<_diamonds[di][dj].iEnd+_nHalo-_sr; ++i) {
          for (int m=0; m<nf; ++m) {
            p = _d[(dir+m)%2]; 
            a = _d[(1-dir+m)%2];
            int margin = m * _sr;
            int iLayer = i - m * _sr;
            if (iLayer < _diamonds[di][dj].iEnd - _nHalo + _sr + margin) {
              if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
              int ij = iLayer * is + (jBegin - margin) * js;
              int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (jBegin-margin-_nHalo) * jsb;
              for (int j=max(jBegin-margin,jDHLow); j<min(jEnd+margin,jDHHigh); ++j) {
                // unpack
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kmbs+ijb+k];
                if (m == 0 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  p[ij+szs[2]+_nHalo+k] = _hs.rBuf[kpbs+ijb+k];
                #pragma omp simd
                for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                  int ijk = ij + k;
                  a[ijk] = c1  * ( p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] + p[ijk-is] + p[ijk+is]) \
                         + c11 * ( p[ijk-is-js] + p[ijk-is+js] + p[ijk+is-js] + p[ijk+is+js] \
                                 + p[ijk-js-1]  + p[ijk-js+1]  + p[ijk+js-1]  + p[ijk+js+1]  \
                                 + p[ijk-is-1]  + p[ijk-is+1]  + p[ijk+is-1]  + p[ijk+is+1]) \
                         + c111* ( p[ijk-is-js-1] + p[ijk-is-js+1] + p[ijk-is+js-1] + p[ijk-is+js+1] \
                                 + p[ijk+is-js-1] + p[ijk+is-js+1] + p[ijk+is+js-1] + p[ijk+is+js+1])
                         + cRhs * b[ijk] + w1 * p[ijk];
                }// end for k
                // pack
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(2))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
                if (m == nf-1 && _bypassKHalo && _ptDd->need_comm(5))
                  for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
                //
                ij += js;
                ijb += jsb;
              }// end for j
            }// end if
          }// end for m
          // poll network if enabled
          if (tid == 0 && ptHt)  ptHt->poll(); 
        }// end for i
      break;
    }
    break;// break case 2
  }

  // reset pipelen's range
  _diamonds[di][dj].iBegin -= pipe * pipeLen;
  _diamonds[di][dj].iEnd   -= pipe * pipeLen;
}


void WJSolver::_compute_range(int rngs[6], bool isEven)
{
  double w0 = 2.0/3.0, w1 = 1.0/3.0, h2 = _h * _h, *p, *a, *b = _d[2];
  double c1, c2, c11, c111, cRhs, q90=1.0/90.0, q128=1.0/128.0;
  int    szs[3];  _ptDd->get_local_size(szs);
  int    js = szs[2] + 2*_nHalo, is = js * (szs[1] + 2*_nHalo), base=rngs[0]*is + rngs[1]*js;
  int    jsb = _nHalo, isb = _nHalo*szs[1];
  int    kmbs = _hs.kmBufStart[0], kpbs = _hs.kpBufStart[0];
  int    tid = omp_get_thread_num();

  if (isEven) {
    p = _d[0]; a = _d[1];
  }
  else {
    p = _d[1]; a = _d[0];
  }

  switch (_scheme) {
    case 0: // 7pt star
      c1    = w0/6.0;
      cRhs  = -h2*w0/6.0;
      for (int i=rngs[0]; i<rngs[3]; ++i) {
        int ij  = base;
        int ijb = (i - _nHalo)*isb + (rngs[1] - _nHalo) * jsb;
        for (int j=rngs[1]; j<rngs[4]; ++j) {
          // unpack
          if (_ptDd->need_comm(2) && _bypassKHalo)
            for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kmbs+ijb+k];
          if (_ptDd->need_comm(5) && _bypassKHalo)
            for (int k=0; k<_nHalo; ++k)  p[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+ijb+k];
          // compute
          #pragma vector nontemporal
          #pragma omp simd
          for (int k=rngs[2]; k<rngs[5]; ++k) {
            int ijk = ij + k;
            a[ijk] = c1 * ( p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] + p[ijk-is] + p[ijk+is]) \
                   + cRhs * b[ijk] + w1 * p[ijk];
          }// end for k
          //  pack
          if (_ptDd->need_comm(2) && _bypassKHalo)
            for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
          if (_ptDd->need_comm(5) && _bypassKHalo)
            for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
          //
          ij  += js;
          ijb += jsb;
        }// end for j
        base    += is;
      }// end for i
    break;
    case 1: // 13pt star
      c1 = 16.0*w0*q90;
      c2 = -w0*q90;
      cRhs = -12.0*h2*w0*q90;
      for (int i=rngs[0]; i<rngs[3]; ++i) {
        int ij  = base;
        int ijb = (i - _nHalo)*isb + (rngs[1] - _nHalo) * jsb;
        for (int j=rngs[1]; j<rngs[4]; ++j) {
          // unpack
          if (_ptDd->need_comm(2) && _bypassKHalo)
            for (int k=0; k<_nHalo; ++k)  p[ij+k] = _hs.rBuf[kmbs+ijb+k];
          if (_ptDd->need_comm(5) && _bypassKHalo)
            for (int k=0; k<_nHalo; ++k)  p[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+ijb+k];
          // compute
          #pragma vector nontemporal
          #pragma omp simd
          for (int k=rngs[2]; k<rngs[5]; ++k) {
            int ijk = ij + k;
            a[ijk] = c1 * (p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] + p[ijk-is] + p[ijk+is]) \
                   + c2 * (p[ijk-2] + p[ijk+2] + p[ijk-2*js] + p[ijk+2*js] + p[ijk-2*is] + p[ijk+2*is]) \
                   + cRhs * b[ijk] + w1 * p[ijk];
          }
          //  pack
          if (_ptDd->need_comm(2) && _bypassKHalo)
            for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
          if (_ptDd->need_comm(5) && _bypassKHalo)
            for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
          //
          ij += js;
          ijb += jsb;
        }
        base += is;
      }
    break;
    case 2: // 27pt box
      c1   = 14.0*w0*q128;
      c11  = 3.0*w0*q128;
      c111 = w0*q128;
      cRhs = -30.0*w0*h2*q128;

      // unpack (iBegin, jBegin-jEnd)
      if (_ptDd->need_comm(2) && _bypassKHalo) {
        int jBegin = max(_nHalo, rngs[1]-1);
        int ij  =  rngs[0]*is + jBegin*js;
        int ijb = (rngs[0] - _nHalo)*isb + (jBegin - _nHalo) * jsb;
        for (int j=jBegin; j<min(rngs[4]+1,szs[1]+_nHalo); ++j) {
          p[ij] = _hs.rBuf[kmbs+ijb];
          p[ij-is] = rngs[0] > _nHalo ? _hs.rBuf[kmbs+ijb-isb] : p[ij-is];
          ij += js; ijb += jsb;
        }
      }
      if (_ptDd->need_comm(5) && _bypassKHalo) {
        int jBegin = max(_nHalo, rngs[1]-1);
        int ij  =  rngs[0]*is + jBegin*js + szs[2] + _nHalo;
        int ijb = (rngs[0] - _nHalo)*isb + (jBegin - _nHalo) * jsb;
        for (int j=jBegin; j<min(rngs[4]+1,szs[1]+_nHalo); ++j) {
          p[ij] = _hs.rBuf[kpbs+ijb];
          p[ij-is] = rngs[0] > _nHalo ? _hs.rBuf[kpbs+ijb-isb] : p[ij-is];
          ij += js; ijb += jsb;
        }
      }

      for (int i=rngs[0]; i<rngs[3]; ++i) {
        int ij  = base;
        int ijb = (i - _nHalo)*isb + (rngs[1] - _nHalo) * jsb;
        // unpack (i+1, jBegin)
        if (_ptDd->need_comm(2) && _bypassKHalo && i < szs[0]+_nHalo-1) {
          p[ij+is]    = _hs.rBuf[kmbs+ijb+isb];
          p[ij+is-js] = rngs[1] > _nHalo ? _hs.rBuf[kmbs+ijb+isb-jsb] : p[ij+is-js];
        }
        if (_ptDd->need_comm(5) && _bypassKHalo && i < szs[0]+_nHalo-1) {
          p[ij+is+_nHalo+szs[2]]    = _hs.rBuf[kpbs+ijb+isb];
          p[ij+is+_nHalo+szs[2]-js] = rngs[1] > _nHalo ? _hs.rBuf[kpbs+ijb+isb-jsb] 
                                                       : p[ij+is+_nHalo+szs[2]-js];
        }

        //
        for (int j=rngs[1]; j<rngs[4]; ++j) {
          // unpack
          if (_ptDd->need_comm(2) && _bypassKHalo && i < szs[0]+_nHalo-1 && j < szs[1]+_nHalo-1)
            for (int k=0; k<_nHalo; ++k)  p[ij+is+js+k] = _hs.rBuf[kmbs+ijb+isb+jsb+k];
          if (_ptDd->need_comm(5) && _bypassKHalo && i < szs[0]+_nHalo-1 && j < szs[1]+_nHalo-1)
            for (int k=0; k<_nHalo; ++k)  p[ij+is+js+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+ijb+isb+jsb+k];
          // compute
          #pragma vector nontemporal
          #pragma omp simd
          for (int k=rngs[2]; k<rngs[5]; ++k) {
            int ijk = ij + k;
            a[ijk] = c1  * ( p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] + p[ijk-is] + p[ijk+is]) \
                   + c11 * ( p[ijk-is-js] + p[ijk-is+js] + p[ijk+is-js] + p[ijk+is+js] \
                           + p[ijk-js-1]  + p[ijk-js+1]  + p[ijk+js-1]  + p[ijk+js+1]  \
                           + p[ijk-is-1]  + p[ijk-is+1]  + p[ijk+is-1]  + p[ijk+is+1]) \
                   + c111* ( p[ijk-is-js-1] + p[ijk-is-js+1] + p[ijk-is+js-1] + p[ijk-is+js+1] \
                           + p[ijk+is-js-1] + p[ijk+is-js+1] + p[ijk+is+js-1] + p[ijk+is+js+1])
                   + cRhs * b[ijk] + w1 * p[ijk];
          }
          //  pack
          if (_ptDd->need_comm(2) && _bypassKHalo)
            for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kmbs+ijb+k] = a[ij+_nHalo+k];
          if (_ptDd->need_comm(5) && _bypassKHalo)
            for (int k=0; k<_nHalo; ++k)  _hs.sBuf[kpbs+ijb+k] = a[ij+szs[2]+k];
          //
          ij  += js;
          ijb += jsb;
        }
        base    += is;
      }
    break;
  }

}


void WJSolver::_nullify_layer(double *c, int basel, int jb, int je, int jsl, int kb, int ke)
{
  for (int j=jb; j<je; ++j) {
    #pragma omp simd
    for (int k=kb; k<ke; ++k) c[basel+k] = 0.0;
    basel += jsl;
  }
}
