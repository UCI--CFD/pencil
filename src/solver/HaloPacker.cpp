#include "HaloPacker.h"

#include <iostream>
#include <vector>
#include <algorithm>
#include <mpi.h>
#include <omp.h>

using std::vector;

HaloPacker::HaloPacker(const DomainDecomp& dd, const int nHalo):
  _nHalo(nHalo),
  _cacheLineLen(8),
  _ptDd(&dd),
  _bypassKHalo(false)
{};


HaloPacker::HaloPacker(const HaloPacker& rhs)
{
  if (_ptDd->is_root())
    std::cout << "Error: HaloPacker object should not be copied" << std::endl;
  MPI_Finalize();
}


const HaloPacker& HaloPacker::operator=(const HaloPacker& rhs)
{
  if (_ptDd->is_root())
    std::cout << "Error: HaloPacker object should not be assigned" << std::endl;
  MPI_Finalize();
  return *this;
}


void HaloPacker::set_from_option()
{
  bool isSet;
  get_option("hp_line",  CmdOption::Int, 1, &_cacheLineLen, &isSet);
}


void HaloPacker::get_local_range(int rngs[6])
{
  _ptDd->get_local_size(rngs);
  for (int i=0; i<3; ++i) {
    rngs[i+3] = rngs[i] + _nHalo;
    rngs[i]   = _nHalo;
  }
}


void HaloPacker::init_buffer_numa(HaloStore& hs)
{
  int tid = omp_get_thread_num();
  for (unsigned int m=0; m<_haloRngs[tid].size(); m+=8) {
    int bufEnd = (_haloRngs[tid][m+5] - _haloRngs[tid][m+2])
               * (_haloRngs[tid][m+4] - _haloRngs[tid][m+1])
               * (_haloRngs[tid][m+3] - _haloRngs[tid][m]  ) + _haloRngs[tid][m+7];
    for (int i=_haloRngs[tid][m+7]; i<bufEnd; ++i) {
      hs.sBuf[i] = 0.0;
      hs.rBuf[i] = 0.0;
    }
  }
}


int HaloPacker::pack(double** d, vector<int>& vs, HaloStore& hs)
{
  int tid = omp_get_thread_num();
  if (tid >= static_cast<int>(_haloRngs.size())) return -1;

  int szs[3];  
  _ptDd->get_local_size(szs);
  int jStrd    = szs[2] + 2*_nHalo; 
  int iStrd    = jStrd * (szs[1] + 2*_nHalo);
  
  for (unsigned int m = 0; m < _bodyRngs[tid].size(); m += 8) {
    int zLen = _bodyRngs[tid][m+5] - _bodyRngs[tid][m+2];
    int vid  = vs[_bodyRngs[tid][m+6]];
    int bufStart = _bodyRngs[tid][m+7];
    // if zLen is greater when given size (cache line length?), use std copy
    if (zLen >= _cacheLineLen) {
      double *base  = d[vid] + _bodyRngs[tid][m]*iStrd + _bodyRngs[tid][m+1]*jStrd;
      for (int i = _bodyRngs[tid][m]; i < _bodyRngs[tid][m+3]; ++i) {
        double* base0 = base;
        for (int j = _bodyRngs[tid][m+1]; j < _bodyRngs[tid][m+4]; ++j) {
          std::copy(base + _bodyRngs[tid][m+2], base + _bodyRngs[tid][m+5], hs.sBuf + bufStart);
          bufStart += zLen;
          base     += jStrd;
        }
        base = base0 + iStrd;
      }
    }
    // else, copy by element
    else {
      int base = _bodyRngs[tid][m]*iStrd + _bodyRngs[tid][m+1]*jStrd;
      for (int i = _bodyRngs[tid][m]; i < _bodyRngs[tid][m+3]; ++i) {
        int base0 = base;
        for (int j = _bodyRngs[tid][m+1]; j < _bodyRngs[tid][m+4]; ++j) {
          for (int k = _bodyRngs[tid][m+2]; k < _bodyRngs[tid][m+5]; ++k) {
            hs.sBuf[bufStart] = d[vid][base + k];
            ++bufStart;
          }
          base += jStrd;
        }
        base = base0 + iStrd;
      }
    }//end else
  }//end for

  return 0;
}


int HaloPacker::unpack(double** d, vector<int>& vs, HaloStore& hs)
{
  int tid = omp_get_thread_num();
  if (tid >= static_cast<int>(_haloRngs.size())) return -1;

  int szs[3];
  _ptDd->get_local_size(szs);
  int jStrd    = szs[2] + 2*_nHalo; 
  int iStrd    = jStrd * (szs[1] + 2*_nHalo);
  
  for (int m = 0; m < (int)_haloRngs[tid].size(); m += 8) {
    int zLen = _haloRngs[tid][m+5] - _haloRngs[tid][m+2];
    int vid  = vs[_haloRngs[tid][m+6]];
    int bufStart = _haloRngs[tid][m+7];
    // if zLen is greater when given size (cache line length?), use std copy
    if (zLen >= _cacheLineLen) {
      double *base  = d[vid] + _haloRngs[tid][m]*iStrd + _haloRngs[tid][m+1]*jStrd;
      for (int i = _haloRngs[tid][m]; i < _haloRngs[tid][m+3]; ++i) {
        double* base0 = base;
        for (int j = _haloRngs[tid][m+1]; j < _haloRngs[tid][m+4]; ++j) {
          std::copy(hs.rBuf + bufStart, hs.rBuf + bufStart + zLen, base + _haloRngs[tid][m+2]);
          bufStart += zLen;
          base     += jStrd;
        }//end for j
        base = base0 + iStrd;
      }//end for i
    }
    // else, copy by element
    else {
      int base = _haloRngs[tid][m]*iStrd + _haloRngs[tid][m+1]*jStrd;
      for (int i = _haloRngs[tid][m]; i < _haloRngs[tid][m+3]; ++i) {
        int base0 = base;
        for (int j = _haloRngs[tid][m+1]; j < _haloRngs[tid][m+4]; ++j) {
          for (int k = _haloRngs[tid][m+2]; k < _haloRngs[tid][m+5]; ++k) {
            d[vid][base + k] = hs.rBuf[bufStart];
            ++bufStart;
          }
          base += jStrd;
        }//end for j
        base = base0 + iStrd;
      }//end for i
    }//end else
  }//end for

  return 0;
}


void HaloPacker::debug_view()
{
  if (_ptDd->is_root()) {
    std::cout << "Debug View of Halo Packer Object" << std::endl;
    std::cout << "Assigned with chunk " << _cBegin << " to " << _cEnd << std::endl;
    std::cout << "Set up for " << _haloRngs.size() << " threads" << std::endl;
    std::cout << "thread's halo range: " << std::endl;
    for (unsigned int tid=0; tid<_haloRngs.size(); ++tid) {
      std::cout << "thread " << tid << std::endl;
      int tVol = 0;
      for (unsigned int m=0; m<_haloRngs[tid].size(); m+=8) {
        tVol += (_haloRngs[tid][m+3] - _haloRngs[tid][m]) \
              * (_haloRngs[tid][m+4] - _haloRngs[tid][m+1]) \
              * (_haloRngs[tid][m+5] - _haloRngs[tid][m+2]);
        for (unsigned int i=0; i<8; ++i)  std::cout << _haloRngs[tid][m+i] << " ";
        std::cout << std::endl;
      }
      std::cout << "thread vol: " << tVol << std::endl;
    }
  }
}


int HaloPacker::set_halo_range(HaloStore& hs, int cBegin, int cEnd, int nThrd)
{
  int sizes[3]; _ptDd->get_local_size(sizes);
  vector<int>         cRngs(8,0);
  vector<vector<int>> cStack;

  _cBegin = cBegin;
  _cEnd   = cEnd;
  vector<int> vecNull;
  _haloRngs.clear();
  _haloRngs.insert(_haloRngs.begin(), nThrd, vecNull);
  _bodyRngs.clear();
  _bodyRngs.insert(_bodyRngs.begin(), nThrd, vecNull);

  // add chunk ranges to stack, count k rows
  int sumCacheLines = 0;
  for (int i=cEnd-1; i>=cBegin; --i) {
    // skip halo chunk that is not used in comm
    if (hs.toRanks[i] == MPI_PROC_NULL)  continue;
    //
    for (int j=0; j<6; ++j)  cRngs[j] = hs.chunks[6*i+j];
    for (int v=hs.nVar-1; v>=0;  --v) {
      cRngs[6] = v;
      cRngs[7] = i;
      cStack.push_back(cRngs);
    }
    sumCacheLines += (cRngs[3] - cRngs[0]) * (cRngs[4] - cRngs[1])
                   * std::max(1, (cRngs[5] - cRngs[2]) / _cacheLineLen);
  }
  sumCacheLines *= hs.nVar;

  // average cache lines per thread
  vector<int> thrdRooms(nThrd, sumCacheLines/nThrd);
  for (int i=0; i<sumCacheLines%nThrd; ++i)  ++thrdRooms[i];

  // set range for each thread
  for (int tid=0; tid<nThrd; ++tid) {
    while (thrdRooms[tid] > 0) {
      // #cache lines of top chunk
      cRngs = cStack.back();
      cStack.pop_back();
      int nLine = (cRngs[3] - cRngs[0]) * (cRngs[4] - cRngs[1]) 
                * std::max(1, (cRngs[5] - cRngs[2]) / _cacheLineLen);
      // if current chunk fit in thread, assign it
      if (nLine <= thrdRooms[tid]) {
        _haloRngs[tid].insert(_haloRngs[tid].end(), cRngs.begin(), cRngs.end());
        thrdRooms[tid] -= nLine;
        if (thrdRooms[tid] > 0 && cStack.empty())
          std::cout << " HP Error: thread not full but chunks all assigned" << std::endl;
      }
      // else the whole chunk cannot fit in the thread room
      else {
        // adjust current and next thread room
        int nLinePerK = std::max(1, (cRngs[5] - cRngs[2]) / _cacheLineLen);
        int remainder = thrdRooms[tid] % nLinePerK;
        if (remainder > nLinePerK/2) {
          for (int j=tid+1; j<nThrd; ++j) {
            int delta = nLinePerK - remainder;
            if (delta > thrdRooms[j]) {
              delta -= thrdRooms[j];
              thrdRooms[tid] += thrdRooms[j];
              thrdRooms[j] = 0;
              //std::cout << "shift " << thrdRooms[j] << " from " << j << " to " << tid << std::endl;
            }
            else {
              thrdRooms[tid] += delta;
              thrdRooms[j]   -= delta;
              //std::cout << "shift " << delta << " from " << j << " to " << tid << std::endl;
              break;
            }
          }
        }
        else if (remainder > 0) {
          thrdRooms[tid] -= remainder;
          if (tid < nThrd-1) thrdRooms[tid+1] += remainder;
          //std::cout << "shift " << remainder << " from " << tid << " to " << tid+1 << std::endl;
        }
        // check if chunk fits in room now
        if (nLine <= thrdRooms[tid]) {
          _haloRngs[tid].insert(_haloRngs[tid].end(), cRngs.begin(), cRngs.end());
          break;
        }
        // split chunk and add to thread, add rest to stack
        int nPlnJK = (thrdRooms[tid] / nLinePerK) / (cRngs[4] - cRngs[1]); // # jk place
        int nRmnK  = (thrdRooms[tid] / nLinePerK) % (cRngs[4] - cRngs[1]); // # remaining k rows
        vector<int> tmpRngs = cRngs;
        // add planes to thread
        if (nPlnJK > 0) {
          tmpRngs[3] = cRngs[0] + nPlnJK;
          _haloRngs[tid].insert(_haloRngs[tid].end(), tmpRngs.begin(), tmpRngs.end());
          //std::cout << "assign " << nPlnJK * (cRngs[4] - cRngs[1]) * nLinePerK << " to room " << thrdRooms[tid] << std::endl;
          thrdRooms[tid]    -= nPlnJK * (cRngs[4] - cRngs[1]) * nLinePerK;
        }
        if (nRmnK > 0) {
          // add remaining k rows on plane
          tmpRngs[0] = cRngs[0] + nPlnJK;  tmpRngs[3] = cRngs[0] + nPlnJK + 1;
          tmpRngs[1] = cRngs[1];           tmpRngs[4] = cRngs[1] + nRmnK;
          _haloRngs[tid].insert(_haloRngs[tid].end(), tmpRngs.begin(), tmpRngs.end());
          //std::cout << "assign " << nRmnK * nLinePerK << " to room " << thrdRooms[tid] << std::endl;
          thrdRooms[tid]    -= nRmnK * nLinePerK;
          // add rest planes of chunk to stack
          if (nPlnJK < cRngs[3] - cRngs[0]) {
            tmpRngs = cRngs; tmpRngs[0] = cRngs[0] + nPlnJK + 1;
            cStack.push_back(tmpRngs);
            //std::cout << "push " << (tmpRngs[3] - tmpRngs[0]) * (tmpRngs[4] - tmpRngs[1]) * (tmpRngs[5] - tmpRngs[2]) << std::endl;
          }
          // add remaining k rows to stack
          tmpRngs[0] = cRngs[0] + nPlnJK;  tmpRngs[3] = cRngs[0] + nPlnJK + 1;
          tmpRngs[1] = cRngs[1] + nRmnK;   tmpRngs[4] = cRngs[4];
          cStack.push_back(tmpRngs);
            //std::cout << "push " << (tmpRngs[3] - tmpRngs[0]) * (tmpRngs[4] - tmpRngs[1]) * (tmpRngs[5] - tmpRngs[2]) << std::endl;
        }
        else { // nRmnK == 0
          tmpRngs = cRngs; 
          tmpRngs[0] = cRngs[0] + nPlnJK;
          cStack.push_back(tmpRngs);
        }
        break;
      }
    }//end while

    // set start index in buffer
    for (unsigned int m=0; m<_haloRngs[tid].size(); m+=8) {
      int c     = _haloRngs[tid][m+7];
      int cSize = (hs.chunks[c*6+3] - hs.chunks[c*6]) * (hs.chunks[c*6+4] - hs.chunks[c*6+1])
                * (hs.chunks[c*6+5] - hs.chunks[c*6+2]);
      _haloRngs[tid][m+7]
        = hs.bufMap[c] + _haloRngs[tid][m+6] * cSize 
        + (_haloRngs[tid][m+2] - hs.chunks[c*6+2])
        + (_haloRngs[tid][m+1] - hs.chunks[c*6+1]) * (hs.chunks[c*6+5] - hs.chunks[c*6+2])
        + (_haloRngs[tid][m]   - hs.chunks[c*6]  ) * (hs.chunks[c*6+4] - hs.chunks[c*6+1])
        * (hs.chunks[c*6+5]  - hs.chunks[c*6+2]);
    }

    _bodyRngs[tid] = _haloRngs[tid];
    // set the face range
    for (unsigned int m=0; m<_haloRngs[tid].size(); m+=8) {
      for (unsigned int i=0; i<3; ++i) {
        if (_haloRngs[tid][m+i] < _nHalo) {
          _bodyRngs[tid][m+i]   += _nHalo;
          _bodyRngs[tid][m+i+3] += _nHalo;
        }
        else if (_haloRngs[tid][m+i] >= sizes[i]+_nHalo) {
          _bodyRngs[tid][m+i]   -= _nHalo;
          _bodyRngs[tid][m+i+3] -= _nHalo;
        }
      }
    }
  }//end for

  return 0;
}
