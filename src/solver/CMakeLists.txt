set(solverSrc
  sys.cpp
  DomainDecomp.cpp
  UniMesh.cpp
  HaloStore.cpp
  HaloTransfer.cpp
  HaloPacker.cpp
  Solver.cpp
  WJSolver.cpp
  BurgersSolver.cpp
  WenoSolver.cpp
)

add_library(solver STATIC ${solverSrc})

if (likwid_dir)
  target_link_libraries(solver ${likwidLib})
endif()

if (papi_dir)
  target_link_libraries(solver ${papiLib})
endif()
