#include "sys.h"
#include <iostream>
#include <sstream>
#include <string>
#include <ios>
#include <mpi.h>
#include <typeinfo>

#ifdef PAPI
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <papi.h>
#endif

// global option hash table
OptionTable optTab;


int sys_init(int argc, char* argv[], int mode = MPI_THREAD_SINGLE)
{
  int modeGot = MPI_THREAD_SINGLE, rank;

#ifdef _OPENMP
  // set thread mode
  MPI_Init_thread(&argc, &argv, mode, &modeGot);
  // check received thread mode
  MPI_Comm_rank(MPI_COMM_WORLD, &rank); 
  if (rank == 0 && mode != modeGot) {
    std::cout << "MPI INIT ERROR: set " << mode << " get " << modeGot << std::endl; 
    MPI_Finalize();
  }
#else
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank); 
#endif

  // save command line options
  std::string name;
  for (int iArg=1; iArg<argc; ++iArg) {
    if (argv[iArg][0] == '-') {
      name         = argv[iArg] + 1;
      optTab[name] = "";
    }
    else {
      optTab[name].append(" ");
      optTab[name].append(argv[iArg]);
    }
  }

  return 0;
}


void view_cmd_line_arg()
{
  OptionTable::iterator iter;
  for (iter=optTab.begin(); iter != optTab.end(); ++iter) {
    if (iter->second.empty())
      std::cout << iter->first << "\t" << true << std::endl;
    else
      std::cout << iter->first << "\t" << iter->second << std::endl;
  }
}


void sys_finalize()
{
  MPI_Finalize();
}


int get_option(const std::string& name, const CmdOption type, const int nVal, 
               void* ptVal, bool* ptIsSet)
{
  // default not found
  *ptIsSet = false;

  // search option tab
  OptionTable::iterator it = optTab.find(name);
  if (it != optTab.end()) {// if find
    std::stringstream sout(optTab[name], std::ios_base::in);
    switch (type) {
      case CmdOption::Int:
        for (int i=0; i<nVal; ++i) 
          sout >> (reinterpret_cast<int*>(ptVal))[i];
        break;
      case CmdOption::Double:
        for (int i=0; i<nVal; ++i) 
          sout >> (reinterpret_cast<double*>(ptVal))[i];
        break;
      case CmdOption::Bool:
        for (int i=0; i<nVal; ++i) 
          (reinterpret_cast<bool*>(ptVal))[i] = true;
        break;
      case CmdOption::Str:
        for (int i=0; i<nVal; ++i) 
          sout >> (reinterpret_cast<std::string*>(ptVal))[i];
        break;
    }// end switch
    //set  found
    *ptIsSet = true;
  }// end if find

  return 0;
}


#ifdef PAPI
void test_fail(int retval, char* call)
{
  if (retval != PAPI_OK) {
    if (retval == PAPI_ESYS) {
      char buf[128];
      memset( buf, '\0', sizeof(buf) );
      sprintf(buf, "System error in %s:", call );
      perror(buf);
    }
    else if ( retval > 0 ) {
      printf("Error calculating: %s\n", call );
    }
    else {
      printf("Error in %s: %s\n", call, PAPI_strerror(retval) );
    }
    exit(1);
  }
}
#endif
