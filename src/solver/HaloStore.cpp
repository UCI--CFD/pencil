#include "HaloStore.h"

#include <vector>

using std::vector;

HaloStore::HaloStore(DomainDecomp& dd, int nHalo, int nVariable): 
  nVar(nVariable),
  _ptDd(&dd),  
  _nh(nHalo),
  _nPipe(1)
{}


HaloStore::~HaloStore()
{
  delete [] sBuf;
  delete [] rBuf;
}


void HaloStore::init(DomainDecomp& dd, int nPipe, int nHalo, int nVariable)
{
  _ptDd  = &dd;
  _nPipe = nPipe;
  _nh    = nHalo;
  nVar   = nVariable;
}


void HaloStore::set_from_option()
{
  bool isSet;
  get_option("hs_nvar",    CmdOption::Int, 1, &nVar,   &isSet);
  get_option("pipeline",   CmdOption::Int, 1, &_nPipe, &isSet);
}


int HaloStore::setup_star()
/* 
 'chunks' have 6 face chunks, 'toChunks' has the opposite chunk id
 'toRanks' is mpi_proc_null if that boundary is not communicated.
*/
{
  int szs[3];
  _ptDd->get_local_size(szs);

  // allocate buffer
  bufSize = (szs[0]*szs[1] + szs[1]*szs[2] + szs[2]*szs[0]) * _nh * 2 * nVar;
  sBuf = new double [bufSize];
  rBuf = new double [bufSize];

  // set chunks
  Halo faces[] = {FaceXm, FaceYm, FaceZm, FaceXp, FaceYp, FaceZp};
  for (int i=0; i<6; ++i) {
    if (_ptDd->need_comm(i)) {
      if (faces[i] == FaceZm) {
        kmBufStart.push_back(0);
        for (unsigned int i=0; i<chunks.size(); i+=6)
          kmBufStart.back() 
            += (chunks[i+3]-chunks[i+0]) * (chunks[i+4]-chunks[i+1])  
             * (chunks[i+5]-chunks[i+2]) * nVar;
      }// end if
      if (faces[i] == FaceZp) {
        kpBufStart.push_back(0);
        for (unsigned int i=0; i<chunks.size(); i+=6)
          kpBufStart.back() 
            += (chunks[i+3]-chunks[i+0]) * (chunks[i+4]-chunks[i+1])  
             * (chunks[i+5]-chunks[i+2]) * nVar;
      }// end if
    }// end if need_comm
    int rngs[6];
    get_halo_chunk_range(faces[i], rngs);
    chunks.insert(chunks.end(), rngs, rngs+6);
    toRanks.push_back(_ptDd->neighbor(static_cast<int>(faces[i])));
    toChunkIDs.push_back((i>=3 ? i-3 : i+3));
  }

  if(kpBufStart.empty()) kpBufStart.push_back(0);
  if(kmBufStart.empty()) kmBufStart.push_back(0);

  // buffer map: start index of each chunk in buffer
  bufMap.push_back(0);
  for (unsigned int i=0; i<chunks.size(); i+=6)
    bufMap.push_back( (chunks[i+3]-chunks[i+0]) * (chunks[i+4]-chunks[i+1]) 
                    * (chunks[i+5]-chunks[i+2]) * nVar + bufMap.back());

  return 0;
}


int HaloStore::setup_box()
/* 
 'chunks' have 26 + 8*(nPipe-1) chunks, including face, edges, corners
 'toChunks' has the opposite chunk id
 'toRanks' is mpi_proc_null if that boundary is not communicated.
*/
{
  int szs[3];
  _ptDd->get_local_size(szs);

  // allocate buffer
  bufSize = ((szs[0]+2*_nh)*(szs[1]+2*_nh)*(szs[2]+2*_nh) - szs[0]*szs[1]*szs[2]) * nVar;
  sBuf = new double [bufSize];
  rBuf = new double [bufSize];

  // set chunks
  vector<Halo> cids; 
  int pipeLen = szs[0] / _nPipe, rngs[6];
  // x-
  cids = {CornerXmYmZm, EdgeXmYm, CornerXmYmZp, EdgeXmZm, FaceXm, EdgeXmZp, \
          CornerXmYpZm, EdgeXmYp, CornerXmYpZp};
  for (unsigned int i=0; i<cids.size(); ++i) {
    get_halo_chunk_range(cids[i], rngs);
    chunks.insert(chunks.end(), rngs, rngs+6);
    toChunkIDs.push_back(25 - i + 8*(_nPipe-1));
    toRanks.push_back(_ptDd->neighbor((int)cids[i]));
  }
  // inbetween
  cids = {EdgeYmZm, FaceYm, EdgeYmZp, FaceZm, /*skip*/ FaceZp, EdgeYpZm, \
          FaceYp, EdgeYpZp};
  for (int i=0; i<_nPipe; ++i) {
    int iBegin = _nh + i*pipeLen, iEnd = iBegin + pipeLen;
    for (unsigned int j=0; j<cids.size(); ++j) {
      if (_ptDd->neighbor(cids[j]) != MPI_PROC_NULL) {
        if (cids[j] == FaceZm) {
          kmBufStart.push_back(0);
          for (unsigned int i=0; i<chunks.size(); i+=6)
            kmBufStart.back() 
              += (chunks[i+3]-chunks[i+0]) * (chunks[i+4]-chunks[i+1])  
               * (chunks[i+5]-chunks[i+2]) * nVar;
        }// end if FaceZm
        if (cids[j] == FaceZp) {
          kpBufStart.push_back(0);
          for (unsigned int i=0; i<chunks.size(); i+=6)
            kpBufStart.back() 
              += (chunks[i+3]-chunks[i+0]) * (chunks[i+4]-chunks[i+1])  
               * (chunks[i+5]-chunks[i+2]) * nVar;
        }// end if FaceZp
      }// end if mpi_proc_null
      get_halo_chunk_range(cids[j], rngs);
      rngs[0] = iBegin;
      rngs[3] = iEnd;
      chunks.insert(chunks.end(), rngs, rngs+6);
      toChunkIDs.push_back(7-j + 8*i + 9);
      toRanks.push_back(_ptDd->neighbor(static_cast<int>(cids[j])));
    }
  }
  // x+
  cids = {CornerXpYmZm, EdgeXpYm, CornerXpYmZp, EdgeXpZm, FaceXp, EdgeXpZp, \
          CornerXpYpZm, EdgeXpYp, CornerXpYpZp};
  for (unsigned int i=0; i<cids.size(); ++i) {
    get_halo_chunk_range(cids[i], rngs);
    chunks.insert(chunks.end(), rngs, rngs+6);
    toChunkIDs.push_back(8 - i);
    toRanks.push_back(_ptDd->neighbor((int)cids[i]));
  }

  if(kpBufStart.empty()) kpBufStart.push_back(0);
  if(kmBufStart.empty()) kmBufStart.push_back(0);

  // buffer map: start index of each chunk in buffer
  bufMap.push_back(0);
  for (unsigned int i=0; i<chunks.size(); i+=6)
    bufMap.push_back( (chunks[i+3]-chunks[i+0]) * (chunks[i+4]-chunks[i+1]) 
                    * (chunks[i+5]-chunks[i+2]) * nVar + bufMap.back());

  return 0;
}


void HaloStore::get_halo_chunk_range(Halo chunk, int hRngs[6]) const
{
  int sizes[3], rngs[6];
  _ptDd->get_local_size(sizes);
  for (int i=0; i<3; ++i) {
    rngs[i]   = _nh;
    rngs[i+3] = sizes[i] + _nh;
  }
  get_halo_chunk_range(rngs, chunk, hRngs);
}


void HaloStore::get_halo_chunk_range(int rngs[6], Halo chunk, int hRngs[6]) const
{
  std::copy(rngs, rngs+6, hRngs);

  switch (chunk) {
    case FaceXm: 
      hRngs[0] = rngs[0] - _nh;   hRngs[3] = rngs[0]; break;
    case FaceYm:
      hRngs[1] = rngs[1] - _nh;   hRngs[4] = rngs[1]; break;
    case FaceZm:
      hRngs[2] = rngs[2] - _nh;   hRngs[5] = rngs[2]; break;
    case FaceXp:
      hRngs[0] = rngs[3];         hRngs[3] = rngs[3] + _nh; break;
    case FaceYp:
      hRngs[1] = rngs[4];         hRngs[4] = rngs[4] + _nh; break;
    case FaceZp:
      hRngs[2] = rngs[5];         hRngs[5] = rngs[5] + _nh; break;
    case EdgeYmZm:
      hRngs[1] = rngs[1] - _nh;   hRngs[4] = rngs[1];
      hRngs[2] = rngs[2] - _nh;   hRngs[5] = rngs[2]; break;
    case EdgeYpZm:
      hRngs[1] = rngs[4];         hRngs[4] = rngs[4] + _nh;
      hRngs[2] = rngs[2] - _nh;   hRngs[5] = rngs[2]; break;
    case EdgeYmZp:
      hRngs[1] = rngs[1] - _nh;   hRngs[4] = rngs[1];
      hRngs[2] = rngs[5];         hRngs[5] = rngs[5] + _nh; break;
    case EdgeYpZp:
      hRngs[1] = rngs[4];         hRngs[4] = rngs[4] + _nh;
      hRngs[2] = rngs[5];         hRngs[5] = rngs[5] + _nh; break;
    case EdgeXmZm:
      hRngs[0] = rngs[0] - _nh;   hRngs[3] = rngs[0];
      hRngs[2] = rngs[2] - _nh;   hRngs[5] = rngs[2]; break;
    case EdgeXmZp:
      hRngs[0] = rngs[0] - _nh;   hRngs[3] = rngs[0];
      hRngs[2] = rngs[5];         hRngs[5] = rngs[5] + _nh; break;
    case EdgeXpZm:
      hRngs[0] = rngs[3];         hRngs[3] = rngs[3] + _nh;
      hRngs[2] = rngs[2] - _nh;   hRngs[5] = rngs[2]; break;
    case EdgeXpZp:
      hRngs[0] = rngs[3];         hRngs[3] = rngs[3] + _nh;
      hRngs[2] = rngs[5];         hRngs[5] = rngs[5] + _nh; break;
    case EdgeXmYm:
      hRngs[0] = rngs[0] - _nh;   hRngs[3] = rngs[0];
      hRngs[1] = rngs[1] - _nh;   hRngs[4] = rngs[1]; break;
    case EdgeXpYm:
      hRngs[0] = rngs[3];         hRngs[3] = rngs[3] + _nh;
      hRngs[1] = rngs[1] - _nh;   hRngs[4] = rngs[1]; break;
    case EdgeXmYp:
      hRngs[0] = rngs[0] - _nh;   hRngs[3] = rngs[0];
      hRngs[1] = rngs[4];         hRngs[4] = rngs[4] + _nh; break;
    case EdgeXpYp:
      hRngs[0] = rngs[3];         hRngs[3] = rngs[3] + _nh;
      hRngs[1] = rngs[4];         hRngs[4] = rngs[4] + _nh; break;
    case CornerXmYmZm:
      hRngs[0] = rngs[0] - _nh;   hRngs[3] = rngs[0];
      hRngs[1] = rngs[1] - _nh;   hRngs[4] = rngs[1];
      hRngs[2] = rngs[2] - _nh;   hRngs[5] = rngs[2]; break;
    case CornerXmYmZp:
      hRngs[0] = rngs[0] - _nh;   hRngs[3] = rngs[0];
      hRngs[1] = rngs[1] - _nh;   hRngs[4] = rngs[1];
      hRngs[2] = rngs[5];         hRngs[5] = rngs[5] + _nh; break;
    case CornerXmYpZm:
      hRngs[0] = rngs[0] - _nh;   hRngs[3] = rngs[0];
      hRngs[1] = rngs[4];         hRngs[4] = rngs[4] + _nh;
      hRngs[2] = rngs[2] - _nh;   hRngs[5] = rngs[2]; break;
    case CornerXmYpZp:
      hRngs[0] = rngs[0] - _nh;   hRngs[3] = rngs[0];
      hRngs[1] = rngs[4];         hRngs[4] = rngs[4] + _nh;
      hRngs[2] = rngs[5];         hRngs[5] = rngs[5] + _nh; break;
    case CornerXpYmZm:
      hRngs[0] = rngs[3];         hRngs[3] = rngs[3] + _nh;
      hRngs[1] = rngs[1] - _nh;   hRngs[4] = rngs[1];
      hRngs[2] = rngs[2] - _nh;   hRngs[5] = rngs[2]; break;
    case CornerXpYmZp:
      hRngs[0] = rngs[3];         hRngs[3] = rngs[3] + _nh;
      hRngs[1] = rngs[1] - _nh;   hRngs[4] = rngs[1];
      hRngs[2] = rngs[5];         hRngs[5] = rngs[5] + _nh; break;
    case CornerXpYpZm:
      hRngs[0] = rngs[3];         hRngs[3] = rngs[3] + _nh;
      hRngs[1] = rngs[4];         hRngs[4] = rngs[4] + _nh;
      hRngs[2] = rngs[2] - _nh;   hRngs[5] = rngs[2]; break;
    case CornerXpYpZp:
      hRngs[0] = rngs[3];         hRngs[3] = rngs[3] + _nh;
      hRngs[1] = rngs[4];         hRngs[4] = rngs[4] + _nh;
      hRngs[2] = rngs[5];         hRngs[5] = rngs[5] + _nh; break;
  }
}


void HaloStore::debug_view()
{
  if (_ptDd->is_root()) {
    std::cout << "Debug View of Halo Store Object" << std::endl;
    std::cout << bufMap.size() - 1<< " halo chunks" << std::endl;

    std::cout << "start loc in buffer: " << std::endl;
    for (unsigned int i=0; i<bufMap.size()-1; ++i)
      std::cout << bufMap[i] << " ";
    std::cout << std::endl;

    std::cout << "chunk: range | toChunk | toRank" << std::endl;
    for (unsigned int i=0; i<chunks.size(); i+=6) {
      int c = i / 6;
      std::cout << c;
      for (unsigned int j=0; j<6; ++j) std::cout << "\t" << chunks[i+j];
      std::cout << " | " << toChunkIDs[c] << " | " << toRanks[c] << std::endl;
    }
  }
}


bool HaloStore::need_comm(Halo chunk) const
{
  switch (chunk) {
    case FaceXm: 
      return _ptDd->need_comm(0); break;
    case FaceYm:
      return _ptDd->need_comm(1); break;
    case FaceZm:
      return _ptDd->need_comm(2); break;
    case FaceXp:
      return _ptDd->need_comm(3); break;
    case FaceYp:
      return _ptDd->need_comm(4); break;
    case FaceZp:
      return _ptDd->need_comm(5); break;
    case EdgeYmZm:
      return (_ptDd->need_comm(1) && _ptDd->need_comm(2)); break;
    case EdgeYpZm:
      return (_ptDd->need_comm(4) && _ptDd->need_comm(2)); break;
    case EdgeYmZp:
      return (_ptDd->need_comm(1) && _ptDd->need_comm(5)); break;
    case EdgeYpZp:
      return (_ptDd->need_comm(4) && _ptDd->need_comm(5)); break;
    case EdgeXmZm:
      return (_ptDd->need_comm(0) && _ptDd->need_comm(2)); break;
    case EdgeXmZp:
      return (_ptDd->need_comm(0) && _ptDd->need_comm(5)); break;
    case EdgeXpZm:
      return (_ptDd->need_comm(3) && _ptDd->need_comm(2)); break;
    case EdgeXpZp:
      return (_ptDd->need_comm(3) && _ptDd->need_comm(5)); break;
    case EdgeXmYm:
      return (_ptDd->need_comm(0) && _ptDd->need_comm(1)); break;
    case EdgeXpYm:
      return (_ptDd->need_comm(3) && _ptDd->need_comm(1)); break;
    case EdgeXmYp:
      return (_ptDd->need_comm(0) && _ptDd->need_comm(4)); break;
    case EdgeXpYp:
      return (_ptDd->need_comm(3) && _ptDd->need_comm(4)); break;
    case CornerXmYmZm:
      return (_ptDd->need_comm(0) && _ptDd->need_comm(1) && _ptDd->need_comm(2)); break;
    case CornerXmYmZp:
      return (_ptDd->need_comm(0) && _ptDd->need_comm(1) && _ptDd->need_comm(5)); break;
    case CornerXmYpZm:
      return (_ptDd->need_comm(0) && _ptDd->need_comm(4) && _ptDd->need_comm(2)); break;
    case CornerXmYpZp:
      return (_ptDd->need_comm(0) && _ptDd->need_comm(4) && _ptDd->need_comm(5)); break;
    case CornerXpYmZm:
      return (_ptDd->need_comm(3) && _ptDd->need_comm(1) && _ptDd->need_comm(2)); break;
    case CornerXpYmZp:
      return (_ptDd->need_comm(3) && _ptDd->need_comm(1) && _ptDd->need_comm(5)); break;
    case CornerXpYpZm:
      return (_ptDd->need_comm(3) && _ptDd->need_comm(4) && _ptDd->need_comm(2)); break;
    case CornerXpYpZp:
      return (_ptDd->need_comm(3) && _ptDd->need_comm(4) && _ptDd->need_comm(5)); break;
    default:
      return false;
  }
}
