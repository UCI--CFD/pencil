#include "Solver.h"
#include "Timer.h"
#include <string>
#include <fstream>
#include <iostream>
#include <numeric>
#include <cassert>
#include <cmath>

#ifdef PAPI
#include <papi.h>
#endif

#ifdef LIKWID_PERFMON
#include <likwid.h>
#include <likwid-marker.h>
#endif

using std::initializer_list;

Solver::Solver(DomainDecomp& dd, UniMesh& mesh, int nh):
  _ptDd(&dd),
  _hs(dd, nh, 1),
  _nHalo(nh),
  _scheme(0),
  _isSyncComm(false),
  _trace(false),
  _nVar(1), _nVarComp(1),
  _sr(1),
  _isBufNuma(true),
  _copyLen(8),
  _bypassKHalo(false),
  _isDiamond(false),
  _nDiamond{1,1},
  _nRectangle{1,1},
  _isWarmUp(false),
  _dhOff(false)
{
  _h  =  mesh.step();

  _t = new double** [_ptDd->num_thread()];
  for (int i=0; i<_ptDd->num_thread(); ++i) {
    _t[i] = new double* [TIMER_MAX_ITEM];
    for (int j=0; j<TIMER_MAX_ITEM; ++j) {
      _t[i][j] = new double [TIMER_MAX_SIZE];
    }
  }

  _thrdIter.insert(_thrdIter.begin(), _ptDd->num_thread(), 0);
}


Solver::~Solver()
{
  for (int i=0; i<_ptDd->num_thread(); ++i) {
    for (int j=0; j<TIMER_MAX_ITEM; ++j) 
      delete [] _t[i][j];
    delete [] _t[i];
  }
  delete [] _t;

  for (int j=0; j<_nDiamond[0]; ++j) 
    delete [] _diamonds[j];
  delete [] _diamonds;
}


void Solver::set_from_option()
{
  bool isSet;
  _hs.set_from_option();
  get_option("trapezoid",CmdOption::Int,  2, _nDiamond,     &_isDiamond); 
  get_option("synccomm", CmdOption::Bool, 1, &_isSyncComm,  &isSet);
  get_option("trace",    CmdOption::Bool, 1, &_trace,       &isSet);
  get_option("split",    CmdOption::Int,  2, _nRectangle,   &isSet);
  get_option("numabuf",  CmdOption::Bool, 1, &_isBufNuma,   &isSet);
  get_option("copylen",  CmdOption::Int,  1, &_copyLen,     &isSet);
  get_option("scheme",   CmdOption::Int,  1, &_scheme,      &isSet);
  get_option("bypassk",  CmdOption::Bool, 1, &_bypassKHalo, &isSet);
  get_option("dhoff",    CmdOption::Bool, 1, &_dhOff,       &isSet);
}


int Solver::setup()
{
  int nThrd = _ptDd->num_thread();
  int nPipe = _hs.num_pipe(); 

  _hts = new HaloTransfer [nPipe];
  for (int i=0; i<nPipe; ++i) _hts[i].init(_ptDd, _nHalo);
  _hps = new HaloPacker [nPipe+2];
  for (int i=0; i<nPipe+2; ++i) _hps[i].init(_ptDd, _nHalo);

  // star shape is not pipelined, box may be
  if (_ptDd->is_neighbor_star()) {
    assert(nPipe == 1);
    _hs.setup_star();
    if (_bypassKHalo) _hts[0].set_bypass_khalo();
    _hts[0].set_halo_range(_hs, 0, _hs.chunks.size()/6, nThrd);
    _hts[0].set_copy_length(_copyLen);
  }
  else {
    _hs.setup_box();
    if (nPipe == 1) {
      if (_bypassKHalo) _hts[0].set_bypass_khalo();
      _hts[0].set_halo_range(_hs, 0, _hs.chunks.size()/6, nThrd);
      _hts[0].set_copy_length(_copyLen);
    }
    else {
      // hard coded assuming pipe dir is i and all faces need communication
      // halo transfer, each item has the halo attached to it
      if (_bypassKHalo) _hts[0].set_bypass_khalo();
      _hts[0].set_halo_range(_hs, 0, 17, nThrd);
      for (int i=1; i<nPipe-1; ++i) {
        if (_bypassKHalo) _hts[i].set_bypass_khalo();
        _hts[i].set_halo_range(_hs, 17+8*(i-1), 17+8*i, nThrd);
      }
      if (_bypassKHalo) _hts[nPipe-1].set_bypass_khalo();
      _hts[nPipe-1].set_halo_range(_hs, 9+8*(nPipe-1), _hs.chunks.size()/6, nThrd);
      // pipe id for an iteration
      _iterPipe.push_back(1);
      _iterPipe.push_back(0);
      for (int i=2; i<nPipe; ++i) _iterPipe.push_back(i);
      // halo packer, i faces are separate from chunks parallel to i
      if (_bypassKHalo) _hps[0].set_bypass_khalo();
      _hps[0].set_halo_range(_hs, 0, 9, nThrd);
      for (int i=1; i<=nPipe; ++i) {
        if (_bypassKHalo) _hps[i].set_bypass_khalo();
        _hps[i].set_halo_range(_hs, 9+8*(i-1), 9+8*i, nThrd);
      }
      if (_bypassKHalo) _hps[nPipe+1].set_bypass_khalo();
      _hps[nPipe+1].set_halo_range(_hs, 9+8*nPipe, 18+8*nPipe, nThrd);
      // halo to unpack for each pipe, the input id corresponds to _hps
      _haloToUnpack.push_back(vector<int>());
      _haloToUnpack.emplace_back(initializer_list<int>({0, nPipe}));
      _haloToUnpack.emplace_back(initializer_list<int>({1, 2}));
      for (int i=3; i<nPipe-1; ++i) _haloToUnpack.emplace_back(initializer_list<int>({i}));
      _haloToUnpack.emplace_back(initializer_list<int>({nPipe-1, nPipe+1}));
      // set copy length
      for (int i=0; i<nPipe; ++i)
        _hts[i].set_copy_length(_copyLen);
      for (int i=0; i<nPipe+2; ++i)
        _hps[i].set_copy_length(_copyLen);
    }
  }

  // init papi event, value
#ifdef PAPI
  _papiSet.insert(_papiSet.begin(), nThrd, PAPI_NULL);
  _papiVal.insert(_papiVal.begin(), nThrd, 0);
  _flop.insert(_flop.begin(), nThrd, 0.0);
#endif

  return 0;
}


int Solver::solve(int nIter, int** dependVars)
{
  _nIter = nIter;
  if (nIter == 0) return 0;

  int tid = omp_get_thread_num();

#ifdef PAPI
  if (_mode != MPI_BSP) {
    PAPI_register_thread();
  }
  PAPI_create_eventset(&_papiSet[tid]);
  PAPI_add_event(_papiSet[tid], PAPI_DP_OPS);
#endif

  _t[tid][Total][0] = mytime();
  switch (_mode) {
    case MPI_BSP: _solve_mpi_bsp(nIter);
    break;
    case Funneled: _solve_funneled(nIter);
    break;
    case Tile: 
      if (_isDiamond)
        _solve_diamond(nIter, dependVars);
      else
        _solve_rectangle(nIter);
    break;
    case PipelineTile:
      if (_isDiamond)
        _solve_pipeline_diamond(nIter, dependVars);
      else
        _solve_pipeline_rectangle(nIter);
    break;
    case PollTile:
      if (_isDiamond)
        _solve_poll_trap(nIter, dependVars);
      else
        _solve_poll_ovlp(nIter);
    break;
  }
  _t[tid][Total][1] = mytime();

#ifdef PAPI
  PAPI_cleanup_eventset(_papiSet[tid]);
  PAPI_destroy_eventset(&_papiSet[tid]);
  if (_mode != MPI_BSP)  PAPI_unregister_thread();
#endif

  if (tid == 0)  _isWarmUp = false;

  return 0;
}


int Solver::analyze_time()
{
  switch (_mode) {
    case MPI_BSP: _analyze_time_mpi();
    break;
    case Funneled:
    case Tile: _analyze_time_hybrid();
    break;
    case PipelineTile: _analyze_time_pipeline();
    break;
    case PollTile: _analyze_time_poll();
    break;
  }

#ifdef DEBUG
  _hts[0].print_pack_time();
#endif

  return 0;
}


void Solver::clear_history()
{
  int tid = omp_get_thread_num();
  for (int i=0; i<TIMER_MAX_ITEM; ++i)
    for (int j=0; j<TIMER_MAX_SIZE; ++j)
      _t[tid][i][j] = 0.0;
}


void Solver::copy_var(int i, double* var)
{ int szs[3]; _ptDd->get_local_size(szs); std::copy(_d[i], _d[i]+(szs[0]+2*_nHalo)*(szs[1]+2*_nHalo)*(szs[2]+2*_nHalo), var);
}


int Solver::_solve_mpi_bsp(int nIter)
{
  int rngs[6]; _ptDd->get_local_size(rngs);
  for (int i=0; i<3; ++i) {
    rngs[i+3] = rngs[i] + _nHalo;
    rngs[i]   = _nHalo;
  }
  int rjSize = (rngs[4]-rngs[1])/_nRectangle[0], rjCeil = (rngs[4]-rngs[1])%_nRectangle[0];

  for (int m=0; m<nIter; ++m) {
    // comp
#ifdef PAPI
    PAPI_start(_papiSet[0]);
#endif
    _t[0][Comp][m] = mytime();
    int rectRngs[6];
    std::copy(rngs, rngs+6, rectRngs);
    for (int rj=0; rj<_nRectangle[0]; ++rj) {
      rectRngs[4] = rectRngs[1] + (rj < rjCeil ? rjSize+1 : rjSize);
      _compute_range(rectRngs, true);
      rectRngs[1] = rectRngs[4];
    }
    ++_thrdIter[0];
#ifdef PAPI
    PAPI_stop(_papiSet[0], &_papiVal[0]);
    _flop[0] += static_cast<double>(_papiVal[0]);
#endif
    std::swap(_d[0], _d[1]);
    // pack
    _t[0][Pack][m] = mytime();
    _hts[0].pack_halo(_d, _haloVar[1], _hs);
    // comm
#ifdef SYNC_COMM
    MPI_Barrier(MPI_COMM_WORLD);
#endif
    _t[0][Comm][m] = mytime();
    _hts[0].update_halo(_hs);
    // unpack
    _t[0][Unpack][m] = mytime();
    _hts[0].unpack_halo(_d, _haloVar[1], _hs);
    _t[0][Last][m] = mytime();
  }

  return 0;
}


int Solver::_solve_funneled(int nIter)
{
  // range work on
  int szs[6], rngs[6], tid=omp_get_thread_num();
  _ptDd->get_local_size(szs);
  _ptDd->get_thread_range(tid, rngs);
  for (int i=0; i<6; ++i) rngs[i] += _nHalo;
  int rjSize = (rngs[4]-rngs[1])/_nRectangle[0], rjCeil = (rngs[4]-rngs[1])%_nRectangle[0];

  for (int m=0; m<nIter; ++m) {
    int m2 = 2*m, oddEven = m%2;
#ifdef LIKWID_PERFMON
    if(!_isWarmUp)  LIKWID_MARKER_START("Comp");
#endif
#ifdef PAPI
    if(!_isWarmUp)  PAPI_start(_papiSet[tid]);
#endif
    _t[tid][Comp][m2] = mytime();
    int rectRngs[6];
    std::copy(rngs, rngs+6, rectRngs);
    for (int rj=0; rj<_nRectangle[0]; ++rj) {
      rectRngs[4] = rectRngs[1] + (rj < rjCeil ? rjSize+1 : rjSize);
      _compute_range(rectRngs, m%2==0);
      rectRngs[1] = rectRngs[4];
    }
    ++_thrdIter[tid];
    _t[tid][Comp][m2+1] = mytime();
#ifdef PAPI
    if (!_isWarmUp) {
      PAPI_stop(_papiSet[tid], &_papiVal[tid]);
      _flop[tid] += static_cast<double>(_papiVal[tid]);
    }
#endif
#ifdef LIKWID_PERFMON
    if(!_isWarmUp)  LIKWID_MARKER_STOP("Comp");
#endif
    #pragma omp barrier
    _t[tid][Pack][m2] = mytime();
    _hts[0].pack_halo(_d, _haloVar[oddEven], _hs);
    _t[tid][Pack][m2+1] = mytime();
    #pragma omp barrier
    if (tid == _ptDd->num_thread()-1) {
#ifdef SYNC_COMM
      MPI_Barrier(MPI_COMM_WORLD);
#endif
      _t[tid][Comm][m2] = mytime();
      _hts[0].update_halo(_hs);
      _t[tid][Comm][m2+1] = mytime();
    }
    #pragma omp barrier
    _t[tid][Unpack][m2] = mytime();
    _hts[0].unpack_halo(_d, _haloVar[oddEven], _hs);
    _t[tid][Unpack][m2+1] = mytime();
    #pragma omp barrier
    _t[tid][Last][m2+1] = mytime(); 
  }

  return 0;
}


int Solver::_solve_diamond(int nIter, int** dependVars)
{
  int szs[6], tid = omp_get_thread_num();
  _ptDd->get_local_size(szs);

  int nf = _nHalo / _sr;
  for (int r=0; r<(int)ceil((double)nIter/nf); ++r) {
    int dir = r * nf % 2;
    int r2  = 2*r;
#ifdef LIKWID_PERFMON
    if (!_isWarmUp)  LIKWID_MARKER_START("Comp");
#endif
#ifdef PAPI
    if(!_isWarmUp)  PAPI_start(_papiSet[tid]);
#endif
  // reset diamond to undone, each will be marked done at the end of 
  // computation function
    _t[tid][Comp][r2] = mytime();
    if (_nDiamond[0] == 1) {
  // diamond tiling computation
      #pragma omp single
      {
        // independent diamonds
        for (int j=0; j<_nDiamond[1]; j+=2) {
          #pragma omp task depend(out: dependVars[0][j])
          _compute_pipe_diamond(0, j, 0, dir, 0);
        }
        // depend on left and right
        for (int j=1; j<_nDiamond[1]; j+=2) {
          #pragma omp task depend(in: dependVars[0][j-1]) depend(in: dependVars[0][j+1])
          _compute_pipe_diamond(0, j, 0, dir, 0);
        }
      }// end single
    }
    else {
      #pragma omp single
      {
        for (int i=0; i<_nDiamond[0]; i+=2) {
          for (int j=0; j<_nDiamond[1]; j+=2) {
          #pragma omp task depend(out: dependVars[i][j])
            _compute_pipe_diamond(i, j, 0, dir, 0);
          }
        }
        for (int i=1; i<_nDiamond[0]; i+=2) {
          for (int j=0; j<_nDiamond[1]; j+=2) {
          #pragma omp task depend(in: dependVars[i-1][j]) depend(in: dependVars[i+1][j]) \
                           depend(out: dependVars[i][j])
            _compute_pipe_diamond(i, j, 0, dir, 0);
          }
        }
        for (int i=0; i<_nDiamond[0]; i+=2) {
          for (int j=1; j<_nDiamond[1]; j+=2) {
          #pragma omp task depend(in: dependVars[i][j-1]) depend(in: dependVars[i][j+1]) \
                           depend(out: dependVars[i][j])
            _compute_pipe_diamond(i, j, 0, dir, 0);
          }
        }
        for (int i=1; i<_nDiamond[0]; i+=2) {
          for (int j=1; j<_nDiamond[1]; j+=2) {
          #pragma omp task depend(in: dependVars[i-1][j]) depend(in: dependVars[i+1][j]) \
                           depend(in: dependVars[i][j-1]) depend(in: dependVars[i][j+1]) \
                           depend(out:dependVars[i][j])
            _compute_pipe_diamond(i, j, 0, dir, 0);
          }
        }// end for i
      }// end single
    }// end else
    ++_thrdIter[tid];
    _t[tid][Comp][r2+1] = mytime();
#ifdef PAPI
    if (!_isWarmUp) {
      PAPI_stop(_papiSet[tid], &_papiVal[tid]);
      _flop[tid] += static_cast<double>(_papiVal[tid]);
    }
#endif
#ifdef LIKWID_PERFMON
    if (!_isWarmUp)  LIKWID_MARKER_STOP("Comp");
#endif
    // pack
    _t[tid][Pack][r2] = mytime();
    dir = ((r+1)*nf-1) % 2;
    _hts[0].pack_halo(_d, _haloVar[dir], _hs);
    _t[tid][Pack][r2+1] = mytime();
    #pragma omp barrier
    // comm
    if (tid == _ptDd->num_thread()-1)  {
#ifdef SYNC_COMM
      MPI_Barrier(MPI_COMM_WORLD);
#endif
      _t[tid][Comm][r2] = mytime();
      _hts[0].update_halo(_hs);
      _t[tid][Comm][r2+1] = mytime();
    }
    #pragma omp barrier
    // unpack
    _t[tid][Unpack][r2] = mytime();
    _hts[0].unpack_halo(_d, _haloVar[dir], _hs);
    _t[tid][Unpack][r2+1] = mytime();
    #pragma omp barrier
    _t[tid][Last][r2+1] = mytime(); 
  }// end for r

  return 0;
}


int Solver::_solve_rectangle(int nIter)
{
  int tRngs[6], tid = omp_get_thread_num();
  _ptDd->get_thread_range(tid, tRngs);
  for (int i=0; i<6; ++i) tRngs[i] += _nHalo;
  int rjSize = (tRngs[4]-tRngs[1])/_nRectangle[0], rjCeil = (tRngs[4]-tRngs[1])%_nRectangle[0];
  int rkSize = (tRngs[5]-tRngs[2])/_nRectangle[1], rkCeil = (tRngs[5]-tRngs[2])%_nRectangle[1];
  int jStrdL = rkSize + 1 + 2*_nHalo, iStrdL = jStrdL * (rjSize + 1 + 2*_nHalo) * _nVarComp;
  double *c  = new double [(2*_sr+1)*(_nHalo/_sr-1) * iStrdL];

  if (_dhOff) std::fill(c, c + (2*_sr+1)*(_nHalo/_sr-1)*iStrdL, 0.0);

  int nf = _nHalo / _sr;
  for (int r=0; r<(int)ceil((double)nIter/nf); ++r) {
    int r2 = 2*r, oddEven = r%2;
    // comp, wavefront
    _t[tid][Comp][r2] = mytime();
#ifdef LIKWID_PERFMON
    if (!_isWarmUp)  LIKWID_MARKER_START("Comp");
#endif
#ifdef PAPI
    if (!_isWarmUp)  PAPI_start(_papiSet[tid]);
#endif
    int rectRngs[6];
    std::copy(tRngs, tRngs+6, rectRngs);
    for (int rj=0; rj<_nRectangle[0]; ++rj) {
      rectRngs[4] = rectRngs[1] + (rj < rjCeil ? rjSize+1 : rjSize);
      for (int rk=0; rk<_nRectangle[1]; ++rk) {
        rectRngs[5] = rectRngs[2] + (rk < rkCeil ? rkSize+1 : rkSize);
        _compute_range_rectangle(rectRngs, r%2, c, iStrdL, jStrdL, 0, 0);
        rectRngs[2] = rectRngs[5];
      }
      rectRngs[1] = rectRngs[4];
    }
    ++_thrdIter[tid];
#ifdef PAPI
    if (!_isWarmUp) {
      PAPI_stop(_papiSet[tid], &_papiVal[tid]);
      _flop[tid] += static_cast<double>(_papiVal[tid]);
    }
#endif
#ifdef LIKWID_PERFMON
    if (!_isWarmUp)  LIKWID_MARKER_STOP("Comp");
#endif
    _t[tid][Comp][r2+1] = mytime();
    #pragma omp barrier
    // pack
    _t[tid][Pack][r2] = mytime();
    _hts[0].pack_halo(_d, _haloVar[oddEven], _hs);
    _t[tid][Pack][r2+1] = mytime();
    #pragma omp barrier
    // comm
    if (tid == _ptDd->num_thread()-1)  {
#ifdef SYNC_COMM
      MPI_Barrier(MPI_COMM_WORLD);
#endif
      _t[tid][Comm][r2] = mytime();
      _hts[0].update_halo(_hs);
      _t[tid][Comm][r2+1] = mytime();
    }
    #pragma omp barrier
    // unpack
    _t[tid][Unpack][r2] = mytime();
    _hts[0].unpack_halo(_d, _haloVar[oddEven], _hs);
    _t[tid][Unpack][r2+1] = mytime();
    #pragma omp barrier
    _t[tid][Last][r2+1] = mytime(); 
  }//end for r

  delete [] c;

  return 0;
}


int Solver::_solve_pipeline_diamond(int nIter, int** dependVars)
{
  int tid = omp_get_thread_num();
  int szs[3];   _ptDd->get_local_size(szs);
  int nPipe = _hs.num_pipe();
  int nf = _nHalo / _sr;

  for (int m=0; m<(int)ceil((double)nIter/nf)*nPipe; ++m) {
    int m2       = 2*m;
    int dir      = m/nPipe*nf % 2;
    int pipe     = _iterPipe[m % nPipe];
    int pipeLast = _iterPipe[(m+nPipe-1) % nPipe];
    _t[tid][Comp][m2] = mytime();
    #pragma omp single
    {
      // communication tasks
      #pragma omp task
      {
#ifdef SYNC_COMM
    MPI_Barrier(MPI_COMM_WORLD);
#endif
      _t[tid][Comm][m2] = mytime();
      if (m > 0)  _hts[pipeLast].update_halo(_hs);
      _t[tid][Comm][m2+1] = mytime();
      }// end task
      // computation tasks
    if (_nDiamond[0] == 1) {
      for (int j=0; j<_nDiamond[1]; j+=2) {
        #pragma omp task depend(out: dependVars[0][j])
          _compute_pipe_diamond(0, j, pipe, dir, 0);
      }
      for (int j=1; j<_nDiamond[1]; j+=2) {
        #pragma omp task depend(in: dependVars[0][j-1]) depend(in: dependVars[0][j+1])
          _compute_pipe_diamond(0, j, pipe, dir, 0);
      }
    }
    else {
      for (int i=0; i<_nDiamond[0]; i+=2) {
        for (int j=0; j<_nDiamond[1]; j+=2) {
        #pragma omp task depend(out: dependVars[i][j])
          _compute_pipe_diamond(i, j, pipe, dir, 0);
        }
      }
      for (int i=1; i<_nDiamond[0]; i+=2) {
        for (int j=0; j<_nDiamond[1]; j+=2) {
        #pragma omp task depend(in: dependVars[i-1][j]) depend(in: dependVars[i+1][j]) \
                         depend(out: dependVars[i][j])
          _compute_pipe_diamond(i, j, pipe, dir, 0);
        }
      }
      for (int i=0; i<_nDiamond[0]; i+=2) {
        for (int j=1; j<_nDiamond[1]; j+=2) {
        #pragma omp task depend(in: dependVars[i][j-1]) depend(in: dependVars[i][j+1]) \
                         depend(out: dependVars[i][j])
          _compute_pipe_diamond(i, j, pipe, dir, 0);
        }
      }
      for (int i=1; i<_nDiamond[0]; i+=2) {
        for (int j=1; j<_nDiamond[1]; j+=2) {
        #pragma omp task depend(in: dependVars[i-1][j]) depend(in: dependVars[i+1][j]) \
                         depend(in: dependVars[i][j-1]) depend(in: dependVars[i][j+1]) \
                         depend(out:dependVars[i][j])
          _compute_pipe_diamond(i, j, pipe, dir, 0);
        }
      }// end for i
    }// end else
    }// end single
    _t[tid][Comp][m2+1] = mytime();
    // update pipe iter, pack halos attached to new updated pipe
    _t[tid][Pack][m2] = mytime();
    dir = 1 - (m/nPipe+1)*nf % 2;
    _hts[pipe].pack_halo(_d, _haloVar[dir], _hs);
    _t[tid][Pack][m2+1] = mytime();
    // unpack halos that are ready
    _t[tid][Unpack][m2] = mytime();
    if (m > 0) {
      dir = (pipe == 1 && nf%2 == 1) ? 1-dir : dir;
      for (auto h: _haloToUnpack[pipe]) {
        _hps[h].unpack(_d, _haloVar[dir], _hs);
      }
    }
    _t[tid][Unpack][m2+1] = mytime();
    #pragma omp barrier
  }

  return 0;
}


int Solver::_solve_pipeline_rectangle(int nIter)
{
  int tid = omp_get_thread_num(), nThrd = _ptDd->num_thread();
  int szs[3];   _ptDd->get_local_size(szs);
  int rngs[]   = {_nHalo, _nHalo, _nHalo, szs[0]+_nHalo, szs[1]+_nHalo, szs[2]+_nHalo};
  int nPipe = _hs.num_pipe(), pipeLen = szs[0] / nPipe;
  int nf = _nHalo / _sr;

  // for overlap tiling
  int nThrds[] = {1, nThrd-1, 1}, tRngs[6] = {0}; 
  if (tid < nThrd-1)  compute_thread_range(rngs, nThrds, tid, tRngs);
  int rjSize = (tRngs[4]-tRngs[1])/_nRectangle[0], rjCeil = (tRngs[4]-tRngs[1])%_nRectangle[0];
  int rkSize = (tRngs[5]-tRngs[2])/_nRectangle[1], rkCeil = (tRngs[5]-tRngs[2])%_nRectangle[1];
  int jStrdL = rkSize + 1 + 2*_nHalo, iStrdL = jStrdL * (rjSize + 1 + 2*_nHalo)*_nVarComp;
  double *c  = new double [(2*_sr+1)*(_nHalo/_sr-1) * iStrdL];

  for (int m=0; m<(int)ceil((double)nIter/nf)*nPipe; ++m) {
    int oddEven  = m / nPipe % 2;
    int m2       = 2*m;
    int pipe     = _iterPipe[m % nPipe];
    int pipeLast = _iterPipe[(m+nPipe-1) % nPipe];
    if (tid == nThrd-1) {
#ifdef SYNC_COMM
      MPI_Barrier(MPI_COMM_WORLD);
#endif
      _t[tid][Comm][m2] = mytime();
      if (m > 0)  _hts[pipeLast].update_halo(_hs);
      _t[tid][Comm][m2+1] = mytime();
    }
    else {
      _t[tid][Comp][m2] = mytime();
      tRngs[0] = _nHalo + pipeLen * pipe;
      tRngs[3] = tRngs[0] + pipeLen;
      int rectRngs[6];
      std::copy(tRngs, tRngs+6, rectRngs);
      for (int rj=0; rj<_nRectangle[0]; ++rj) {
        rectRngs[4] = rectRngs[1] + (rj < rjCeil ? rjSize+1 : rjSize);
        for (int rk=0; rk<_nRectangle[1]; ++rk) {
          rectRngs[5] = rectRngs[2] + (rk < rkCeil ? rkSize+1 : rkSize);
          _compute_range_rectangle(rectRngs, oddEven, c, iStrdL, jStrdL, pipe, 0);
          rectRngs[2] = rectRngs[5];
        }
        rectRngs[1] = rectRngs[4];
      }
      _t[tid][Comp][m2+1] = mytime();
    }
    #pragma omp barrier
    // update pipe iter, pack halos attached to new updated pipe
    _t[tid][Pack][m2] = mytime();
    _hts[pipe].pack_halo(_d, _haloVar[oddEven], _hs);
    _t[tid][Pack][m2+1] = mytime();
    // unpack halos that are ready
    _t[tid][Unpack][m2] = mytime();
    if (m > 0) {
      oddEven = (pipe == 1 ? 1 - oddEven : oddEven);
      for (auto h: _haloToUnpack[pipe]) {
        _hps[h].unpack(_d, _haloVar[oddEven], _hs);
      }
    }
    _t[tid][Unpack][m2+1] = mytime();
    #pragma omp barrier
  }

  delete [] c;

  return 0;
}


int Solver::_solve_poll_ovlp(int nIter)
{
  int tRngs[6], szs[3], tid = omp_get_thread_num();
  _ptDd->get_thread_range(tid, tRngs);
  _ptDd->get_local_size(szs);
  for (int i=0; i<6; ++i) tRngs[i] += _nHalo;
  int nPipe = _hs.num_pipe(), pipeLen = szs[0] / nPipe;
  int nf = _nHalo / _sr;
  int rjSize = (tRngs[4]-tRngs[1])/_nRectangle[0], rjCeil = (tRngs[4]-tRngs[1])%_nRectangle[0];
  int rkSize = (tRngs[5]-tRngs[2])/_nRectangle[1], rkCeil = (tRngs[5]-tRngs[2])%_nRectangle[1];
  int jStrdL = rkSize + 1 + 2*_nHalo, iStrdL = jStrdL * (rjSize + 1 + 2*_nHalo) * _nVarComp;
  double *c  = new double [(2*_sr+1)*(_nHalo/_sr-1) * iStrdL];

  for (int m=0; m<(int)ceil((double)nIter/nf)*nPipe; ++m) {
    int oddEven  = m / nPipe % 2;
    int m2       = 2*m;
    int pipe     = _iterPipe[m % nPipe];
    int pipeLast = _iterPipe[(m+nPipe-1) % nPipe];

    _t[tid][Comp][m2] = mytime();
    // init comm
    if (tid == 0 && m > 0) {
      _t[tid][Comm][m2] = mytime();
      _hts[pipeLast].update_halo_begin(_hs);
      _t[tid][Comm][m2] = mytime() - _t[tid][Comm][2*m];
    }
    // comp while poll with MPI_Testall inside 
    tRngs[0] = _nHalo + pipeLen * pipe;
    tRngs[3] = tRngs[0] + pipeLen;
    int rectRngs[6];
    std::copy(tRngs, tRngs+6, rectRngs);
    for (int rj=0; rj<_nRectangle[0]; ++rj) {
      rectRngs[4] = rectRngs[1] + (rj < rjCeil ? rjSize+1 : rjSize);
      for (int rk=0; rk<_nRectangle[1]; ++rk) {
        rectRngs[5] = rectRngs[2] + (rk < rkCeil ? rkSize+1 : rkSize);
        _compute_range_rectangle(rectRngs, oddEven, c, iStrdL, jStrdL, 
                                 pipe, (m > 0 ? &_hts[pipeLast] : 0));
        rectRngs[2] = rectRngs[5];
      }
      rectRngs[1] = rectRngs[4];
    }
    // complete communication
    if (tid == 0 && m > 0) {
      _t[tid][Comm][m2+1] = mytime();
      _hts[pipeLast].update_halo_end();
      _t[tid][Comm][m2+1] = mytime() - _t[tid][Comm][m2+1];
    }
    _t[tid][Comp][m2+1] = mytime();
    #pragma omp barrier
    // update pipe iter, pack halos attached to new updated pipe
    _t[tid][Pack][m2] = mytime();
    _hts[pipe].pack_halo(_d, _haloVar[oddEven], _hs);
    _t[tid][Pack][m2+1] = mytime();
    // unpack halos that are ready
    _t[tid][Unpack][m2] = mytime();
    if (m > 0) {
      oddEven = (pipe == 1 ? 1 - oddEven : oddEven);
      for (auto h: _haloToUnpack[pipe]) {
        _hps[h].unpack(_d, _haloVar[oddEven], _hs);
      }
    }
    _t[tid][Unpack][m2+1] = mytime();
    #pragma omp barrier
  }

  delete [] c;

  return 0;
}


int Solver::_solve_poll_trap(int nIter, int** dependVars)
{
  int tid = omp_get_thread_num();
  int szs[3];   _ptDd->get_local_size(szs);
  int nPipe = _hs.num_pipe();
  int nf = _nHalo / _sr;

  for (int m=0; m<(int)ceil((double)nIter/nf)*nPipe; ++m) {
    int m2       = 2*m;
    int dir      = m/nPipe*nf % 2;
    int pipe     = _iterPipe[m % nPipe];
    int pipeLast = _iterPipe[(m+nPipe-1) % nPipe];

    _t[tid][Comp][m2] = mytime();
    // computation tasks
    #pragma omp single
    {
    // init comm
    if (m > 0) {
      _t[tid][Comm][m2] = mytime();
      _hts[pipeLast].update_halo_begin(_hs);
      _t[tid][Comm][m2] = mytime() - _t[tid][Comm][m2];
    }
    // comp while poll with MPI_Testall inside 
    if (_nDiamond[0] == 1) {
      for (int j=0; j<_nDiamond[1]; j+=2) {
        #pragma omp task depend(out: dependVars[0][j])
          _compute_pipe_diamond(0, j, pipe, dir, (m > 0 ? &_hts[pipeLast] : 0));
      }
      for (int j=1; j<_nDiamond[1]; j+=2) {
        #pragma omp task depend(in: dependVars[0][j-1]) depend(in: dependVars[0][j+1])
          _compute_pipe_diamond(0, j, pipe, dir, (m > 0 ? &_hts[pipeLast] : 0));
      }
    }
    else {
      for (int i=0; i<_nDiamond[0]; i+=2) {
        for (int j=0; j<_nDiamond[1]; j+=2) {
        #pragma omp task depend(out: dependVars[i][j])
          _compute_pipe_diamond(i, j, pipe, dir, (m > 0 ? &_hts[pipeLast] : 0));
        }
      }
      for (int i=1; i<_nDiamond[0]; i+=2) {
        for (int j=0; j<_nDiamond[1]; j+=2) {
        #pragma omp task depend(in: dependVars[i-1][j]) depend(in: dependVars[i+1][j]) \
                         depend(out: dependVars[i][j])
          _compute_pipe_diamond(i, j, pipe, dir, (m > 0 ? &_hts[pipeLast] : 0));
        }
      }
      for (int i=0; i<_nDiamond[0]; i+=2) {
        for (int j=1; j<_nDiamond[1]; j+=2) {
        #pragma omp task depend(in: dependVars[i][j-1]) depend(in: dependVars[i][j+1]) \
                         depend(out: dependVars[i][j])
          _compute_pipe_diamond(i, j, pipe, dir, (m > 0 ? &_hts[pipeLast] : 0));
        }
      }
      for (int i=1; i<_nDiamond[0]; i+=2) {
        for (int j=1; j<_nDiamond[1]; j+=2) {
        #pragma omp task depend(in: dependVars[i-1][j]) depend(in: dependVars[i+1][j]) \
                         depend(in: dependVars[i][j-1]) depend(in: dependVars[i][j+1]) \
                         depend(out:dependVars[i][j])
          _compute_pipe_diamond(i, j, pipe, dir, (m > 0 ? &_hts[pipeLast] : 0));
        }
      }// end for i
    }// end else
      #pragma omp taskwait
      // complete comm
      if (m > 0) {
        _t[tid][Comm][m2+1] = mytime();
        _hts[pipeLast].update_halo_end();
        _t[tid][Comm][m2+1] = mytime() - _t[tid][Comm][m2+1];
      }
    }// end single
    _t[tid][Comp][m2+1] = mytime();
    // pack halos attached to new updated pipe
    _t[tid][Pack][m2] = mytime();
    dir = 1 - (m/nPipe+1)*nf % 2;
    _hts[pipe].pack_halo(_d, _haloVar[dir], _hs);
    _t[tid][Pack][m2+1] = mytime();
    // unpack halos that are ready
    _t[tid][Unpack][m2] = mytime();
    if (m > 0) {
      dir = (pipe == 1 && nf%2 == 1) ? 1-dir : dir;
      for (auto h: _haloToUnpack[pipe]) {
        _hps[h].unpack(_d, _haloVar[dir], _hs);
      }
    }
    _t[tid][Unpack][m2+1] = mytime();
    #pragma omp barrier
  }

  return 0;
}


int Solver::_analyze_time_mpi()
{
  double tMaxs[TIMER_MAX_ITEM], tAvgs[TIMER_MAX_ITEM], tSteps[TIMER_MAX_ITEM];
  double tMins[TIMER_MAX_ITEM], tSDs[TIMER_MAX_ITEM];
  std::fill(tSteps, tSteps+TIMER_MAX_ITEM, 0.0);

  for (int m=0; m<_nIter; ++m) {
    tSteps[Comp]   += _t[0][Pack][m]   - _t[0][Comp][m];
    tSteps[Pack]   += _t[0][Comm][m]   - _t[0][Pack][m];
    tSteps[Comm]   += _t[0][Unpack][m] - _t[0][Comm][m];
    tSteps[Unpack] += _t[0][Last][m]   - _t[0][Unpack][m];
  }
  tSteps[Total] = _t[0][Total][1] - _t[0][Total][0];

  struct {double v; int r;} ValRank;
  ValRank.r = _ptDd->rank();
  ValRank.v = tSteps[Total];

  MPI_Allreduce(tSteps, tAvgs, TIMER_MAX_ITEM, MPI_DOUBLE, MPI_SUM, _ptDd->comm());
  MPI_Allreduce(tSteps, tMaxs, TIMER_MAX_ITEM, MPI_DOUBLE, MPI_MAX, _ptDd->comm());
  MPI_Allreduce(tSteps, tMins, TIMER_MAX_ITEM, MPI_DOUBLE, MPI_MIN, _ptDd->comm());
  MPI_Allreduce(MPI_IN_PLACE, &ValRank, 1, MPI_DOUBLE_INT, MPI_MAXLOC, _ptDd->comm());
  for (int i=0; i<TIMER_MAX_ITEM; ++i) {
    tAvgs[i] /= _ptDd->num_proc();
    tSDs[i]   = (tSteps[i] - tAvgs[i]) * (tSteps[i] - tAvgs[i]) / _ptDd->num_proc();
  }
  MPI_Allreduce(MPI_IN_PLACE, tSDs, TIMER_MAX_ITEM, MPI_DOUBLE, MPI_SUM, _ptDd->comm());
  for (int i=0; i<TIMER_MAX_ITEM; ++i)  tSDs[i] = sqrt(tSDs[i]);

  if (_ptDd->is_root()) {
    std::cout << "Avg:       " << std::fixed << tAvgs[Comp] << " " << tAvgs[Pack] << " " 
              << tAvgs[Comm] << " " << tAvgs[Unpack] << " " << tAvgs[Total] << std::endl;
    std::cout << "Max:       " << std::fixed << tMaxs[Comp] << " " << tMaxs[Pack] << " " 
              << tMaxs[Comm] << " " << tMaxs[Unpack] << " " << tMaxs[Total] << std::endl;
    std::cout << "Min:       " << std::fixed << tMins[Comp] << " " << tMins[Pack] << " " 
              << tMins[Comm] << " " << tMins[Unpack] << " " << tMins[Total] << std::endl;
    std::cout << "SD:        " << std::fixed << tSDs[Comp] << " " << tSDs[Pack] << " " 
              << tSDs[Comm] << " " << tSDs[Unpack] << " " << tSDs[Total] << std::endl;
    std::cout << "Max Proc : " << std::fixed << tSteps[Comp] << " " << tSteps[Pack] << " " 
              << tSteps[Comm] << " " << tSteps[Unpack] << " " << tSteps[Total] << std::endl;
  }

  // output tracer to file
  if (_trace) {
    std::ofstream fout("tracer" + std::to_string(_ptDd->rank()));
    fout.precision(8);
    fout << std::fixed << _t[0][Total][0] << std::endl;
    for (int m=0; m<_nIter; ++m) {
      fout << std::fixed <<  _t[0][Comp][m] << "\n" << _t[0][Pack][m] << " \n"
           << _t[0][Comm][m] << "\n" << _t[0][Unpack][m] << "\n"
           << _t[0][Last][m] << "\n";
    }
    fout << std::fixed << _t[0][Total][1] << std::endl;
    fout.close();
  }

  return 0;
}


int Solver::_analyze_time_hybrid()
{
  int    nt = _ptDd->num_thread();
  double tMaxs[TIMER_MAX_ITEM], tAvgs[TIMER_MAX_ITEM], imbls[TIMER_MAX_ITEM], tSteps[TIMER_MAX_ITEM];
  double tMins[TIMER_MAX_ITEM], tSDs[TIMER_MAX_ITEM];
  std::fill(tSteps, tSteps + TIMER_MAX_ITEM, 0.0);
  std::fill(imbls,   imbls + TIMER_MAX_ITEM, 0.0);

  // use the max among threads as the time of that step, store it at averge time array
  // (max - min) as the imbalance
  for (int i=0; i<_nIter; ++i) {
    // steps other than comm
    for (auto step: {Comp, Pack, Unpack}) {
      double stepMax = 0.0, stepMin = 1.0e10;
      for (int j=0; j<nt; ++j) {
        stepMax = std::max(stepMax, _t[j][step][2*i+1] - _t[j][step][2*i]);
        stepMin = std::min(stepMin, _t[j][step][2*i+1] - _t[j][step][2*i]);
      }
      tSteps[step] += stepMax;
      imbls[step]  += stepMax - stepMin;
    }
    // comm
    tSteps[Comm] += _isSyncComm ? _t[nt-1][Comm][2*i+1] - _t[nt-1][Sync][2*i] \
                                : _t[nt-1][Comm][2*i+1] - _t[nt-1][Comm][2*i];
  }
  // Total
  for (int j=0; j<nt; ++j) 
    tSteps[Total] = std::max(tSteps[0], _t[j][Total][1] - _t[j][Total][0]);

  struct {double v; int r;} ValRank;
  ValRank.r = _ptDd->rank();
  ValRank.v = tSteps[Total];

  MPI_Allreduce(MPI_IN_PLACE, imbls, TIMER_MAX_ITEM, MPI_DOUBLE, MPI_MAX, _ptDd->comm());
  MPI_Allreduce(tSteps, tAvgs, TIMER_MAX_ITEM, MPI_DOUBLE, MPI_SUM, _ptDd->comm());
  MPI_Allreduce(tSteps, tMaxs, TIMER_MAX_ITEM, MPI_DOUBLE, MPI_MAX, _ptDd->comm());
  MPI_Allreduce(tSteps, tMins, TIMER_MAX_ITEM, MPI_DOUBLE, MPI_MIN, _ptDd->comm());
  MPI_Allreduce(MPI_IN_PLACE, &ValRank, 1, MPI_DOUBLE_INT, MPI_MAXLOC, _ptDd->comm());
  for (int i=0; i<TIMER_MAX_ITEM; ++i) {
    tAvgs[i] /= _ptDd->num_proc();
    tSDs[i]   = (tSteps[i] - tAvgs[i]) * (tSteps[i] - tAvgs[i]) / _ptDd->num_proc();
  }
  MPI_Allreduce(MPI_IN_PLACE, tSDs, TIMER_MAX_ITEM, MPI_DOUBLE, MPI_SUM, _ptDd->comm());
  for (int i=0; i<TIMER_MAX_ITEM; ++i)  tSDs[i] = sqrt(tSDs[i]);

  if (_ptDd->rank() == ValRank.r) {
    std::cout << "Avg:       " << std::fixed << tAvgs[Comp] << " " << tAvgs[Pack] << " " 
              << tAvgs[Comm] << " " << tAvgs[Unpack] << " " << tAvgs[Total] << std::endl;
    std::cout << "Max:       " << std::fixed << tMaxs[Comp] << " " << tMaxs[Pack] << " " 
              << tMaxs[Comm] << " " << tMaxs[Unpack] << " " << tMaxs[Total] << std::endl;
    std::cout << "Min:       " << std::fixed << tMins[Comp] << " " << tMins[Pack] << " " 
              << tMins[Comm] << " " << tMins[Unpack] << " " << tMins[Total] << std::endl;
    std::cout << "SD:        " << std::fixed << tSDs[Comp] << " " << tSDs[Pack] << " " 
              << tSDs[Comm] << " " << tSDs[Unpack] << " " << tSDs[Total] << std::endl;
    std::cout << "Max Proc : " << std::fixed << tSteps[Comp] << " " << tSteps[Pack] << " " 
              << tSteps[Comm] << " " << tSteps[Unpack] << " " << tSteps[Total] << std::endl;
  }

  // write to file
  if (_trace) {
    std::ofstream fout("tracer" + std::to_string(_ptDd->rank()));
    fout.precision(8);
    for (int j=0; j<nt; ++j) fout << std::fixed << _t[j][Total][0] << " ";
    fout << std::endl;
    for (int m=0; m<_nIter; ++m) {
      for (auto step: {Comp, Pack, Comm, Unpack}) {
        for (int j=0; j<nt; ++j)  fout << std::fixed << _t[j][step][2*m] << " ";
        fout << std::endl;
        for (int j=0; j<nt; ++j)  fout << std::fixed << _t[j][step][2*m+1] << " ";
        fout << std::endl;
      }
      for (int j=0; j<nt; ++j)  fout << std::fixed << _t[j][Last][2*m+1] << " ";
      fout << std::endl;
    }
    for (int j=0; j<nt; ++j) fout << std::fixed << _t[j][Total][1] << " ";
    fout << std::endl;
    fout.close();
  }

  return 0;
}


int Solver::_analyze_time_pipeline()
{
  int    nt = _ptDd->num_thread(), nPipe = _hs.num_pipe();
  int    Barrier = TIMER_MAX_ITEM, NonOvlp = TIMER_MAX_ITEM+1, CompIntv = TIMER_MAX_ITEM+2;
  double tMaxs[TIMER_MAX_ITEM+3], tAvgs[TIMER_MAX_ITEM+3], imbls[TIMER_MAX_ITEM+3], tSteps[TIMER_MAX_ITEM+3];
  double tMins[TIMER_MAX_ITEM+3], tSDs[TIMER_MAX_ITEM+3];
  std::fill(tSteps, tSteps + TIMER_MAX_ITEM, 0.0);
  std::fill(imbls,   imbls + TIMER_MAX_ITEM, 0.0);

  // use the max among threads as the time of that step, store it at averge time array
  // (max - min) as the imbalance
  int nf = _nHalo / _sr;
  double compBegin, compEnd, packBegin, unpackEnd, stepMax, stepMin;
  for (int i=0; i<((int)ceil((double)_nIter/nf))*nPipe; ++i) {
    // computattion is meansured by both interval and max time
    compBegin = _t[0][Comp][2*i];
    compEnd   = _t[0][Comp][2*i+1];
    stepMax   = 0.0;
    stepMin   = 1.0e10;
    int ntComp = _isDiamond ? nt : nt-1;
    for (int j=0; j<ntComp; ++j) {
      stepMax   = std::max(stepMax,   _t[j][Comp][2*i+1] - _t[j][Comp][2*i]);
      stepMin   = std::min(stepMin,   _t[j][Comp][2*i+1] - _t[j][Comp][2*i]);
      compBegin = std::min(compBegin, _t[j][Comp][2*i]);
      compEnd   = std::max(compEnd,   _t[j][Comp][2*i+1]);
    }
    tSteps[CompIntv] += compEnd - compBegin;
    tSteps[Comp] += stepMax;
    imbls[Comp]  += stepMax - stepMin;
    // comm, find the thread doing communication
    // nonoverlap is the part not covered by the comp interval
    int thrdComm = 0;
    for (int j=0; j<nt; ++j) {
      if (_t[j][Comm][2*i+1] - _t[j][Comm][2*i] > 1.0e-8) {
        thrdComm      = j;
        tSteps[Comm] += _t[j][Comm][2*i+1] - _t[j][Comm][2*i];  
      }
    }
    if (i > 0) {
      tSteps[NonOvlp] += std::max(compBegin - _t[thrdComm][Comm][2*i], 0.0);
      tSteps[NonOvlp] += std::max(_t[thrdComm][Comm][2*i+1] - compEnd, 0.0);
    }
    // barrier
    if (i > 0)
      tSteps[Barrier] += std::min(_t[thrdComm][Comm][2*i], compBegin) - unpackEnd;
    // pack and unpack is combined into pack, interval time
    stepMax   = 0.0;
    stepMin   = 1.0e10;
    packBegin = _t[0][Pack][2*i];
    unpackEnd = _t[0][Unpack][2*i+1];
    for (int j=0; j<nt; ++j) {
      stepMax   = std::max(stepMax,   _t[j][Unpack][2*i+1] - _t[j][Pack][2*i]);
      stepMin   = std::min(stepMin,   _t[j][Unpack][2*i+1] - _t[j][Pack][2*i]);
      packBegin = std::min(packBegin, _t[j][Pack][2*i]);
      unpackEnd = std::max(unpackEnd, _t[j][Unpack][2*i+1]);
    }
    tSteps[Pack] += unpackEnd - packBegin;
    imbls[Pack]  += stepMax   - stepMin;
    // barrier
    tSteps[Barrier] += packBegin - std::max(compEnd, _t[thrdComm][Comm][2*i+1]);
  }
  // Total, interval time
  double intvb = _t[0][Total][0], intve = _t[0][Total][1];
  for (int j=1; j<nt; ++j) {
    intvb = std::min(_t[j][Total][0], intvb);
    intve = std::max(_t[j][Total][1], intve);
  }
  tSteps[Total] = intve - intvb;

  struct {double v; int r;} ValRank;
  ValRank.r = _ptDd->rank();
  ValRank.v = tSteps[Total];

  MPI_Allreduce(MPI_IN_PLACE, imbls, TIMER_MAX_ITEM+3, MPI_DOUBLE, MPI_MAX, _ptDd->comm());
  MPI_Allreduce(tSteps, tAvgs, TIMER_MAX_ITEM+3, MPI_DOUBLE, MPI_SUM, _ptDd->comm());
  MPI_Allreduce(tSteps, tMaxs, TIMER_MAX_ITEM+3, MPI_DOUBLE, MPI_MAX, _ptDd->comm());
  MPI_Allreduce(tSteps, tMins, TIMER_MAX_ITEM+3, MPI_DOUBLE, MPI_MIN, _ptDd->comm());
  MPI_Allreduce(MPI_IN_PLACE, &ValRank, 1, MPI_DOUBLE_INT, MPI_MAXLOC, _ptDd->comm());
  for (int i=0; i<TIMER_MAX_ITEM+3; ++i) {
    tAvgs[i] /= _ptDd->num_proc();
    tSDs[i]   = (tSteps[i] - tAvgs[i]) * (tSteps[i] - tAvgs[i]) / _ptDd->num_proc();
  }
  MPI_Allreduce(MPI_IN_PLACE, tSDs, TIMER_MAX_ITEM+3, MPI_DOUBLE, MPI_SUM, _ptDd->comm());
  for (int i=0; i<TIMER_MAX_ITEM+3; ++i)  tSDs[i] = sqrt(tSDs[i]);

  if (_ptDd->rank() == ValRank.r) {
    std::cout << "Avg:       " << std::fixed << tAvgs[CompIntv] << " " << tAvgs[Comm] << " " 
              << tAvgs[Pack] << " " << tAvgs[Total] << " " << tAvgs[Comp] << " " 
              << tAvgs[Barrier] << " " << tAvgs[NonOvlp] << std::endl;
    std::cout << "Max:       " << std::fixed << tMaxs[CompIntv] << " " << tMaxs[Comm] << " " 
              << tMaxs[Pack] << " " << tMaxs[Total] << " " << tMaxs[Comp] << " " 
              << tMaxs[Barrier] << " " << tMaxs[NonOvlp] << std::endl;
    std::cout << "Min:       " << std::fixed << tMins[CompIntv] << " " << tMins[Comm] << " " 
              << tMins[Pack] << " " << tMins[Total] << " " << tMins[Comp] << " " 
              << tMins[Barrier] << " " << tMins[NonOvlp] << std::endl;
    std::cout << "SD:        " << std::fixed << tSDs[CompIntv] << " " << tSDs[Comm] << " " 
              << tSDs[Pack] << " " << tSDs[Total] << " " << tSDs[Comp] << std::endl;
    std::cout << "Max Proc:  " << std::fixed << tSteps[CompIntv] << " " << tSteps[Comm] << " " 
              << tSteps[Pack] << " " << tSteps[Total] << " " << tSteps[Comp] << " " 
              << tSteps[Barrier] << " " << tSteps[NonOvlp] << std::endl;
  }

  // write to file
  if (_trace) {
    std::ofstream fout("tracer" + std::to_string(_ptDd->rank()));
    fout.precision(8);
    for (int j=0; j<nt; ++j) fout << std::fixed << _t[j][Total][0] << " ";
    fout << std::endl;
    for (int m=0; m<_nIter/_nHalo*nPipe; ++m) {
      // 0~nt-2 comp, nt-1 comm
      for (int j=0; j<nt-1; ++j)  fout << std::fixed << _t[j][Comp][2*m] << " ";
      fout << std::fixed << _t[nt-1][Comm][2*m] << std::endl;
      for (int j=0; j<nt-1; ++j)  fout << std::fixed << _t[j][Comp][2*m+1] << " ";
      fout << std::fixed << _t[nt-1][Comm][2*m+1] << std::endl;
      // pack, unpack, last
      for (auto step: {Pack, Unpack, Last}) {
        for (int j=0; j<nt; ++j)  fout << std::fixed << _t[j][step][2*m] << " ";
        fout << std::endl;
        for (int j=0; j<nt; ++j)  fout << std::fixed << _t[j][step][2*m+1] << " ";
        fout << std::endl;
      }
    }
    for (int j=0; j<nt; ++j) fout << std::fixed << _t[j][Total][1] << " ";
    fout << std::endl;
    fout.close();
  }

  return 0;
}


int Solver::_analyze_time_poll()
{
  int    nt = _ptDd->num_thread(), nPipe = _hs.num_pipe();
  int    Barrier = TIMER_MAX_ITEM, NonOvlp = TIMER_MAX_ITEM+1, CompIntv = TIMER_MAX_ITEM+2;
  double tMaxs[TIMER_MAX_ITEM+3], tAvgs[TIMER_MAX_ITEM+3], imbls[TIMER_MAX_ITEM+3], tSteps[TIMER_MAX_ITEM+3];
  double tMins[TIMER_MAX_ITEM+3], tSDs[TIMER_MAX_ITEM+3];
  std::fill(tSteps, tSteps + TIMER_MAX_ITEM, 0.0);
  std::fill(imbls,   imbls + TIMER_MAX_ITEM, 0.0);

  // use the max among threads as the time of that step, store it at averge time array
  // (max - min) as the imbalance
  int nf = _nHalo / _sr;
  double compBegin, compEnd, packBegin, unpackEnd, stepMax, stepMin;
  for (int i=0; i<((int)ceil((double)_nIter/nf))*nPipe; ++i) {
    // computattion is meansured by both interval and max time
    // comm, find the thread doing comm, this is unknown for trap tiling
    // 2*i saves time for isend/irecv, 2*i+1 for waitall
    compBegin = _t[0][Comp][2*i];
    compEnd   = _t[0][Comp][2*i+1];
    stepMax   = 0.0;
    stepMin   = 1.0e10;
    for (int j=0; j<nt; ++j) {
      double tComp = _t[j][Comp][2*i+1] - _t[j][Comp][2*i];
      if (_t[j][Comm][2*i] > 1.0e-7) {
        tSteps[Comm] += _t[j][Comm][2*i];
        tComp        -= _t[j][Comm][2*i];
      }
      if (_t[j][Comm][2*i+1] > 1.0e-7) {
        tSteps[Comm] += _t[j][Comm][2*i+1];
        tComp        -= _t[j][Comm][2*i+1];
      }
      stepMax   = std::max(stepMax,   tComp);
      stepMin   = std::min(stepMin,   tComp);
      compBegin = std::min(compBegin, _t[j][Comp][2*i]);
      compEnd   = std::max(compEnd,   _t[j][Comp][2*i+1]);
    }
    tSteps[CompIntv] += compEnd - compBegin;
    tSteps[Comp] += stepMax;
    imbls[Comp]  += stepMax - stepMin;
    // barrier
    if (i > 0)  tSteps[Barrier] += compBegin - unpackEnd;
    // pack and unpack is combined into pack, interval time
    stepMax   = 0.0;
    stepMin   = 1.0e10;
    packBegin = _t[0][Pack][2*i];
    unpackEnd = _t[0][Unpack][2*i+1];
    for (int j=0; j<nt; ++j) {
      stepMax   = std::max(stepMax,   _t[j][Unpack][2*i+1] - _t[j][Pack][2*i]);
      stepMin   = std::min(stepMin,   _t[j][Unpack][2*i+1] - _t[j][Pack][2*i]);
      packBegin = std::min(packBegin, _t[j][Pack][2*i]);
      unpackEnd = std::max(unpackEnd, _t[j][Unpack][2*i+1]);
    }
    tSteps[Pack] += unpackEnd - packBegin;
    imbls[Pack]  += stepMax   - stepMin;
    // barrier
    tSteps[Barrier] += packBegin - compEnd;
  }
  // Total, interval time
  double intvb = _t[0][Total][0], intve = _t[0][Total][1];
  for (int j=1; j<nt; ++j) {
    intvb = std::min(_t[j][Total][0], intvb);
    intve = std::max(_t[j][Total][1], intve);
  }
  tSteps[Total] = intve - intvb;

  struct {double v; int r;} ValRank;
  ValRank.r = _ptDd->rank();
  ValRank.v = tSteps[Total];

  MPI_Allreduce(MPI_IN_PLACE, imbls, TIMER_MAX_ITEM+3, MPI_DOUBLE, MPI_MAX, _ptDd->comm());
  MPI_Allreduce(tSteps, tAvgs, TIMER_MAX_ITEM+3, MPI_DOUBLE, MPI_SUM, _ptDd->comm());
  MPI_Allreduce(tSteps, tMaxs, TIMER_MAX_ITEM+3, MPI_DOUBLE, MPI_MAX, _ptDd->comm());
  MPI_Allreduce(tSteps, tMins, TIMER_MAX_ITEM+3, MPI_DOUBLE, MPI_MAX, _ptDd->comm());
  MPI_Allreduce(MPI_IN_PLACE, &ValRank, 1, MPI_DOUBLE_INT, MPI_MAXLOC, _ptDd->comm());
  for (int i=0; i<TIMER_MAX_ITEM+3; ++i) {
    tAvgs[i] /= _ptDd->num_proc();
    tSDs[i]   = (tSteps[i] - tAvgs[i]) * (tSteps[i] - tAvgs[i]) / _ptDd->num_proc();
  }
  MPI_Allreduce(MPI_IN_PLACE, tSDs, TIMER_MAX_ITEM+3, MPI_DOUBLE, MPI_SUM, _ptDd->comm());
  for (int i=0; i<TIMER_MAX_ITEM+3; ++i)  tSDs[i] = sqrt(tSDs[i]);

  if (_ptDd->rank() == ValRank.r) {
    std::cout << "Avg:       " << std::fixed << tAvgs[CompIntv] << " " << tAvgs[Comm] << " " 
              << tAvgs[Pack] << " " << tAvgs[Total] << " " << tAvgs[Comp] << " " 
              << tAvgs[Barrier] << " " << tAvgs[NonOvlp] << std::endl;
    std::cout << "Max:       " << std::fixed << tMaxs[CompIntv] << " " << tMaxs[Comm] << " " 
              << tMaxs[Pack] << " " << tMaxs[Total] << " " << tMaxs[Comp] << " " 
              << tMaxs[Barrier] << " " << tMaxs[NonOvlp] << std::endl;
    std::cout << "Min:       " << std::fixed << tMins[CompIntv] << " " << tMins[Comm] << " " 
              << tMins[Pack] << " " << tMins[Total] << " " << tMins[Comp] << " " 
              << tMins[Barrier] << " " << tMins[NonOvlp] << std::endl;
    std::cout << "SD:        " << std::fixed << tSDs[CompIntv] << " " << tSDs[Comm] << " " 
              << tSDs[Pack] << " " << tSDs[Total] << " " << tSDs[Comp] << std::endl;
    std::cout << "Max Proc:  " << std::fixed << tSteps[CompIntv] << " " << tSteps[Comm] << " " 
              << tSteps[Pack] << " " << tSteps[Total] << " " << tSteps[Comp] << " " 
              << tSteps[Barrier] << " " << tSteps[NonOvlp] << std::endl;
  }

  // write to file
  if (_trace) {
    std::ofstream fout("tracer" + std::to_string(_ptDd->rank()));
    fout.precision(8);
    for (int j=0; j<nt; ++j) fout << std::fixed << _t[j][Total][0] << " ";
    fout << std::endl;
    for (int m=0; m<_nIter/_nHalo*nPipe; ++m) {
      // 0~nt-2 comp, nt-1 comm
      for (int j=0; j<nt-1; ++j)  fout << std::fixed << _t[j][Comp][2*m] << " ";
      fout << std::fixed << _t[nt-1][Comm][2*m] << std::endl;
      for (int j=0; j<nt-1; ++j)  fout << std::fixed << _t[j][Comp][2*m+1] << " ";
      fout << std::fixed << _t[nt-1][Comm][2*m+1] << std::endl;
      // pack, unpack, last
      for (auto step: {Pack, Unpack, Last}) {
        for (int j=0; j<nt; ++j)  fout << std::fixed << _t[j][step][2*m] << " ";
        fout << std::endl;
        for (int j=0; j<nt; ++j)  fout << std::fixed << _t[j][step][2*m+1] << " ";
        fout << std::endl;
      }
    }
    for (int j=0; j<nt; ++j) fout << std::fixed << _t[j][Total][1] << " ";
    fout << std::endl;
    fout.close();
  }

  return 0;
}


void Solver::_setup_diamonds()
{
  int szs[3]; _ptDd->get_local_size(szs);
  int nPipe = _hs.num_pipe();

  _diamonds = new Diamond* [_nDiamond[0]];
  for (int i=0; i<_nDiamond[0]; ++i)  _diamonds[i] = new Diamond [_nDiamond[1]];

  if (_nDiamond[0] > (szs[0]/nPipe + 3*(_nHalo-_sr)) / _nHalo && _ptDd->is_root())
    std::cout << "ERROR: too many dimonds in i, allow " << (szs[0]/nPipe+3*(_nHalo-_sr))/_nHalo << std::endl; 
  if (_nDiamond[1] > (szs[1] + 3*(_nHalo-_sr)) / _nHalo && _ptDd->is_root())
    std::cout << "ERROR: too many dimonds in j, allow " << (szs[1]+3*(_nHalo-_sr))/_nHalo << std::endl; 

  if (_nDiamond[0] == 1) {
    // set width for each diamond
    int widthFloor  = (szs[1] + (_nDiamond[1]+1)*(_nHalo-_sr)) / _nDiamond[1];
    int nCeil       = (szs[1] + (_nDiamond[1]+1)*(_nHalo-_sr)) - _nDiamond[1]*widthFloor;
    // 1st diamond
    _diamonds[0][0].type   = TriangleUp;
    _diamonds[0][0].iBegin = _sr;
    _diamonds[0][0].iEnd   = szs[0]/nPipe + 2*_nHalo - _sr;
    _diamonds[0][0].jBegin = _sr;
    _diamonds[0][0].jEnd   = _sr + (0 < nCeil ? widthFloor+1 : widthFloor);
    for (int j=1; j<_nDiamond[1]; ++j) {
      _diamonds[0][j].type   = (j % 2 == 0 ? TriangleUp : TriangleDown);
      _diamonds[0][j].iBegin = _sr;
      _diamonds[0][j].iEnd   = szs[0]/nPipe + 2*_nHalo - _sr;
      _diamonds[0][j].jBegin = _diamonds[0][j-1].jEnd - _nHalo + _sr;
      _diamonds[0][j].jEnd   = _diamonds[0][j].jBegin + (j < nCeil ? widthFloor+1 : widthFloor);
    }
  }
  else {
    int widthFloor0  = (szs[0]/nPipe + (_nDiamond[0]+1)*(_nHalo-_sr)) / _nDiamond[0];
    int nCeil0       = (szs[0]/nPipe + (_nDiamond[0]+1)*(_nHalo-_sr)) - _nDiamond[0]*widthFloor0;
    int widthFloor1  = (szs[1] + (_nDiamond[1]+1)*(_nHalo-_sr)) / _nDiamond[1];
    int nCeil1       = (szs[1] + (_nDiamond[1]+1)*(_nHalo-_sr)) - _nDiamond[1]*widthFloor1;
    for (int i=0; i<_nDiamond[0]; i+=2) {
      for (int j=0; j<_nDiamond[1]; j+=2) {
        // triangleup
        _diamonds[i][j].type = TriangleUp;
        _diamonds[i][j].iBegin = _sr + i * (widthFloor0-_nHalo+_sr) + std::min(nCeil0, i);
        _diamonds[i][j].iEnd   = _diamonds[i][j].iBegin + (i >= nCeil0 ? widthFloor0 : widthFloor0+1);
        _diamonds[i][j].jBegin = _sr + j * (widthFloor1-_nHalo+_sr) + std::min(nCeil1, j);
        _diamonds[i][j].jEnd   = _diamonds[i][j].jBegin + (j >= nCeil1 ? widthFloor1 : widthFloor1+1);
        // triangleleft
        if (i > 0) {
          _diamonds[i-1][j].type   = TriangleLeft;
          _diamonds[i-1][j].iBegin = _diamonds[i-2][j].iEnd   - _nHalo + _sr;
          _diamonds[i-1][j].iEnd   = _diamonds[i  ][j].iBegin + _nHalo - _sr;
          _diamonds[i-1][j].jBegin = _diamonds[i-2][j].jBegin;
          _diamonds[i-1][j].jEnd   = _diamonds[i-2][j].jEnd;
        }
        // triangledown
        if (j > 0) {
          _diamonds[i][j-1].type   = TriangleDown;
          _diamonds[i][j-1].iBegin = _diamonds[i][j-2].iBegin;
          _diamonds[i][j-1].iEnd   = _diamonds[i][j-2].iEnd;
          _diamonds[i][j-1].jBegin = _diamonds[i][j-2].jEnd   - _nHalo + _sr;
          _diamonds[i][j-1].jEnd   = _diamonds[i][j  ].jBegin + _nHalo - _sr;
        }
        // center
        if (i > 0 && j > 0) {
          _diamonds[i-1][j-1].type = Center;
          _diamonds[i-1][j-1].iBegin = _diamonds[i-2][j-2].iEnd   - _nHalo + _sr; 
          _diamonds[i-1][j-1].jBegin = _diamonds[i-2][j-2].jEnd   - _nHalo + _sr; 
          _diamonds[i-1][j-1].iEnd   = _diamonds[i  ][j  ].iBegin + _nHalo - _sr; 
          _diamonds[i-1][j-1].jEnd   = _diamonds[i  ][j  ].jBegin + _nHalo - _sr;
        }
      }
    }
  }
  //for (int j=0; j<_nDiamond[0]; ++j) {
    //for (int k=0; k<_nDiamond[1]; ++k) {
      //std::cout << _diamonds[j][k].type << "  " << _diamonds[j][k].iBegin << " " << _diamonds[j][k].iEnd << " "
        //<< _diamonds[j][k].jBegin << " " << _diamonds[j][k].jEnd << std::endl;
    //}
  //}
}


#ifdef PAPI
void Solver::collect_view_flop()
{
  MPI_Allreduce(MPI_IN_PLACE, &_flop[0], _ptDd->num_thread(), MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  if (_ptDd->is_root())
    std::cout << "flop: " << std::scientific <<std::accumulate(_flop.begin(), _flop.end(), 0.0)
              << std::endl;
}
#endif
