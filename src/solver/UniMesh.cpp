#include "UniMesh.h"
#include <iostream>
#include <cmath>

UniMesh::UniMesh(const DomainDecomp& dd):
  _x0{0.0, 0.0, 0.0},
  _l{1.0, 1.0, 1.0},
  _ptDd(&dd)
{
  int gSizes[3];
  dd.get_global_size(gSizes);
  _h = 1.0 / gSizes[0];
}


UniMesh::UniMesh(const DomainDecomp& dd, const double x0[3], const double l[3]):
  _x0{x0[0], x0[1], x0[2]},
  _l{l[0], l[1], l[2]},
  _ptDd(&dd)
{
  int gSizes[3];
  dd.get_global_size(gSizes);
  _h = l[0] / gSizes[0];
}


void UniMesh::set_from_option()
{ 
  bool isSet;
  get_option("mesh_len", CmdOption::Double, 3, _l, &isSet);
}


double UniMesh::step() const
{
  return _h;
}


void UniMesh::view() const
{
  if (_ptDd->is_root()) {
    std::cout << "--------------- Mesh ---------------" << std::endl;
    std::cout << "step:\t" << _h << std::endl;
    std::cout << "length:\t" << _l[0] << "\t" << _l[1] << "\t" << _l[2] << std::endl;
    std::cout << "origin:\t" << _x0[0] << "\t" << _x0[1] << "\t" << _x0[2] << std::endl;
  }
}


int UniMesh::compute_coord(const int indices[3], double coords[3]) const
{
  for (int i=0; i<3; ++i)
    coords[i] = _x0[i] + (indices[i] + 0.5) * _h;

  return 0;
}


int UniMesh::compute_index(const double coords[3], int indices[3]) const
{
  for (int i=0; i<3; ++i)
    indices[i] = floor(coords[i] - _x0[i] - 0.5*_h) / _h;

  return 0;
}
