#include "Timer.h"

#include <iostream>
#include <algorithm>


int setup(int nThrd)
{
  vector<double> tvec(TIMER_MAX_ITEM, 0.0);
  _t.insert(_t.begin(), nThrd, tvec);
  return 0;
}

void Timer::clear()
{
  for (unsigned int i=0; i<_t.size(); ++i) _t[i].clear();
}


int Timer::analyze_time_mpi()
{
  double tAvgs[TIMER_MAX_ITEM], tMaxs[TIMER_MAX_ITEM];
  std::copy(_t[0], _t[0]+TIMER_MAX_ITEM, tAvgs);
  std::copy(_t[0], _t[0]+TIMER_MAX_ITEM, tMaxs);
  MPI_Allreduce(MPI_IN_PLACE, tAvgs, TIMER_MAX_ITEM, MPI_DOUBLE, MPI_SUM, _ptDd->comm());
  MPI_Allreduce(MPI_IN_PLACE, tMaxs, TIMER_MAX_ITEM, MPI_DOUBLE, MPI_MAX, _ptDd->comm());
  for (int i=0; i<TIMER_MAX_ITEM; ++i) 
    tAvgs[i] /= _ptDd->num_proc();

  if (_ptDd->is_root()) {
    std::cout << "Avg:       " << std::fixed << tAvgs[SmootherTime::Comp] << "\t"
              << tAvgs[SmootherTime::Pack] << "\t" 
              << tAvgs[SmootherTime::Comm] << "\t"
              << tAvgs[SmootherTime::Unpack] << "\t"
              << tAvgs[SmootherTime::Total] << std::endl;
    std::cout << "Max:       " << std::fixed << tMaxs[SmootherTime::Comp] << "\t"
              << tMaxs[SmootherTime::Pack] << "\t" 
              << tMaxs[SmootherTime::Comm] << "\t"
              << tMaxs[SmootherTime::Unpack] << "\t"
              << tMaxs[SmootherTime::Total] << std::endl;
  }

  struct {double v; int r;} ValRank;
  ValRank.v = _t[0][SmootherTime::Total];
  ValRank.r = _ptDd->rank();
  MPI_Allreduce(MPI_IN_PLACE, &ValRank, 1, MPI_DOUBLE_INT, MPI_MAXLOC, _ptDd->comm());
  if (_ptDd->rank() == ValRank.r)
    std::cout << "Max Proc : " << std::fixed << _t[0][SmootherTime::Comp] << "\t"
              << _t[0][SmootherTime::Pack] << "\t" 
              << _t[0][SmootherTime::Comm] << "\t"
              << _t[0][SmootherTime::Unpack] << "\t"
              << _t[0][SmootherTime::Total] << std::endl;
  
  return 0;
}

