#include "BurgersSolver.h"
#include "constants.h"
#include "Timer.h"
#include <cassert>
#include <algorithm>

using std::max;
using std::min;


BurgersSolver::BurgersSolver(DomainDecomp& dd, UniMesh& mesh, int nh):
  Solver(dd, mesh, nh),
  _nu(0.01),
  _dt(1.0e-4)
{};


BurgersSolver::~BurgersSolver()
{
  for (int i=0; i<_nVar; ++i) delete [] _d[i];
  delete [] _d;
}


void BurgersSolver::set_from_option()
{
  Solver::set_from_option();
}


int BurgersSolver::setup()
{
  // set halo store, transfer, etc, nVar, pipe are set from option
  Solver::setup();

  _sr = 1; // stencil radius

  // allocate data
  _nVar     = 6;
  _nVarComp = 3;
  int szs[3];
  _ptDd->get_local_size(szs);
  _d = new double* [_nVar];
  for (int i=0; i<_nVar; ++i) 
    _d[i] = new double [(szs[0]+2*_nHalo) *(szs[1]+2*_nHalo)*(szs[2]+2*_nHalo)];

  // halo variables
  _haloVar.push_back(vector<int>({3, 4, 5}));
  _haloVar.push_back(vector<int>({0, 1, 2}));

  _setup_diamonds();

  _nu = 0.1*_h*_h;

  return 0;
}


int BurgersSolver::init()
{
  int szs[3], starts[3], gszs[3];
  _ptDd->get_local_size(szs);
  _ptDd->get_local_start(starts);
  _ptDd->get_global_size(gszs);
  int jStrd = szs[2] + 2*_nHalo;
  int iStrd = jStrd * (szs[1] + 2*_nHalo);
  int totSz = (szs[0]+2*_nHalo) * iStrd; 

  for (int i=0; i<szs[0]+2*_nHalo; ++i) {
    for (int j=0; j<szs[1]+2*_nHalo; ++j) {
      for (int k=0; k<szs[2]+2*_nHalo; ++k) {
        int ijk = i*iStrd + j*jStrd + k;
        _d[3][ijk] = 0.0;
        _d[4][ijk] = 0.0;
        _d[5][ijk] = 0.0;
        bool isDHBCZero = _dhOff && (i+starts[0] < _nHalo || i+starts[0] >= gszs[0]+_nHalo ||
                                     j+starts[1] < _nHalo || j+starts[1] >= gszs[1]+_nHalo ||
                                     k+starts[2] < _nHalo || k+starts[2] >= gszs[2]+_nHalo);
        _d[0][ijk] = isDHBCZero ? 0.0 : 0.01*sin(2*PI*(i-_nHalo+starts[0]+0.5)*_h);
        _d[1][ijk] = isDHBCZero ? 0.0 : 0.02*cos(2*PI*(j-_nHalo+starts[1]+0.5)*_h);
        _d[2][ijk] = isDHBCZero ? 0.0 : 0.03*sin(2*PI*(k-_nHalo+starts[2]+0.5)*_h);
      }
    }
  }

  for (int i=0; i<_ptDd->num_thread(); ++i)
    for (int j=0; j<TIMER_MAX_ITEM; ++j)
      std::fill(_t[i][j], _t[i][j]+TIMER_MAX_SIZE, 0.0);

  return 0;
}


int BurgersSolver::init_numa()
{
  int tid = omp_get_thread_num();
  int rngs[6], tRngs[6], starts[3], szs[3], gszs[3];
  _ptDd->get_local_size(szs);
  _ptDd->get_local_start(starts);
  _ptDd->get_global_size(gszs);
  for (int i=0; i<3; ++i) {
    rngs[i+3] = szs[i] + _nHalo;
    rngs[i]   = _nHalo;
  }
  int jStrd = rngs[2] + 2*_nHalo, iStrd = jStrd * (rngs[1] + 2*_nHalo);

  // pipeline (+ovlp) only init with nThrd-1 threads, numa
  if ((_mode == PipelineTile && !_isDiamond)) {
    int nThrd = _ptDd->num_thread();
    if (tid < nThrd-1) {
      // thread range include halo
      int nThrds[3] = {1, nThrd-1, 1};
      compute_thread_range(rngs, nThrds, tid, tRngs);
      for (int i=0; i<3; ++i) {
        if (tRngs[i] == rngs[i]) tRngs[i] = 0;
        if (tRngs[i+3] == rngs[i+3]) tRngs[i+3] += _nHalo;
      }
      // numa init
      for (int i=tRngs[0]; i<tRngs[3]; ++i) {
        for (int j=tRngs[1]; j<tRngs[4]; ++j) {
          for (int k=tRngs[2]; k<tRngs[5]; ++k) {
            int ijk = i*iStrd + j*jStrd + k;
            _d[3][ijk] = 0.0;
            _d[4][ijk] = 0.0;
            _d[5][ijk] = 0.0;
            bool isDHBCZero = _dhOff && (i+starts[0] < _nHalo || i+starts[0] >= gszs[0]+_nHalo ||
                                         j+starts[1] < _nHalo || j+starts[1] >= gszs[1]+_nHalo ||
                                         k+starts[2] < _nHalo || k+starts[2] >= gszs[2]+_nHalo);
            _d[0][ijk] = isDHBCZero ? 0.0 : 0.01*sin(2*PI*(i-_nHalo+starts[0])*_h);
            _d[1][ijk] = isDHBCZero ? 0.0 : 0.02*cos(2*PI*(j-_nHalo+starts[1])*_h);
            _d[2][ijk] = isDHBCZero ? 0.0 : 0.03*sin(2*PI*(k-_nHalo+starts[2])*_h);
          }
        }
      }// end for i
    }// end if tid
  }// end if pipeline
  // all threads init, numa
  else {
    // thread range include halo
    _ptDd->get_thread_range(tid, tRngs);
    for (int i=0; i<6; ++i)  tRngs[i] += _nHalo;
    for (int i=0; i<3; ++i) {
      if (tRngs[i] == rngs[i]) tRngs[i]  = 0;
      if (tRngs[i+3] == rngs[i+3]) tRngs[i+3] += _nHalo;
    }
    // numa init
    for (int i=tRngs[0]; i<tRngs[3]; ++i) {
      for (int j=tRngs[1]; j<tRngs[4]; ++j) {
        for (int k=tRngs[2]; k<tRngs[5]; ++k) {
          int ijk = i*iStrd + j*jStrd + k;
          _d[3][ijk] = 0.0;
          _d[4][ijk] = 0.0;
          _d[5][ijk] = 0.0;
          bool isDHBCZero = _dhOff && (i+starts[0] < _nHalo || i+starts[0] >= gszs[0]+_nHalo ||
                                       j+starts[1] < _nHalo || j+starts[1] >= gszs[1]+_nHalo ||
                                       k+starts[2] < _nHalo || k+starts[2] >= gszs[2]+_nHalo);
          _d[0][ijk] = isDHBCZero ? 0.0 : 0.01*sin(2*PI*(i-_nHalo+starts[0])*_h);
          _d[1][ijk] = isDHBCZero ? 0.0 : 0.02*cos(2*PI*(j-_nHalo+starts[1])*_h);
          _d[2][ijk] = isDHBCZero ? 0.0 : 0.03*sin(2*PI*(k-_nHalo+starts[2])*_h);
        }
      }
    }
  }// end else

  // init time
  for (int i=0; i<TIMER_MAX_ITEM; ++i)
    for (int j=0; j<TIMER_MAX_SIZE; ++j)
      _t[tid][i][j] = 0.0;

  // init buffer
  if (_isBufNuma) {
    int nPipe = _hs.num_pipe();
    if (nPipe == 1) {
      _hts[0].init_buffer_numa(_hs);
    }
    else {
      for (int i=0; i<nPipe; ++i)
        _hts[i].init_buffer_numa(_hs);
      for (int i=0; i<nPipe+2; ++i)
        _hps[i].init_buffer_numa(_hs);
    }
  }

  return 0;
}


void BurgersSolver::_compute_range(int rngs[6], bool isEven)
{
  double q4h = 0.25/_h, nuqh2 = _nu/_h/_h, *u0, *v0, *w0, *u, *v, *w;
  int    szs[3];  _ptDd->get_local_size(szs);
  int    js = szs[2] + 2*_nHalo, is = js * (szs[1] + 2*_nHalo), base=rngs[0]*is + rngs[1]*js;
  int    jsb = _nHalo, isb = _nHalo*szs[1], kmbs = _hs.kmBufStart[0], kpbs = _hs.kpBufStart[0];
  int    varStrd = szs[0]*szs[1]*_nHalo;
  int    tid = omp_get_thread_num();
  bool   mergePackkm = _ptDd->need_comm(2) && _bypassKHalo;
  bool   mergePackkp = _ptDd->need_comm(5) && _bypassKHalo;
  bool   mergeUnpackkm = mergePackkm && _thrdIter[tid] > 0;
  bool   mergeUnpackkp = mergePackkp && _thrdIter[tid] > 0;

  if (!isEven) {
    u  = _d[0];  v  = _d[1]; w  = _d[2];
    u0 = _d[3];  v0 = _d[4]; w0 = _d[5];
  }
  else {
    u0 = _d[0];  v0 = _d[1]; w0 = _d[2];
    u  = _d[3];  v  = _d[4]; w  = _d[5];
  }

  // unpack (iBegin, jBegin-jEnd)
  if (mergeUnpackkm) {
    int ij  =  rngs[0]*is + rngs[1]*js;
    int ijb = (rngs[0] - _nHalo)*isb + (rngs[1] - _nHalo) * jsb;
    for (int j=rngs[1]; j<max(rngs[4]+1,szs[2]+_nHalo); ++j) {
      w0[ij] = _hs.rBuf[kmbs+2*varStrd+ijb];
      ij += js; ijb += jsb;
    }
  }
  if (mergeUnpackkp) {
    int ij  =  rngs[0]*is + rngs[1]*js + szs[2] + _nHalo;
    int ijb = (rngs[0] - _nHalo)*isb + (rngs[1] - _nHalo) * jsb;
    for (int j=rngs[1]; j<max(rngs[4]+1,szs[2]+_nHalo); ++j) {
      if (rngs[0] > _nHalo)  u0[ij-is] = _hs.rBuf[kpbs+ijb-isb];
      ij += js; ijb += jsb;
    }
  }

  for (int i=rngs[0]; i<rngs[3]; ++i) {
    int ij = base;
    int ijb   = (i - _nHalo)*isb + (rngs[1] - _nHalo) * jsb;
    if (mergeUnpackkm && i < szs[0]+_nHalo-1)  w0[ij+is] = _hs.rBuf[kmbs+2*varStrd+ijb+isb];
    for (int j=rngs[1]; j<rngs[4]; ++j) {
      // unpack
      if (mergeUnpackkm) {
        for (int k=0; k<_nHalo; ++k) {
          u0[ij+k] = _hs.rBuf[kmbs+ijb+k];
          v0[ij+k] = _hs.rBuf[kmbs+varStrd+ijb+k];
        }
        if (i < szs[0]+_nHalo-1 && j < szs[1]+_nHalo-1)
          for (int k=0; k<_nHalo; ++k) 
            w0[ij+is+js+k] = _hs.rBuf[kmbs+2*varStrd+ijb+isb+jsb+k];
      }
      if (mergeUnpackkp) {
        for (int k=0; k<_nHalo; ++k) {
          u0[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+ijb+k];
          v0[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+varStrd+ijb+k];
          w0[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+2*varStrd+ijb+k];
        }
      }
      #pragma vector nontemporal
      #pragma omp simd
      for (int k=rngs[2]; k<rngs[5]; ++k) {
        int ijk = ij + k;
        double uvipjp = (u0[ijk] + u0[ijk+js]) * (v0[ijk] + v0[ijk+is]);
        double wuipkp = (w0[ijk] + w0[ijk+is]) * (u0[ijk] + u0[ijk+1]);
        double vwjpkp = (v0[ijk] + v0[ijk+1] ) * (w0[ijk] + w0[ijk+js]);
        //
        u[ijk] = ( ( 2.0*(u0[ijk+is] + u0[ijk-is]) + u0[ijk+js] + u0[ijk-js] + u0[ijk+1] + u0[ijk-1] - 8.0*u0[ijk]
                   + v0[ijk+is] - v0[ijk+is-js] - v0[ijk] + v0[ijk-js]
                   + w0[ijk+is] - w0[ijk+is-1] - w0[ijk] + w0[ijk-1]) *nuqh2
                 - ( (u0[ijk] + u0[ijk+is]) * (u0[ijk] + u0[ijk+is]) - (u0[ijk] + u0[ijk-is]) * (u0[ijk] + u0[ijk-is])
                   + uvipjp - (u0[ijk-js] + u0[ijk]) * (v0[ijk-js] + v0[ijk+is-js])
                   + wuipkp - (w0[ijk-1]  + w0[ijk+is-1] ) * (u0[ijk-1] + u0[ijk])) * q4h
                 ) * _dt + u0[ijk];
        //
        v[ijk] = ( ( v0[ijk+is] + v0[ijk-is] + 2.0*(v0[ijk+js] + v0[ijk-js]) + v0[ijk+1] + v0[ijk-1] - 8.0*v0[ijk]
                   + u0[ijk+js] - u0[ijk-is+js] - u0[ijk] + u0[ijk-is]
                   + w0[ijk+js] - w0[ijk+js-1] - w0[ijk] + w0[ijk-1]) * nuqh2
                 - ( uvipjp - (u0[ijk-is] + u0[ijk-is+js]) * (v0[ijk-is] + v0[ijk])
                   + (v0[ijk] + v0[ijk+js]) * (v0[ijk] + v0[ijk+js]) - (v0[ijk] + v0[ijk-js]) * (v0[ijk] + v0[ijk-js])
                   + vwjpkp - (v0[ijk-1] + v0[ijk]) * (w0[ijk-1] + w0[ijk+js-1])) * q4h
                 ) * _dt + v0[ijk];
        //
        w[ijk] = ( ( w0[ijk+is] + w0[ijk-is] + w0[ijk+js] + w0[ijk-js] + 2*(w0[ijk+1] + w0[ijk-1]) - 8.0*w0[ijk]
                   + u0[ijk+1] - u0[ijk] - u0[ijk-is+1] + u0[ijk-is]
                   + v0[ijk+1] - v0[ijk] - v0[ijk-js+1] + v0[ijk-js]) * nuqh2
                 - ( wuipkp - (w0[ijk-is] + w0[ijk]) * (u0[ijk-is] + u0[ijk-is+1])
                   + vwjpkp - (v0[ijk-js] + v0[ijk-js+1]) * (w0[ijk-js] + w0[ijk])
                   + (w0[ijk] + w0[ijk+1]) * (w0[ijk] + w0[ijk+1]) - (w0[ijk] + w0[ijk-1]) * (w0[ijk] + w0[ijk-1])) * q4h
                 ) * _dt + w0[ijk];
        //if (i==2&&j==2&&k==2)
          //std::cout << u[ijk] << " " << u0[ijk] << " " << u0[ijk-is] << " " << u0[ijk+is] << " "
            //<< u0[ijk-js] << " " << u0[ijk+js] << " " << u0[ijk-1] << " " << u0[ijk+1] << " "
            //<< v0[ijk+is] << " " << v0[ijk+is-js] << " " << v0[ijk] << " " << v0[ijk-js] << " "
            //<< w0[ijk+is] << " " << w0[ijk+is-1]  << " " << w0[ijk] << " " << w0[ijk-1] << " " << ijk+1 << std::endl;
      }// end for k
      // pack
      if (mergePackkm) {
        for (int k=0; k<_nHalo; ++k) {
          _hs.sBuf[kmbs+ijb+k]           = u[ij+_nHalo+k];
          _hs.sBuf[kmbs+varStrd+ijb+k]   = v[ij+_nHalo+k];
          _hs.sBuf[kmbs+2*varStrd+ijb+k] = w[ij+_nHalo+k];
        }
      }
      if (mergePackkp) {
        for (int k=0; k<_nHalo; ++k) {
          _hs.sBuf[kpbs+ijb+k]           = u[ij+szs[2]+k];
          _hs.sBuf[kpbs+varStrd+ijb+k]   = v[ij+szs[2]+k];
          _hs.sBuf[kpbs+2*varStrd+ijb+k] = w[ij+szs[2]+k];
        }
      }
      //
      ij += js;
      ijb += jsb;
    }// end for j
    base += is;
  }// end for i
       
}


void BurgersSolver::_compute_range_rectangle(int rngs[6], int dir, double *c, int isl, 
                                             int jsl, int pipe, HaloTransfer* ptHt)
{
  int tid    = omp_get_thread_num();
  int szs[3];
  _ptDd->get_local_size(szs);
  int pipeLen = szs[0] / _hs.num_pipe();
  int js  = szs[2] + 2*_nHalo, is  = js * (szs[1] + 2*_nHalo);
  int vsl = isl/_nVarComp; // var stride local: stride between u, v in local array
  int sw = 2*_sr + 1;
  int jsb = _nHalo, isb = _nHalo*szs[1], vsb = pipeLen*szs[1]*_nHalo;
  int kmbs = _hs.kmBufStart[pipe], kpbs = _hs.kpBufStart[pipe];
  int offset = _sr*(is+js), offsetb=_sr*(isb+jsb);
  double q4h = 0.25/_h, nuqh2 = _nu/_h/_h, *u0, *v0, *w0, *u, *v, *w;
  bool mergePackkm = _ptDd->need_comm(2) && _bypassKHalo;
  bool mergePackkp = _ptDd->need_comm(5) && _bypassKHalo;
  bool mergeUnpackkm = mergePackkm && _hs.num_pipe() == 1 && _thrdIter[tid] > 0;
  bool mergeUnpackkp = mergePackkp && _hs.num_pipe() == 1 && _thrdIter[tid] > 0;

  if (dir == 0) {
    u0 = _d[0]; v0 = _d[1]; w0 = _d[2];
    u  = _d[3]; v  = _d[4]; w  = _d[5];
  }
  else {
    u  = _d[0]; v  = _d[1]; w  = _d[2];
    u0 = _d[3]; v0 = _d[4]; w0 = _d[5];
  }

  int iBegin = max(rngs[0]-_nHalo+_sr, ((_dhOff && !_ptDd->need_comm(0))? _nHalo : -100));
  int iEnd   = min(rngs[3]+_nHalo-_sr, ((_dhOff && !_ptDd->need_comm(3))? szs[0]+_nHalo : 10000));
  int jBegin = max(rngs[1]-_nHalo+_sr, ((_dhOff && !_ptDd->need_comm(1))? _nHalo : -100));
  int jEnd   = min(rngs[4]+_nHalo-_sr, ((_dhOff && !_ptDd->need_comm(4))? szs[1]+_nHalo : 10000));
  int kBegin = max(rngs[2]-_nHalo+_sr, ((_dhOff && !_ptDd->need_comm(2))? _nHalo : -100));
  int kEnd   = min(rngs[5]+_nHalo-_sr, ((_dhOff && !_ptDd->need_comm(5))? szs[2]+_nHalo : 10000));
  int jlDHLow  = (_dhOff && !_ptDd->need_comm(1) && rngs[1] <= _nHalo) ? _nHalo : -100;
  int jlDHHgih = (_dhOff && !_ptDd->need_comm(4) && rngs[4] >= szs[1]+_nHalo) ? rngs[4]-rngs[1]+_nHalo : 10000;
  int klDHLow  = (_dhOff && !_ptDd->need_comm(2) && rngs[2] <= _nHalo) ? _nHalo : -100;
  int klDHHgih = (_dhOff && !_ptDd->need_comm(5) && rngs[5] >= szs[2]+_nHalo) ? rngs[5]-rngs[2]+_nHalo : 10000;

  // unpack i=nHalo to nHalo+_sr if deep halo is off
  if (_dhOff) {
    if (mergeUnpackkm) {
      for (int i=rngs[0]; i<rngs[0]+_sr; ++i) {
        for (int j=max(jBegin-_sr,_nHalo); j<min(jEnd+_sr,szs[1]+_nHalo); ++j) {
          int ij  =  i*is + j*js;
          int ijb = (i - _nHalo)*isb + (j - _nHalo) * jsb;
          for (int k=0; k<_nHalo; ++k)  {
            u0[ij+k] = _hs.rBuf[kmbs+ijb+k];
            v0[ij+k] = _hs.rBuf[kmbs+vsb+ijb+k];
            w0[ij+k] = _hs.rBuf[kmbs+2*vsb+ijb+k];
          }
        }
      }
    }
    if (mergeUnpackkp) {
      for (int i=rngs[0]; i<rngs[0]+_sr; ++i) {
        for (int j=max(jBegin-_sr,_nHalo); j<min(jEnd+_sr,szs[1]+_nHalo); ++j) {
          int ij  =  i*is + j*js + szs[2] + _nHalo;
          int ijb = (i - _nHalo)*isb + (j - _nHalo) * jsb;
          for (int k=0; k<_nHalo; ++k)  {
            u0[ij+k] = _hs.rBuf[kpbs+ijb+k];
            v0[ij+k] = _hs.rBuf[kpbs+vsb+ijb+k];
            w0[ij+k] = _hs.rBuf[kpbs+2*vsb+ijb+k];
          }
        }
      }
    }
  }

  for (int i=rngs[0]-_nHalo+_sr; i<rngs[3]+_nHalo-_sr; ++i) {
    // p -> c
    int ij    = i * is + jBegin * js;
    int basel = (i-1)%sw*isl + (jBegin-rngs[1]+_nHalo)*jsl - (rngs[2]-_nHalo);
    bool isIReadInside = (i+_sr >= _nHalo) && (i+_sr < szs[0]+_nHalo);
    if (_dhOff && (i < _nHalo || i >= szs[0]+_nHalo)) {
      _nullify_layer(c, basel, jBegin, jEnd, jsl, kBegin, kEnd, vsl);
    }
    else {
      int ijb = (i - _nHalo - pipe*pipeLen)*isb + (jBegin - _nHalo) * jsb;
      int nInitUpack = jBegin > _nHalo ? 2*_sr : (_dhOff ? _sr : 0);
      if (mergeUnpackkp) {
        for (int j=1; j<=nInitUpack; ++j) 
          for (int k=0; k<_nHalo; ++k)  
            v0[ij+_nHalo+szs[2]-j*js+k] = _hs.rBuf[kpbs+vsb+ijb-j*jsb+k];

      }
      for (int j=jBegin; j<jEnd; ++j) {
        // unpack
        int ijb   = (i - _nHalo - pipe*pipeLen)*isb + (j - _nHalo) * jsb;
        if (_ptDd->need_comm(2) && _bypassKHalo) {
          for (int k=0; k<_nHalo; ++k) {
            u0[ij+k] = _hs.rBuf[kmbs+ijb+k];
            v0[ij+k] = _hs.rBuf[kmbs+vsb+ijb+k];
            w0[ij+k] = _hs.rBuf[kmbs+2*vsb+ijb+k];
          }
        }
        if (_ptDd->need_comm(5) && _bypassKHalo) {
          for (int k=0; k<_nHalo; ++k) {
            u0[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+ijb+k];
            v0[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+vsb+ijb+k];
            w0[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+2*vsb+ijb+k];
          }
        }
        #pragma omp simd
        for (int k=kBegin; k<kEnd; ++k) {
          int ijk = ij + k;
          double uvipjp = (u0[ijk] + u0[ijk+js]) * (v0[ijk] + v0[ijk+is]);
          double wuipkp = (w0[ijk] + w0[ijk+is]) * (u0[ijk] + u0[ijk+1]);
          double vwjpkp = (v0[ijk] + v0[ijk+1] ) * (w0[ijk] + w0[ijk+js]);
          //
          c[basel+k] 
            = ( ( 2.0*(u0[ijk+is] + u0[ijk-is]) + u0[ijk+js] + u0[ijk-js] + u0[ijk+1] + u0[ijk-1] - 8.0*u0[ijk]
              + v0[ijk+is] - v0[ijk+is-js] - v0[ijk] + v0[ijk-js]
              + w0[ijk+is] - w0[ijk+is-1] - w0[ijk] + w0[ijk-1]) *nuqh2
            - ( (u0[ijk] + u0[ijk+is]) * (u0[ijk] + u0[ijk+is]) - (u0[ijk] + u0[ijk-is]) * (u0[ijk] + u0[ijk-is])
              + uvipjp - (u0[ijk-js] + u0[ijk]) * (v0[ijk-js] + v0[ijk+is-js])
              + wuipkp - (w0[ijk-1]  + w0[ijk+is-1] ) * (u0[ijk] + u0[ijk-1])) * q4h
              ) * _dt + u0[ijk];
          //
          c[basel+vsl+k]
            = ( ( v0[ijk+is] + v0[ijk-is] + 2.0*(v0[ijk+js] + v0[ijk-js]) + v0[ijk+1] + v0[ijk-1] - 8.0*v0[ijk]
              + u0[ijk+js] - u0[ijk-is+js] - u0[ijk] + u0[ijk-is]
              + w0[ijk+js] - w0[ijk+js-1] - w0[ijk] + w0[ijk-1]) * nuqh2
            - ( uvipjp - (u0[ijk-is] + u0[ijk-is+js]) * (v0[ijk-is] + v0[ijk])
              + (v0[ijk] + v0[ijk+js]) * (v0[ijk] + v0[ijk+js]) - (v0[ijk] + v0[ijk-js]) * (v0[ijk] + v0[ijk-js])
              + vwjpkp - (v0[ijk-1] + v0[ijk]) * (w0[ijk-1] + w0[ijk+js-1])) * q4h
            ) * _dt + v0[ijk];
          //
          c[basel+2*vsl+k]
            = ( ( w0[ijk+is] + w0[ijk-is] + w0[ijk+js] + w0[ijk-js] + 2*(w0[ijk+1] + w0[ijk-1]) - 8.0*w0[ijk]
              + u0[ijk+1] - u0[ijk] - u0[ijk-is+1] + u0[ijk-is]
              + v0[ijk+1] - v0[ijk] - v0[ijk-js+1] + v0[ijk-js]) * nuqh2
            - ( wuipkp - (w0[ijk-is] + w0[ijk]) * (u0[ijk-is] + u0[ijk-is+1])
              + vwjpkp - (v0[ijk-js] + v0[ijk-js+1]) * (w0[ijk-js] + w0[ijk])
              + (w0[ijk] + w0[ijk+1]) * (w0[ijk] + w0[ijk+1]) - (w0[ijk] + w0[ijk-1]) * (w0[ijk] + w0[ijk-1])) * q4h
            ) * _dt + w0[ijk];
        //if (i==241&&j==2&&k==2)
          //std::cout << c[basel+k] << " " << u0[ijk] << " " << u0[ijk-is] << " " << u0[ijk+is] << " "
            //<< u0[ijk-js] << " " << u0[ijk+js] << " " << u0[ijk-1] << " " << u0[ijk+1] << " "
            //<< v0[ijk+is] << " " << v0[ijk+is-js] << " " << v0[ijk] << " " << v0[ijk-js] << " "
            //<< w0[ijk+is] << " " << w0[ijk+is-1]  << " " << w0[ijk] << " " << w0[ijk-1] << " " << ijk+is << std::endl;
        }// end for k
        ij    += js;
        basel += jsl;
      }// end for j
    }
    // c -> c
    int nf     = _nHalo / _sr; // # fuesd iterations
    int iLayer = i - _sr;                      // current iteration layer in domain
    int iLow   = rngs[0] - _nHalo + 2*_sr;     // i lower bound for current iter
    int iHigh  = rngs[3] + _nHalo - 1 - 2*_sr; // i higher bound 
    int margin = 2 * _sr;
    for (int m=1; m<nf-1; ++m) {
      int jlBegin = max(margin, jlDHLow);
      int jlEnd   = min(rngs[4]-rngs[1]+2*_nHalo-margin,jlDHHgih);
      if (iLayer >= iLow && iLayer <= iHigh) {
        int ijl   = ((iLayer-1+sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
        int im1jl = ((iLayer-2+sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
        int ip1jl = ((iLayer  +sw)%sw + (m-1)*sw) * isl + jlBegin*jsl;
        int ijlTo = ijl + sw*isl;
        int ij    = iLayer*is + (rngs[1]-_nHalo+jlBegin)*js;
        if (_dhOff && (iLayer < _nHalo || iLayer >= szs[0]+_nHalo)) {
          for (int j=jlBegin; j<jlEnd; ++j) {
            int klBegin = max(margin,klDHLow), klEnd = min(rngs[5]-rngs[2]+2*_nHalo-margin,klDHHgih);
            #pragma omp simd
            for (int k=klBegin; k<klEnd; ++k) {
              c[ijlTo+k] = 0.0;
              c[ijlTo+k+vsl] = 0.0;
              c[ijlTo+k+2*vsl] = 0.0;
            }
            ijlTo += jsl;
          }// end for j
        }
        else {
          for (int j=jlBegin; j<jlEnd; ++j) {
            int klBegin = max(margin,klDHLow), klEnd = min(rngs[5]-rngs[2]+2*_nHalo-margin,klDHHgih);
            #pragma omp simd
            for (int k=klBegin; k<klEnd; ++k) {
              int ijkl   = ijl + k;
              int ip1jkl = ip1jl + k;
              int im1jkl = im1jl + k;
              double uvipjp = (c[ijkl] + c[ijkl+jsl]) * (c[ijkl+vsl] + c[ip1jl+k+vsl]);
              double wuipkp = (c[ijkl+2*vsl] + c[ip1jl+2*vsl+k]) * (c[ijkl] + c[ijkl+1]);
              double vwjpkp = (c[ijkl+vsl] + c[ijkl+vsl+1] ) * (c[ijkl+2*vsl] + c[ijkl+2*vsl+jsl]);
              //
              c[ijlTo+k] 
                = ( ( 2.0*(c[ip1jkl] + c[im1jkl]) + c[ijkl+jsl] + c[ijkl-jsl] + c[ijkl+1] + c[ijkl-1] - 8.0*c[ijkl]
                    + c[ip1jkl+vsl] - c[ip1jkl-jsl+vsl] - c[ijkl+vsl] + c[ijkl-jsl+vsl]
                    + c[ip1jkl+2*vsl] - c[ip1jkl-1+2*vsl] - c[ijkl+2*vsl] + c[ijkl-1+2*vsl]) *nuqh2
                  - ( (c[ijkl] + c[ip1jkl]) * (c[ijkl] + c[ip1jkl]) - (c[ijkl] + c[im1jkl]) * (c[ijkl] + c[im1jkl])
                    + uvipjp - (c[ijkl-jsl] + c[ijkl]) * (c[ijkl-jsl+vsl] + c[ip1jkl-jsl+vsl])
                    + wuipkp - (c[ijkl-1+2*vsl]  + c[ip1jkl-1+2*vsl] ) * (c[ijkl] + c[ijkl-1])) * q4h
                    ) * _dt + c[ijkl];
              //
              c[ijlTo+k+vsl] 
                = ( ( c[ip1jkl+vsl] + c[im1jkl+vsl] + 2.0*(c[ijkl+jsl+vsl] + c[ijkl-jsl+vsl]) 
                    + c[ijkl+1+vsl] + c[ijkl-1+vsl] - 8.0*c[ijkl+vsl]
                    + c[ijkl+jsl] - c[im1jkl+jsl] - c[ijkl] + c[im1jkl]
                    + c[ijkl+jsl+2*vsl] - c[ijkl+jsl-1+2*vsl] - c[ijkl+2*vsl] + c[ijkl-1+2*vsl]) * nuqh2
                  - ( uvipjp - (c[im1jkl] + c[im1jkl+jsl]) * (c[im1jkl+vsl] + c[ijkl+vsl])
                    + (c[ijkl+vsl] + c[ijkl+jsl+vsl]) * (c[ijkl+vsl] + c[ijkl+jsl+vsl]) 
                    - (c[ijkl+vsl] + c[ijkl-jsl+vsl]) * (c[ijkl+vsl] + c[ijkl-jsl+vsl])
                    + vwjpkp - (c[ijkl-1+vsl] + c[ijkl+vsl]) * (c[ijkl-1+2*vsl] + c[ijkl+jsl-1+2*vsl])) * q4h
                  ) * _dt + c[ijkl+vsl];
              //
              c[ijlTo+k+2*vsl] 
                = ( ( c[ip1jkl+2*vsl] + c[im1jkl+2*vsl] + c[ijkl+jsl+2*vsl] + c[ijkl-jsl+2*vsl] 
                    + 2*(c[ijkl+1+2*vsl] + c[ijkl-1+2*vsl]) - 8.0*c[ijkl+2*vsl]
                    + c[ijkl+1] - c[ijkl] - c[im1jkl+1] + c[im1jkl]
                    + c[ijkl+1+vsl] - c[ijkl+vsl] - c[ijkl-jsl+1+vsl] + c[ijkl-jsl+vsl]) * nuqh2
                  - ( wuipkp - (c[im1jkl+2*vsl] + c[ijkl+2*vsl]) * (c[im1jkl] + c[im1jkl+1])
                    + vwjpkp - (c[ijkl-jsl+vsl] + c[ijkl-jsl+1+vsl]) * (c[ijkl-jsl+2*vsl] + c[ijkl+2*vsl])
                    + (c[ijkl+2*vsl] + c[ijkl+1+2*vsl]) * (c[ijkl+2*vsl] + c[ijkl+1+2*vsl]) 
                    - (c[ijkl+2*vsl] + c[ijkl-1+2*vsl]) * (c[ijkl+2*vsl] + c[ijkl-1+2*vsl])) * q4h
                  ) * _dt + c[ijkl+2*vsl];
            }// end for k
            ij    += js;
            ijl   += jsl; im1jl += jsl; ip1jl += jsl; ijlTo += jsl;
          }// end for j
        }
        margin += _sr;
        iLayer -= _sr;
        iLow   += _sr;
        iHigh  -= _sr;
      }// end if           
    }// end for m
    // c -> a
    if (iLayer >= iLow && iLayer <= iHigh) {
      if (iLayer < iBegin || iLayer >= iEnd)  continue;
      int ijl   = ((iLayer-1+sw)%sw + (nf-2)*sw) * isl + margin*jsl;
      int im1jl = ((iLayer-2+sw)%sw + (nf-2)*sw) * isl + margin*jsl;
      int ip1jl = ((iLayer  +sw)%sw + (nf-2)*sw) * isl + margin*jsl;
      int ij    = iLayer*is + rngs[1]*js;
      int ijb   = (iLayer - _nHalo - pipe*pipeLen)*isb + (rngs[1] - _nHalo) * jsb;
      for (int j=_nHalo; j<rngs[4]-rngs[1]+_nHalo; ++j) {
        #pragma omp simd
        for (int k=_nHalo; k<rngs[5]-rngs[2]+_nHalo; ++k) {
          int ijkl   = ijl + k, ijk  = ij  + k + rngs[2] - _nHalo;
          int ip1jkl = ip1jl + k;
          int im1jkl = im1jl + k;
          double uvipjp = (c[ijkl] + c[ijkl+jsl]) * (c[ijkl+vsl] + c[ip1jl+k+vsl]);
          double wuipkp = (c[ijkl+2*vsl] + c[ip1jl+2*vsl+k]) * (c[ijkl] + c[ijkl+1]);
          double vwjpkp = (c[ijkl+vsl] + c[ijkl+vsl+1] ) * (c[ijkl+2*vsl] + c[ijkl+2*vsl+jsl]);
          //
          u[ijk] 
            = ( ( 2.0*(c[ip1jkl] + c[im1jkl]) + c[ijkl+jsl] + c[ijkl-jsl] + c[ijkl+1] + c[ijkl-1] - 8.0*c[ijkl]
                + c[ip1jkl+vsl] - c[ip1jkl-jsl+vsl] - c[ijkl+vsl] + c[ijkl-jsl+vsl]
                + c[ip1jkl+2*vsl] - c[ip1jkl-1+2*vsl] - c[ijkl+2*vsl] + c[ijkl-1+2*vsl]) *nuqh2
              - ( (c[ijkl] + c[ip1jkl]) * (c[ijkl] + c[ip1jkl]) - (c[ijkl] + c[im1jkl]) * (c[ijkl] + c[im1jkl])
                + uvipjp - (c[ijkl-jsl] + c[ijkl]) * (c[ijkl-jsl+vsl] + c[ip1jkl-jsl+vsl])
                + wuipkp - (c[ijkl-1+2*vsl]  + c[ip1jkl-1+2*vsl] ) * (c[ijkl] + c[ijkl-1])) * q4h
                ) * _dt + c[ijkl];
          //
          v[ijk]
            = ( ( c[ip1jkl+vsl] + c[im1jkl+vsl] + 2.0*(c[ijkl+jsl+vsl] + c[ijkl-jsl+vsl]) 
                + c[ijkl+1+vsl] + c[ijkl-1+vsl] - 8.0*c[ijkl+vsl]
                + c[ijkl+jsl] - c[im1jkl+jsl] - c[ijkl] + c[im1jkl]
                + c[ijkl+jsl+2*vsl] - c[ijkl+jsl-1+2*vsl] - c[ijkl+2*vsl] + c[ijkl-1+2*vsl]) * nuqh2
              - ( uvipjp - (c[im1jkl] + c[im1jkl+jsl]) * (c[im1jkl+vsl] + c[ijkl+vsl])
                + (c[ijkl+vsl] + c[ijkl+jsl+vsl]) * (c[ijkl+vsl] + c[ijkl+jsl+vsl]) 
                - (c[ijkl+vsl] + c[ijkl-jsl+vsl]) * (c[ijkl+vsl] + c[ijkl-jsl+vsl])
                + vwjpkp - (c[ijkl-1+vsl] + c[ijkl+vsl]) * (c[ijkl-1+2*vsl] + c[ijkl+jsl-1+2*vsl])) * q4h
              ) * _dt + c[ijkl+vsl];
          //
          w[ijk]
            = ( ( c[ip1jkl+2*vsl] + c[im1jkl+2*vsl] + c[ijkl+jsl+2*vsl] + c[ijkl-jsl+2*vsl] 
                + 2*(c[ijkl+1+2*vsl] + c[ijkl-1+2*vsl]) - 8.0*c[ijkl+2*vsl]
                + c[ijkl+1] - c[ijkl] - c[im1jkl+1] + c[im1jkl]
                + c[ijkl+1+vsl] - c[ijkl+vsl] - c[ijkl-jsl+1+vsl] + c[ijkl-jsl+vsl]) * nuqh2
              - ( wuipkp - (c[im1jkl+2*vsl] + c[ijkl+2*vsl]) * (c[im1jkl] + c[im1jkl+1])
                + vwjpkp - (c[ijkl-jsl+vsl] + c[ijkl-jsl+1+vsl]) * (c[ijkl-jsl+2*vsl] + c[ijkl+2*vsl])
                + (c[ijkl+2*vsl] + c[ijkl+1+2*vsl]) * (c[ijkl+2*vsl] + c[ijkl+1+2*vsl]) 
                - (c[ijkl+2*vsl] + c[ijkl-1+2*vsl]) * (c[ijkl+2*vsl] + c[ijkl-1+2*vsl])) * q4h
              ) * _dt + c[ijkl+2*vsl];
        //if (ijk==241*is+2*js+2)
          //std::cout << u[ijk] << " " << c[ijkl] << " " << c[im1jkl] << " " << c[ip1jkl] << " " 
            //<< c[ijkl-jsl] << " " << c[ijkl+jsl] << " " << c[ijkl-1] << " " << c[ijkl+1]  << " "
            //<< c[ip1jkl+vsl] << " " << c[ip1jkl-jsl+vsl] << " " << c[ijkl+vsl] << " " << c[ijkl-jsl+vsl] << " "
            //<< c[ip1jkl+2*vsl] << " " << c[ip1jkl-1+2*vsl] << " " << c[ijkl+2*vsl] << " " << c[ijkl-1+2*vsl] << std::endl;
        }// end for k
        // pack
        if (_ptDd->need_comm(2) && _bypassKHalo) {
          for (int k=0; k<_nHalo; ++k) {
            _hs.sBuf[kmbs+ijb+k]       = u[ij+_nHalo+k];
            _hs.sBuf[kmbs+vsb+ijb+k]   = v[ij+_nHalo+k];
            _hs.sBuf[kmbs+2*vsb+ijb+k] = w[ij+_nHalo+k];
          }
        }
        if (_ptDd->need_comm(5) && _bypassKHalo) {
          for (int k=0; k<_nHalo; ++k) {
            _hs.sBuf[kpbs+ijb+k]       = u[ij+szs[2]+k];
            _hs.sBuf[kpbs+vsb+ijb+k]   = v[ij+szs[2]+k];
            _hs.sBuf[kpbs+2*vsb+ijb+k] = w[ij+szs[2]+k];
          }
        }
        //
        ij  += js;
        ijl += jsl; im1jl += jsl; ip1jl += jsl;
        ijb += jsb;
      }// end for j
    }// end if      
    // poll network if enabled
    if (tid == 0 && ptHt)  ptHt->poll(); 
  }// end for i

}


void BurgersSolver::_compute_pipe_diamond(int di, int dj, int pipe, int dir, HaloTransfer* ptHt)
{
  int szs[3]; _ptDd->get_local_size(szs);
  int js  = szs[2] + 2*_nHalo, is  = js * (szs[1] + 2*_nHalo);
  int kBegin = _sr, kEnd = szs[2] + 2*_nHalo - _sr, jBegin, jEnd;
  int tid = omp_get_thread_num();
  int nf = _nHalo / _sr;
  int nPipe = _hs.num_pipe(), pipeLen = szs[0] / nPipe;
  int jsb = _nHalo, isb = _nHalo*szs[1], vsb = pipeLen*szs[1]*_nHalo;
  int kmbs = _hs.kmBufStart[pipe], kpbs = _hs.kpBufStart[pipe];

  double q4h = 0.25/_h, nuqh2 = _nu/_h/_h;
  double *u0=_d[0], *v0=_d[1], *w0=_d[2], *u=_d[3], *v=_d[4], *w=_d[5];

  // adjust diamond's range based on current pipe
  _diamonds[di][dj].iBegin += pipe * pipeLen;
  _diamonds[di][dj].iEnd   += pipe * pipeLen;
 
  // adjust the triangleup/down range if it locates on pipe boundary
  int iBegin = _diamonds[di][dj].iBegin, iEnd  = _diamonds[di][dj].iEnd;
  int lBound = _diamonds[di][dj].iBegin, lSign =  1;
  int hBound = _diamonds[di][dj].iEnd,   hSign = -1; 
  if (_hs.num_pipe() > 1) {
    if (pipe == 0 && di == _nDiamond[0]-1) {// pipe 0, top is done
      hBound = _diamonds[di][dj].iEnd - 2*(_nHalo - _sr);
      hSign  = 1;
    }
    if (pipe > 1 && di == 0) {// pipe > 1, bottom is done
      iBegin = _diamonds[di][dj].iBegin + 2*(_nHalo - _sr);
      lBound = -1;
      lSign  =  0;
    }
  }

  int iDHLow  = max(iBegin, (_dhOff && !_ptDd->need_comm(0)) ? _nHalo : -100);
  int iDHHigh = min(iEnd,   (_dhOff && !_ptDd->need_comm(3)) ? szs[0]+_nHalo : 10000);
  int jDHLow  = (_dhOff && !_ptDd->need_comm(1)) ? _nHalo : -100;
  int jDHHigh = (_dhOff && !_ptDd->need_comm(4)) ? szs[1]+_nHalo : 10000;
  int kDHLow  = (_dhOff && !_ptDd->need_comm(2)) ? _nHalo : -100;
  int kDHHigh = (_dhOff && !_ptDd->need_comm(5)) ? szs[2]+_nHalo : 10000;

  switch (_diamonds[di][dj].type) {
    case TriangleUp:
      for (int i=iBegin; i<iEnd; ++i) {
        for (int m=0; m<nf; ++m) {
          int from = (dir + m) % 2;
          u0 = _d[3*from];      v0 = _d[3*from+1];      w0 = _d[3*from+2];
          u  = _d[3*(1-from)];  v  = _d[3*(1-from)+1];  w  = _d[3*(1-from)+2];
          int margin = m * _sr;
          int iLayer = i - m * _sr;
          if ((iLayer >= lBound + lSign*margin) && (iLayer <  hBound + hSign*margin)) {
            if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
  //if (di == 18) 
    //std::cout << iBegin << " " << iEnd << " " << lBound+lSign*margin << " " << hBound+hSign*margin << " "
              //<< max(_diamonds[di][dj].jBegin+margin,jDHLow) << " " << min(_diamonds[di][dj].jEnd-margin,jDHHigh) << " "
              //<< max(kBegin+margin,kDHLow) << " " << min(kEnd-margin,kDHHigh) << std::endl;
            int ij = iLayer * is + max(_diamonds[di][dj].jBegin+margin,jDHLow) * js;
            for (int j=max(_diamonds[di][dj].jBegin+margin,jDHLow); j<min(_diamonds[di][dj].jEnd-margin,jDHHigh); ++j) {
              int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (j - _nHalo) * jsb;
              // unpack
              if (m == 0 && _ptDd->need_comm(2) && _bypassKHalo) {
                for (int k=0; k<_nHalo; ++k) {
                  u0[ij+k] = _hs.rBuf[kmbs+ijb+k];
                  v0[ij+k] = _hs.rBuf[kmbs+vsb+ijb+k];
                  w0[ij+k] = _hs.rBuf[kmbs+2*vsb+ijb+k];
                }
              }
              if (m == 0 && _ptDd->need_comm(5) && _bypassKHalo) {
                for (int k=0; k<_nHalo; ++k) {
                  u0[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+ijb+k];
                  v0[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+vsb+ijb+k];
                  w0[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+2*vsb+ijb+k];
                }
              }
              // compute
              #pragma omp simd
              for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                int ijk = ij + k;
                double uvipjp = (u0[ijk] + u0[ijk+js]) * (v0[ijk] + v0[ijk+is]);
                double wuipkp = (w0[ijk] + w0[ijk+is]) * (u0[ijk] + u0[ijk+1]);
                double vwjpkp = (v0[ijk] + v0[ijk+1] ) * (w0[ijk] + w0[ijk+js]);
                //
                u[ijk] = ( ( 2.0*(u0[ijk+is] + u0[ijk-is]) + u0[ijk+js] + u0[ijk-js] + u0[ijk+1] + u0[ijk-1] - 8.0*u0[ijk]
                           + v0[ijk+is] - v0[ijk+is-js] - v0[ijk] + v0[ijk-js]
                           + w0[ijk+is] - w0[ijk+is-1] - w0[ijk] + w0[ijk-1]) *nuqh2
                         - ( (u0[ijk] + u0[ijk+is]) * (u0[ijk] + u0[ijk+is]) - (u0[ijk] + u0[ijk-is]) * (u0[ijk] + u0[ijk-is])
                           + uvipjp - (u0[ijk-js] + u0[ijk]) * (v0[ijk-js] + v0[ijk+is-js])
                           + wuipkp - (w0[ijk-1]  + w0[ijk+is-1] ) * (u0[ijk] + u0[ijk-1])) * q4h
                         ) * _dt + u0[ijk];
                //
                v[ijk] = ( ( v0[ijk+is] + v0[ijk-is] + 2.0*(v0[ijk+js] + v0[ijk-js]) + v0[ijk+1] + v0[ijk-1] - 8.0*v0[ijk]
                           + u0[ijk+js] - u0[ijk-is+js] - u0[ijk] + u0[ijk-is]
                           + w0[ijk+js] - w0[ijk+js-1] - w0[ijk] + w0[ijk-1]) * nuqh2
                         - ( uvipjp - (u0[ijk-is] + u0[ijk-is+js]) * (v0[ijk-is] + v0[ijk])
                           + (v0[ijk] + v0[ijk+js]) * (v0[ijk] + v0[ijk+js]) - (v0[ijk] + v0[ijk-js]) * (v0[ijk] + v0[ijk-js])
                           + vwjpkp - (v0[ijk-1] + v0[ijk]) * (w0[ijk-1] + w0[ijk+js-1])) * q4h
                         ) * _dt + v0[ijk];
                //
                w[ijk] = ( ( w0[ijk+is] + w0[ijk-is] + w0[ijk+js] + w0[ijk-js] + 2*(w0[ijk+1] + w0[ijk-1]) - 8.0*w0[ijk]
                           + u0[ijk+1] - u0[ijk] - u0[ijk-is+1] + u0[ijk-is]
                           + v0[ijk+1] - v0[ijk] - v0[ijk-js+1] + v0[ijk-js]) * nuqh2
                         - ( wuipkp - (w0[ijk-is] + w0[ijk]) * (u0[ijk-is] + u0[ijk-is+1])
                           + vwjpkp - (v0[ijk-js] + v0[ijk-js+1]) * (w0[ijk-js] + w0[ijk])
                           + (w0[ijk] + w0[ijk+1]) * (w0[ijk] + w0[ijk+1]) - (w0[ijk] + w0[ijk-1]) * (w0[ijk] + w0[ijk-1])) * q4h
                         ) * _dt + w0[ijk];
        //if (iLayer==2&&j==2&&k==2)
          //std::cout << u[ijk] << " " << u0[ijk] << " " << u0[ijk-is] << " " << u0[ijk+is] << " "
            //<< u0[ijk-js] << " " << u0[ijk+js] << " " << u0[ijk-1] << " " << u0[ijk+1] << " "
            //<< v0[ijk+is] << " " << v0[ijk+is-js] << " " << v0[ijk] << " " << v0[ijk-js] << " "
            //<< w0[ijk+is] << " " << w0[ijk+is-1]  << " " << w0[ijk] << " " << w0[ijk-1] << " " << ijk+1 << std::endl;
              }// end for k
              // pack
              if (m == nf-1 && _ptDd->need_comm(2) && _bypassKHalo) {
                for (int k=0; k<_nHalo; ++k) {
                  _hs.sBuf[kmbs+ijb+k]       = u[ij+_nHalo+k];
                  _hs.sBuf[kmbs+vsb+ijb+k]   = v[ij+_nHalo+k];
                  _hs.sBuf[kmbs+2*vsb+ijb+k] = w[ij+_nHalo+k];
                }
              }
              if (m == nf-1 && _ptDd->need_comm(5) && _bypassKHalo) {
                for (int k=0; k<_nHalo; ++k) {
                  _hs.sBuf[kpbs+ijb+k]       = u[ij+szs[2]+k];
                  _hs.sBuf[kpbs+vsb+ijb+k]   = v[ij+szs[2]+k];
                  _hs.sBuf[kpbs+2*vsb+ijb+k] = w[ij+szs[2]+k];
                }
              }
              //
              ij += js;
            }// end for j
          }// end if
        }// end for m
        if (tid == 0 && ptHt)  ptHt->poll(); 
      }// end for i
    break;

    case TriangleDown:
      jBegin = _diamonds[di][dj].jBegin + _nHalo - _sr;
      jEnd   = _diamonds[di][dj].jEnd   - _nHalo + _sr;
      for (int i=iBegin; i<iEnd; ++i) {
        for (int m=0; m<nf; ++m) {
          int from = (dir + m) % 2;
          u0 = _d[3*from];      v0 = _d[3*from+1];      w0 = _d[3*from+2];
          u  = _d[3*(1-from)];  v  = _d[3*(1-from)+1];  w  = _d[3*(1-from)+2];
          int margin = m * _sr;
          int iLayer = i - m * _sr;
          if ((iLayer >= lBound + lSign*margin) && (iLayer <  hBound + hSign*margin)) {
            if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
            int ij = iLayer * is + max(jBegin - margin, jDHLow) * js;
            for (int j=max(jBegin-margin,jDHLow); j<min(jEnd+margin,jDHHigh); ++j) {
              // unpack
              int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (j - _nHalo) * jsb;
              if (m == 0 && _ptDd->need_comm(2) && _bypassKHalo) {
                for (int k=0; k<_nHalo; ++k) {
                  u0[ij+k] = _hs.rBuf[kmbs+ijb+k];
                  v0[ij+k] = _hs.rBuf[kmbs+vsb+ijb+k];
                  w0[ij+k] = _hs.rBuf[kmbs+2*vsb+ijb+k];
                }
              }
              if (m == 0 && _ptDd->need_comm(5) && _bypassKHalo) {
                for (int k=0; k<_nHalo; ++k) {
                  u0[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+ijb+k];
                  v0[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+vsb+ijb+k];
                  w0[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+2*vsb+ijb+k];
                }
              }
              #pragma omp simd
              for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                int ijk = ij + k;
                double uvipjp = (u0[ijk] + u0[ijk+js]) * (v0[ijk] + v0[ijk+is]);
                double wuipkp = (w0[ijk] + w0[ijk+is]) * (u0[ijk] + u0[ijk+1]);
                double vwjpkp = (v0[ijk] + v0[ijk+1] ) * (w0[ijk] + w0[ijk+js]);
                //
                u[ijk] = ( ( 2.0*(u0[ijk+is] + u0[ijk-is]) + u0[ijk+js] + u0[ijk-js] + u0[ijk+1] + u0[ijk-1] - 8.0*u0[ijk]
                           + v0[ijk+is] - v0[ijk+is-js] - v0[ijk] + v0[ijk-js]
                           + w0[ijk+is] - w0[ijk+is-1] - w0[ijk] + w0[ijk-1]) *nuqh2
                         - ( (u0[ijk] + u0[ijk+is]) * (u0[ijk] + u0[ijk+is]) - (u0[ijk] + u0[ijk-is]) * (u0[ijk] + u0[ijk-is])
                           + uvipjp - (u0[ijk-js] + u0[ijk]) * (v0[ijk-js] + v0[ijk+is-js])
                           + wuipkp - (w0[ijk-1]  + w0[ijk+is-1] ) * (u0[ijk] + u0[ijk-1])) * q4h
                         ) * _dt + u0[ijk];
                //
                v[ijk] = ( ( v0[ijk+is] + v0[ijk-is] + 2.0*(v0[ijk+js] + v0[ijk-js]) + v0[ijk+1] + v0[ijk-1] - 8.0*v0[ijk]
                           + u0[ijk+js] - u0[ijk-is+js] - u0[ijk] + u0[ijk-is]
                           + w0[ijk+js] - w0[ijk+js-1] - w0[ijk] + w0[ijk-1]) * nuqh2
                         - ( uvipjp - (u0[ijk-is] + u0[ijk-is+js]) * (v0[ijk-is] + v0[ijk])
                           + (v0[ijk] + v0[ijk+js]) * (v0[ijk] + v0[ijk+js]) - (v0[ijk] + v0[ijk-js]) * (v0[ijk] + v0[ijk-js])
                           + vwjpkp - (v0[ijk-1] + v0[ijk]) * (w0[ijk-1] + w0[ijk+js-1])) * q4h
                         ) * _dt + v0[ijk];
                //
                w[ijk] = ( ( w0[ijk+is] + w0[ijk-is] + w0[ijk+js] + w0[ijk-js] + 2*(w0[ijk+1] + w0[ijk-1]) - 8.0*w0[ijk]
                           + u0[ijk+1] - u0[ijk] - u0[ijk-is+1] + u0[ijk-is]
                           + v0[ijk+1] - v0[ijk] - v0[ijk-js+1] + v0[ijk-js]) * nuqh2
                         - ( wuipkp - (w0[ijk-is] + w0[ijk]) * (u0[ijk-is] + u0[ijk-is+1])
                           + vwjpkp - (v0[ijk-js] + v0[ijk-js+1]) * (w0[ijk-js] + w0[ijk])
                           + (w0[ijk] + w0[ijk+1]) * (w0[ijk] + w0[ijk+1]) - (w0[ijk] + w0[ijk-1]) * (w0[ijk] + w0[ijk-1])) * q4h
                         ) * _dt + w0[ijk];
        //if (iLayer==14&&j==241&&k==2)
          //std::cout << u[ijk] << " " << u0[ijk] << " " << u0[ijk-is] << " " << u0[ijk+is] << " "
            //<< u0[ijk-js] << " " << u0[ijk+js] << " " << u0[ijk-1] << " " << u0[ijk+1] << " "
            //<< v0[ijk+is] << " " << v0[ijk+is-js] << " " << v0[ijk] << " " << v0[ijk-js] << " "
            //<< w0[ijk+is] << " " << w0[ijk+is-1]  << " " << w0[ijk] << " " << w0[ijk-1] << " " << ijk+1 << std::endl;
              }// end for k
              // pack
              if (m == nf-1 && _ptDd->need_comm(2) && _bypassKHalo) {
                for (int k=0; k<_nHalo; ++k) {
                  _hs.sBuf[kmbs+ijb+k]       = u[ij+_nHalo+k];
                  _hs.sBuf[kmbs+vsb+ijb+k]   = v[ij+_nHalo+k];
                  _hs.sBuf[kmbs+2*vsb+ijb+k] = w[ij+_nHalo+k];
                }
              }
              if (m == nf-1 && _ptDd->need_comm(5) && _bypassKHalo) {
                for (int k=0; k<_nHalo; ++k) {
                  _hs.sBuf[kpbs+ijb+k]       = u[ij+szs[2]+k];
                  _hs.sBuf[kpbs+vsb+ijb+k]   = v[ij+szs[2]+k];
                  _hs.sBuf[kpbs+2*vsb+ijb+k] = w[ij+szs[2]+k];
                }
              }
              //
              ij += js;
              ijb +=jsb;
            }// end for j
          }// end if
        }// end for m
        if (tid == 0 && ptHt)  ptHt->poll(); 
      }// end for i
    break;

    case TriangleLeft:
      for (int i=_diamonds[di][dj].iBegin+_nHalo-_sr; i<_diamonds[di][dj].iEnd+_nHalo-_sr; ++i) {
        for (int m=0; m<nf; ++m) {
          int from = (dir + m) % 2;
          u0 = _d[3*from];      v0 = _d[3*from+1];      w0 = _d[3*from+2];
          u  = _d[3*(1-from)];  v  = _d[3*(1-from)+1];  w  = _d[3*(1-from)+2];
          int margin = m * _sr;
          int iLayer = i - m * _sr;
          if (iLayer < _diamonds[di][dj].iEnd - _nHalo + _sr + margin) {
            if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
            int ij = iLayer * is + (_diamonds[di][dj].jBegin + margin) * js;
            int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (_diamonds[di][dj].jBegin+margin-_nHalo) * jsb;
            for (int j=max(_diamonds[di][dj].jBegin+margin, jDHLow); j<min(_diamonds[di][dj].jEnd-margin, jDHHigh); ++j) {
#ifndef SINGLE_NODE
              // unpack
              if (m == 0 && _ptDd->need_comm(2) && _bypassKHalo) {
                for (int k=0; k<_nHalo; ++k) {
                  u0[ij+k] = _hs.rBuf[kmbs+ijb+k];
                  v0[ij+k] = _hs.rBuf[kmbs+vsb+ijb+k];
                  w0[ij+k] = _hs.rBuf[kmbs+2*vsb+ijb+k];
                }
              }
              if (m == 0 && _ptDd->need_comm(5) && _bypassKHalo) {
                for (int k=0; k<_nHalo; ++k) {
                  u0[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+ijb+k];
                  v0[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+vsb+ijb+k];
                  w0[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+2*vsb+ijb+k];
                }
              }
#endif
              #pragma omp simd
              for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                int ijk = ij + k;
                double uvipjp = (u0[ijk] + u0[ijk+js]) * (v0[ijk] + v0[ijk+is]);
                double wuipkp = (w0[ijk] + w0[ijk+is]) * (u0[ijk] + u0[ijk+1]);
                double vwjpkp = (v0[ijk] + v0[ijk+1] ) * (w0[ijk] + w0[ijk+js]);
                //
                u[ijk] = ( ( 2.0*(u0[ijk+is] + u0[ijk-is]) + u0[ijk+js] + u0[ijk-js] + u0[ijk+1] + u0[ijk-1] - 8.0*u0[ijk]
                           + v0[ijk+is] - v0[ijk+is-js] - v0[ijk] + v0[ijk-js]
                           + w0[ijk+is] - w0[ijk+is-1] - w0[ijk] + w0[ijk-1]) *nuqh2
                         - ( (u0[ijk] + u0[ijk+is]) * (u0[ijk] + u0[ijk+is]) - (u0[ijk] + u0[ijk-is]) * (u0[ijk] + u0[ijk-is])
                           + uvipjp - (u0[ijk-js] + u0[ijk]) * (v0[ijk-js] + v0[ijk+is-js])
                           + wuipkp - (w0[ijk-1]  + w0[ijk+is-1] ) * (u0[ijk] + u0[ijk-1])) * q4h
                         ) * _dt + u0[ijk];
                //
                v[ijk] = ( ( v0[ijk+is] + v0[ijk-is] + 2.0*(v0[ijk+js] + v0[ijk-js]) + v0[ijk+1] + v0[ijk-1] - 8.0*v0[ijk]
                           + u0[ijk+js] - u0[ijk-is+js] - u0[ijk] + u0[ijk-is]
                           + w0[ijk+js] - w0[ijk+js-1] - w0[ijk] + w0[ijk-1]) * nuqh2
                         - ( uvipjp - (u0[ijk-is] + u0[ijk-is+js]) * (v0[ijk-is] + v0[ijk])
                           + (v0[ijk] + v0[ijk+js]) * (v0[ijk] + v0[ijk+js]) - (v0[ijk] + v0[ijk-js]) * (v0[ijk] + v0[ijk-js])
                           + vwjpkp - (v0[ijk-1] + v0[ijk]) * (w0[ijk-1] + w0[ijk+js-1])) * q4h
                         ) * _dt + v0[ijk];
                //
                w[ijk] = ( ( w0[ijk+is] + w0[ijk-is] + w0[ijk+js] + w0[ijk-js] + 2*(w0[ijk+1] + w0[ijk-1]) - 8.0*w0[ijk]
                           + u0[ijk+1] - u0[ijk] - u0[ijk-is+1] + u0[ijk-is]
                           + v0[ijk+1] - v0[ijk] - v0[ijk-js+1] + v0[ijk-js]) * nuqh2
                         - ( wuipkp - (w0[ijk-is] + w0[ijk]) * (u0[ijk-is] + u0[ijk-is+1])
                           + vwjpkp - (v0[ijk-js] + v0[ijk-js+1]) * (w0[ijk-js] + w0[ijk])
                           + (w0[ijk] + w0[ijk+1]) * (w0[ijk] + w0[ijk+1]) - (w0[ijk] + w0[ijk-1]) * (w0[ijk] + w0[ijk-1])) * q4h
                         ) * _dt + w0[ijk];
              }// end for k
#ifndef SINGLE_NODE
              // pack
              if (m == nf-1 && _ptDd->need_comm(2) && _bypassKHalo) {
                for (int k=0; k<_nHalo; ++k) {
                  _hs.sBuf[kmbs+ijb+k]       = u[ij+_nHalo+k];
                  _hs.sBuf[kmbs+vsb+ijb+k]   = v[ij+_nHalo+k];
                  _hs.sBuf[kmbs+2*vsb+ijb+k] = w[ij+_nHalo+k];
                }
              }
              if (m == nf-1 && _ptDd->need_comm(5) && _bypassKHalo) {
                for (int k=0; k<_nHalo; ++k) {
                  _hs.sBuf[kpbs+ijb+k]       = u[ij+szs[2]+k];
                  _hs.sBuf[kpbs+vsb+ijb+k]   = v[ij+szs[2]+k];
                  _hs.sBuf[kpbs+2*vsb+ijb+k] = w[ij+szs[2]+k];
                }
              }
#endif
              //
              ij += js;
              ijb += jsb;
            }// end for j
          }// end if
        }// end for m
#ifndef SINGLE_NODE
        if (tid == 0 && ptHt)  ptHt->poll(); 
#endif
      }// end for i
    break;

    case Center:
      jBegin = _diamonds[di][dj].jBegin + _nHalo - _sr;
      jEnd   = _diamonds[di][dj].jEnd   - _nHalo + _sr;
      for (int i=_diamonds[di][dj].iBegin+_nHalo-_sr; i<_diamonds[di][dj].iEnd+_nHalo-_sr; ++i) {
        for (int m=0; m<nf; ++m) {
          int from = (dir + m) % 2;
          u0 = _d[3*from];      v0 = _d[3*from+1];      w0 = _d[3*from+2];
          u  = _d[3*(1-from)];  v  = _d[3*(1-from)+1];  w  = _d[3*(1-from)+2];
          int margin = m * _sr;
          int iLayer = i - m * _sr;
          if (iLayer < _diamonds[di][dj].iEnd - _nHalo + _sr + margin) {
            if (_dhOff && (iLayer < iDHLow || iLayer >= iDHHigh))  continue;
            int ij = iLayer * is + (jBegin - margin) * js;
            int ijb = (iLayer - _nHalo - pipe*pipeLen)*isb + (jBegin-margin-_nHalo) * jsb;
            for (int j=max(jBegin-margin,jDHLow); j<min(jEnd+margin,jDHHigh); ++j) {
#ifndef SINGLE_NODE
              // unpack
              if (m == 0 && _ptDd->need_comm(2) && _bypassKHalo) {
                for (int k=0; k<_nHalo; ++k) {
                  u0[ij+k] = _hs.rBuf[kmbs+ijb+k];
                  v0[ij+k] = _hs.rBuf[kmbs+vsb+ijb+k];
                  w0[ij+k] = _hs.rBuf[kmbs+2*vsb+ijb+k];
                }
              }
              if (m == 0 && _ptDd->need_comm(5) && _bypassKHalo) {
                for (int k=0; k<_nHalo; ++k) {
                  u0[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+ijb+k];
                  v0[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+vsb+ijb+k];
                  w0[ij+_nHalo+szs[2]+k] = _hs.rBuf[kpbs+2*vsb+ijb+k];
                }
              }
#endif
              #pragma omp simd
              for (int k=max(kBegin+margin,kDHLow); k<min(kEnd-margin,kDHHigh); ++k) {
                int ijk = ij + k;
                double uvipjp = (u0[ijk] + u0[ijk+js]) * (v0[ijk] + v0[ijk+is]);
                double wuipkp = (w0[ijk] + w0[ijk+is]) * (u0[ijk] + u0[ijk+1]);
                double vwjpkp = (v0[ijk] + v0[ijk+1] ) * (w0[ijk] + w0[ijk+js]);
                //
                u[ijk] = ( ( 2.0*(u0[ijk+is] + u0[ijk-is]) + u0[ijk+js] + u0[ijk-js] + u0[ijk+1] + u0[ijk-1] - 8.0*u0[ijk]
                           + v0[ijk+is] - v0[ijk+is-js] - v0[ijk] + v0[ijk-js]
                           + w0[ijk+is] - w0[ijk+is-1] - w0[ijk] + w0[ijk-1]) *nuqh2
                         - ( (u0[ijk] + u0[ijk+is]) * (u0[ijk] + u0[ijk+is]) - (u0[ijk] + u0[ijk-is]) * (u0[ijk] + u0[ijk-is])
                           + uvipjp - (u0[ijk-js] + u0[ijk]) * (v0[ijk-js] + v0[ijk+is-js])
                           + wuipkp - (w0[ijk-1]  + w0[ijk+is-1] ) * (u0[ijk] + u0[ijk-1])) * q4h
                         ) * _dt + u0[ijk];
                //
                v[ijk] = ( ( v0[ijk+is] + v0[ijk-is] + 2.0*(v0[ijk+js] + v0[ijk-js]) + v0[ijk+1] + v0[ijk-1] - 8.0*v0[ijk]
                           + u0[ijk+js] - u0[ijk-is+js] - u0[ijk] + u0[ijk-is]
                           + w0[ijk+js] - w0[ijk+js-1] - w0[ijk] + w0[ijk-1]) * nuqh2
                         - ( uvipjp - (u0[ijk-is] + u0[ijk-is+js]) * (v0[ijk-is] + v0[ijk])
                           + (v0[ijk] + v0[ijk+js]) * (v0[ijk] + v0[ijk+js]) - (v0[ijk] + v0[ijk-js]) * (v0[ijk] + v0[ijk-js])
                           + vwjpkp - (v0[ijk-1] + v0[ijk]) * (w0[ijk-1] + w0[ijk+js-1])) * q4h
                         ) * _dt + v0[ijk];
                //
                w[ijk] = ( ( w0[ijk+is] + w0[ijk-is] + w0[ijk+js] + w0[ijk-js] + 2*(w0[ijk+1] + w0[ijk-1]) - 8.0*w0[ijk]
                           + u0[ijk+1] - u0[ijk] - u0[ijk-is+1] + u0[ijk-is]
                           + v0[ijk+1] - v0[ijk] - v0[ijk-js+1] + v0[ijk-js]) * nuqh2
                         - ( wuipkp - (w0[ijk-is] + w0[ijk]) * (u0[ijk-is] + u0[ijk-is+1])
                           + vwjpkp - (v0[ijk-js] + v0[ijk-js+1]) * (w0[ijk-js] + w0[ijk])
                           + (w0[ijk] + w0[ijk+1]) * (w0[ijk] + w0[ijk+1]) - (w0[ijk] + w0[ijk-1]) * (w0[ijk] + w0[ijk-1])) * q4h
                         ) * _dt + w0[ijk];
              }// end for k
#ifndef SINGLE_NODE
              // pack
              if (m == nf-1 && _ptDd->need_comm(2) && _bypassKHalo) {
                for (int k=0; k<_nHalo; ++k) {
                  _hs.sBuf[kmbs+ijb+k]       = u[ij+_nHalo+k];
                  _hs.sBuf[kmbs+vsb+ijb+k]   = v[ij+_nHalo+k];
                  _hs.sBuf[kmbs+2*vsb+ijb+k] = w[ij+_nHalo+k];
                }
              }
              if (m == nf-1 && _ptDd->need_comm(5) && _bypassKHalo) {
                for (int k=0; k<_nHalo; ++k) {
                  _hs.sBuf[kpbs+ijb+k]       = u[ij+szs[2]+k];
                  _hs.sBuf[kpbs+vsb+ijb+k]   = v[ij+szs[2]+k];
                  _hs.sBuf[kpbs+2*vsb+ijb+k] = w[ij+szs[2]+k];
                }
              }
#endif
              //
              ij += js;
              ijb += jsb;
            }// end for j
          }// end if
        }// end for m
#ifndef SINGLE_NODE
        if (tid == 0 && ptHt)  ptHt->poll(); 
#endif
      }// end for i
    break;
  }
  
  // reset pipelen's range
  _diamonds[di][dj].iBegin -= pipe * pipeLen;
  _diamonds[di][dj].iEnd   -= pipe * pipeLen;
}


void BurgersSolver::_nullify_layer(double *c, int basel, int jb, int je, int jsl, 
                                   int kb, int ke, int vsl)
{
  for (int j=jb; j<je; ++j) {
    #pragma omp simd
    for (int k=kb; k<ke; ++k) {
      c[basel+k] = 0.0;
      c[basel+vsl+k] = 0.0;
      c[basel+2*vsl+k] = 0.0;
    }
    basel += jsl;
  }

}
