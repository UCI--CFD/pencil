#include "sys.h"
#include "constants.h"
#include "DomainDecomp.h"
#include "HaloTransfer.h"

#include <iostream>
#include <algorithm>
#include <mpi.h>
#include <omp.h>

using namespace std;

int main(int argc, char* argv[])
{
  sys_init(argc, argv, MPI_THREAD_FUNNELED);

  int  n[3] = {64,64,64}, np[3] = {2, 2, 2}, nThrd = 4, nHalo = 2;
  bool isPrds[3] = {1, 1, 1}, isSet;

  get_option("nhalo", CmdOption::Int, 1, &nHalo, &isSet); 

  // create decomp
  DomainDecomp dd(n, np, isPrds, nThrd, MPI_COMM_WORLD, Neighbor::star);
  dd.set_from_option();
  dd.setup();
  // udpdate decomp info, maybe changed by cmd option
  nThrd = dd.num_thread();

  // local domain size, stride
  int szs[3], iStrd, jStrd;
  dd.get_local_size(szs);
  jStrd = szs[2] + 2*nHalo;
  iStrd = jStrd * (szs[1] + 2*nHalo);

  // halo stroe
  HaloStore hs(dd, nHalo, 1);
  hs.set_from_option();
  if (dd.is_neighbor_star())  
    hs.setup_star();
  else
    hs.setup_box();
  hs.debug_view();

  // create ha
  double **d = new double* [hs.nVar];
  for (int i=0; i<hs.nVar; ++i) 
    d[i] = new double [(szs[0]+2*nHalo)*(szs[1]+2*nHalo)*(szs[2]+2*nHalo)];

  //// init ha
  //int jStrd = szs[2] + 2*nHalo;
  //int iStrd = jStrd * (szs[1] + 2*nHalo);
  //for (int v=0; v<hs.nVar; ++v)
  //for (int i=nHalo; i<szs[0]+nHalo; ++i)
    //for (int j=nHalo; j<szs[1]+nHalo; ++j)
      //for (int k=nHalo; k<szs[2]+nHalo; ++k)
        //d[v][k+j*jStrd+i*iStrd] = 1.0 + v + dd.rank();

  // halo transfer facility
  HaloTransfer ht(dd, nHalo);
  ht.set_from_option();
  if (dd.is_neighbor_star())
    ht.set_halo_range(hs, 0, 6, nThrd);
  else
    ht.set_halo_range(hs, 0, 26, nThrd);
  ht.debug_view();

  // vs marks the variable used in comm, in current test all are used
  vector<int> vs;
  for (int i=0; i<hs.nVar; ++i) vs.push_back(i);

  // test
  #pragma omp parallel num_threads(nThrd)
  {
    // init array, numa
    int tid = omp_get_thread_num(), rngs[6], tRngs[6];
    for (int i=0; i<3; ++i) {
      rngs[i+3] = szs[i] + nHalo;
      rngs[i]   = nHalo;
    }
    compute_thread_range(rngs, nThrd, tid, tRngs);
    for (int v=0; v<hs.nVar; ++v)
      for (int i=tRngs[0]; i<tRngs[3]; ++i)
        for (int j=tRngs[1]; j<tRngs[4]; ++j)
          for (int k=tRngs[2]; k<tRngs[5]; ++k)
            d[v][k+j*jStrd+i*iStrd] = 1.0 + v + dd.rank();
    // init comm buffer, numa
    ht.init_buffer_numa(hs);
    #pragma omp barrier
    ht.pack_halo(d, vs, hs);
    #pragma omp barrier
    if (tid == nThrd-1) ht.update_halo(hs);
    #pragma omp barrier
    ht.unpack_halo(d, vs, hs);
    #pragma omp barrier
  }// end parallel

  // check face values
  double err = 0.0;
  for (unsigned int c=0; c<hs.chunks.size(); c+=6)
    for (int v=0; v<hs.nVar; ++v)
      for (int i=hs.chunks[c]; i<hs.chunks[c+3]; ++i)
        for (int j=hs.chunks[c+1]; j<hs.chunks[c+4]; ++j)
          for (int k=hs.chunks[c+2]; k<hs.chunks[c+5]; ++k) {
            err += fabs(hs.toRanks[c/6]+1.0 + v - d[v][i*iStrd+j*jStrd+k]);
            //if (dd.is_root() && fabs(hs.toRanks[c/6]+1.0 + v - d[v][i*iStrd+j*jStrd+k])>0.0)
            //std::cout << i << " " << j << " " << k << " "  << v << " "
            //<< hs.toRanks[c/6]+1.0 + v << " " << d[v][i*iStrd+j*jStrd+k] << "\n";
          }

  MPI_Allreduce(MPI_IN_PLACE, &err, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  if (dd.is_root())  std::cout << "Halo Error: " << err << std::endl;

  for (int i=0; i<hs.nVar; ++i) delete [] d[i];
  delete [] d;

  sys_finalize();

  return 0;
}
