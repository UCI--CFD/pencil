#include "sys.h"
#include "constants.h"
#include "DomainDecomp.h"

#include <iostream>
#include <mpi.h>

int main(int argc, char* argv[])
{
  sys_init(argc, argv, MPI_THREAD_FUNNELED);

  int  n[3] = {64,64,64}, np[3] = {2, 2, 2}, nThrd = 4;
  bool isPrds[3] = {1, 1, 1};

  DomainDecomp dd(n, np, isPrds, nThrd, MPI_COMM_WORLD, Neighbor::star);
  dd.set_from_option();
  dd.setup();
  //if (dd.is_root()) dd.fwrite_mapfile();
  dd.fread_mapfile();
  dd.debug_view();
  //int rngs[6]; dd.get_info(DecompOpt::lRanges, rngs);
  //int szs[3]; dd.get_info(DecompOpt::lSizes, szs);
  //if (dd.is_root()) {
    //for (int i=0; i<6; ++i) std::cout << rngs[i] << std::endl;
    //for (int i=0; i<3; ++i) std::cout << szs[i] << std::endl;
  //}
  
  //if (dd.is_root()) view_cmd_line_arg();

  sys_finalize();

  return 0;
};
