#include "sys.h"
#include "Timer.h"
#include <iostream>
#include <mpi.h>
#include <omp.h>
#include <unistd.h>
#include <numeric>
#include <algorithm>


using namespace std;

void compute_avg_sd(double *t, int len, double *avg, double *sd)
{
  *avg = 0.0;
  *sd  = 0.0;
  *avg = accumulate(t, t+len, 0.0) / len;
  for (int i=0; i<len; ++i) *sd += (t[i] - *avg) * (t[i] - *avg);
  *sd = sqrt(*sd/len);
}

const int nmax = 400;

int main(int argc, char* argv[])
{
  sys_init(argc, argv, MPI_THREAD_FUNNELED);

  double t0 = 0.0, t1 = 0.0;
  int    nms = 1, nRun0 = 200, nRun1 = 200;
  double tmpi[nmax], tomp[nmax], tmy[nmax];

  for (int i=0; i<nRun0; ++i) {
    t0 = MPI_Wtime();
    usleep(nms);
    t1 = MPI_Wtime();
    tmpi[i] = t1 - t0;

    t0 = omp_get_wtime();
    usleep(nms);
    t1 = omp_get_wtime();
    tomp[i] = t1 - t0;

    t0 = mytime();
    usleep(nms);
    t1 = mytime();
    tmy[i] = t1 - t0;
  }

  double avg, sd;
  compute_avg_sd(tmpi, nRun0, &avg, &sd);
  cout << "mpi_wtime time " << nms << " micro sec:" << endl;
  cout << avg << "\t" << sd << "\t" << *max_element(tmpi, tmpi+nRun0) << "\t" << *min_element(tmpi, tmpi+nRun0) << endl;

  compute_avg_sd(tomp, nRun0, &avg, &sd);
  cout << "omp_get_wtime " << nms << " micro sec:" << endl;
  cout << avg << "\t" << sd << "\t" << *max_element(tomp, tomp+nRun0) << "\t" << *min_element(tomp, tomp+nRun0) << endl;

  compute_avg_sd(tmy, nRun0, &avg, &sd);
  cout << "mytime time " << nms << " micro sec:" << endl;
  cout << avg << "\t" << sd << "\t" << *max_element(tmy, tmy+nRun0) << "\t" << *min_element(tmy, tmy+nRun0) << endl;


  // function ovehead
  for (int i=0; i<nRun1; ++i) {
    t0 = MPI_Wtime();
    t1 = MPI_Wtime();
    tmpi[i] = t1 - t0;
    t0 = omp_get_wtime();
    t1 = omp_get_wtime();
    tomp[i] = t1 - t0;
    t0 = mytime();
    t1 = mytime();
    tmy[i] = t1 - t0;
  }

  compute_avg_sd(tmpi, nRun1, &avg, &sd);
  cout << "mpi_wtime overhead" << endl;
  cout << avg << "\t" << sd << "\t" << *max_element(tmpi, tmpi+nRun1) << "\t" << *min_element(tmpi, tmpi+nRun1) << endl;

  compute_avg_sd(tomp, nRun1, &avg, &sd);
  cout << "omp_get_wtime overhead" << endl;
  cout << avg << "\t" << sd << "\t" << *max_element(tomp, tomp+nRun1) << "\t" << *min_element(tomp, tomp+nRun1) << endl;

  compute_avg_sd(tmy, nRun1, &avg, &sd);
  cout << "mytime time overhead" << endl;
  cout << avg << "\t" << sd << "\t" << *max_element(tmy, tmy+nRun1) << "\t" << *min_element(tmy, tmy+nRun1) << endl;

  sys_finalize();
}
