#include "sys.h" 
#include "constants.h"
#include "Timer.h"
#include "DomainDecomp.h"
#include "HaloTransfer.h"

#include <iostream>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <mpi.h>
#include <omp.h>

using namespace std;


int main(int argc, char* argv[])
{
  sys_init(argc, argv, MPI_THREAD_FUNNELED);

  int  n[3] = {64,64,64}, np[3] = {2, 2, 2}, nThrd = 4; 
  int  nRun = 20, nLayer = 10, nVar = 1, nPipe = 1;
  bool isPrds[3] = {1, 1, 1}, isSet;

  get_option("nlayer", CmdOption::Int, 1, &nLayer, &isSet); 
  get_option("nvar",   CmdOption::Int, 1, &nVar,   &isSet); 
  get_option("npipe",  CmdOption::Int, 1, &nPipe,  &isSet); 

  // create decomp
  DomainDecomp dd(n, np, isPrds, nThrd, MPI_COMM_WORLD, Neighbor::star);
  dd.set_from_option();
  dd.setup();
  // udpdate decomp info, maybe changed by cmd option
  nThrd = dd.num_thread();

  // local domain size, stride
  int szs[3], iStrd, jStrd;
  dd.get_local_size(szs);
  jStrd = szs[2] + 2*nLayer;
  iStrd = jStrd * (szs[1] + 2*nLayer);

  // halo stroe
  HaloStore *hss = new HaloStore [nLayer];
  for (int i=0; i<nLayer; ++i) {
    hss[i].init(dd, nPipe, i+1, nVar);
    if (dd.is_neighbor_star())  
      hss[i].setup_star();
    else
      hss[i].setup_box();
  }

  // create ha
  double **d = new double* [nVar];
  for (int i=0; i<nVar; ++i) 
    d[i] = new double [(szs[0]+2*nLayer)*(szs[1]+2*nLayer)*(szs[2]+2*nLayer)];
  for (int v=0; v<nVar; ++v)
    for (int i=nLayer; i<szs[0]+nLayer; ++i)
      for (int j=nLayer; j<szs[1]+nLayer; ++j)
        for (int k=nLayer; k<szs[2]+nLayer; ++k)
          d[v][k+j*jStrd+i*iStrd] = 1.0 + v + dd.rank();

  // halo transfer facility
  HaloTransfer **hts = new HaloTransfer* [nLayer];
  for (int i=0; i<nLayer; ++i)  hts[i] = new HaloTransfer[nPipe];
  for (int i=0; i<nLayer; ++i) {
    for (int j=0; j<nPipe; ++j)  hts[i][j].init(&dd, i+1);
    if (dd.is_neighbor_star())
      hts[i][0].set_halo_range(hss[i], 0, 6, nThrd);
    else {
      if (nPipe == 1) {
        hts[i][0].set_halo_range(hss[i], 0, 26, nThrd);
      }
      else {
        hts[i][0].set_halo_range(hss[i], 0, 17, nThrd);
        for (int j=1; j<nPipe-1; ++j) {
          hts[i][j].set_halo_range(hss[i], 17+8*(j-1), 17+8*j, nThrd);
        }
        hts[i][nPipe-1].set_halo_range(hss[i], 9+8*(nPipe-1), hss[i].chunks.size()/6, nThrd);
      }
    }// end if
  }

  // vs marks the variable used in comm, in current test all are used
  vector<int> vs;
  for (int i=0; i<nVar; ++i) vs.push_back(i);

  // timer
  double *t = new double [nLayer];
  double *tAvg = new double [nLayer], *tMax = new double [nLayer], *tMin = new double [nLayer];
  double *tSD = new double [nLayer];
  fill(t, t+nLayer, 0.0);

  // test
  for (int i=0; i<nLayer; ++i) {
    for (int j=0; j<nPipe; ++j)  hts[i][j].pack_halo(d, vs, hss[i]);
    //warm up 
    for (int j=0; j<5; ++j) {
      for (int j=0; j<nPipe; ++j) {
        MPI_Barrier(MPI_COMM_WORLD);
        hts[i][j].update_halo(hss[i]);
        busy_wait(0.1);
      }
    }
    MPI_Barrier(MPI_COMM_WORLD);
    // time test
    for (int j=0; j<nRun; ++j) {
      for (int j=0; j<nPipe; ++j) {
        MPI_Barrier(MPI_COMM_WORLD);
        t[i] -= mytime();
        hts[i][j].update_halo(hss[i]);
        t[i] += mytime();
        busy_wait(0.1);
      }
    }
    for (int j=0; j<nPipe; ++j)  hts[i][j].unpack_halo(d, vs, hss[i]);
  }

  // collect time
  MPI_Allreduce(t, tAvg, nLayer, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(t, tMax, nLayer, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  MPI_Allreduce(t, tMin, nLayer, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  for (int i=0; i<nLayer; ++i) {
    tAvg[i] = tAvg[i] / dd.num_proc();
    tSD[i]  = (t[i] - tAvg[i]) * (t[i] - tAvg[i]) / dd.num_proc();
  }
  MPI_Allreduce(MPI_IN_PLACE, tSD, nLayer, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  for (int i=0; i<nLayer; ++i) tSD[i] = sqrt(tSD[i]);
  

  if (dd.is_root()) {
    cout.precision(8);
    for (int i=0; i<nLayer; ++i)
      cout << i+1 << "," << tAvg[i] << "," << tMin[i] << "," << tMax[i] << "," << tSD[i] << endl;
    if (d[0][0] < 0) std::cout << d[0][0] << std::endl;
  }

  for (int i=0; i<nVar; ++i) delete [] d[i];
  delete [] d;
  delete [] t;
  delete [] tAvg;
  delete [] tMax;
  delete [] tMin;
  delete [] tSD;

  sys_finalize();

  return 0;
}
