import subprocess as subp
import re
import sys
import os
import numpy as np

def run_get_bw(fout, nt):
  out = subp.check_output("./stream", shell=True)
  outLines = re.split("\n", out.decode("utf-8"))
  for line in outLines:
    bwLine = re.match(re.compile("^Copy:.*"), line)
    if (bwLine): 
      items = bwLine.group().split()
      print("{} {} {}".format(nt, items[1], items[3]))
      fout.write("{} {} {}\n".format(nt, items[1], items[3]))


nCorePerSocket = np.int(sys.argv[1])
nSocket = np.int(sys.argv[2])
nThreads = [1]

for i in range(np.int(nCorePerSocket/2)):
  nThreads.append(2*(i+1))

fout = open("bw.txt", "w")

# test for cores in one socket
for nt in nThreads:
  os.environ["KMP_HW_SUBSET"] = "1s,{}c,1t".format(nt)
  os.environ["OMP_NUM_THREADS"] = repr(nt)
  run_get_bw(fout, nt)

# test among sockets
for ns in range(2, nSocket+1):
  nt = ns * nCorePerSocket
  os.environ["KMP_HW_SUBSET"] = "{}s,{}c,1t".format(ns, nCorePerSocket)
  os.environ["OMP_NUM_THREADS"] = repr(nt)
  run_get_bw(fout, nt)

fout.close()
