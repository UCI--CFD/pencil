add_executable(rdma.exe rdma.cpp)
target_link_libraries(rdma.exe solver)

add_executable(mpi_test_overlap.exe mpi_test_overlap.cpp)
target_link_libraries(mpi_test_overlap.exe solver)

add_executable(mpi_test_overlap_threads.exe mpi_test_overlap_threads.cpp)
target_link_libraries(mpi_test_overlap_threads.exe solver)

add_executable(halo_bw.exe halo_bw.cpp)
target_link_libraries(halo_bw.exe solver)
