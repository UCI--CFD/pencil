#include "sys.h"
#include "Timer.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <mpi.h>
#include <omp.h>

using namespace std;

void print_line_avg(double *a, int s)
{
  double avg = accumulate(a, a+s, 0.0) / s;
  cout << setprecision(8) << setw(10) << avg << " "
       << setprecision(3) << setw(5) 
       << (*max_element(a, a+s) - *min_element(a, a+s)) / avg << " ";
}

int main(int argc, char* argv[])
{
  sys_init(argc, argv, MPI_THREAD_FUNNELED);

  int  nr       = 64;
  int  nComp    = 4;
  int  arrSize  = 10000000;
  int  nc       = 4;
  int  bufSize  = 10000000;
  int  nt       = 8;
  int  withComp = 1;
  int  nDiv     = 1000000;
  bool isSet;

  get_option("comp",    CmdOption::Int, 1, &nComp, &isSet);
  get_option("poll",    CmdOption::Int, 1, &nc,    &isSet);
  get_option("nthread", CmdOption::Int, 1, &nt,    &isSet);
  get_option("withcomp",CmdOption::Int, 1, &withComp, &isSet);

  // comm buffer
  double *sBuf = new double [bufSize];
  double *rBuf = new double [bufSize];
  fill(sBuf, sBuf+bufSize, 1.4);
  fill(rBuf, rBuf+bufSize, 1.8);

  // comp arr
  double *a = new double [nt*arrSize];
  double *b = new double [nt*arrSize];
  double *c = new double [nt*arrSize];
  double w0 = 0.01, w1 = 0.02, w2 = 0.1;
  // init numa
  #pragma omp parallel num_threads(nt)
  {
    int tid = omp_get_thread_num();
    int is  = tid * arrSize;
    int ie  = is  + arrSize;
    for (int i=is; i<ie; ++i) {
      a[i] = 1.2;
      b[i] = 2.3;
      c[i] = 3.4;
    }
  }

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  cout.precision(8);

  // time isr alone
  double tComm = 0.0, tCommNbr;
  for (int r=0; r<nr; ++r) {
    MPI_Request reqs[2];
    MPI_Barrier(MPI_COMM_WORLD);
    tComm -= mytime();
    MPI_Irecv(rBuf, bufSize, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, &reqs[1]);
    MPI_Isend(sBuf, bufSize, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD, &reqs[0]);
    MPI_Waitall(2, reqs, MPI_STATUSES_IGNORE);
    tComm += mytime();
  }
  if (rank == 0) 
    MPI_Recv(&tCommNbr, 1, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  else
    MPI_Send(&tComm,    1, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD);
  // print tComm
  if (rank == 0)
    cout << "tcomm: " << tComm/nr << " " << tCommNbr/nr << endl;

  busy_wait(0.5);

  // time comp alone
  int    pad   = 8;
  double *tComp    = new double [nt*pad];
  MPI_Barrier(MPI_COMM_WORLD);
  if (withComp == 1) {
  #pragma omp parallel num_threads(nt)
  {
    int tid = omp_get_thread_num();
    tComp[tid*pad] = 0.0;
    // time comp
    for (int r=0; r<nr; ++r) {
      #pragma omp barrier
      tComp[tid*pad] -= mytime();
      for (int j=0; j<nComp; ++j) {
        int cSize = arrSize / nc;
        for (int chuck=0; chuck<nc; ++chuck) {
          int is = chuck * cSize + tid * arrSize;
          int ie = is + cSize;
          #pragma omp simd
          for (int i=is; i<ie; ++i) 
            a[i] = w0*b[i] + w1*c[i] + w2*a[i];
        }// for for chunk
      }// end for j
      tComp[tid*pad] += mytime();
    }// end for r
  }
  }
  else if (withComp == 2) {
  #pragma omp parallel num_threads(nt)
  {
    int    tid  = omp_get_thread_num();
    double x    = a[tid*pad];
    tComp[tid*pad] = 0.0;
    // time comp
    for (int r=0; r<nr; ++r) {
      #pragma omp barrier
      tComp[tid*pad] -= mytime();
      for (int j=0; j<nComp; ++j) {
        int cSize = nDiv / nc;
        for (int chuck=0; chuck<nc; ++chuck) {
          int is = chuck * cSize;
          int ie = is + cSize;
          for (int i=is; i<ie; ++i)
            x = 1.0 / (1.0 + x);
        }// for for chunk
      }// end for j
      tComp[tid*pad] += mytime();
    }// end for r
    a[tid*pad] = x;
  }
  }
  // collect comp time
  double tCompRng[2] = {0.0, 100.0}, tCompRngNbr[2];
  for (int i=0; i<nt*pad; i+=pad) {
    tCompRng[0] = max(tCompRng[0], tComp[i]);
    tCompRng[1] = min(tCompRng[1], tComp[i]);
  }
  if (rank == 0) 
    MPI_Recv(&tCompRngNbr, 2, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  else
    MPI_Send(&tCompRng,    2, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD);
  // print tComp
  if (rank == 0)
    cout << "tcomp: " << tCompRng[0]/nr << " " << tCompRng[1]/nr << " "
         << tCompRngNbr[0]/nr << " " << tCompRngNbr[1]/nr << " " << endl;

  busy_wait(0.5);

  // timer array for overlap
  double **tRP = new double* [nt+2], **tDC = new double* [nt+2];
  for (int i=0; i<nt+2; ++i) {
    tRP[i] = new double [nr];
    tDC[i] = new double [nr];
  }
  double **tRPNbr  = new double* [2], **tDCNbr = new double* [2];
  for (int i=0; i<2; ++i) {
    tRPNbr[i] = new double [nr];
    tDCNbr[i] = new double [nr];
  }

  // time overlap - repeated poll
  if (withComp == 1) {
  #pragma omp parallel num_threads(nt)
  {
    int  tid = omp_get_thread_num(), done;
    MPI_Request reqs[2];
    // time comp
    for (int r=0; r<nr; ++r) {
      #pragma omp master
        MPI_Barrier(MPI_COMM_WORLD);
      #pragma omp barrier
      tRP[tid][r] = mytime();
      if (tid == 0) {
        MPI_Irecv(rBuf, bufSize, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, &reqs[1]);
        MPI_Isend(sBuf, bufSize, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD, &reqs[0]);
      }
      for (int j=0; j<nComp; ++j) {
        int cSize = arrSize / nc;
        for (int chuck=0; chuck<nc; ++chuck) {
          int is = chuck * cSize + tid * arrSize;
          int ie = is + cSize;
          #pragma omp simd
          for (int i=is; i<ie; ++i) 
            a[i] = w0*b[i] + w1*c[i] + w2*a[i];
          // poll the background with mpi_test
          if (tid == 0) {
            MPI_Test(&reqs[0], &done, MPI_STATUS_IGNORE);
            MPI_Test(&reqs[1], &done, MPI_STATUS_IGNORE);
          }
        }// for for chunk
      }// end for j
      if (tid == 0) 
        MPI_Waitall(2, reqs, MPI_STATUSES_IGNORE);
      tRP[tid][r] = mytime() - tRP[tid][r];
    }// end for r
  }
  }// end if
  else if (withComp == 2) {
  #pragma omp parallel num_threads(nt)
  {
    int  tid = omp_get_thread_num(), done;
    MPI_Request reqs[2];
    double x = a[tid*pad];
    // time comp
    for (int r=0; r<nr; ++r) {
      #pragma omp master
        MPI_Barrier(MPI_COMM_WORLD);
      #pragma omp barrier
      tRP[tid][r] = mytime();
      if (tid == 0) {
        MPI_Irecv(rBuf, bufSize, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, &reqs[1]);
        MPI_Isend(sBuf, bufSize, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD, &reqs[0]);
      }
      for (int j=0; j<nComp; ++j) {
        int cSize = nDiv / nc;
        for (int chuck=0; chuck<nc; ++chuck) {
          int is = chuck * cSize; 
          int ie = is + cSize;
          for (int i=is; i<ie; ++i) 
            x = 1.0 / (1.0 + x);
          // poll the background with mpi_test
          if (tid == 0) {
            MPI_Test(&reqs[0], &done, MPI_STATUS_IGNORE);
            MPI_Test(&reqs[1], &done, MPI_STATUS_IGNORE);
          }
        }// for for chunk
      }// end for j
      if (tid == 0) 
        MPI_Waitall(2, reqs, MPI_STATUSES_IGNORE);
      tRP[tid][r] = mytime() - tRP[tid][r];
    }// end for r
    a[tid*pad] = x;
  }
  }

  // collect times - repeated poll
  for (int i=0; i<nr; ++i) {
    tRP[nt][i] = 0.0;
    tRP[nt+1][i] = 100.0;
    for (int j=0; j<nt; ++j) {
      tRP[nt][i]   = max(tRP[j][i], tRP[nt][i]);
      tRP[nt+1][i] = min(tRP[j][i], tRP[nt+1][i]);
    }
  }
  if (rank == 0) {
    MPI_Recv(tRPNbr[0], nr, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
    MPI_Recv(tRPNbr[1], nr, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
  }
  else {
    MPI_Send(tRP[nt],   nr, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD);
    MPI_Send(tRP[nt+1], nr, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD);
  }
  // print
  if (rank == 0) {
    cout << "root ";
    print_line_avg(tRP[nt],   nr);
    print_line_avg(tRP[nt+1], nr);
    cout << endl;
    cout << "nbr  ";
    print_line_avg(tRPNbr[0], nr);
    print_line_avg(tRPNbr[1], nr);
    cout << endl;
  }

  // time overlap - dedicated core
  #pragma omp parallel num_threads(nt)
  {
    int  tid = omp_get_thread_num();
    MPI_Request reqs[2];
    // time comp
    for (int r=0; r<nr; ++r) {
      #pragma omp master
        MPI_Barrier(MPI_COMM_WORLD);
      #pragma omp barrier
      tDC[tid][r] = mytime();
      if (tid == 0) {
        MPI_Irecv(rBuf, bufSize, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, &reqs[1]);
        MPI_Isend(sBuf, bufSize, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD, &reqs[0]);
        MPI_Waitall(2, reqs, MPI_STATUSES_IGNORE);
      }
      else {
        int cSize = nt*arrSize/(nt-1)/nc;
        for (int j=0; j<nComp; ++j) {
          for (int chuck=0; chuck<nc; ++chuck) {
            int is = chuck * cSize + (tid-1) * (nt*arrSize/(nt-1));
            int ie = min(is + cSize, nt*arrSize);
            #pragma omp simd
            for (int i=is; i<ie; ++i) 
              a[i] = w0*b[i] + w1*c[i] + w2*a[i];
          }// for for chunk
        }// end for j
      }// end else
      tDC[tid][r] = mytime() - tDC[tid][r];
    }// end for r
  }

  // trick compiler that array is useful
  if (rank == 0 && a[0] < 0) {
    cout << *max_element(sBuf, sBuf+bufSize) << endl;
    cout << *max_element(rBuf, rBuf+bufSize) << endl;
  }

  // collect times - dedicated core
  for (int i=0; i<nr; ++i) {
    tDC[nt][i] = 0.0;
    for (int j=0; j<nt; ++j)  tDC[nt][i] = max(tDC[j][i], tDC[nt][i]);
  }
  if (rank == 0) {
    MPI_Recv(tDCNbr[0], nr, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
    MPI_Recv(tDCNbr[1], nr, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
  }
  else {
    MPI_Send(tDC[0],  nr, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD);
    MPI_Send(tDC[nt], nr, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD);
  }
  // print
  if (rank == 0) {
    cout << "root ";
    print_line_avg(tDC[0],    nr);
    print_line_avg(tDC[nt],   nr);
    cout << endl;
    cout << "nbr  ";
    print_line_avg(tDCNbr[0], nr);
    print_line_avg(tDCNbr[1], nr);
    cout << endl;
  }

  // write to file
  if (rank == 0) {
    string fname = "nt_" + to_string(nt) + "_poll_" + to_string(nc) \
                 + "_comp_" + to_string(withComp) + "_" + to_string(nComp) + ".dat";
    ofstream fout(fname);
    for (int i=0; i<nr; ++i)
      fout << tRP[nt][i] << " " << tRP[nt+1][i]  << " " << tRPNbr[0][i] << " " << tRPNbr[1][i] << " "
           << tDC[0][i]  << " " << tDC[nt][i]    << " " << tDCNbr[0][i] << " " << tDCNbr[1][i] << endl;
  }

  delete [] a;
  delete [] b;
  delete [] c;
  delete [] sBuf;
  delete [] rBuf;
  for (int i=0; i<nt+2; ++i) delete tRP[i];
  delete [] tRP;
  for (int i=0; i<2; ++i)    delete tRPNbr[i];
  delete [] tRPNbr;
  for (int i=0; i<nt+2; ++i) delete tDC[i];
  delete [] tDC;
  for (int i=0; i<2; ++i)    delete tDCNbr[i];
  delete [] tDCNbr;

  sys_finalize();
  return 0;
}
