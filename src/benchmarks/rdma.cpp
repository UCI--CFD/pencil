#include "sys.h" 
#include "Timer.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <mpi.h>
#include <omp.h>

using namespace std;

const int nMsg = 20;

void pingpong(double *sBuf, double *rBuf, double *t, double *tNbr)
{
  int msgSize = 8, nr = 40, rank, reqs[2];
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  for (int m=0; m<nMsg; m++) {
    MPI_Barrier(MPI_COMM_WORLD);
    t[m] = mytime();
    for (int r=0; r<nr; ++r) {
      MPI_Irecv(rBuf, msgSize, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, &reqs[1]);
      MPI_Isend(sBuf, msgSize, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD, &reqs[0]);
      MPI_Waitall(2, reqs, MPI_STATUSES_IGNORE);
    }
    t[m] = (mytime() - t[m]) / nr;
    msgSize *= 2;
  }
  // collect nbr's time
  if (rank == 1)
    MPI_Send(t,    nMsg, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD);
  else 
    MPI_Recv(tNbr, nMsg, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
}


int main(int argc, char* argv[])
{
  sys_init(argc, argv, MPI_THREAD_FUNNELED);

  int  arrSize  = 10000000;
  int  nComp    = 8;
  int  nt       = 1;
  bool isSet;

  get_option("comp",     CmdOption::Int, 1, &nComp,    &isSet);
  get_option("nthread",  CmdOption::Int, 1, &nt,       &isSet);

  // comm buffer
  int bufSize = 8;
  for (int i=0; i<nMsg-1; ++i) bufSize *= 2;
  double *sBuf = new double [bufSize];
  double *rBuf = new double [bufSize];
  fill(sBuf, sBuf+bufSize, 1.4);
  fill(rBuf, rBuf+bufSize, 1.8);

  // comp arr
  double *a = new double [nt*arrSize+1];
  double *b = new double [nt*arrSize+1];
  double *c = new double [nt*arrSize+1];
  double w0 = 0.01, w1 = 0.02, w2 = 0.1;
  // init, numa
#pragma omp parallel num_threads(nt)
  {
    int tid = omp_get_thread_num();
    int is = tid * arrSize;
    int ie = is + arrSize;
    for (int i=is; i<ie; ++i) {
      a[i] = 1.2;
      b[i] = 2.3;
      c[i] = 3.4;
    }
  }

  // time pingpong, 1 proc per node
  double *tComm0    = new double [nMsg];
  double *tCommNbr0 = new double [nMsg];
  MPI_Barrier(MPI_COMM_WORLD);
  pingpong(sBuf, rBuf, tComm0, tCommNbr0);

  double *tComp    = new double [nt];
  double *tCompNbr = new double [nt];
  double *tComm    = new double [nMsg];
  double *tCommNbr = new double [nMsg];

  #pragma omp parallel num_threads(nt)
  {
    int tid = omp_get_thread_num();
    int is  = tid * arrSize;
    int ie  = is + arrSize;
    // pingpong, use 1 core, other cores busy
    if (tid == 0) {
      #pragma omp simd
      for (int i=is; i<ie; ++i) 
        a[i] = (w0*b[i] + w1*c[i] + w2*a[i]);
      MPI_Barrier(MPI_COMM_WORLD);
      pingpong(sBuf, rBuf, tComm, tCommNbr);
    }
    else {
      tComp[tid] = mytime();
      for (int j=0; j<nComp; ++j)
        #pragma vector nontemporal
        #pragma omp simd
        for (int i=is; i<ie; ++i) 
          a[i] = (w0*b[i] + w1*c[i] + w2*a[i]);
      tComp[tid] = mytime() - tComp[tid];
    }
  }// end omp parallel

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // trick compiler that array is useful
  if (rank == 0 && a[0] < 0.0) {
    cout << *max_element(sBuf, sBuf+10) << endl;
    cout << *max_element(rBuf, rBuf+10) << endl;
  }

  // collect time
  if (rank == 1)
    MPI_Send(tComp,    nt, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD);
  else
    MPI_Recv(tCompNbr, nt, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);

  // output
  if (rank == 0) {
    // print total comm time and comp time to see if comm is covered
    cout << accumulate(tComm, tComm+nMsg, 0.0) << " " 
      << *max_element(tComp+1, tComp+nt) << " "
      << *min_element(tComp+1, tComp+nt) << endl;
    cout << accumulate(tCommNbr, tCommNbr+nMsg, 0.0) << " " 
      << *max_element(tCompNbr+1, tCompNbr+nt) << " "
      << *min_element(tCompNbr+1, tCompNbr+nt) << endl;
    // write to file
    ofstream fout("pingpong.txt");
    int msgSize = 8;
    for (int m=0; m<nMsg; ++m) {
      fout << msgSize << " " << tComm0[m] << " " << tCommNbr0[m] << " "
        << tComm[m] << " " << tCommNbr[m] << endl;
      msgSize *= 2;
    }
  }

  delete [] a;      delete [] b;         delete [] c;
  delete [] sBuf;   delete [] rBuf;
  delete [] tComm0; delete [] tCommNbr0;
  delete [] tComm;  delete [] tCommNbr;
  delete [] tComp;  delete [] tCompNbr;

  sys_finalize();
  return 0;
}
