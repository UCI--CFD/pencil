#include "sys.h"
#include "Timer.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <mpi.h>
#include <omp.h>

using namespace std;


int main(int argc, char* argv[])
{
  sys_init(argc, argv, MPI_THREAD_FUNNELED);

  int  nr       = 64;
  int  nComp    = 4;
  int  arrSize  = 10000000;
  int  nc       = 4;
  int  bufSize  = 10000000;
  bool isSet;

  get_option("comp", CmdOption::Int, 1, &nComp, &isSet);
  get_option("poll", CmdOption::Int, 1, &nc,    &isSet);

  // comm buffer
  double *sBuf = new double [bufSize];
  double *rBuf = new double [bufSize];
  fill(sBuf, sBuf+bufSize, 1.4);
  fill(rBuf, rBuf+bufSize, 1.8);

  // comp arr
  double *a = new double [arrSize];
  double *b = new double [arrSize];
  double *c = new double [arrSize];
  fill(a, a+arrSize, 1.2);
  fill(b, b+arrSize, 2.3);
  fill(c, c+arrSize, 3.4);
  double w0 = 0.01, w1 = 0.02, w2 = 0.1;

  // time arr
  double *tNaive    = new double [nr];
  double *tPoll     = new double [nr];
  double *tNaiveNbr = new double [nr];
  double *tPollNbr  = new double [nr];


  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  cout.precision(8);

  // time isr alone
  double tComm = 0.0, tCommNbr;
  for (int r=0; r<nr; ++r) {
    int reqs[2];
    MPI_Barrier(MPI_COMM_WORLD);
    tComm -= mytime();
    MPI_Irecv(rBuf, bufSize, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, &reqs[1]);
    MPI_Isend(sBuf, bufSize, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD, &reqs[0]);
    MPI_Waitall(2, reqs, MPI_STATUSES_IGNORE);
    tComm += mytime();
  }
  if (rank == 0) 
    MPI_Recv(&tCommNbr, 1, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  else
    MPI_Send(&tComm,    1, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD);
  // print tComm
  if (rank == 0)
    cout << "tcomm: " << tComm/nr << " " << tCommNbr/nr << endl;

  busy_wait(0.5);

  // time comp alone
  MPI_Barrier(MPI_COMM_WORLD);
  double tCompNbr, tComp = mytime();
  for (int r=0; r<nr; ++r) {
    for (int j=0; j<nComp; ++j) {
      int cSize = arrSize / nc;
      for (int chuck=0; chuck<nc; ++chuck) {
        int is = chuck * cSize;
        int ie = is + cSize;
        #pragma omp simd
        for (int i=is; i<ie; ++i) 
          a[i] = w0*b[i] + w1*c[i] + w2*a[i];
      }// for for chunk
    }// end for j
  }// end for r
  tComp = mytime() - tComp;
  if (rank == 0) 
    MPI_Recv(&tCompNbr, 1, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  else
    MPI_Send(&tComp,    1, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD);
  // print tComp
  if (rank == 0)
    cout << "tcomp: " << tComp/nr << " " << tCompNbr/nr << endl;

  // time the naive overlap
  for (int r=0; r<nr; ++r) {
    int reqs[2];
    MPI_Barrier(MPI_COMM_WORLD);
    tNaive[r] = mytime();
    MPI_Irecv(rBuf, bufSize, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, &reqs[1]);
    MPI_Isend(sBuf, bufSize, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD, &reqs[0]);
    for (int j=0; j<nComp; ++j) {
      int cSize = arrSize / nc;
      for (int chuck=0; chuck<nc; ++chuck) {
        int is = chuck * cSize;
        int ie = is + cSize;
        #pragma omp simd
        for (int i=is; i<ie; ++i) 
          a[i] = w0*b[i] + w1*c[i] + w2*a[i];
      }// end for chunk
    }// end for j
    MPI_Waitall(2, reqs, MPI_STATUSES_IGNORE);
    tNaive[r] = mytime() - tNaive[r];
  }// end for r

  busy_wait(0.5);

  // time the mpi_test polling overlap
  for (int r=0; r<nr; ++r) {
    int  reqs[2], done = 0;
    MPI_Barrier(MPI_COMM_WORLD);
    tPoll[r] = mytime();
    MPI_Irecv(rBuf, bufSize, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, &reqs[1]);
    MPI_Isend(sBuf, bufSize, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD, &reqs[0]);
    for (int j=0; j<nComp; ++j) {
      int cSize = arrSize / nc;
      for (int chuck=0; chuck<nc; ++chuck) {
        int is = chuck * cSize;
        int ie = is + cSize;
        #pragma omp simd
        for (int i=is; i<ie; ++i) 
          a[i] = w0*b[i] + w1*c[i] + w2*a[i];
        // poll the background with mpi_test
        MPI_Test(&reqs[0], &done, MPI_STATUS_IGNORE);
        MPI_Test(&reqs[1], &done, MPI_STATUS_IGNORE);
      }// end for c
    }// end for j
    MPI_Waitall(2, reqs, MPI_STATUSES_IGNORE);
    tPoll[r] = mytime() - tPoll[r];
  }// end for r

  // trick compiler that array is useful
  if (rank == 0 && a[0] < 0) {
    cout << *max_element(sBuf, sBuf+bufSize) << endl;
    cout << *max_element(rBuf, rBuf+bufSize) << endl;
  }

  // collect times
  if (rank == 0)
    MPI_Recv(tNaiveNbr, nr, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  else
    MPI_Send(tNaive,    nr, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD);
  //
  if (rank == 0)
    MPI_Recv(tPollNbr, nr, MPI_DOUBLE, 1-rank, 1-rank, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
  else
    MPI_Send(tPoll,    nr, MPI_DOUBLE, 1-rank, rank,   MPI_COMM_WORLD);

  // root print and write times
  if (rank == 0) {
    // root print
    cout << "root ";
    double avg = accumulate(tNaive, tNaive+nr, 0.0)/nr;
    cout << avg << " "  
         << setprecision(3) << (*max_element(tNaive, tNaive+nr) - *min_element(tNaive, tNaive+nr)) / avg << " ";
    avg = accumulate(tPoll, tPoll+nr, 0.0)/nr;
    cout << avg << " "
         << setprecision(3) << (*max_element(tPoll, tPoll+nr) - *min_element(tPoll, tPoll+nr)) / avg << endl;
    // neighbor print
    cout << "nbr  ";
    avg = accumulate(tNaiveNbr, tNaiveNbr+nr, 0.0)/nr;
    cout << avg << " "
         << setprecision(3) << (*max_element(tNaiveNbr, tNaiveNbr+nr) - *min_element(tNaiveNbr, tNaiveNbr+nr)) / avg << " ";
    avg = accumulate(tPollNbr, tPollNbr+nr, 0.0)/nr;
    cout << avg << " "
         << setprecision(3) << (*max_element(tPollNbr, tPollNbr+nr) - *min_element(tPollNbr, tPollNbr+nr)) / avg << endl;
    // write to file
    string fname = "mpi_test_poll_" + to_string(nc) + "_comp_" + to_string(nComp) + ".dat";
    ofstream fout(fname);
    for (int i=0; i<nr; ++i)
      fout << tNaive[i]    << " " << tPoll[i]    << " "
           << tNaiveNbr[i] << " " << tPollNbr[i] << endl;
  }


  delete [] a;
  delete [] b;
  delete [] c;
  delete [] sBuf;
  delete [] rBuf;
  delete [] tNaive;
  delete [] tPoll;
  delete [] tNaiveNbr;
  delete [] tPollNbr;

  sys_finalize();
  return 0;
}
