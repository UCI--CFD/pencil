#include "sys.h"
#include "constants.h"
#include "DomainDecomp.h"
#include "Solver.h"
#include "WJSolver.h"
#include "BurgersSolver.h"
#include "WenoSolver.h"

#include <iostream>
#include <mpi.h>
#include <omp.h>

#ifdef PAPI
#include <papi.h>
#endif

#ifdef LIKWID_PERFMON
#include <likwid.h>
#include <likwid-marker.h>
#endif

int main(int argc, char* argv[])
{
  sys_init(argc, argv, MPI_THREAD_FUNNELED);

  int  n[3] = {64,64,64}, np[3] = {2, 2, 2}, nThrd = 4, nHalo = 2;
  bool isPrds[3] = {0, 0, 0};
  int  nIter = 16, nWarm = 32;
  bool isSet, isNuma = false;
  int  testType = 0, solType = 0;

  get_option("niter", CmdOption::Int,  1, &nIter,    &isSet);
  get_option("nwarm", CmdOption::Int,  1, &nWarm,    &isSet);
  get_option("test",  CmdOption::Int,  1, &testType, &isSet);
  get_option("nhalo", CmdOption::Int,  1, &nHalo,    &isSet);
  get_option("numa",  CmdOption::Bool, 1, &isNuma,   &isSet);
  get_option("sol",   CmdOption::Int,  1, &solType,  &isSet);
  
  std::cout.precision(8);

#ifdef PAPI
  int ierr;
  ierr = PAPI_library_init(PAPI_VER_CURRENT);
  if (ierr != PAPI_VER_CURRENT) {
    std::cout << "ERROR: init lib" << std::endl;
    exit(-1);
  }
  if (testType != 0) {
    ierr = PAPI_thread_init((unsigned long(*)(void))(omp_get_thread_num)); 
    test_fail(ierr, "thread init");
  }
#endif

  // decomp
  DomainDecomp dd(n, np, isPrds, nThrd, MPI_COMM_WORLD, Neighbor::star);
  dd.set_from_option();
  dd.setup();
  // udpdate decomp info, maybe changed by cmd option
  nThrd = dd.num_thread();
  dd.get_global_size(n);

  // mesh
  UniMesh mesh(dd);
  mesh.set_from_option();
//  mesh.view();

  // depend variable for trap tiling
  int **dependVars = new int* [100];
  for (int i=0; i<100; ++i) dependVars[i] = new int [100];

  // creat solver
  Solver* ptSol;
  if (solType == 0) // weighted jacobi
    ptSol = new WJSolver(dd, mesh, nHalo);
  else if (solType == 1) // weno
    ptSol = new WenoSolver(dd, mesh, nHalo);
  else if (solType == 2) // burgers
    ptSol = new BurgersSolver(dd, mesh, nHalo);

  // configure (and init) solver
  ptSol->set_from_option();
  ptSol->setup();
  if (!isNuma) ptSol->init();

  if (testType== 0) {
    ptSol->setup_parallel_mode(SolverMode::MPI_BSP);
    ptSol->set_warmup();
    ptSol->solve(nWarm, dependVars);
    ptSol->clear_history();
    MPI_Barrier(MPI_COMM_WORLD);
    ptSol->solve(nIter, dependVars);
    ptSol->analyze_time();
  }
  else {
#ifdef LIKWID_PERFMON
    LIKWID_MARKER_INIT;
#pragma omp parallel num_threads(nThrd)
{
    LIKWID_MARKER_THREADINIT;
}
    LIKWID_MARKER_REGISTER("Comp");
#endif
    ptSol->setup_parallel_mode(static_cast<SolverMode>(testType));
    #pragma omp parallel num_threads(nThrd)
    {
      if (isNuma) ptSol->init_numa();
      #pragma omp barrier
      #pragma omp single
        ptSol->set_warmup();
      ptSol->solve(nWarm, dependVars);
      ptSol->clear_history();
      #pragma omp single 
        MPI_Barrier(MPI_COMM_WORLD);
      ptSol->solve(nIter, dependVars);
    }
    ptSol->analyze_time();
#ifdef LIKWID_PERFMON
    LIKWID_MARKER_CLOSE;
#endif
  }
#ifdef PAPI
  ptSol->collect_view_flop();
#endif

  delete ptSol;

  sys_finalize();
  return 0;
}
