#include "sys.h"
#include "DomainDecomp.h"
#include "Solver.h"
#include "WJSolver.h"
#include "BurgersSolver.h"
#include "WenoSolver.h"

#include <iostream>
#include <cmath>
#include <mpi.h>
#include <omp.h>

int main(int argc, char* argv[])
{
  sys_init(argc, argv, MPI_THREAD_FUNNELED);

  int  n[3] = {64,64,64}, np[3] = {2, 2, 2}, nThrd = 4, nHalo = 2;
  bool isPrds[3] = {0, 0, 0};
  int  nIter = 32, solType = 0, testType = 1;
  bool isSet;

  get_option("niter", CmdOption::Int, 1, &nIter, &isSet);
  get_option("nhalo", CmdOption::Int, 1, &nHalo, &isSet);
  get_option("sol",   CmdOption::Int, 1, &solType,  &isSet);
  get_option("test",  CmdOption::Int, 1, &testType, &isSet);

  std::cout.precision(8);

  // decomp
  DomainDecomp dd(n, np, isPrds, nThrd, MPI_COMM_WORLD, Neighbor::star);
  dd.set_from_option();
  dd.setup();
  dd.get_global_size(n);
  // udpdate decomp info, maybe changed by cmd option
  nThrd = dd.num_thread();

  // mesh
  UniMesh mesh(dd);

  // creat solver
  Solver *ptSol0, *ptSol1;
  if (solType == 0) {// weighted jacobi
    ptSol0 = new WJSolver(dd, mesh, nHalo);
    ptSol1 = new WJSolver(dd, mesh, nHalo);
  }
  else if (solType == 1) {// weno
    ptSol0 = new WenoSolver(dd, mesh, nHalo);
    ptSol1 = new WenoSolver(dd, mesh, nHalo);
  }
  else if (solType == 2) {// burgers
    ptSol0 = new BurgersSolver(dd, mesh, nHalo);
    ptSol1 = new BurgersSolver(dd, mesh, nHalo);
  }

  // configure (and init) solver
  ptSol1->set_from_option();
  ptSol1->setup();
  ptSol1->init();
  ptSol1->setup_parallel_mode(static_cast<SolverMode>(testType));
  ptSol0->set_scheme(ptSol1->scheme());
  ptSol0->set_deephalo_off(ptSol1->is_deephalo_off());
  ptSol0->set_num_var(ptSol1->num_var());
  ptSol0->setup();
  ptSol0->init();
  ptSol0->setup_parallel_mode(SolverMode::Funneled);

  // save results
  int sizes[3];
  dd.get_local_size(sizes);
  double *p0 = new double [(sizes[0]+2*nHalo)*(sizes[1]+2*nHalo)*(sizes[2]+2*nHalo)];
  double *p1 = new double [(sizes[0]+2*nHalo)*(sizes[1]+2*nHalo)*(sizes[2]+2*nHalo)];

  // depend variable for trap tiling
  int **dependVars = new int* [100];
  for (int i=0; i<100; ++i) dependVars[i] = new int [100];

  // solver with hybrid
  #pragma omp parallel num_threads(nThrd)
    ptSol0->solve(nIter, dependVars);
  ptSol0->copy_var(0, p0);

  // solve with another
  #pragma omp parallel num_threads(nThrd)
    ptSol1->solve(nIter, dependVars);
  ptSol1->copy_var(0, p1);

  // compare the value from two models
  double errl0 = 0.0, errl1 = 0.0;
  int    jStrd = sizes[2] + 2*nHalo, iStrd = jStrd * (sizes[1]+2*nHalo);
  int    nerr = 0;
  for (int i=nHalo; i<sizes[0]+nHalo; ++i) {
    for (int j=nHalo; j<sizes[1]+nHalo; ++j) {
      for (int k=nHalo; k<sizes[2]+nHalo; ++k) {
        if (nerr > 10) break;
        errl0  = std::max(fabs(p0[i*iStrd+j*jStrd+k] - p1[i*iStrd+j*jStrd+k]), errl0);
        errl1 += fabs(p0[i*iStrd+j*jStrd+k] - p1[i*iStrd+j*jStrd+k]);
        if (fabs(p0[i*iStrd+j*jStrd+k] - p1[i*iStrd+j*jStrd+k]) > 1.0e-10) {
          if (dd.is_root())
            std::cout << i << " " << j << " " << k << " "
              << p0[i*iStrd+j*jStrd+k] << " " << p1[i*iStrd+j*jStrd+k] << std::endl;
          nerr ++;
        }
      }
    }
  }
  MPI_Allreduce(MPI_IN_PLACE, &nerr,  1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, &errl0, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, &errl1, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  if (dd.is_root()) {
    if (nerr > 0) 
      std::cout << "Fail, ";
    else
      std::cout << "Succeed, ";
    std::cout << "Error: " << errl0 << " " << errl1/n[0]/n[1]/n[2] << std::endl;
  }

  delete [] p0; 
  delete [] p1; 
  for (int i=0; i<100; ++i) delete [] dependVars[i];
  delete [] dependVars;

  sys_finalize();
  return 0;
}
