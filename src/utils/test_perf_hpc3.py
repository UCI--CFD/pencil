""" script to run performance test on hpc3
  The command line optiosn:
  -dd_size ni nj nk       - domain size
  -dd_proc pi pj pk       - proc decomposition
  -dd_nthreads ti tj tk   - thread decomposition
  -dd_nthread nt          - total number of threads
  -dd_pbc                 - periodic bc
  -dd_box                 - include edge and corner halos
  -dhoff                  - nullify deep halo, no redundant computation
  -split nj nk            - space tile dimension
  -sol                    - solver type, 0 WJ, 1 Weno/update, 2 Burgers
  -scheme                 - scheme type, for WJ 0 WJ7, 1 WJ13, 2 WJ27;
                            for Weno/upwind, 0 weno3, 1 upwind.
  -test                   - 0 flat mpi, 1 mpi funneled, 2 tiling, 3 pipeline
                            4 pipeline with poll
  -trapezoid ni nj        - # trapzoid in i, j           
  -nwarm                  - # warmups
  -niter                  - # iterations
  -numa                   - numa initialzation
"""
import subprocess as subp
import re
import sys
import os
import argparse

def run_get_time(testName, cmd, nRun):
  # data buffer
  avgs  = []
  for i in range(nRun):
    avgs.append([0.0, 0.0, 0.0, 0.0, 0.0]);
  # find run with min total time
  rmin = 0
  for r in range(nRun):
    # find the line of avg time in stdout
    out = subp.check_output(cmd)
    outLines = re.split("\n", out.decode("utf-8"))
    for line in outLines:
      avgLine = re.match(re.compile("^Avg.*"), line)
      if (avgLine):
        #print(avgLine.group())
        break;
    # save the  avg time
    avgStrs = avgLine.group().split()
    for i in range(1,6):
      avgs[r][i-1] = float(avgStrs[i])
    if r > 0 and avgs[rmin][4] > avgs[r][4]:
      rmin = r
  # print out min time among all runs
  print(testName + "," + ",".join([str(i) for i in avgs[rmin]]))

# parse cmd line options
parser = argparse.ArgumentParser('parse cmd line for performance test')
parser.add_argument('--dd_size', nargs=3, help='domain size')
parser.add_argument('--sol', nargs='?', default=0, help='solver type')
parser.add_argument('--scheme', nargs='?', default=0, help='numerical scheme')

args = parser.parse_args()

# domain size -dd_size
ddSize = ' '.join(args.dd_size)

# #halos and #fused in time
nh = 1
nTest = 9
nVar = 1
if args.sol == '0':
  if args.scheme == "0" or args.scheme == "2":
    nh = 1
    nTest = 11
  if args.scheme == "1":
    nh = 2
    nTest = 8
  if args.scheme == "3":
    nh = 2
    nTest = 2
elif args.sol == '1':
  nh = 2
  nTest = 6
elif args.sol == '2':
  nTest = 8
  nVar = 3

# number of runs 
nRun = 1

############ Flat MPI
for i in [1,2,4,8]:
  cmd = ["mpirun", "-n", "40", "-bind-to", "core", "./perf_solver.exe", \
         "-dd_size", ddSize,\
         "-dd_proc", "5 4 2", \
         "-dd_nthread", "1", \
         "-test", "0", \
         "-sol", args.sol,\
         "-scheme", args.scheme,\
         "-nhalo", repr(nh), \
         "-split", repr(i) + " 1",\
         "-hs_nvar", repr(nVar),\
         "-nwarm", "60", \
         "-niter", "60"]
  run_get_time("MPI_5_4_2_sp"+repr(i), cmd, nRun)

############# Flat MPI sync
#for i in [1,2,4,8]:
  #cmd = ["mpirun", "-n", "40", "-bind-to", "core", "./perf_solver_sync.exe", \
         #"-dd_size", ddSize,\
         #"-dd_proc", "5 4 2", \
         #"-dd_nthread", "1", \
         #"-test", "1", \
         #"-sol", args.sol,\
         #"-scheme", args.scheme,\
         #"-split", repr(i) + " 1",\
         #"-nhalo", repr(nh), \
         #"-hs_nvar", repr(nVar),\
         #"-nwarm", "60", \
         #"-niter", "60"]
  #run_get_time("MPI_5_4_2_sync_sp"+repr(i), cmd, nRun)

############ Flat MPI bypassk
for i in [1,2,4,8]:
  cmd = ["mpirun", "-n", "40", "-bind-to", "core", "./perf_solver.exe", \
         "-dd_size", ddSize,\
         "-dd_proc", "5 4 2", \
         "-dd_nthread", "1", \
         "-test", "0", \
         "-sol", args.sol,\
         "-scheme", args.scheme,\
         "-nhalo", repr(nh), \
         "-bypassk",\
         "-split", repr(i) + " 1",\
         "-hs_nvar", repr(nVar),\
         "-nwarm", "60", \
         "-niter", "60"]
  run_get_time("MPI_5_4_2_sp"+repr(i)+"_nok", cmd, nRun)

########## Flat MPI ovlp
#ddProc = "1 8 5"
#for i in range(2,nTest):
  #cmd = ["mpirun", "-n", "40", "-bind-to", "core", "./perf_solver.exe", \
         #"-dd_size", ddSize,\
         #"-dd_proc", ddProc, \
         #"-dd_nthread", "1", \
         #"-dd_box",\
         #"-sol", args.sol,\
         #"-scheme", args.scheme,\
         #"-nhalo", repr(i*nh), \
         #"-test", "3", \
         #"-hs_nvar", repr(nVar),\
         #"-bypassk",\
         #"-dhoff",\
         #"-nwarm", "60", \
         #"-niter", "60"]
  #run_get_time("MPI_"+ddProc.replace(' ', '_')+"_ovlp_dhoff_nok_nh"+repr(i*nh), cmd, nRun)

######### Flat MPI trap
#ddProc = "1 8 5"
#nTrap  = "1 5"
#for i in range(2,nTest):
  #cmd = ["mpirun", "-n", "40", "-bind-to", "core", "./perf_solver.exe", \
         #"-dd_size", ddSize,\
         #"-dd_proc", ddProc, \
         #"-dd_nthread", "1", \
         #"-dd_box",\
         #"-sol", args.sol,\
         #"-scheme", args.scheme,\
         #"-nhalo", repr(i*nh), \
         #"-test", "3", \
         #"-trapezoid", nTrap,\
         #"-bypassk",\
         #"-dhoff",\
         #"-hs_nvar", repr(nVar),\
         #"-nwarm", "60", \
         #"-niter", "60"]
  #testName = "MPI_" + ddProc.replace(' ','_') + '_trap_' \
           #+ nTrap.replace(' ','_') + '_dhoff_nok_nh' + repr(i*nh)
  #run_get_time(testName, cmd, nRun)

#########Funneled Space Tiling
os.environ["OMP_PLACES"] = "cores"
os.environ["OMP_PROC_BIND"] = "close"
for i in [1,48,120]:
  cmd = ["mpirun", "-n", "1", "./perf_solver.exe", \
         "-dd_size", ddSize,\
         "-dd_proc", "1 1 1", \
         "-dd_nthreads", "40 1 1", \
         "-numa",\
         "-sol", args.sol,\
         "-scheme", args.scheme,\
         "-nhalo", repr(nh), \
         "-split", repr(i) + " 1",\
         "-test", "1", \
         "-hs_nvar", repr(nVar),\
         "-nwarm", "60", \
         "-niter", "60"]
  run_get_time('omp_sp'+repr(i), cmd, nRun)

#########Overlap Tile
#os.environ["OMP_PLACES"] = "cores"
#os.environ["OMP_PROC_BIND"] = "close"
#for i in range(2,nTest):
  #cmd = ["mpirun", "-n", "1",  "./perf_solver.exe", \
         #"-dd_size", ddSize,\
         #"-dd_proc", "1 1 1", \
         #"-dd_nthreads", "1 40 1", \
         #"-dd_box",\
         #"-numa",\
         #"-sol", args.sol,\
         #"-scheme", args.scheme,\
         #"-nhalo", repr(i*nh), \
         #"-test", "3", \
         #"-hs_nvar", repr(nVar),\
         #"-nwarm", "60", \
         #"-niter", "60"]
  #run_get_time("omp_ovlp_nh"+repr(i*nh), cmd, nRun)

##########K2 ovlp
os.environ["OMP_PLACES"] = "cores"
os.environ["OMP_PROC_BIND"] = "close"
for i in range(2,nTest):
  cmd = ["mpirun", "-n", "2", "-bind-to", "socket",  "./perf_solver.exe", \
         "-dd_size", ddSize,\
         "-dd_proc", "1 1 2", \
         "-dd_nthreads", "1 20 1", \
         "-dd_box",\
         "-numa",\
         "-sol", args.sol,\
         "-scheme", args.scheme,\
         "-nhalo", repr(i*nh), \
         "-test", "2", \
         "-hs_nvar", repr(nVar),\
         "-dhoff",\
         "-nwarm", "60", \
         "-niter", "60"] 
  run_get_time("K2_OMP_ovlp_nh"+repr(i*nh),cmd, nRun)

###########Trap ovlp
#os.environ["OMP_PLACES"] = "cores"
#os.environ["OMP_PROC_BIND"] = "close"
#nTrap = "3 23"
#for i in range(2,nTest):
  #cmd = ["mpirun", "-n", "2", "-bind-to", "socket", "./perf_solver.exe", \
         #"-dd_size", ddSize,\
         #"-dd_proc", "1 2 1", \
         #"-dd_nthread", "20", \
         #"-dd_box",\
         #"-sol", args.sol,\
         #"-scheme", args.scheme,\
         #"-nhalo", repr(i*nh), \
         #"-trapezoid", nTrap,\
         #"-test", "3", \
         #"-hs_nvar", repr(nVar),\
         #"-nwarm", "60", \
         #"-niter", "60"]
  #run_get_time("OMP_trap_"+nTrap.replace(' ','_')+"_nh"+repr(i*nh), cmd, nRun)

##########K2 Trap
os.environ["OMP_PLACES"] = "cores"
os.environ["OMP_PROC_BIND"] = "close"
nTrap = "1 39"
for i in range(2,nTest):
  cmd = ["mpirun", "-n", "2", "-bind-to", "socket",  "./perf_solver.exe", \
         "-dd_size", ddSize,\
         "-dd_proc", "1 1 2", \
         "-dd_nthread", "20", \
         "-dd_box",\
         "-sol", args.sol,\
         "-scheme", args.scheme,\
         "-nhalo", repr(i*nh), \
         "-trapezoid", nTrap,\
         "-test", "2", \
         "-hs_nvar", repr(nVar),\
         "-dhoff",\
         "-nwarm", "60", \
         "-niter", "60"]
  run_get_time("K2_OMP_trap_"+nTrap.replace(' ','_')+"_nh"+repr(i*nh), cmd, nRun)
