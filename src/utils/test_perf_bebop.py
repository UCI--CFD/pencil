import subprocess as subp
import re
import sys
import os
import argparse

def run_get_time(testName, cmd, nRun):
  # data buffer
  avgs  = []
  for i in range(nRun):
    avgs.append([0.0, 0.0, 0.0, 0.0, 0.0]);
  # find run with min total time
  rmin = 0
  for r in range(nRun):
    # find the line of avg time in stdout
    out = subp.check_output(cmd)
    outLines = re.split("\n", out.decode("utf-8"))
    for line in outLines:
      avgLine = re.match(re.compile("^Avg.*"), line)
      if (avgLine):
        #print(avgLine.group())
        break;
    # save the  avg time
    avgStrs = avgLine.group().split()
    for i in range(1,6):
      avgs[r][i-1] = float(avgStrs[i])
    if r > 0 and avgs[rmin][4] > avgs[r][4]:
      rmin = r
  # print out min time among all runs
  print(testName + "," + ",".join([str(i) for i in avgs[rmin]]))

# parse cmd line options
parser = argparse.ArgumentParser('parse cmd line for performance test')
parser.add_argument('--dd_size', nargs=3, help='domain size')
parser.add_argument('--sol', nargs='?', default=0, help='solver type')
parser.add_argument('--scheme', nargs='?', default=0, help='numerical scheme')

args = parser.parse_args()

# domain size -dd_size
ddSize = ' '.join(args.dd_size)

# #halos and #fused in time
nh = 1
nTest = 9
nVar = 1
if args.sol == '0':
  if args.scheme == "0" or args.scheme == "2":
    nh = 1
    nTest = 11
  if args.scheme == "1":
    nh = 2
    nTest = 8
  if args.scheme == "3":
    nh = 2
    nTest = 2
elif args.sol == '1':
  nh = 2
  nTest = 6
elif args.sol == '2':
  nTest = 8
  nVar = 3

# number of runs 
nRun = 3

############ Flat MPI
for i in [1,2,4,8]:
  cmd = ["mpirun", "-n", "36", "./perf_solver.exe", \
         "-dd_size", ddSize,\
         "-dd_proc", "4 3 3", \
         "-dd_nthread", "1", \
         "-test", "0", \
         "-sol", args.sol,\
         "-scheme", args.scheme,\
         "-nhalo", repr(nh), \
         "-split", repr(i) + " 1",\
         "-hs_nvar", repr(nVar),\
         "-nwarm", "60", \
         "-niter", "60"]
  run_get_time("MPI_4_3_3_sp"+repr(i), cmd, nRun)

############ Flat MPI sync
#for i in [1,2,4,8]:
  #cmd = ["mpirun", "-n", "36", "./perf_solver_sync.exe", \
         #"-dd_size", ddSize,\
         #"-dd_proc", "4 3 3", \
         #"-dd_nthread", "1", \
         #"-test", "1", \
         #"-sol", args.sol,\
         #"-scheme", args.scheme,\
         #"-split", repr(i) + " 1",\
         #"-nhalo", repr(nh), \
         #"-hs_nvar", repr(nVar),\
         #"-nwarm", "60", \
         #"-niter", "60"]
  #run_get_time("MPI_4_3_3_sync_sp"+repr(i), cmd, nRun)

############ Flat MPI bypassk
for i in [1,2,4,8]:
  cmd = ["mpirun", "-n", "36", "./perf_solver.exe", \
         "-dd_size", ddSize,\
         "-dd_proc", "4 3 3", \
         "-dd_nthread", "1", \
         "-test", "0", \
         "-sol", args.sol,\
         "-scheme", args.scheme,\
         "-nhalo", repr(nh), \
         "-bypassk",\
         "-split", repr(i) + " 1",\
         "-hs_nvar", repr(nVar),\
         "-nwarm", "60", \
         "-niter", "60"]
  run_get_time("MPI_4_3_3_sp"+repr(i)+"_nok", cmd, nRun)

########## Flat MPI ovlp
#ddProc = " 1 6 6"
#for i in range(2,nTest):
  #cmd = ["mpirun", "-n", "36", "./perf_solver.exe", \
         #"-dd_size", ddSize,\
         #"-dd_proc", ddProc, \
         #"-dd_nthread", "1", \
         #"-dd_box",\
         #"-sol", args.sol,\
         #"-scheme", args.scheme,\
         #"-nhalo", repr(i*nh), \
         #"-test", "3", \
         #"-hs_nvar", repr(nVar),\
         #"-bypassk",\
         #"-nwarm", "60", \
         #"-niter", "60"]
  #run_get_time("MPI_"+ddProc.replace(' ', '_')+"_ovlp_nok_nh"+repr(i*nh), cmd, nRun)

######### Flat MPI trap
#ddProc = " 1 6 6"
#nTrap  = "1 5"
#for i in range(2,nTest):
  #cmd = ["mpirun", "-n", "36", "./perf_solver.exe", \
         #"-dd_size", ddSize,\
         #"-dd_proc", ddProc, \
         #"-dd_nthread", "1", \
         #"-dd_box",\
         #"-sol", args.sol,\
         #"-scheme", args.scheme,\
         #"-nhalo", repr(i*nh), \
         #"-test", "3", \
         #"-trapezoid", nTrap,\
         #"-bypassk",\
         #"-hs_nvar", repr(nVar),\
         #"-nwarm", "60", \
         #"-niter", "60"]
  #testName = "MPI_" + ddProc.replace(' ','_') + '_trap_' \
           #+ nTrap.replace(' ','_') + '_nok_nh' + repr(i*nh)
  #run_get_time(testName, cmd, nRun)

###########Funneled
#os.environ["KMP_HW_SUBSET"] = "2s,18c"
#os.environ["KMP_AFFINITY"] = "granularity=core,compact"
#cmd = ["mpirun", "-n", "1", "./perf_solver.exe", \
       #"-dd_size", ddSize,\
       #"-dd_proc", "1 1 1", \
       #"-dd_nthreads", "36 1 1", \
       #"-numa",\
       #"-sol", args.sol,\
       #"-scheme", args.scheme,\
       #"-nhalo", repr(nh), \
       #"-test", "1", \
       #"-hs_nvar", repr(nVar),\
       #"-nwarm", "60", \
       #"-niter", "60"]
#run_get_time("Omp_36_1_1", cmd, nRun)

##########Funneled Space Tiling
os.environ["KMP_HW_SUBSET"] = "2s,18c"
os.environ["KMP_AFFINITY"] = "granularity=core,compact"
for i in [1, 24, 48, 80]:
  cmd = ["mpirun", "-n", "1", "./perf_solver.exe", \
         "-dd_size", ddSize,\
         "-dd_proc", "1 1 1", \
         "-dd_nthreads", "36 1 1", \
         "-numa",\
         "-sol", args.sol,\
         "-scheme", args.scheme,\
         "-nhalo", repr(nh), \
         "-split", repr(i) + " 1",\
         "-test", "1", \
         "-hs_nvar", repr(nVar),\
         "-nwarm", "60", \
         "-niter", "60"]
  run_get_time('omp_sp'+repr(i), cmd, nRun)

###########Overlap Tile
#os.environ["KMP_HW_SUBSET"] = "2s,18c"
#os.environ["KMP_AFFINITY"] = "granularity=core,compact"
#for i in range(2,nTest):
  #cmd = ["mpirun", "-n", "1",  "./perf_solver.exe", \
         #"-dd_size", ddSize,\
         #"-dd_proc", "1 1 1", \
         #"-dd_nthreads", "1 36 1", \
         #"-dd_box",\
         #"-numa",\
         #"-sol", args.sol,\
         #"-scheme", args.scheme,\
         #"-nhalo", repr(i*nh), \
         #"-test", "3", \
         #"-hs_nvar", repr(nVar),\
         #"-nwarm", "60", \
         #"-niter", "60"]
  #run_get_time("omp_ovlp_nh"+repr(i*nh), cmd, nRun)

##########K2 ovlp
os.environ["KMP_HW_SUBSET"] = "1s,18c"
os.environ["KMP_AFFINITY"] = "granularity=core,compact"
for i in range(2,nTest):
  cmd = ["mpirun", "-env", "I_MPI_PIN_DOMAIN", "cache3", "-n", "2",  "./perf_solver.exe", \
         "-dd_size", ddSize,\
         "-dd_proc", "1 1 2", \
         "-dd_nthreads", "1 18 1", \
         "-dd_box",\
         "-numa",\
         "-sol", args.sol,\
         "-scheme", args.scheme,\
         "-nhalo", repr(i*nh), \
         "-test", "2", \
         "-hs_nvar", repr(nVar),\
         "-dhoff",\
         "-nwarm", "60", \
         "-niter", "60"]
  run_get_time("K2_OMP_ovlp_nh"+repr(i*nh),cmd, nRun)

###########Trap ovlp
#os.environ["KMP_HW_SUBSET"] = "1s,18c"
#os.environ["KMP_AFFINITY"] = "granularity=core,compact"
#nTrap = "3 23"
#for i in range(2,nTest):
  #cmd = ["mpirun", "-env", "I_MPI_PIN_DOMAIN", "cache3", "-n", "2",  "./perf_solver.exe", \
         #"-dd_size", ddSize,\
         #"-dd_proc", "1 2 1", \
         #"-dd_nthread", "20", \
         #"-dd_box",\
         #"-sol", args.sol,\
         #"-scheme", args.scheme,\
         #"-nhalo", repr(i*nh), \
         #"-trapezoid", nTrap,\
         #"-test", "3", \
         #"-hs_nvar", repr(nVar),\
         #"-nwarm", "60", \
         #"-niter", "60"]
  #run_get_time("OMP_trap_"+nTrap.replace(' ','_')+"_nh"+repr(i*nh), cmd, nRun)

##########K2 Trap
os.environ["KMP_HW_SUBSET"] = "1s,18c"
os.environ["KMP_AFFINITY"] = "granularity=core,compact"
nTrap = "1 35"
for i in range(2,nTest):
  cmd = ["mpirun", "-env", "I_MPI_PIN_DOMAIN", "cache3", "-n", "2",  "./perf_solver.exe", \
         "-dd_size", ddSize,\
         "-dd_proc", "1 1 2", \
         "-dd_nthread", "18", \
         "-dd_box",\
         "-sol", args.sol,\
         "-scheme", args.scheme,\
         "-nhalo", repr(i*nh), \
         "-trapezoid", nTrap,\
         "-test", "2", \
         "-hs_nvar", repr(nVar),\
         "-dhoff",\
         "-nwarm", "60", \
         "-niter", "60"]
  run_get_time("K2_OMP_trap_"+nTrap.replace(' ','_')+"_nh"+repr(i*nh), cmd, nRun)
