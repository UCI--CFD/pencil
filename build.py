import subprocess, os, shutil, sys

if __name__ == '__main__':
  # environment vars for compiler, etc
  os.environ['CC']  = 'mpicc'
  os.environ['CXX'] = 'mpicxx'

  # command for cmake, accept following format for papi and likwid
  # '-Dpapi_dir=/blues/gpfs/home/software/spack-0.10.1/opt/spack/linux-centos7-x86_64/intel-17.0.4/papi-5.7.0-dv3z4viqtu7aiawjioyq47o2vnwknj53/',
  # '-Dlikwid_dir=/home/hewang/opt/likwid/',
  options = [
    'cmake', '..',
  ]

  # build dir
  buildDir = 'build'
  if len(sys.argv) == 2: buildDir = sys.argv[1]
  buildDir = './' + buildDir + '/'
  if os.path.isdir(buildDir): shutil.rmtree(buildDir) # remove dir if existed
  os.mkdir(buildDir)

  subprocess.call(options, cwd=buildDir)
