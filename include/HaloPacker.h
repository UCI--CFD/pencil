#ifndef HALOPACKER_H
#define HALOPACKER_H

#include "DomainDecomp.h"
#include "HaloStore.h"
#include <vector>

using std::vector;

class HaloPacker
{
  public:
  HaloPacker(): _nHalo(1), _cacheLineLen(8), _ptDd(NULL), _cBegin(0), _cEnd(6) {}
  HaloPacker(const DomainDecomp& dd, const int nHalo);

  HaloPacker(const HaloPacker& rhs);

  const HaloPacker& operator=(const HaloPacker& rhs);

  void init(DomainDecomp* dd, int nHalo)
  { _ptDd = dd; _nHalo = nHalo; }

  void set_copy_length(int len) {_cacheLineLen = len;}

  void set_from_option();

  void set_bypass_khalo() {_bypassKHalo = true;}

  void get_local_range(int rngs[6]);

  void init_buffer_numa(HaloStore& hs);
    
  int num_halo() {return _nHalo;}

  int pack(double** d, vector<int>& vs, HaloStore& hs);

  int unpack(double** d, vector<int>& vs, HaloStore& hs);

  void debug_view();

  int set_halo_range(HaloStore& hs, int cBegin, int cEnd, int nThrd);

  private:
  int                 _nHalo;
  int                 _cacheLineLen;
  const DomainDecomp* _ptDd;
  vector<vector<int>> _haloRngs;    // thread's chunk range in halo
  vector<vector<int>> _bodyRngs;    // thread's chunk range in body
  int                 _cBegin, _cEnd;
  bool                _bypassKHalo;
};


#endif
