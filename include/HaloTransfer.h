#ifndef HALOTRANSFER_H
#define HALOTRANSFER_H

#include "DomainDecomp.h"
#include "HaloStore.h"
#include <vector>

using std::vector;

class HaloTransfer
{
  public:
  HaloTransfer();

  HaloTransfer(const DomainDecomp& dd, const int nHalo);

  HaloTransfer(const HaloTransfer& rhs);

  const HaloTransfer& operator=(const HaloTransfer& rhs);

  void init(DomainDecomp* dd, int nHalo)
  { _ptDd = dd; _nHalo = nHalo; }

  void set_from_option();

  void set_bypass_khalo() {_bypassKHalo = true;}

  void set_copy_length(int len) {_cacheLineLen = len;}

  void get_local_range(int rngs[6]);

  void init_buffer_numa(HaloStore& hs);
    
  int num_halo() {return _nHalo;}

  int pack_halo(double** d, vector<int>& vs, HaloStore& hs);

  int unpack_halo(double** d, vector<int>& vs, HaloStore& hs);

  int update_halo_begin(HaloStore& hs);

  int update_halo_end();

  int update_halo(HaloStore& hs);

  int poll();

  void debug_view();

  int set_halo_range(HaloStore& hs, int cBegin, int cEnd, int nThrd);

  int set_halo_range(HaloStore& hs, vector<int>& cids, int nThrd);

  int set_comm_chunk(HaloStore& hs, int cBegin, int cEnd, int nThrd);

#ifdef DEBUG
  void print_pack_time();
#endif

  private:
  int                 _nHalo;
  int                 _cacheLineLen;
  const DomainDecomp* _ptDd;
  vector<vector<int>> _haloRngs;    // thread's chunk range in halo
  vector<vector<int>> _bodyRngs;    // thread's chunk range in body
  vector<MPI_Request> _reqs;
  vector<int>         _flags;
  int                 _cBegin, _cEnd;
  bool                _bypassKHalo;
  vector<int>         _sendCids, _recvCids;
#ifdef DEBUG
  vector<vector<double>> _tPack, _tUnpack;
#endif
};


#endif
