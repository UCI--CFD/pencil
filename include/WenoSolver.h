#ifndef WenoSolver_H
#define WenoSolver_H

#include "Solver.h"

class WenoSolver: public Solver
{
  public:
  WenoSolver(DomainDecomp& dd, UniMesh& mesh, int nh);
  ~WenoSolver();
  void set_from_option();
  int setup();
  int init();
  int init_numa();

  private:
  void _compute_range(int rngs[6], bool isEven);
  void _compute_range_rectangle(int rngs[6], int dir, double *c, int is, int js,   
                                int pipe, HaloTransfer* ptHt);
  void _compute_pipe_diamond(int di, int dj, int pipe, int dir, HaloTransfer* ptHt);
  void _nullify_layer(double *c, int basel, int jb, int je, int jsl, int kb, int ke);

  private:
  double _dt;
};

inline double weno5(double vm3, double vm2, double vm1, double v, double v1, double v2, double h);

inline double weno3(double vm2, double vm1, double v, double v1, double h);

#endif
