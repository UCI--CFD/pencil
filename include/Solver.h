#ifndef SOLVER_H
#define SOLVER_H

#include "DomainDecomp.h"
#include "UniMesh.h"
#include "HaloStore.h"
#include "HaloTransfer.h"
#include "HaloPacker.h"
#include "Timer.h"
#include "Diamond.h"
#include <algorithm>

using std::vector;

enum SolverTime {Comp, Pack, Comm, Unpack, Total, Sync, Last};
enum SolverMode {MPI_BSP, Funneled, Tile, PipelineTile, PollTile};
enum UnpackStat {Old, Ready, Done};

const int TIMER_MAX_ITEM = 8;
const int TIMER_MAX_SIZE = 1024;

class Solver
{
  public:
  Solver(DomainDecomp& dd, UniMesh& mesh, int nh);
  virtual ~Solver();

  virtual void set_from_option();

  void set_scheme(int scheme) {_scheme = scheme;};

  void set_num_var(int nVar) {_hs.nVar = nVar;};

  virtual int setup();

  void set_warmup() {_isWarmUp = true;}

  void setup_parallel_mode(SolverMode mode) {_mode = mode;}

  int solve(int nIter, int**);

  int analyze_time();

  void clear_history();

  void copy_var(int i, double *var);

  virtual int init() =0;
  virtual int init_numa() =0;

  int scheme() {return _scheme;};

  int num_var() {return _hs.nVar;}

  bool is_deephalo_off()
  { return _dhOff; };

  void set_deephalo_off(bool dhOff)
  { _dhOff = dhOff; };

#ifdef PAPI
  void collect_view_flop();
#endif

  private:
  int _solve_mpi_bsp(int nIter);
  int _solve_funneled(int nIter);
  int _solve_diamond(int nIter, int**);
  int _solve_rectangle(int nIter);
  int _solve_pipeline_diamond(int nIter, int**);
  int _solve_pipeline_rectangle(int nIter);
  int _solve_poll_ovlp(int nIter);
  int _solve_poll_trap(int nIter, int**);

  virtual void _compute_range(int rngs[6], bool isEven) =0;
  virtual void _compute_range_rectangle(int rngs[6], int dir, double *c, int is, 
                                        int js, int pipe, HaloTransfer* ptHt) =0;
  virtual void _compute_pipe_diamond(int di, int dj, int pipe, int dir, HaloTransfer* ptHt) =0;

  int _analyze_time_mpi();
  int _analyze_time_hybrid();
  int _analyze_time_pipeline();
  int _analyze_time_poll();

  protected:
  void _setup_diamonds();

  protected:
  DomainDecomp*          _ptDd;
  double                 _h;
  double**               _d;
  HaloStore              _hs;
  HaloTransfer*          _hts;
  int                    _nHalo;
  SolverMode             _mode;
  int                    _scheme;
  double***              _t;
  bool                   _isSyncComm;
  int                    _nIter;
  bool                   _trace;
  HaloPacker*            _hps;
  int                    _nVar, _nVarComp;
  vector<vector<int>>    _haloVar;
  int                    _sr; // stencil radius
  bool                   _isBufNuma;
  int                    _copyLen;
  bool                   _bypassKHalo;
  vector<int>            _iterPipe;
  vector<vector<int>>    _haloToUnpack;
  bool                   _isWarmUp;
  bool                   _dhOff;
  vector<int>            _thrdIter;
  // diamond blocking
  bool                   _isDiamond;
  int                    _nDiamond[2];
  Diamond**              _diamonds;
  // rectangle tiling
  int                    _nRectangle[2];
  // papi
#ifdef PAPI
  vector<int>            _papiSet;
  vector<long long>      _papiVal;
  vector<double>         _flop;
#endif
};

#endif
