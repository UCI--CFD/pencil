#ifndef TIMER_H
#define TIMER_H

#include <time.h> 
#include <sys/time.h>   
#include <sys/resource.h> 
#include <mpi.h>
#include <omp.h>

#if defined(MIRA_CETUS)
  #include "rdtsc.h"
  #define mpi_time() rdtsc()/1.6e9
  #define omp_time() rdtsc()/1.6e9
#elif defined(MYTIME)
  #define mpi_time() mytime()
  #define omp_time() mytime()
#else 
  #define mpi_time() MPI_Wtime()
  #define omp_time() omp_get_wtime()
#endif

inline double mytime()
{
  struct timespec t;
  clock_gettime(CLOCK_REALTIME, &t);
  return static_cast<double>(t.tv_sec) + static_cast<double>(t.tv_nsec)*1.0e-9;
}


inline void busy_wait(double t)
{
  double t0 = mytime();
  while (mytime() - t0 < t) {};
}

#endif
