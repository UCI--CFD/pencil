#ifndef WJSolver_H
#define WJSolver_H

#include "Solver.h"

class WJSolver: public Solver
{
  public:
  WJSolver(DomainDecomp& dd, UniMesh& mesh, int nh);
  ~WJSolver();
  void set_from_option();
  int setup();
  int init();
  int init_numa();

  private:
  void _compute_range(int rngs[6], bool isEven);
  void _compute_range_rectangle(int rngs[6], int dir, double *c, int is, int js,    
                                int pipe, HaloTransfer* ptHt);
  void _compute_pipe_diamond(int di, int dj, int pipe, int dir, HaloTransfer* ptHt);
  inline void _nullify_layer(double *c, int basel, int jb, int je, int jsl, int kb, int ke);
};


#endif
