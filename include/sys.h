#ifndef SYS_H
#define SYS_H

#include <iostream>
#include <unordered_map>
#include <string>

enum class CmdOption {Int, Double, Bool, Str};

typedef std::unordered_map<std::string, std::string> OptionTable;

extern OptionTable optTab;

int sys_init(int argc, char* argv[], int mode);
/* init the mpi environment*/

void view_cmd_line_arg();
/* print the command line options */

void sys_finalize();
/* terminate the mpi environment*/

int get_option(const std::string& name, const CmdOption type, const int nVal, 
               void* ptVal, bool* ptIsSet);
/*----------------------------------------------------------
Get value from command line option

IN:  name    - arg name
     type    - arg type, only supports Option_t
     nVal    - # values, can be single variable or array
OUT: ptVal   - value address
     ptIsSet - if arg is input via cmd

TODO: handle error when less than nVal is input
----------------------------------------------------------*/  

#ifdef PAPI
void test_fail(int retval, char* call);
#endif

#endif
