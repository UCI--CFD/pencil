#ifndef BURGERSSOLVER_H
#define BURGERSSOLVER_H

#include "Solver.h"

class BurgersSolver: public Solver
{
  public:
  BurgersSolver(DomainDecomp& dd, UniMesh& mesh, int nh);
  ~BurgersSolver();
  void set_from_option();
  int setup();
  int init();
  int init_numa();

  private:
  void _compute_range(int rngs[6], bool isEven);
  void _compute_range_rectangle(int rngs[6], int dir, double *c, int is, int js, 
                                int pipe, HaloTransfer* ptHt);
  void _compute_pipe_diamond(int di, int dj, int pipe, int dir, HaloTransfer* ptHt);
  inline void _nullify_layer(double *c, int basel, int jb, int je, int jsl, int kb, int ke, int vsl);

  private:
  double _nu;
  double _dt;
};

#endif
