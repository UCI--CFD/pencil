#ifndef HALOSTORE_H
#define HALOSTORE_H

#include "DomainDecomp.h"
#include <vector>

using std::vector;

enum Halo {CornerXmYmZm, EdgeXmYm, CornerXmYmZp, EdgeXmZm, FaceXm, EdgeXmZp, \
           CornerXmYpZm, EdgeXmYp, CornerXmYpZp, EdgeYmZm, FaceYm, EdgeYmZp, \
           FaceZm,       /*skip*/  FaceZp,       EdgeYpZm, FaceYp, EdgeYpZp, \
           CornerXpYmZm, EdgeXpYm, CornerXpYmZp, EdgeXpZm, FaceXp, EdgeXpZp, \
           CornerXpYpZm, EdgeXpYp, CornerXpYpZp};

class HaloStore
{
  public:
  HaloStore() {};
  HaloStore(DomainDecomp& dd, int nHalo, int nVariable);
  ~HaloStore();
  void init(DomainDecomp& dd, int nPipe, int nHalo, int nVariable);
  void set_pipeline(int nPipe) {_nPipe = nPipe;}
  void set_from_option();
  int setup_star();
  int setup_box();
  void get_halo_chunk_range(Halo chunk, int rngs[6]) const;
  void get_halo_chunk_range(int rngs[6], Halo chunk, int hRngs[6]) const;
  int num_pipe() {return _nPipe;}
  void debug_view();
  bool need_comm(Halo chunk) const;

  public:
  int         nVar;
  int         bufSize;
  double*     sBuf;
  double*     rBuf;
  vector<int> chunks;
  vector<int> bufMap;
  vector<int> toChunkIDs;
  vector<int> toRanks;
  vector<int> tags;
  vector<int> kmBufStart, kpBufStart;

  private:
  DomainDecomp* _ptDd;
  int _nh;      // # halo
  int _nPipe;
};


#endif
