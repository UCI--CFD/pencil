#ifndef DIAMOND_H
#define DIAMOND_H


enum DiamondType {TriangleUp, TriangleDown, TriangleLeft, Center};


struct Diamond
{
  DiamondType type;  
  int         iBegin, iEnd;
  int         jBegin, jEnd;
};

#endif
